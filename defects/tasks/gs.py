from __future__ import annotations

from dataclasses import dataclass
from pathlib import Path

import numpy as np
import taskblaster as tb
from ase import Atoms
from ase.units import Bohr, Hartree

from asrlib.parameters import Parameter
from asrlib.parameters.gpaw import GPAWCalculatorFactory
from asrlib.recipes.gs import GPAWGroundStateRecipe
from asrlib.results import Result, StandardResult
from asrlib.results.gpaw import GPWFile
from asrlib.typing import TBCommunicator
from asrlib.typing.gpaw import BuiltInGPAWCalculator
from crysp.postprocess.gs import CryspGroundStateResults


@dataclass
class TwoDimensionalVacuumLevel(Result):
    v_z: np.ndarray  # NB: fishy index notation! XXX
    evacdiff: float

    @classmethod
    def from_restart(cls, atoms: Atoms,
                     calc: BuiltInGPAWCalculator
                     ) -> TwoDimensionalVacuumLevel:
        v_z = calc.get_electrostatic_potential().mean(0).mean(0)
        # Here we assume 2D and the nonperiodic direction to be last...
        assert np.allclose(atoms.get_pbc(), np.array([True, True, False]))
        Area = np.linalg.det(atoms.cell[:2, :2] / Bohr)
        dipz = calc.get_dipole_moment()[2] / Bohr
        evacdiff = 4 * np.pi * dipz / Area * Hartree
        return TwoDimensionalVacuumLevel(v_z, evacdiff)

    def evac_av(self, n: int = 8):
        return (self.evac_below(n=n) + self.evac_above(n=n)) / 2.

    def evac_below(self, n: int = 8):
        return self.v_z[n]

    def evac_above(self, n: int = 8):
        return self.v_z[-n]


@dataclass
class TwoDimensionalGroundStateResults(CryspGroundStateResults):
    vacuum_lvl: TwoDimensionalVacuumLevel

    @staticmethod
    def extract_data(atoms, calc):
        return {
            **CryspGroundStateResults.extract_data(atoms, calc),
            'vacuum_lvl': TwoDimensionalVacuumLevel.from_restart(atoms, calc),
        }


@dataclass
class DefectsGroundStateRecipe(Parameter):
    # At the project level, we can specify defaults and care less about
    # generality of the input parameters
    calc_factory: GPAWCalculatorFactory
    filename: str = 'gs.gpw'
    write_mode: str = ''
    d3: bool = False
    dimensionality: int = 2

    @staticmethod
    def from_legacy_inputs(calc_params: dict, factory: str = 'GPAWFactory',
                           **kwargs):
        return DefectsGroundStateRecipe(
            calc_factory=GPAWCalculatorFactory(calc_params),
            d3=factory == 'GPAWD3Factory',
            **kwargs,
        )

    def calculate(self, atoms: Atoms, comm: TBCommunicator) -> GPWFile:
        if 'test' in self.calc_factory.params:
            # Hack the ground state to read cached calculator when testing
            return GPWFile(Path('gs.gpw'))
        # Otherwise, we reuse the calculate step from asrlib
        return self._recipe.calculate(atoms, comm=comm)

    def results(self, raw_output: GPWFile, comm: TBCommunicator):
        return self.results_cls.from_gpw(
            raw_output, d3=self.d3, comm=comm)

    @property
    def results_cls(self):
        if self.dimensionality == 2:
            return TwoDimensionalGroundStateResults
        return CryspGroundStateResults

    @property
    def _recipe(self):
        return GPAWGroundStateRecipe.build(
            calc_factory=self.calc_factory,
            filename=self.filename,
            write_mode=self.write_mode,
            d3=self.d3,
        )


@tb.mpi
def ground_state(atoms: Atoms, calc_params: dict, *,
                 factory: str = 'GPAWFactory',
                 mpi: tb.mpi, **kwargs) -> StandardResult:
    recipe = DefectsGroundStateRecipe.from_legacy_inputs(
        calc_params, factory, **kwargs)
    gpw_file = recipe.calculate(atoms=atoms, comm=mpi.comm)
    results = recipe.results(gpw_file, comm=mpi.comm)
    return StandardResult(calculate=gpw_file, results=results)


@tb.mpi
def defect_gs(atoms: Atoms, calculator: dict, *,
              factory: str = 'GPAWFactory',
              mpi: tb.mpi, **kwargs) -> dict:
    """Hack inputs and outputs to be backwards compatible."""
    results = ground_state(
        atoms, calc_params=calculator, factory=factory, mpi=mpi, **kwargs)
    return {'gpw_path': results.calculate.path,
            'magmom': results.results.properties.magmom,
            'path': Path('.')}
