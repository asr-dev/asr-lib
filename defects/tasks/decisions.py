from asrlib.results import StandardResult


def do_excited_calc(magmom, gapstates=False):
    """
    checks if magmom corresponds to a singlet.
    if gapstates is not False also check so that
    the lumo is non-degenerate from gapstates result
    """

    # magmom was not calculated
    if magmom is None:
        return False
    elif abs(magmom) < 0.9:
        return False
    elif gapstates is not False:
        if gapstates.degenerate_lumo:
            return False
    return True


def list_not_empty(input_list: list):
    if len(input_list) > 0:
        return True
    else:
        return False


def check_cond(bool1):
    return bool1


def collect_results(**kwargs):
    """ Collects charge state results.

    :return: StandardResult with results.
    """
    results_dict = {}
    for key, item in kwargs.items():
        if isinstance(item, bool) and not item:
            results_dict[key] = None
        else:
            results_dict[key] = item

    return StandardResult(**results_dict)


def check_charge(charge):
    if abs(charge) == 0:
        return False
    else:
        return True


def eform_results(eforms):
    return eforms


def do_charge_state(prev_gapstates, calculate_all_charge_states,
                    charge):
    """
    Analyzes reulsts of previious charge state and checks if there is an
    available state in the gap. If user has specified
    calculate_all_charge_states=True it always returns True.
    """
    if calculate_all_charge_states or charge == 0:
        return True
    if charge < 0:
        return prev_gapstates.state_above
    else:
        return prev_gapstates.state_below
