from asrlib.results import StandardResult


def update_calc_params(charge, **kwargs):

    # For pristine material only do neutral calculation
    calculators = kwargs
    for key, item in calculators.items():
        item['charge'] = charge
    return StandardResult(**calculators, recursive=False)
