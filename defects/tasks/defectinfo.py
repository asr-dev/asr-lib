from pathlib import Path


class DefectInfo:
    """Class containing all information about a specific defect."""

    def __init__(self, defecttoken):
        self.names, self.specs = self._defects_from_token(
            defecttoken=defecttoken)

    def _defects_from_token(self, defecttoken):
        """Return defecttype, and kind."""
        if len(defecttoken) >= 2:
            defects = defecttoken[:-1]
            specs_str = defecttoken[-1].split('-')
            specs = [int(spec) for spec in specs_str]
        else:
            defects = defecttoken
            specs = [0]

        return defects, specs

    def get_defect_type_and_kind_from_defectname(self, defectname):
        tokens = defectname.split('_')
        return tokens[0], tokens[1]

    def is_vacancy(self, defectname):
        return defectname.split('_')[0] == 'v'

    def is_interstitial(self, defectname):
        return defectname.split('_')[0] == 'i'

    @property
    def number_of_vacancies(self):
        Nvac = 0
        for name in self.names:
            if self.is_vacancy(name):
                Nvac += 1

        return Nvac


# XXX Will be deprecated once eform is updated
def find_defect_path():
    calcpath = Path('.').absolute()
    defectpath = None
    for ppath in calcpath.parents:
        if 'defect' in ppath.name:
            defectpath = Path(ppath)
            break
    assert defectpath is not None
    return defectpath
