from __future__ import annotations

from dataclasses import dataclass
from pathlib import Path

import numpy as np
import taskblaster as tb
from ase import Atoms
from ase.calculators.calculator import PropertyNotImplementedError

from asrlib.recipes.relax.ase.results import GPAWRelaxResults
from crysp.actions.utils import viewer
from crysp.projectfiles.relax import magnetize
from defects.default_params import default_optimizer_params


@dataclass
class RelaxResultsWithTraj(GPAWRelaxResults):
    traj: Path

    @classmethod
    def from_trajectory(cls, path: Path) -> RelaxResultsWithTraj:
        atoms = cls.read_image(path)
        return cls(atoms, cls.extract_properties(atoms), path)

    def read_trajectory(self):
        return self.reader(self.traj)


@tb.mpi
@tb.actions(view=viewer)
def relax_fixcell(atoms: Atoms,
                  calc_params: dict, factory: str = 'GPAWFactory',
                  optimizer_params: dict = default_optimizer_params,
                  *, mpi: tb.mpi) -> RelaxResultsWithTraj:
    """
    Perform a structure relaxation with fixed cell using GPAW.

    :param atoms: Atoms object.
    :param calc_params: Calculator input parameters.
    :param optimizer_params: Optimizer key word arguments: fmax, smask,
        smax, gmax, identifier (for traj & log file), append_trajectory.
    :param factory: Name of the factory you want to use.
    :param mpi: Communicator or TaskBlaster MPI object.
    :return: dict('atoms': final_atoms, 'traj_path': Path(traj))
    """
    from ase.io.trajectory import Trajectory

    from asrlib.parameters.gpaw import GPAWCalculatorFactory
    from defects.tasks.utils import run_optimization

    comm = mpi.comm

    if 'test' in calc_params:
        return RelaxResultsWithTraj.from_trajectory(Path('relax.traj'))

    # try to restart from last trajectory, if present
    try:
        mpi.parprint('Reading structure from trajectory file')
        traj = Trajectory('relax.traj')
        atoms = traj[-1]
    except IOError:
        mpi.parprint('No existing trajectory file, start from scratch.')
    except IndexError:
        mpi.parprint('Empty trajectory file, start from scratch')
    # Make sure all ranks have loaded the same atoms
    mpi.comm.barrier()

    # NB: we should take the calc_factory as input...
    calc_params = calc_params.copy()
    calc_factory = GPAWCalculatorFactory(calc_params)
    atoms.calc = calc_factory(atoms, comm)
    if factory == 'GPAWD3Factory':
        atoms.calc = calc_factory.d3_wrapper(atoms.calc, comm=comm)

    # set initial magnetic moment
    if not atoms.has('initial_magmoms') or calc_params.get('spinpol'):
        atoms = magnetize(atoms=atoms, magmoms=1.0)

    # Construct & run default optimizer
    atoms, traj = run_optimization(atoms, **optimizer_params,
                                   comm=mpi.comm)

    # Check magnetic moments
    try:
        magmoms = atoms.get_magnetic_moments()
    except PropertyNotImplementedError:
        # We assume this means that the magnetic moments are zero
        # for this calculator.
        magmoms = np.zeros(len(atoms))

    # If a magnetic moment is small, relax again with zero magnetic moment
    if not abs(magmoms).max() > 0.1:
        atoms = magnetize(atoms=atoms, magmoms=0.0)

        # re-initialize calculator and update txt log file name
        nm_params = calc_params.copy()
        nm_params.update({'txt': "relax-nm.txt"})
        nm_factory = GPAWCalculatorFactory(nm_params)
        atoms.calc = nm_factory(atoms, comm)
        optimizer_params.update({'append_trajectory': False,  # Is this wanted?
                                 'identifier': 'relax-nm'})
        atoms, traj = run_optimization(atoms, **optimizer_params,
                                       comm=mpi.comm)
    return RelaxResultsWithTraj.from_trajectory(traj)
