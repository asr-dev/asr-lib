from dataclasses import dataclass

from ase.db import connect

from asrlib.results import Result
from defects.tasks.defectinfo import DefectInfo


@dataclass
class EformResults(Result):
    eforms: dict  # dict with calculated eforms
    correction: float = 0  # correction term

    @classmethod
    def from_previous_eform(cls, previous_eform, correction):
        new_eforms = {}
        for k, v in previous_eform.eforms.items():
            new_eforms[k] = v + correction
        return cls(eforms=new_eforms, correction=correction)


# Take hof as input instead of path here. Use defecttoken as input.
def get_neutral_eforms(host_atoms, oqmd, hof, etot_def, etot_pris,
                       defecttoken):

    defectinfo = DefectInfo(defecttoken=defecttoken)
    eform, sstates = calculate_neutral_formation_energy(etot_def,
                                                        etot_pris,
                                                        oqmd,
                                                        defectinfo)

    el_list = get_element_list(host_atoms)
    eform_dict = {}
    eform_dict['standard-states'] = eform
    min_eform = eform
    # EF = 0 valid for uncharged defects, update if consider charge states
    for element in el_list:
        mu = get_adjusted_chemical_potentials(host_atoms, hof, element)
        # Adjust to element-poor conditions
        new_eform = adjust_formation_energies(defectinfo, eform, mu)
        eform_dict[element+'-poor'] = new_eform
        if new_eform < min_eform:
            min_eform = new_eform
    eform_dict['min_eform'] = min_eform

    return EformResults(eforms=eform_dict)


def get_element_list(atoms):
    """Return list of unique chem. elements of a structure."""
    symbollist = []
    for i, atom in enumerate(atoms):
        symbol = atoms.symbols[i]
        if symbol not in symbollist:
            symbollist.append(symbol)

    return symbollist


def get_stoichiometry(atoms, reduced=False):
    from ase.formula import Formula

    if reduced:
        w = Formula(atoms.get_chemical_formula()).stoichiometry()[1]
    else:
        w = Formula(atoms.get_chemical_formula())

    return w.count()


def get_adjusted_chemical_potentials(host, hof, element):
    el_list = get_element_list(host)
    stoi = get_stoichiometry(host)
    sstates = {}
    for el in el_list:
        name = el
        if el == element:
            mu_el = hof / stoi[element]
            sstates[f'{name}'] = mu_el
        else:
            sstates[f'{name}'] = 0

    return sstates


def adjust_formation_energies(defectinfo, eform,  mu):
    """Return defect dict extrapolated to mu chemical potential conditions."""
    # sstates = get_adjusted_chemical_potentials(host, hof, element)
    defects = defectinfo.names
    for defectname in defects:
        def_type, def_pos = \
                defectinfo.get_defect_type_and_kind_from_defectname(
                    defectname)

        # If mu for substitutional defect has not been added set to zero
        if f'{def_type}' not in mu:
            mu[f'{def_type}'] = 0.0
        if def_type == 'v':
            add = 0
            remove = mu[f'{def_pos}']
        elif def_pos == 'i':
            add = mu[f'{def_type}']
            remove = 0
        else:
            add = mu[f'{def_type}']
            remove = mu[f'{def_pos}']
        # adjust formation energy for defects one by one
        eform = eform - add + remove
    return eform


def obtain_chemical_potential(symbol, db):
    """Extract the standard state of a given element."""
    energies_ss = []
    db = connect(db)
    if symbol == 'v' or symbol == 'i':
        eref = 0.
    else:
        for row in db.select(symbol, ns=1):
            energies_ss.append(row.energy / row.natoms)
        eref = min(energies_ss)

    return {'element': symbol, 'eref': eref}


def calculate_neutral_formation_energy(etot_def, etot_pris, db, defectinfo):
    """Calculate the neutral formation energy with chemical potential shift.

    Only the neutral one is needed as for the higher charge states we
    will use the sj transitions for the formation energy plot.

    Formation energy is calculated w r t standard states
    """
    eform = etot_def - etot_pris
    if 'pristine' in defectinfo.names:
        return 0.0, None
    # next, extract standard state energies for particular defect
    for defect in defectinfo.names:
        def_add, def_remove = \
           defectinfo.get_defect_type_and_kind_from_defectname(defect)
        # extract standard states of defect atoms from OQMD
        standard_states = []
        standard_states.append(obtain_chemical_potential(def_add, db))
        standard_states.append(obtain_chemical_potential(def_remove, db))
        # add, subtract chemical potentials to/from formation energy
        eform = eform - standard_states[0]['eref'] + standard_states[1]['eref']

    return eform, standard_states
