"""SJ task -
    Requires: sj_+
"""

import taskblaster as tb
from ase.io import Trajectory

from asrlib.results import StandardResult
from defects.tasks.analyze_gap_states import get_potential_refs
from defects.tasks.eform import EformResults


def prepare_inputs(calc_params, charge, traj):
    if charge < 0:
        calc_params['charge'] = charge + 0.5
    else:
        calc_params['charge'] = charge - 0.5

    traj = Trajectory(traj)
    output = {'atoms': traj[0], 'calc_params': calc_params}

    return StandardResult(**output, recursive=False)


def update_eform(ev_def, relax_cor, pot_ref, charge,
                 homo_index_q, eform_result_q):
    """
    Defect formation energies are referenced to pristine's evac.
    """
    evs = ev_def

    # DOUBLE CHECK THE SIGNS STUFF HERE (looks ok)
    # XXX check values again when we have a working version
    if charge < 0:
        e_trans = evs[homo_index_q + 1]
        e_trans = e_trans + relax_cor - pot_ref
    else:
        e_trans = evs[homo_index_q]
        e_trans = e_trans - relax_cor - pot_ref

    eform = EformResults.from_previous_eform(eform_result_q, e_trans)

    return eform


# Want to get the homo level of q charge (previous charge state)
# which is calculated in gapstates of q charge.
@tb.mpi
def calc_charge_eform(defecttoken, homo_index_q,
                      halfint_gs, gs_results_pris, traj_path,
                      charge, eform_result_q, mpi):
    """Calculate charge transition levels for defect systems.
     - pot_def: Electrostacic potential of an atom
    far away from the defect.
     - pot_pris: Electrostacic potential of the same atom
    in pristine system.
     - evac: Pristine's vacuum energy [eV]
     - ev_def: Array given by calc.get_eigenvalues()
    """

    pot_def, pot_pris, evac, ev_def = get_potential_refs(defecttoken,
                                                         halfint_gs,
                                                         gs_results_pris,
                                                         mpi.comm)

    pot_ref = pot_def - pot_pris + evac
    traj = Trajectory(traj_path)

    relax_cor = traj[-1].get_potential_energy() \
        - traj[0].get_potential_energy()

    # Eform neutral then eform charge
    eform_charge = update_eform(ev_def, relax_cor, pot_ref, charge,
                                homo_index_q, eform_result_q)
    return eform_charge
