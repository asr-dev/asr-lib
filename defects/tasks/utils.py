from pathlib import Path
from typing import Tuple

import numpy as np
from ase import Atoms
from ase.parallel import paropen
from gpaw.mpi import broadcast_exception


def run_optimization(
        atoms: Atoms, gmax: int, fmax: float, smax: float, smask: np.ndarray,
        identifier: str, append_trajectory: bool, comm) -> Tuple[Atoms, Path]:
    """
    Use the BGFS for ionic relaxation or CellAwareBFGS + FrechetCellFilter to
    perform various types of geometric optimizations. Provide a custom force
    and stress convergence criteria to the optimizer when needed.

    Note: Provide smask with all 0's, if doing ionic relaxations. This ensures
    only the atoms move while cell shape/volume does not change.

    :param atoms: Atoms with ASE calculator.
    :param gmax: Maximum allowed number of geometric steps.
    :param fmax: Maximum allowed force per atom.
    :param smax: Maximum allowed value in the stress tensor.
    :param smask: A numpy array representing the six independent components of
        the strain that are allowed to change (xx yy zz yz xz xy) in the
        cartesian basis. 0=ignored, 1=checked. Example: fixed cell: np.array([
        0, 0, 0, 0, 0, 0]), full volume relax: np.array([1, 1, 1, 1, 1, 1])
    :param identifier: The filename for restart, trajectory, and log file.
    :param append_trajectory: Open the trajectory file in append-mode or not.
    :param comm: Communicator object for parallelization.
    :return: atoms.copy(), Path('traj').
    """
    # XXX Dynamics baseclass hardcoded comm=world, and this break printing for
    # subworkers is used. Now we use this to get around it until we can pass
    # the correct comm to open the log file with.
    with paropen(f'{identifier}.log', mode='a', comm=comm) as logfile:
        # XXX use get_optimizer to handle messy parts of constructing the
        # optimizer + filters.
        optimizer_params = {
            'logfile': logfile, 'trajectory': f'{identifier}.traj',
            'append_trajectory': append_trajectory, 'comm': comm,
            # XXX we have world barrier issues with restart files so we comment
            # out for now until it is fixed in ASE
            # 'restart': f'restart.{identifier}.json'
        }
        optimizer = get_optimizer(atoms=atoms, mask=smask, smax=smax,
                                  optimizer_params=optimizer_params)

        with broadcast_exception(comm):
            with optimizer as opt:
                # set force cut off convergence; apply custom criteria inside
                # irun
                for i, _ in enumerate(opt.irun(fmax=fmax, steps=gmax)):

                    if opt.converged():
                        opt.call_observers()
                        break

        return atoms, Path(optimizer_params['trajectory'])


def get_optimizer(atoms: Atoms, optimizer_params: dict, mask: dict,
                  smax: float):
    """
    Based on a given mask, instantiate the correct combination of optimizer +
    filter for relaxing a structure.

    Note: If a mask of all 0's is given, a BFGS optimizer is returned,
    ignoring smax and mask to avoid unexpected behavior. Otherwise, a
    CellAwareBFGS optimizer + FrechetCellFilter is returned.

    :param atoms: Atoms object for the calculation.
    :param optimizer_params: Dictionary specifying the input to instantiate the
        ASE.
    :param mask: 1x6 np.array for masking the unit cell associated with
        geometric optimization. Use all 0's for no filter.
    :param smax: Stress tensor cut-off. Ignored if mask is all 0.
    :return: ase.optimize((ase.filter(atoms)) or ase.optimize(atoms)
    """
    if np.allclose(mask, 0.):
        from ase.optimize import BFGS
        return BFGS(atoms=atoms, **optimizer_params)
    else:
        from ase.filters import FrechetCellFilter
        from ase.optimize.cellawarebfgs import CellAwareBFGS

        # XXX remove when MR for CellAwareBFGS adds append_trajectory
        optimizer_params.pop('append_trajectory')

        opt = CellAwareBFGS(atoms=FrechetCellFilter(
            atoms=atoms, mask=mask, exp_cell_factor=1.0),
            **optimizer_params)
        opt.smax = smax  # remove if optimizer supports smax via irun

        return opt
