import numpy as np
import taskblaster as tb
from gpaw import restart

from asrlib.results import StandardResult
from defects.tasks.defectinfo import DefectInfo


@tb.mpi
def gapstates(gs_results_def: StandardResult,
              gs_results_pris: StandardResult,
              defecttoken: str, mpi: tb.mpi,
              max_pot_diff: float = 0.5,
              n_evac: int = 8) -> dict:
    """
    Evaluate states within the pristine band gap and return band indices.

    This function compares a defect calculation to a pristine one and
    evaluates the gap states by referencing both calculations to the
    electrostatic potential of an atom far away from the defect. Next, the
    VBM and CBM of the pristine system get projected onto the defect
    calculation and all defect states will be saved. Lastly, the absolute
    energy scale will be referenced back to the pristine vacuum level.

    :param defecttoken: string defining defect (see defect wfs for definition).
    :param mpi: Communicator or TaskBlaster MPI object.
    :param max_pot_diff: Maximum allowed potential difference away from defect.
        Too large value indicates that supercell is too small.
    :param n_evac: z-index from the edge for which the vacuum level
        is evaluated.

    :return: Dict defining states in gap
    """
    pot_def, pot_pris, evac, ev_def = get_potential_refs(defecttoken,
                                                         gs_results_def,
                                                         gs_results_pris,
                                                         mpi.comm,
                                                         n_evac)

    vbm = gs_results_pris.results.gap_info.vbm - evac
    cbm = gs_results_pris.results.gap_info.cbm - evac

    # dif is eventually called eref in results of get_wfs
    # XXX remove comment once get_wfs is implemented
    dif = pot_def - pot_pris + evac

    ev_def = ev_def - dif
    ef_def = gs_results_def.results.gap_info.efermi - dif

    homo_index = get_homo_index(ev_def, ef_def)

    # evaluate whether there are states above or below the fermi level
    # and within the bandgap
    above, below = get_above_below(ev_def, ef_def, vbm, cbm)

    # check whether difference in atomic electrostatic potentials is
    # not too large
    pot_diff = abs(pot_def - pot_pris)
    assert pot_diff < max_pot_diff, (f'large potential difference {pot_diff}'
                                     'in energy referencing')
    print(f'Difference in atomic electrostatic potentials is {pot_diff}')

    # evaluate states within the gap
    statelist = [i for i, state in enumerate(ev_def) if vbm < state < cbm]

    # Check if there are degenerate excited states
    degenerate_lumo = np.isclose(ev_def[homo_index + 1],
                                 ev_def[homo_index + 2])

    return StandardResult(statelist=statelist, state_above=above,
                          state_below=below,
                          dif=dif, pot_diff=pot_diff, homo_index=homo_index,
                          degenerate_lumo=degenerate_lumo)


def get_potential_refs(defecttoken, gs_results_def, gs_results_pris, comm,
                       n_evac: int = 8):
    defectinfo = DefectInfo(defecttoken=defecttoken)
    def_names = defectinfo.names
    def_index = defectinfo.specs[0]
    is_vacancy = [defectinfo.is_vacancy(name) for name in def_names]
    gs_gpw_def = gs_results_def.calculate.path
    gs_gpw_pris = gs_results_pris.calculate.path

    # get calculators and atoms for pristine and defect calculation
    struc_pris, calc_pris = restart(gs_gpw_pris, communicator=comm)
    struc_def, calc_def = restart(gs_gpw_def, txt=None, communicator=comm)

    if sum(struc_def.pbc) == 3:
        evac = 0
    else:
        evac = gs_results_pris.results.vacuum_lvl.evac_av(n=n_evac)

        # Algorithm does not work if vacuum level above and below are different
        if np.abs(gs_results_pris.results.vacuum_lvl.evac_above(n=n_evac) -
                  gs_results_pris.results.vacuum_lvl.evac_below(n=n_evac)) \
                > 1e-4:
            raise ValueError('Vacuum level above/below is different')

    # evaluate which atom possesses maximum distance to the defect site
    ref_index = get_reference_index(def_index, struc_pris)

    # is_vacancy is used to reference ref_index in the pristine material
    # to the corresponding index in the defect by removing 1 from ref_index
    # for each vacancy in the defect structure. However, this should only be
    # done if the ref_index is larger than the index of the atom which was
    # removed. Here we correct for this.
    for i in range(len(is_vacancy)):
        if is_vacancy[i] and ref_index < defectinfo.specs[i]:
            is_vacancy[i] = False

    # get atomic electrostatic potentials at the atom far away for both the
    # defect and pristine system
    pot_def, pot_pris = extract_atomic_potentials(calc_def, calc_pris,
                                                  ref_index, is_vacancy)

    return pot_def, pot_pris, evac, calc_def.get_eigenvalues()


def get_above_below(evs, ef, vbm, cbm):
    """Check whether there are states above/below EF in the gap.
    XXX Seems to assume that there is a state exactly at ef?"""
    above = False
    below = False
    for ev in evs:
        is_inside_gap = vbm < ev < cbm
        if is_inside_gap:
            above |= ev > ef
            below |= ev < ef
    return (above, below)


def return_erange_states(evs, ef, erange):
    """Return states within a certain energy range wrt. the Fermi level."""
    return [i for i, state in enumerate(evs) if (
        (ef + erange[0]) <= state <= (ef + erange[1]))]


def extract_atomic_potentials(calc_def, calc_pris, ref_index, is_vacancy):
    """Evaluate atomic potentials far away from the defect for
    pristine and defect."""
    struc_def = calc_def.atoms
    struc_pris = calc_pris.atoms

    pot_pris = calc_pris.get_atomic_electrostatic_potentials()[ref_index]
    def_index = ref_index - is_vacancy.count(True)

    pot_def = calc_def.get_atomic_electrostatic_potentials()[def_index]

    # check whether chemical symbols of both reference atoms are equal
    if not (struc_def.symbols[def_index]
            == struc_pris.symbols[ref_index]):
        raise ValueError('chemical symbols of reference atoms '
                         'are not the same.')

    return pot_def, pot_pris


def get_reference_index(index, atoms):
    """Get index of atom furthest away from the atom i."""
    from ase.geometry import get_distances

    distances = []
    ref_index = None

    pos = atoms.get_positions()
    cell = atoms.get_cell()
    for i in range(len(atoms)):
        dist = get_distances(pos[i], pos[index],
                             cell=cell, pbc=True)[1][0, 0]
        distances.append(dist)

    for i, element in enumerate(distances):
        if element == max(distances):
            ref_index = i
            break

    return ref_index


def get_homo_index(evs, ef):
    occ = []
    [occ.append(ev) for ev in evs if ev < ef]

    return len(occ) - 1
