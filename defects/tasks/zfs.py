from pathlib import Path

import numpy as np
import taskblaster as tb

from asrlib.results.gpaw import GPWFile


def legacy_restart(calc_path, factory, *, comm, **kwargs):
    if factory == 'GPAWD3Factory':
        return GPWFile(calc_path).d3_restart(comm=comm)
    return GPWFile(calc_path).restart(comm=comm)


def check_triplet_magmom(magmom: float, atol: float = 1e-1) -> bool:
    """
    Simple function to check if a magnetic moment (magmom) corresponds
    to a triplet.

    :param magmom: Magnetic moment.
    :param atol: Absolute tolerance.
    :return: If true, the magmom corresponds to a triplet.
    """
    return np.isclose(magmom, 2., atol=atol)


@tb.mpi
def zero_field_splitting(calc_path: Path, magmom: float, *,
                         factory='GPAWFactory',
                         mpi: tb.mpi) -> dict:
    """Calculate zero-field-splitting.

    :param calc_path: GPAW gpw file path.
    :param magmom: The magnetic moments of the atoms.
    :return: dict('is_triplet': is_triplet, 'D_vv': D_vv).
    """
    # read in atoms and calculator, check whether spin and magnetic
    # moment are suitable, i.e. spin-polarized triplet systems
    atoms, calc = legacy_restart(calc_path, factory, comm=mpi.comm)
    is_triplet = check_triplet_magmom(magmom)

    if not is_triplet:
        return {'is_triplet': is_triplet, 'D_vv': np.zeros([3, 3])}

    # run fixed density Gamma point calculation
    calc = calc.fixed_density(kpts={'size': (1, 1, 1), 'gamma': True})

    # evaluate zero field splitting components for both spin channels
    D_vv = get_zfs_components(calc)

    return {'is_triplet': is_triplet, 'D_vv': D_vv}


def get_zfs_components(calc):
    """Get ZFS components from GPAW function, and convert tensor scale."""
    from ase.units import _e, _hplanck
    from gpaw.zero_field_splitting import convert_tensor, zfs

    D_vv = zfs(calc)
    scale = _e / _hplanck * 1e-6
    D, E, axis, D_vv = convert_tensor(D_vv * scale)

    return D_vv
