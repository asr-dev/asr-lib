from __future__ import annotations

from dataclasses import dataclass
from math import sqrt
from pathlib import Path
from typing import Union

import numpy as np
import taskblaster as tb
from ase.atoms import Atoms
from gpaw import GPAW
from gpaw.directmin.tools import excite
from gpaw.mom import prepare_mom_calculation

from asrlib.parameters.gpaw import GPAWCalculatorFactory
from asrlib.recipes.relax.ase.results import GPAWRelaxResults
from asrlib.results import StandardResult
from defects.tasks.utils import run_optimization

etdm_eigensolver = {'name': 'etdm-fdpw',
                    'converge_unocc': True}

etdm_excited_eigensolver = {'name': 'etdm-fdpw',
                            'excited_state': True,
                            'momevery': 10,
                            'printinnerloop': False,
                            'max_step_inner_loop': 0.1,
                            'maxiter_inner_loop': 50,
                            'converge_unocc': False}


def _get_homo_lumo(calc, spin: int) -> Union[float, float]:
    """
    Given a GPAW calculator object from a ground state calculation, compute the
    difference in the HOMO-LUMO states for a given spin channel.
    :param calc: Calculator object, e.g., groundstate GPAW calculator object.
    :param spin: The spin should either 0 or 1 with: 0 indicating spin up and
        1 indicating spin down.
    :return: homo, lumo.
    """
    nbands = calc.get_number_of_bands()
    eigs_n = calc.get_eigenvalues(spin=spin)
    occ_n = calc.get_occupation_numbers(spin=spin)
    for n in range(nbands):
        if occ_n[n] < 0.1:
            homo = eigs_n[n - 1]
            lumo = eigs_n[n]
            break
    return homo, lumo


def check_excitations(min_homo_lumo: float, gs_gpw_path: str):
    """
    Checks which excitations that are available for a given defect
    (alpha or beta) channel.

    :param min_homo_lumo: float, minimum allowed homo-lumo excitation energy.
    :param gs_gpw_path: pathlib.Path Path to gs gpw file.
    """

    calc = GPAW(gs_gpw_path)
    excitations = ['alpha', 'beta']
    possible_excitations = []
    for spin, excitation in enumerate(excitations):
        homo, lumo = _get_homo_lumo(calc, spin)
        # XXX calc.get_homo_lumo(spin) gives different results
        # for some moaterials. Need to investigate this.
        if lumo - homo >= min_homo_lumo:
            possible_excitations.append(excitation)

    return possible_excitations


def collect_results(atoms_gs, atoms_exc, energy_gs, energy_exc):
    """ Collect results from excited state calculation.

    :param atoms_gs: Atoms from gs relaxation with excited state params.
    :param atoms_exc: Atoms from excited state relaxation.
    :paramenergy_gs: Total energy from gs relaxation with excited state params.
    :paramenergy_exc: Total energy from excited state relaxation.
    """
    zpl = energy_exc - energy_gs

    # define the 1D coordinate
    m_a = atoms_gs.get_masses()
    delta_R = atoms_exc.positions - atoms_gs.positions
    delta_Q = sqrt(((delta_R**2).sum(axis=-1) * m_a).sum())

    return StandardResult(zpl=zpl, delta_Q=delta_Q)


def update_calc_params_excited(calc_params: dict, eigensolver_params: dict):
    """ Updates and checks so that calc_params are suitible for
    excited state calculation.

    :param calc_params: dict.
    :param eigensolver_params: dict with eigensolver params for excited calc.
    :return calc_params with updated values.
    """
    # XXX DOM requires these parameters, so we assert it is true.
    assert calc_params['mixer'].get('backend', False) == 'no-mixing'
    assert calc_params.get('symmetry', False) == 'off'
    assert calc_params.get('occupations', False) == {'name': 'fixed-uniform'}

    if 'eigensolver' in calc_params:
        raise ValueError('For excited state calculations eigensolver \
        has to be passed as a seperate input parameter')

    calc_params['eigensolver'] = eigensolver_params
    return calc_params


@dataclass
class RelaxResultsWithGPW(GPAWRelaxResults):
    gpw_file: Path

    @classmethod
    def from_paths(cls, traj: Path, gpw: Path) -> RelaxResultsWithGPW:
        atoms = cls.read_image(traj)
        return cls(atoms, cls.extract_properties(atoms), gpw)


@tb.mpi
def relax_ground(atoms: Atoms, calc_params: dict,
                 optimizer_params: dict,
                 mpi: tb.mpi,
                 factory: str = 'GPAWFactory',
                 eigensolver_params: dict = etdm_eigensolver,
                 magmom: float = None,
                 round_magmom: bool = True
                 ) -> RelaxResultsWithGPW:
    """
    Perform a structure relaxation for gs state using GPAW.
    In order to get the correct zpl calc_params should be the same as
    for the excited state calculation.

    Do not pass 'txt' in calc_params if you would like the default behavior
    for naming output files.

    :param atoms: Atoms object to relax.
    :param calc_params: Dictionary containing the GPAW input parameters.
    :param optimizer_params: Dictionary containing optimizer key word
        arguments for run_optimization: fmax, smask, smax, gmax, identifier
        (for traj & log file), append_trajectory.
    :param mpi: Communicator or Taskblaster MPI object.
    :param eigensolver_params: dict, eigensolver parameters.
    :param magmom: Value to fix total magmom to, if None magmom
                  is set to np.sum(atoms.initial_magmoms).
    :param round_magmom: Round total magmom to integer
    :return: {'atoms': final_atoms, 'traj_path': Path,
              'gpw_path': Path to gpw file,
              'energy': Energy}
    """
    # This is extremely dangerous! We *never* want update mutable inputs!!!
    calc_params = update_calc_params_excited(calc_params, eigensolver_params)
    # To clean up this mess, calc_factory should replace calc_params,
    # eigensolver_params and factory as input, in which case one might as well
    # use asrlib.tasks.relax directly...
    calc_factory = GPAWCalculatorFactory(calc_params)
    atoms.calc = calc_factory(atoms, comm=mpi.comm)
    if factory == 'GPAWD3Factory':
        atoms.calc = calc_factory.d3_wrapper(atoms.calc, comm=mpi.comm)

    # Set initial magmoms to give the correct total magmom
    if magmom is not None:
        if round_magmom:
            magmom = np.round(magmom)
        num_atoms = len(atoms)
        magmoms = np.zeros(num_atoms)
        magmoms[:] = magmom / num_atoms
        atoms.set_initial_magnetic_moments(magmoms=magmoms)

    # Relax...
    atoms, traj = run_optimization(atoms, comm=mpi.comm,
                                   **optimizer_params)
    gpw_file = Path('relax_ground.gpw')
    atoms.calc.write(gpw_file, mode='all')
    return RelaxResultsWithGPW.from_paths(traj, gpw_file)


def restart_excited_mom(
        restart_file: Path, excitation: str,
        eigensolver_params: dict, comm,  # why not tb.mpi? XXX
        use_fixed_occupations=False,
        factory='GPAWFactory') -> Atoms:
    """
    Prepare a calculator for excited state calculations.

    :param restart_file: Path object defining GPAW GPW file from a gs
        calculation performed specifically for excited state calculations.
    :param excitation: 'alpha' or 'beta'. THIS NEEDS TO EXPLAIN WHAT ALPHA AND
        BETA MEAN.
    :param eigensolver_params: Eigensolver parameters.
    :param comm: Communicator object for parallelization.
    :param use_fixed_occupations: If False, use MOM else, do NOT use MOM.
    """
    from defects.tasks.zfs import legacy_restart
    # In the future, we should take the restarter as input, replacing the
    # restart_file and factory inputs
    atoms, calc = legacy_restart(restart_file, factory,
                                 comm=comm, txt=f'{excitation}.txt')

    # atoms, calc = restart(restart_file, txt=f'{excitation}.txt',
    #                       communicator=comm)
    if excitation == 'alpha':
        f_sn = excite(calc=calc, i=0, a=0, spin=(0, 0))
    elif excitation == 'beta':
        f_sn = excite(calc=calc, i=0, a=0, spin=(1, 1))
    calc.set(eigensolver=eigensolver_params)
    if not excitation == 'gs':
        # Switch off MOM set to use_fixed_occupations True
        prepare_mom_calculation(calc, atoms, f_sn,
                                use_projections=True,
                                use_fixed_occupations=use_fixed_occupations)

    atoms.calc = calc

    return atoms


@tb.mpi
def relax_excited(restart_file: Path,
                  optimizer_params: dict,
                  excitation: str, mpi: tb.mpi,
                  use_fixed_occupations=False,
                  eigensolver_params=etdm_excited_eigensolver,
                  factory='GPAWFactory',
                  **kwargs) -> RelaxResultsWithGPW:
    """
    Perform a structure relaxation for excited state using GPAW.

    To have all files named using identifier, provide calc_params without
    the txt.

    :param restart_file: GPAW gpw file from a gs relaxation.
    :param optimizer_params: Optimizer key value pair arguments for
        run_optimization, namely: fmax, smask, smax, gmax, identifier
        (for traj & log file), append_trajectory.
    :param excitation: Excitation channel, alpha or beta.
    :param mpi: Communicator or Taskblaster MPI object.
    :param use_fixed_occupations: If False, use MOM else, do NOT use MOM.
    :return: {'atoms': final_atoms, 'traj_path': Path,
              'gpw_path': Path(gpw_file),
              'energy': Energy}
    """
    atoms = restart_excited_mom(restart_file, excitation, comm=mpi.comm,
                                eigensolver_params=eigensolver_params,
                                use_fixed_occupations=use_fixed_occupations,
                                factory=factory)
    atoms.calc.set(txt=f'gpaw_{excitation}.txt')
    atoms, traj = run_optimization(atoms, comm=mpi.comm,
                                   **optimizer_params)

    gpw_file = Path('relax_excited.gpw')
    atoms.calc.write(gpw_file, mode='all')
    return RelaxResultsWithGPW.from_paths(traj, gpw_file)
