import taskblaster as tb

from defects.default_params import default_optimizer_params
from defects.workflows.screening import ScreeningWf

# Main workflow function


@tb.parametrize_glob('*/material')
def workflow(material):

    # XXX Need to double check calculators against ASR defaults
    calc_relax = {'mode': {
                      'name': 'pw',
                      'ecut': 800,
                      'dedecut': 'estimate'},
                  'xc': 'PBE',
                  'kpts': {
                      'density': 3.0,
                      'gamma': True},
                  'basis': 'dzp',
                  'mixer': {'method': 'fullspin', 'backend': 'pulay'},
                  'symmetry': {
                      'symmorphic': False},
                  'convergence': {
                      'forces': 1e-4,
                      'maximum iterations': 200},
                  'txt': 'relax.txt',
                  'occupations': {
                      'name': 'fermi-dirac',
                      'width': 0.02},  # Note: reduced fermi smearing again
                  'spinpol': True,
                  'maxiter': 400}
    # Low params calculator with only Gamma point and smaller ecut
    calc_lowparams = {'mode': {
                          'name': 'pw',
                          'ecut': 500,
                          'dedecut': 'estimate'},
                      'xc': 'PBE',
                      'kpts': {
                          'size': [1, 1, 1],
                          'gamma': True},
                      'basis': 'dzp',
                      'mixer': {'method': 'fullspin', 'backend': 'pulay'},
                      'symmetry': {
                          'symmorphic': False},
                      'convergence': {
                          'forces': 1e-3,
                          'maximum iterations': 200},
                      'txt': 'relax.txt',
                      'occupations': {
                          'name': 'fermi-dirac',
                          'width': 0.02},
                      'spinpol': True,
                      'maxiter': 400}

    calc_gs = {'mode': {
                   'name': 'pw',
                   'ecut': 800,
                   'dedecut': 'estimate'},
               'xc': 'PBE',
               'kpts': {
                   'density': 3.0,
                   'gamma': True},
               'basis': 'dzp',
               'symmetry': {
                   'symmorphic': False},
               'convergence': {
                   'bands': "CBM+3.0"},
               'nbands': "200%",
               'txt': 'gs.txt',
               'occupations': {
                   'name': 'fermi-dirac',
                   'width': 0.02},
               'spinpol': True,
               'maxiter': 400}

    oqmd = '/home/niflheim/fafb/db/oqmd12.db'
    # c2db = '/home/niflheim2/cmr/WIP/doubledefects/databases/c2db.db'
    c2db = '/home/niflheim/manja/test-env/asr-lib/defects/'\
           '3Dscreening2023/bulk.db'
    host_structure_folder = '/home/niflheim2/cmr/WIP/defect-screening'\
                            '-2023/calculations/Fredrik/hosts/'
    max_eform = 10.
    factory = 'GPAWD3Factory'

    # Low params calculator with only Gamma point and smaller ecut
    calc_excited = {'mode': {
                          'name': 'pw',
                          'ecut': 800,
                          'dedecut': 'estimate'},
                    'xc': 'PBE',
                    'kpts': {
                        'size': [1, 1, 1],
                        'gamma': True},
                    'basis': 'dzp',
                    'symmetry': 'off',  # XXX Why?
                    'convergence': {
                        'forces': 1e-3,
                        'maximum iterations': 400},
                    'txt': 'relax.txt',
                    'occupations': {'name': 'fixed-uniform'},
                    'spinpol': True,
                    'nbands': '101%',  # defined in task
                    'maxiter': 5000}

    optimizer_params = default_optimizer_params.copy()
    optimizer_params_excited = optimizer_params.copy()
    optimizer_params_excited['fmax'] = 0.02

    min_homo_lumo = 0.4

    return ScreeningWf(initial_atoms=material,
                       calc_lowparams=calc_lowparams,
                       calc_relax=calc_relax,
                       calc_gs=calc_gs,
                       optimizer_params=optimizer_params,
                       oqmd=oqmd,
                       c2db=c2db,
                       factory=factory,
                       host_folder=host_structure_folder,
                       max_eform=max_eform,
                       calc_excited=calc_excited,
                       optimizer_params_excited=optimizer_params_excited,
                       min_homo_lumo=min_homo_lumo)
