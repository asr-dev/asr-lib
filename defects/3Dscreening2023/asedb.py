import sys

from ase.db import connect
from ase.io import read

from defects.utils.material_fingerprint import (
                                                get_hash_of_atoms,
                                                get_uid_of_atoms,
)

db = connect('bulk.db')
file1 = sys.argv[1]
atoms = read(file1)
eform = -2.7036
# eform = -1.2426
hash = get_hash_of_atoms(atoms)
uid = get_uid_of_atoms(atoms, hash)
db.write(atoms, hform=eform, uid=uid)
