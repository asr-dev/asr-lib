from pathlib import Path

import numpy as np
import pytest
from ase import Atoms
from gpaw import GPAW
from gpaw.mpi import serial_comm

from asrlib import call_mpi_method
from asrlib.parameters.gpaw import GPAWCalculatorFactory
from defects.tasks.gs import DefectsGroundStateRecipe, defect_gs


@pytest.mark.parametrize('d3', [False, True])
def test_gs_2D(newrepo, d3):
    recipe = DefectsGroundStateRecipe(
        calc_factory=GPAWCalculatorFactory(dict(
            xc='PBE',
            mode={'name': 'pw', 'ecut': 150},
            nbands='200%',
            kpts=(2, 2, 1),
            occupations={'name': 'fermi-dirac', 'width':  0.5},
            convergence={'bands': 'CBM+1.0'},
            txt='gs.txt',
        )),
        d3=d3,
    )

    abn = 2.51
    BN = Atoms("BN", scaled_positions=[[0, 0, 0.5], [1 / 3, 2 / 3, 0.5]],
               cell=[[abn, 0.0, 0.0], [-0.5 * abn, np.sqrt(3) / 2 * abn, 0],
                     [0.0, 0.0, 15.0]], pbc=[True, True, False])
    output = call_mpi_method('calculate', recipe, atoms=BN)
    gs_results = call_mpi_method('results', recipe, raw_output=output)

    assert gs_results.properties.forces is None

    vacuum_lvl = gs_results.vacuum_lvl
    evac = 2.575
    assert vacuum_lvl.evacdiff == pytest.approx(0.0, abs=1e-3)
    assert vacuum_lvl.evac_av() == pytest.approx(evac, rel=1e-2)
    assert vacuum_lvl.evac_above() == pytest.approx(evac, rel=1e-2)
    assert vacuum_lvl.evac_below() == pytest.approx(evac, rel=1e-2)


def test_defect_gs(newrepo, std_test_materials_dict):
    """
    Simple test of gs task
    """
    atoms = std_test_materials_dict['BN']
    calculator = {'mode': {'name': 'pw', 'ecut': 200}, 'xc': 'PBE',
                  'kpts': {'density': 1, 'gamma': True},
                  'txt': 'gs.txt'
                  }

    output = defect_gs(atoms, calculator)
    # check magnetic moment
    assert abs(output['magmom']) < 0.0001

    _ = GPAW(output['gpw_path'], communicator=serial_comm)
    # check path and output file
    assert Path(output['path']/'gs.txt').is_file()
