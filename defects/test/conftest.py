import json
import os
import shutil
from pathlib import Path

import pytest
from ase.io.jsonio import object_hook

from asrlib.results.gpaw import GPAWProperties, GPWFile
from asrlib.test.conftest import exec_dftd3, in_tmp_dir, newrepo, repo, testdir
from crysp.postprocess.gs import CryspGapResults
from defects.tasks.gs import (
    TwoDimensionalGroundStateResults,
    TwoDimensionalVacuumLevel,
)

# appease flake8
__all__ = ['newrepo', 'repo', 'testdir', 'in_tmp_dir', 'exec_dftd3']


@pytest.fixture
def std_test_materials():
    from defects.test.materials import std_test_materials as mat
    return mat


@pytest.fixture
def std_test_materials_dict():
    from defects.test.materials import std_test_materials_dict as mat
    return mat


def copy_restart_files(new_repo, old_tree, host):
    """Creates symbolic linkx for restart files for host material {host}
       from {old_tree} to new tree.

       Modified to search also for non-existing paths at new_repo.
    """
    with new_repo:
        new_folders = {node.name for node in new_repo.registry.index.nodes()}

    old_folders = {
        str(path.relative_to(old_tree))
        for path in (Path(old_tree) / host).rglob('*')}

    for path in old_folders & new_folders:
        old_path = Path(old_tree) / path
        new_path = Path('tree') / path
        new_path.mkdir(parents=True, exist_ok=True)
        for filename in old_path.glob('*'):
            filename = filename.relative_to(old_path)
            if filename.name not in {'relax.traj', 'gs.gpw'}:
                continue
            new_file = new_path / filename
            old_file = old_path / filename
            if not new_file.exists():
                # os.symlink(old_file, new_file)
                shutil.copyfile(old_file, new_file)


@pytest.fixture
def defect_workflows(pytestconfig):
    return pytestconfig.rootpath / 'defects/test/workflows'


@pytest.fixture(scope='session')
def defectfiles():
    return defect_testfiles()


def defect_testfiles():
    import asrlibtestfiles
    return asrlibtestfiles.root() / 'defects'


# XXX I Really hope there is a better way to do this
def fake_gs(folder):
    from asrlib.results import StandardResult
    with open(Path(folder / 'output.json'), 'r') as f:
        data = json.load(f, object_hook=object_hook)['__tb_enc__'][1]
        gr = data['gap_info']['__tb_enc__'][1]
        gr = CryspGapResults(**gr)
        v_z = data.pop('_v_z')
        evacdiff = data.pop('_evacdiff')
        vacuum_lvl = TwoDimensionalVacuumLevel(v_z=v_z, evacdiff=evacdiff)
        properties = {'energy': data['energy'],
                      'free_energy': data['free_energy'],
                      'dipole': data['dipole'],
                      'magmom': data['magmom'],
                      'magmoms': data['magmoms']}
        properties = GPAWProperties(**properties)

        new_data = {}
        new_data['properties'] = properties
        new_data['gap_info'] = gr
        new_data['vacuum_lvl'] = vacuum_lvl
    return StandardResult(calculate=GPWFile(path=folder / 'gs.gpw'),
                          results=TwoDimensionalGroundStateResults(**new_data))


@pytest.fixture(scope='session')
def gpaw_version():
    from gpaw import __version__
    return __version__


@pytest.fixture(scope='session')
def oldgpaw24(gpaw_version):
    version = int(gpaw_version.split('.', 1)[0])
    if version < 24 or '1' == os.environ.get('GPAW_NEW'):
        pytest.skip('requires old gpaw with version >= 24.0')
