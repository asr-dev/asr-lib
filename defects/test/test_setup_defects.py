import numpy as np
import pytest

from defects.tasks.setup_defects import setup_defect_dict


def test_nearest_distance(std_test_materials):
    import numpy as np

    from defects.tasks.setup_defects import return_distances_cell

    atoms = std_test_materials[1]
    cell = atoms.get_cell()
    distances = return_distances_cell(cell)
    refs = [cell[0][0], np.sqrt(cell[1][0]**2 + cell[1][1]**2),
            np.sqrt((-cell[0][0] + cell[1][0])**2 + cell[1][1]**2),
            np.sqrt((cell[0][0] + cell[1][0])**2 + cell[1][1]**2)]
    for i, dist in enumerate(distances):
        assert dist == refs[i]


@pytest.mark.xfail(reason='Deprecated functionality')
def test_setup_defects(repo, std_test_materials):
    from pathlib import Path

    # from ase.calculators.calculator import compare_atoms
    # import json
    from defects.workflows.totree import defecttotree
    pathname = 'material*'
    atoms = std_test_materials[1]
    kwargs = {}
    kwargs['supercell'] = (3, 3, 1)
    rn = repo.runner()
    workflow = defecttotree(setup_defect_dict(atoms, **kwargs),
                            name='material')
    workflow(rn)
    atoms = atoms.repeat((3, 3, 1))

    # skip for now...
    """
    # Opening input.json file
    codec = ASECodec #ASRJSONCodec()
    with open('tree/BN/defects.pristine_sc.331/material/input.json') as f:
        pristine = json.load(f, object_hook=codec.decode)[1]['obj']
    print(pristine)
    assert compare_atoms(atoms, pristine) == []
    """
    pathlist = list(Path('.').glob('tree/BN/defects.pristine_sc.331/' +
                                   pathname))
    assert len(pathlist) == 1
    assert Path(pathlist[0] / 'input.json').is_file()

    pathlist = list(Path('.').glob('tree/BN/defects.BN_331*/' + pathname))
    for path in pathlist:
        assert Path(path / 'input.json').is_file()

    pathlist = list(Path('.').glob('tree/BN/defects.BN_331*'))
    pathlist = [str(thispath) for thispath in pathlist]
    complist = ['tree/BN/defects.BN_331.v_B',
                'tree/BN/defects.BN_331.v_N',
                'tree/BN/defects.BN_331.N_B',
                'tree/BN/defects.BN_331.B_N']
    assert len(pathlist) == len(complist)

    for item in complist:
        assert item in pathlist


@pytest.mark.parametrize('vac', [True, False])
def test_apply_vacuum(vac, std_test_materials):
    atoms = std_test_materials[1]
    def_dict = setup_defect_dict(host=atoms,
                                 general_algorithm=15.,
                                 uniform_vacuum=vac)
    assert len(def_dict) > 0
    for def_key, structure in def_dict.items():
        cell = structure.get_cell()
        ref = (cell.lengths()[0] + cell.lengths()[1]) / 2.
        if vac:
            assert cell[2, 2] == pytest.approx(ref)
        else:
            assert cell[2, 2] == pytest.approx(
                atoms.get_cell().lengths()[2])


def test_setup_supercell(testdir, std_test_materials_dict):
    from defects.tasks.setup_defects import setup_supercell

    atoms = [std_test_materials_dict['BN'],
             std_test_materials_dict['GaAs']]
    dim = [True, False]
    x = [6, 4]
    y = [6, 4]
    z = [1, 4]
    for i, atom in enumerate(atoms):
        structure, N_x, N_y, N_z = setup_supercell(atom,
                                                   15,
                                                   dim[i])
        assert N_x == x[i]
        assert N_y == y[i]
        assert N_z == z[i]
        assert len(structure) == x[i] * y[i] * z[i] * len(atom)


def test_intrinsic_single_defects(testdir, std_test_materials_dict):
    lengths = {'Si': 1, 'BN': 4, 'Fe': 1}
    materials = std_test_materials_dict.copy()
    materials.pop('Agchain')
    materials.pop('O2')
    materials.pop('GaAs')
    for k, v in materials.items():
        def_dict = setup_defect_dict(v)
        # Check so that pristine material is in dict
        pristine = [key for key in def_dict if 'pristine' in key.lower()]
        assert len(pristine) == 1
        # number of defects + pristine
        assert len(def_dict) == lengths[k] + 1


def test_general_supercell_3D(testdir, std_test_materials_dict):
    materials = std_test_materials_dict['Si']
    def_dict_Si = setup_defect_dict(materials,
                                    general_3D_supercell=3.0,
                                    maxn=10)

    # Check so that pristine material is in dict
    pristine_name = [key for key in def_dict_Si if 'pristine' in key.lower()]
    assert len(pristine_name) == 1
    pristine = def_dict_Si[pristine_name[0]]

    for lattice_vec in pristine.get_cell():
        print(lattice_vec)
        print(np.linalg.norm(lattice_vec))
        print(pristine.get_cell_lengths_and_angles())
        assert np.linalg.norm(lattice_vec) > 3.0
    # assert len(pristine) == 10


def test_chemical_elements(testdir, std_test_materials):
    from defects.tasks.setup_defects import add_intrinsic_elements
    results = {'Si2': ['Si'],
               'BN': ['B', 'N'],
               'Ag': ['Ag'],
               'Fe': ['Fe']}
    for i, atoms in enumerate(std_test_materials):
        name = atoms.get_chemical_formula()
        elements = add_intrinsic_elements(atoms, elements=[])
        for element in elements:
            assert element in results[name]
            assert len(elements) == len(results[name])


def test_extrinsic_single_defects(newrepo, std_test_materials_dict):
    lengths = {'Si': 3, 'BN': 8, 'Fe': 3}
    materials = std_test_materials_dict.copy()
    materials.pop('Agchain')
    materials.pop('O2')
    materials.pop('GaAs')
    for k, v in materials.items():
        def_dict = setup_defect_dict(v, extrinsic='V,Nb')
        # Check so that pristine material is in dict
        pristine = [key for key in def_dict if 'pristine' in key.lower()]
        assert len(pristine) == 1
        # number of defects + pristine
        assert len(def_dict) == lengths[k] + 1


@pytest.mark.parametrize('double_type', ['vac-vac',
                                         'vac-sub',
                                         'sub-sub'])
def test_extrinsic_double_defects(double_type, newrepo,
                                  std_test_materials_dict):
    lengths = {'vac-vac': 8,
               'vac-sub': 12,
               'sub-sub': 13}
    host = 'BN'
    material = std_test_materials_dict[host]
    def_dict = setup_defect_dict(material,
                                 extrinsic='Nb',
                                 double=double_type,
                                 scaling_double=1.5)
    # Check so that pristine material is in dict
    pristine = [key for key in def_dict if 'pristine' in key.lower()]
    assert len(pristine) == 1
    # number of defects + pristine
    assert len(def_dict) == lengths[double_type] + 1


@pytest.mark.parametrize('double_type', ['vac-vac',
                                         'vac-sub',
                                         'sub-sub'])
def test_exclude_double_defects(double_type, newrepo,
                                std_test_materials_dict):
    lengths = {'vac-vac': 10,
               'vac-sub': 17,
               'sub-sub': 21}
    host = 'BN'
    material = std_test_materials_dict[host]
    def_dict = setup_defect_dict(material,
                                 extrinsic='Nb,Yb',
                                 double=double_type,
                                 double_exclude='Yb',
                                 scaling_double=1.5)

    # Check so that pristine material is in dict
    pristine = [key for key in def_dict if 'pristine' in key.lower()]
    assert len(pristine) == 1
    # number of defects + pristine
    assert len(def_dict) == lengths[double_type] + 1


@pytest.mark.parametrize('M', ['Mo', 'W'])
@pytest.mark.parametrize('X', ['S', 'Se', 'Te'])
@pytest.mark.parametrize('scaling', [0, 1, 1.5, 2])
def test_get_maximum_distance(M, X, scaling):
    from ase.build import mx2
    from ase.data import atomic_numbers, covalent_radii

    from defects.tasks.setup_defects import get_maximum_distance

    atoms = mx2(f'{M}{X}2')
    metal = atomic_numbers[M]
    chalcogen = atomic_numbers[X]
    reference = ((covalent_radii[metal] + covalent_radii[chalcogen])
                 * scaling)

    R = get_maximum_distance(atoms, 0, 1, scaling)
    assert R == pytest.approx(reference)


def test_new_double():
    from defects.tasks.setup_defects import is_new_double_defect

    complex_list = ['v_N.v_B', 'Cr_N.N_B', 'N_B.B_N', 'Nb_B.F_N']
    newlist = ['N_B.Cr_N', 'Cr_N.N_B', 'v_N.v_B', 'F_N.I_B', 'V_N.v_B']
    refs = [False, False, False, True, True]
    for i, new in enumerate(newlist):
        el1 = new.split('.')[0]
        el2 = new.split('.')[1]
        assert is_new_double_defect(el1, el2, complex_list) == refs[i]


# XXX Should be part of sj_analyse wf
@pytest.mark.xfail
def test_setup_halfinteger(asr_tmpdir, std_test_materials):
    from pathlib import Path

    from ase.io import write
    from asr.core import chdir
    from asr.setup.defects import main

    atoms = std_test_materials[1]
    write('unrelaxed.json', atoms)
    main()
    p = Path('.')
    pathlist = list(p.glob('defects.*/charge_0'))
    for path in pathlist:
        with chdir(path):
            write('structure.json', atoms)
            main(halfinteger=True)
            plus = Path('sj_+0.5/params.json')
            minus = Path('sj_-0.5/params.json')
            assert plus.is_file()
            assert minus.is_file()


# XXX Should be part of sj_analyse wf
@pytest.mark.xfail
def test_write_halfinteger(asr_tmpdir, std_test_materials):
    from pathlib import Path

    from ase.io import write
    from asr.core import chdir, read_json
    from asr.setup.defects import main, write_halfinteger_files

    materials = std_test_materials.copy()
    materials.pop(2)
    for atoms in materials:
        Path(f'{atoms.get_chemical_formula()}').mkdir()
        write(f'{atoms.get_chemical_formula()}/unrelaxed.json', atoms)
        with chdir(f'{atoms.get_chemical_formula()}'):
            main()
            p = Path('.')
            pathlist = list(p.glob('defects.*/charge_*'))
            for path in pathlist:
                with chdir(path):
                    write('structure.json', atoms)
                    params = read_json('params.json')
                    charge = int(str(
                        path.absolute()).split('/')[-1].split('_')[-1])
                    write_halfinteger_files(0.5, '+0.5', params, charge, '.')
                    write_halfinteger_files(-0.5, '-0.5', params, charge, '.')
                    params_p = read_json('sj_+0.5/params.json')
                    params_m = read_json('sj_-0.5/params.json')
                    deltas = [0.5, -0.5]
                    for i, par in enumerate([params_p, params_m]):
                        assert (par['asr.gs@calculate']['calculator']['charge']
                                == charge + deltas[i])
