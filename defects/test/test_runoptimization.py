import csv
from math import sqrt
from pathlib import Path

import numpy as np
import pytest
from ase.atoms import Atoms
from ase.build import bulk
from ase.io.trajectory import Trajectory
from ase.parallel import world
from gpaw import GPAW

from defects.tasks.utils import run_optimization


def parse_gpaw_logfile(file: str):
    data = []
    with open(file, newline='') as csvfile:
        reader = csv.reader(csvfile, delimiter=' ', skipinitialspace=True)
        next(reader, None)
        for row in reader:
            if row[0] == 'BFGS:':
                step = float(row[1])
                energy = float(row[3])
                fmax = float(row[4])
                tmp = {
                    "step": step,
                    "energy": energy,
                    "fmax": fmax
                }
                data.append(tmp)
    return data


def read_traj(file: str, is_stress: bool = True):
    data = []
    traj = Trajectory(file, "r")
    for idx, atoms in enumerate(traj):
        pos = atoms.get_positions()
        forces = atoms.calc.results["forces"]
        energy = atoms.calc.results["energy"]
        free_energy = atoms.calc.results["free_energy"]
        fmax = sqrt((forces**2).sum(axis=1).max())
        tmp = {
            "step": idx,
            "energy": energy,
            "free_energy": free_energy,
            "position": pos,
            "forces": forces,
            "fmax": fmax
        }
        if is_stress:
            stress = atoms.calc.results["stress"]
            smax = abs(stress).max()
            tmp.update({"smax": smax})
        data.append(tmp)
    traj.close()
    return data


def get_setup(atoms, identifier):
    # init gpaw calc & attach to atoms
    calc = GPAW(**{'mode': {'name': 'pw', 'ecut': 200},
                   'kpts': (1, 1, 1), 'txt': f'{identifier}.txt'})
    atoms_new = atoms.copy()
    atoms_new.calc = calc
    return atoms_new


@pytest.mark.parametrize(["fmax", "disp", "assertion", "smask", "smax"], [
   #                              g
   [0.015, 0.00, 'all', np.array([1, 1, 1, 1, 1, 1]), 0.005],
   [0.05,  0.00, 'xy',  np.array([1, 1, 0, 0, 0, 1]), 0.2122],
   [0.05,  0.02, 'no',  np.array([0, 0, 0, 0, 0, 0]), 0.0],
])
def test_smax_smask_fmax(in_tmp_dir, newrepo, fmax, disp, assertion, smask,
                         smax):
    cubic = True if assertion == 'xy' else False
    atoms = bulk('Au', cubic=cubic)
    atoms.positions[0] += disp
    orig_atoms = atoms.copy()

    # init gpaw calc & attach to atoms
    print(f'\n  smask: {smask} \n  disp: {disp}\n  fmax: {fmax**2}')
    gmax = 100  # fmax stopping criteria
    atoms = get_setup(atoms=atoms, identifier='relax')

    atoms, traj = run_optimization(
        atoms=atoms, fmax=fmax**2, smax=smax, gmax=gmax, identifier='relax',
        smask=smask, append_trajectory=False, comm=world)

    # ###### ASSERT SOME THINGS #######
    # collect final fmax and smax
    traj = Trajectory('relax.traj')
    g = len(traj)
    f = np.linalg.norm(atoms.calc.get_forces(), axis=1).max()
    s = abs(atoms.calc.get_stress() * smask).max()

    # assert they are below tols & restart file exists
    assert f <= fmax and s <= smax and g <= gmax

    if assertion == 'all':
        assert np.all(np.not_equal(atoms.cell, orig_atoms.cell))
        is_stress = True
    elif assertion == 'xy':
        assert np.allclose(atoms.cell[2, :], orig_atoms.cell[2, :])
        assert np.allclose(atoms.cell[:, 2], orig_atoms.cell[:, 2])
        cell_lengths = abs(atoms.cell.lengths()[:2] -
                           orig_atoms.cell.lengths()[:2]) > 0.01
        assert np.all(cell_lengths)
        is_stress = True
    elif assertion == 'no':
        assert np.allclose(atoms.cell, orig_atoms.cell)
        is_stress = False

    # #### COMPARE THE VALUES OF THE LOG FILE AND TRAJECTORY AGREE #######
    # make sure log file and trajectory are writing the same information
    log_info = parse_gpaw_logfile('relax.log')
    traj_info = read_traj('relax.traj', is_stress=is_stress)
    for traj, log in zip(traj_info, log_info):
        assert traj['step'] == log['step']
        assert np.allclose(traj['fmax'], log['fmax'], 1e-5, 1e-5)
        assert np.allclose(traj['free_energy'], log['energy'], 1e-5, 1e-5)
        if is_stress:
            assert np.allclose(traj['smax'], log['smax'])


def test_gmax_restart(in_tmp_dir, newrepo):
    atoms = bulk('Si')
    atoms.positions[0] += 0.0001
    gbreak, gmax = 1, 10
    fmax, smask = 0.003, np.array([0, 0, 0, 0, 0, 0])

    # #### SINGLE SHOT CALCULATION ######
    # One shot, no restart optimization run, for comparison
    identifier = 'single_relax'
    atoms_single = get_setup(atoms=atoms, identifier=identifier)
    _, traj_single = run_optimization(
        atoms=atoms_single, fmax=fmax, smax=0.0, gmax=gmax, smask=smask,
        identifier=identifier, append_trajectory=False, comm=world)

    # check the name for all the written files are correct and exist
    # XXX we have world barrier issues with restart files so we comment out
    # for now until it is fixed in ASE
    # assert Path(f"restart.{identifier}.json").is_file()
    for ext in ['txt', 'log', 'traj']:
        assert Path(f"{identifier}.{ext}").is_file()

    # ######## FRAGILE RESTART #########
    # get a restart from a gmax met condition
    fragile_identifier = 'fragile_relax'
    atoms_fragile = get_setup(atoms=atoms.copy(),
                              identifier=fragile_identifier)
    atoms_gmax, traj_fragile_0 = run_optimization(
        atoms=atoms_fragile, fmax=fmax, smax=0.0, gmax=gbreak, smask=smask,
        identifier=fragile_identifier, append_trajectory=True, comm=world)
    assert isinstance(atoms_gmax, Atoms) and isinstance(traj_fragile_0, Path)

    # collect final fmax, so we know we aren't converged yet, sanity check
    traj_fragile = Trajectory(f'{fragile_identifier}.traj')
    _, atoms_fragile2 = len(traj_fragile), traj_fragile[-1]  # g is the
    # number of steps, whereas len(traj) contains step 0, step 1 (the first
    # real step) thus g - 1 must equal gbreak
    # this count is wrong in old gpaw but correct in the new interface.
    # assert g - 1 == gbreak  # and Path(f'restart'
    #                                f'.{fragile_identifier}.json').is_file()
    # XXX we have world barrier issues with restart files so we comment
    # out for now until it is fixed in ASE

    # gmax tested, restart file written, aren't converged so let's keep going
    atoms_fragile2 = get_setup(atoms=atoms_fragile2,
                               identifier=fragile_identifier)
    _, traj_fragile = run_optimization(
        atoms=atoms_fragile2, fmax=fmax, smax=0.0, gmax=gmax, smask=smask,
        identifier=fragile_identifier, append_trajectory=True, comm=world)

    # make sure we append traj and actually restart
    fragile_traj = read_traj(traj_fragile, is_stress=False)
    assert len(fragile_traj) > gbreak

    # ### COMPARE SINGLE-SHOT AND FRAGILE RESTART ARE THE "SAME"
    # comparison can only be done with very high ecut and convergence.forces
    # e.g. ecut=800, convergence.forces=1e-7, kpts (2,2,2) get you almost close
    # enough to compare 1:1. I will leave this here for the sake of recording
    # that the test was performed.
    # assert len(fragile_traj) == len(single_traj)
    # for f_traj, s_traj in zip(fragile_traj, single_traj):
    #     for f, s in zip(f_traj, s_traj):
    #         print(f_traj[f], s_traj[s])
    #         assert np.allclose(f_traj[f], s_traj[s], rtol=1e-4, atol=1e-4)
