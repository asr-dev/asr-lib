import os

import numpy as np
from ase import Atoms
from gpaw.mpi import world

from defects.default_params import default_optimizer_params
from defects.tasks.excited import (
    check_excitations,
    collect_results,
    relax_excited,
    relax_ground,
)


def test_excited(newrepo, oldgpaw24):
    """
    Minimal working example of excited state calculation
    """
    if world.rank == 0:
        # Create Mock defect directory for DefectInfo() to work
        os.mkdir('defect.BN.v_B')
    world.barrier()
    os.chdir('defect.BN.v_B')
    if world.rank == 0:
        os.mkdir('task1')
    world.barrier()
    os.chdir('task1')
    # Water molecule:
    d = 0.9575
    t = np.pi / 180 * 104.51
    atoms = Atoms('OH2',
                  positions=[(0, 0, 0),
                             (d, 0, 0),
                             (d * np.cos(t), d * np.sin(t), 0)])
    atoms.center(vacuum=4.0)

    '''
    Ground state calculation
    - Converge unoccupied orbitals for better excited state initial guess
    converge_unocc=True
    - Choose fewest bands possible (include up to orbital of target excitation)
    '''
    calc_params = {'mode': {'name': 'pw',
                            'ecut': 300,
                            'dedecut': 'estimate'},
                   'xc': 'PBE',
                   'kpts': {
                       'size': [1, 1, 1],
                       'gamma': True},
                   'spinpol': True,
                   'occupations': {'name': 'fixed-uniform'},
                   'mixer': {'backend': 'no-mixing'},
                   'symmetry': 'off',
                   'nbands': 5}

    optimizer_params = default_optimizer_params.copy()
    optimizer_params['fmax'] = 0.9
    optimizer_params['gmax'] = 100
    ground = relax_ground(atoms=atoms, calc_params=calc_params,
                          optimizer_params=optimizer_params)

    exc = check_excitations(0.01, ground.gpw_file)
    assert exc == ['alpha', 'beta']
    '''
    Excited state calculation
    - Do not converge unoccupied orbitals
    converge_unocc=False

    If convergence problems
        - Try decreasing the max_step (default is 0.20)
          (sometimes decreasing the max_step also seems to help...)
        eigensolver=FDPWETDM(excited_state=True,
                             max_step_inner_loop=0.20)
        - Try decreasing inner loop steps (default is 100)
        calc.set(eigensolver=FDPWETDM(excited_state=True,
                                      maxiter_inner_loop=100))
        - Try disabling MOM
        prepare_mom_calculation(calc,atoms,f_sn,
                                use_fixed_occupations=True)
    '''
    optimizer_params['identifier'] = 'excited'
    excited = relax_excited(restart_file=ground.gpw_file,
                            optimizer_params=optimizer_params,
                            excitation='alpha')
    results = collect_results(ground.atoms,
                              excited.atoms,
                              ground.energy,
                              excited.energy)
    assert np.isclose(results['zpl'], 6.0566, atol=1e-3)
    assert np.isclose(results['delta_Q'], 0.4199, atol=1e-3)
