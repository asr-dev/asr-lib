import os

import click
import pytest

from asrlib.test.conftest import run_tb_cli
from defects.test.conftest import copy_restart_files


def ls_list():
    result = run_tb_cli(['ls', '-cs'])
    assert result.exit_code == 0
    out = [item.strip() for item in result.output.split('\n')[2:]]
    if out[-1] == '':
        out = out[:-1]
    return out


def count_states(done=0, fail=0, cancel=0, new=0):
    lines = ls_list()
    assert lines.count('done') == done
    assert lines.count('fail') == fail
    assert lines.count('cancel') == cancel
    assert lines.count('new') == new


# test host totree workflow declaration
@pytest.fixture
def BNtotree(in_tmp_dir, newrepo, defect_workflows):
    exec_totree = f'{defect_workflows.as_posix()}/host_totree.py'
    host_file = f'{defect_workflows.as_posix()}/structure_BN.json'

    os.symlink(host_file, 'structure_BN.json')
    result = run_tb_cli(['workflow', exec_totree])
    print(click.unstyle(result.stdout))
    print(click.unstyle(result.output))
    assert result.exit_code == 0


@pytest.fixture
def pristine_wf(in_tmp_dir, newrepo, defect_workflows,
                BNtotree, oldgpaw24, defectfiles):
    exec_main = f'{defect_workflows}/main_wf.py'

    # tb workflow main_wf.py
    run_tb_cli(['workflow', exec_main])
    count_states(new=6)

    # copy restart files and run pristine relax and gs
    copy_restart_files(newrepo, defectfiles, 'BN')
    run_tb_cli(['run', 'tree'])
    count_states(done=9, new=5)
    return exec_main


# XXX Should extend test to excited state wf as well
# XXX Should add test files with light parameters that are
# fast to postprocess.
# Should make testfiles repo that is installed in CI and venv
@pytest.mark.slow
def test_2D_screening_wf(in_tmp_dir, newrepo, defect_workflows,
                         pristine_wf, oldgpaw24, defectfiles):

    # Generate new tasks, copy restart files and run charge_0 calcs
    run_tb_cli(['workflow', pristine_wf])
    copy_restart_files(newrepo, defectfiles, 'BN')
    run_tb_cli(['run', 'tree'])
    count_states(done=13, new=7)

    # SJ tasks
    run_tb_cli(['workflow', pristine_wf])
    count_states(done=13, new=8)
    run_tb_cli(['run', 'tree'])
    count_states(done=18, new=3)

    # zfs
    run_tb_cli(['workflow', pristine_wf])
    zfs = 'tree/BN/defect_wf/defects.BN_000.v_B/charge_states/charge_0/zfs'
    run_tb_cli(['run', zfs])

    # Charge -1 calculations
    count_states(done=19, new=11)
    copy_restart_files(newrepo, defectfiles, 'BN')
    m1 = 'tree/BN/defect_wf/defects.BN_000.v_B/charge_states/charge_-1/'
    run_tb_cli(['run', m1])
    count_states(done=23, new=7)

    # SJ for charge -1
    run_tb_cli(['workflow', pristine_wf])
    count_states(done=23, new=10)

    # copy restart files for SJ halfint calc
    copy_restart_files(newrepo, defectfiles, 'BN')
    sj_path = ('tree/BN/defect_wf/defects.BN_000.v_B/'
               'charge_states/charge_-1/slaterjanak/')
    run_tb_cli(['run', sj_path])
    run_tb_cli(['run', sj_path])
    count_states(done=27, new=6)
    run_tb_cli(['workflow', pristine_wf])

    # XXX Todo Excited states when restart is implemented
