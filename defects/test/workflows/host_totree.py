import taskblaster as tb
from ase.io import read

# Hosts: XXX EDIT FOR YOUR PURPOSE
# corresponding structures should be in
# folder hosts/structure_{hostname}.json
hostnames = ['BN']
hof_dict = {'BN': -1.292}
hostdict = {}
for hostname in hostnames:
    # read structure downloaded from c2db
    hostfile = f'structure_{hostname}.json'
    host = read(hostfile)
    hostdict[hostname] = {'atoms': host,
                          'hof': hof_dict[hostname]}


workflow = tb.totree(hostdict, name='host')
