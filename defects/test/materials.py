"""Module containing a set of test materials.
"""

import numpy as np
from ase import Atoms
from ase.build import bulk

# Make some 1D, 2D and 3D test materials
Si = bulk("Si")
Fe = bulk("Fe")
Fe.set_initial_magnetic_moments([1])
abn = 2.51
BN = Atoms(
    "BN",
    scaled_positions=[[0, 0, 0.5], [1 / 3, 2 / 3, 0.5]],
    cell=[
        [abn, 0.0, 0.0],
        [-0.5 * abn, np.sqrt(3) / 2 * abn, 0],
        [0.0, 0.0, 15.0],
    ],
    pbc=[True, True, False],
)

Agchain = Atoms(
    "Ag",
    scaled_positions=[[0.5, 0.5, 0]],
    cell=[
        [15.0, 0.0, 0.0],
        [0.0, 15.0, 0.0],
        [0.0, 0.0, 2],
    ],
    pbc=[False, False, True],
)

GaAs = Atoms(
    'GaAs',
    cell=bulk('Ga', 'fcc', a=5.68).cell, pbc=True,
    scaled_positions=((0, 0, 0), (0.25, 0.25, 0.25)))


O2 = Atoms('O2', positions=[(0, 0, 0), (0, 0, 1.16)], pbc=(0, 0, 0))
O2.center(vacuum=10)
O2.set_initial_magnetic_moments(np.ones(len(O2), float))
std_test_materials = [Si, BN, Agchain, Fe]
std_test_materials_dict = {'Si': Si, 'BN': BN, 'Agchain': Agchain, 'Fe': Fe,
                           'O2': O2, 'GaAs': GaAs}
