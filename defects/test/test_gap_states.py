from pathlib import Path

import numpy as np

from defects.tasks.analyze_gap_states import gapstates
from defects.test.conftest import fake_gs


def test_gap_states(in_tmp_dir, newrepo, oldgpaw24, defectfiles):
    gs_def = Path(defectfiles /
                  'BN/defect_wf/defects.BN_000.v_B/charge_states/charge_0/gs')
    gs_pris = Path(defectfiles / 'BN/pristine_wf/gs')
    gs_results_def = fake_gs(gs_def)
    gs_results_pris = fake_gs(gs_pris)
    gapstates_res = gapstates(gs_results_def, gs_results_pris,
                              ['v_B'])
    actual_results1 = {'statelist': [165, 166, 167],
                       'state_above': True,
                       'state_below': True,
                       'homo_index': 166,
                       'degenerate_lumo': False}
    actual_results2 = {'dif': 2.1014327827697086,
                       'pot_diff': 0.08318186243221248}
    for key, item in actual_results1.items():
        assert getattr(gapstates_res, key) == item

    for key, item in actual_results2.items():
        assert np.isclose(getattr(gapstates_res, key), item, atol=1e-3)
