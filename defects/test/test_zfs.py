import numpy as np

from defects.tasks.gs import defect_gs
from defects.tasks.zfs import check_triplet_magmom
from defects.tasks.zfs import zero_field_splitting as zfs
from defects.test.materials import std_test_materials_dict


def test_check_triplet_magmom():
    out = check_triplet_magmom(2.2)
    assert not out

    out = check_triplet_magmom(1.95)
    assert out


def test_zfs(newrepo):
    """
    Simple test of gs task
    """
    atoms = std_test_materials_dict['O2']
    calculator = {'mode': {'name': 'pw', 'ecut': 50}, 'xc': 'PBE',
                  'kpts': {'density': 1, 'gamma': True},
                  'txt': 'gs.txt'
                  }
    print('Computing gs')
    output = defect_gs(atoms=atoms, calculator=calculator, dimensionality=3)
    output_zfs = zfs(calc_path=output['gpw_path'],
                     magmom=output['magmom'])
    print(output_zfs['is_triplet'])
    print(output_zfs['D_vv'])

    D_vv = [[-1.33583303e+03, -5.27543318e-05, 6.32642672e-05],
            [-5.27543318e-05, -1.33583281e+03, 1.85868991e-04],
            [6.32642672e-05, 1.85868991e-04, 2.67166583e+03]]
    assert output_zfs['is_triplet']
    assert np.allclose(output_zfs['D_vv'], D_vv, atol=1e-3, rtol=1e-4)
