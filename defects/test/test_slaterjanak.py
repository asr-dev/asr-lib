
import numpy as np
import pytest
from ase.io import Trajectory, read

from defects.tasks.eform import EformResults, get_neutral_eforms
from defects.tasks.slaterjanak import calc_charge_eform, prepare_inputs
from defects.test.conftest import fake_gs


@pytest.mark.parametrize("charge, calc_params_sj", [
    (-1, {'charge': -0.5}),
    (1, {'charge': 0.5})])
def test_prepare_inputs(in_tmp_dir, newrepo, charge, calc_params_sj,
                        defectfiles):
    traj = (defectfiles / 'BN/defect_wf/defects.BN_000.v_B/'
            'charge_states/charge_-1/relax/relax.traj')
    calc_params = {'charge': None}

    actual_results = {'atoms': Trajectory(traj)[0],
                      'calc_params': calc_params_sj}
    results = prepare_inputs(calc_params, charge, traj)
    assert results == actual_results


def test_calc_neutral_eform(in_tmp_dir, newrepo, defect_workflows,
                            oldgpaw24, defectfiles):
    # Testing for v_B charge 0
    host_atoms = read(f'{defect_workflows.as_posix()}/structure_BN.json')
    oqmd = f'{defectfiles}/oqmd12.db'
    hof = -1.292
    etot_def = -698.12
    etot_pris = -714.88
    defecttoken = ['v_B']
    eform_dict_neutral = get_neutral_eforms(host_atoms, oqmd, hof,
                                            etot_def, etot_pris,
                                            defecttoken)

    eform_test = {'standard-states': 10.055315858321322,
                  'B-poor': 8.763315858321322,
                  'N-poor': 10.055315858321322,
                  'min_eform': 8.763315858321322}

    for key, item in eform_test.items():
        assert np.isclose(eform_dict_neutral.eforms[key], item, atol=1e-3)


def test_calc_charge_eform(in_tmp_dir, newrepo, oldgpaw24, defectfiles):
    # Testing for v_B charge -1
    defecttoken = ['v_B']
    homo_index_q = 166
    BN = defectfiles / 'BN'

    halfint_gs = fake_gs(BN / ('defect_wf/defects.BN_000.v_B/'
                               'charge_states/charge_-1/'
                               'slaterjanak/halfint_gs'))
    gs_results_pris = fake_gs(defectfiles / 'BN/pristine_wf/gs')
    traj_path = BN / ('defect_wf/defects.BN_000.v_B/'
                      'charge_states/charge_-1/relax/relax.traj')
    charge = -1
    # eform dict of charge_0
    eform_dict_q = {'standard-states': 8.295315858321331,
                    'B-poor': 7.0475413051345255,
                    'N-poor': 8.295315858321331,
                    'min_eform': 7.0475413051345255}

    eform_q = EformResults(eform_dict_q)
    result = calc_charge_eform(defecttoken, homo_index_q, halfint_gs,
                               gs_results_pris, traj_path, charge,
                               eform_q)

    eform_test = {'standard-states': 2.8744896465019716,
                  'B-poor': 1.6267150933151662,
                  'N-poor': 2.8744896465019716,
                  'min_eform': 1.6267150933151662}

    for key, item in eform_test.items():
        assert np.isclose(result.eforms[key], item, atol=1e-3)
