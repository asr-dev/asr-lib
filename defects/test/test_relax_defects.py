
import numpy as np
import pytest

from defects.tasks.relax import relax_fixcell

expected_positions = {
    'GPAWFactory': [[2.58822714e-03, 2.50524993e-03, 7.50485711],
                    [2.32471793e-03, 1.45170312, 7.50000601]],
    'GPAWD3Factory': [[-0.09745590,  0.06589973,  7.50105647],
                      [-0.09737304, 1.51487369,  7.50113772]]
}


@pytest.mark.parametrize(['factory', 'atol'], [
    ['GPAWFactory', 0.005],
    pytest.param('GPAWD3Factory', 0.01,
                 marks=[pytest.mark.xfail(reason='unsafe access of '
                                                 'materials.py structures '
                                                 'breaks this test.')])
])
def test_relax_fixcell(newrepo, factory, atol, std_test_materials_dict,
                       exec_dftd3):
    atoms = std_test_materials_dict['BN']
    atoms.positions[0] += 0.005
    initial_atoms = atoms.copy()
    calc_params = {'mode': {'name': 'pw', 'ecut': 100}, 'xc': 'PBE',
                   'kpts': {'size': [1, 1, 1], 'gamma': True},
                   'txt': 'relax.txt'
                   }

    output = relax_fixcell(atoms=atoms, factory=factory,
                           calc_params=calc_params)
    relaxed_atoms = output.atoms
    assert not np.allclose(relaxed_atoms.positions, initial_atoms.positions)
    print(relaxed_atoms.positions)
    assert np.allclose(relaxed_atoms.positions, expected_positions[factory],
                       atol=atol)
    assert np.allclose(relaxed_atoms.cell, initial_atoms.cell)

    nimages1 = len(output.read_trajectory())

    # Test restart
    output_restart = relax_fixcell(atoms=atoms, factory=factory,
                                   calc_params=calc_params)
    relaxed_atoms = output_restart.atoms
    print(relaxed_atoms.positions)
    assert np.allclose(relaxed_atoms.positions, expected_positions[factory],
                       atol=atol)
    assert np.allclose(relaxed_atoms.cell, initial_atoms.cell)
    nimages2 = len(output_restart.read_trajectory())

    # structure should already be relaxed
    assert nimages1 > 1
    assert nimages1 == nimages2
