import numpy as np

default_calc_params = {'mode': {'name': 'pw',
                                'ecut': 800,
                                'dedecut': 'estimate'},
                       'xc': 'PBE',
                       'kpts': {
                           'density': 3.0,
                           'gamma': True},
                       'basis': 'dzp',
                       'mixer': {'method': 'fullspin',
                                 'backend': 'pulay'},
                       'symmetry': {
                           'symmorphic': False},
                       'convergence': {
                           'forces': 1e-4,
                           'maximum iterations': 200},
                       'txt': 'relax.txt',
                       'occupations': {
                           'name': 'fermi-dirac',
                           'width': 0.02},
                       'spinpol': True,
                       'maxiter': 400}

default_optimizer_params = {'fmax': 0.05,
                            'gmax': 1000,
                            'smask':  np.array([0, 0, 0, 0, 0, 0]),
                            'smax': 0.1,  # Not used with smask above
                            'append_trajectory': True,
                            'identifier': 'relax'}
