import shutil
from pathlib import Path

"""
This is a script to copy relax restart files from the old tree, generated
with the deprecated wfs to the new tree. Only relevant for the screening
project 2023 that was started with the old workflows
"""

# Path to old tree, modify for your needs
old_tree = '../Fredrik/tree'


# host name according to tree, modify for your needs
host = 'BN'
#######


def copy_restart_files(old_tree, host):
    """Copies restart files for host material {host}
       from {old_tree} to new tree.
    """
    # Defect files gs and relax
    new_folders = Path('tree').glob(f'{host}/*/*/charge_states/*/*')

    for folder in new_folders:
        name = folder.name
        charge = folder.parent.name
        if '-' in charge:
            actual_charge = charge[-1]
            charge = f'charge_m{actual_charge}'

        defect = folder.parents[2].name
        old_path = f'{old_tree}/{host}/{defect}/BasicWF/{charge}/{name}'

        if 'relax' in name and not Path(folder / 'relax.traj').is_file():
            try:
                traj = Path(old_path + '/relax.traj')
                shutil.copy(traj, Path(folder / 'relax.traj'))
                print(f'Successfully copied file: {traj}')
                outp = Path(old_path + '/output.json')
                shutil.copy(outp, Path(folder / 'output_tmp.json'))
                print(f'Successfully copied file: {outp}')
            except FileNotFoundError:
                print(f'Could not copy file: {traj}')
        if 'gs' in name and not Path(folder / 'gs.gpw').is_file():
            try:
                gpw = Path(old_path + '/gs.gpw')
                shutil.copy(gpw, Path(folder / 'gs.gpw'))
                print(f'Successfully copied file: {gpw}')
            except FileNotFoundError:
                print(f'Could not copy file: {gpw}')

    # Pristine material
    new_folders = Path('tree').glob(f'{host}/pristine*/*')
    for folder in new_folders:
        name = folder.name
        defect = folder.parent.name
        old_path = (f'{old_tree}/{host}/defects.pristine_sc.000/'
                    f'BasicWF/charge_0/{name}')
        if 'relax' in name and not Path(folder / 'relax.traj').is_file():
            try:
                traj = Path(old_path + '/relax.traj')
                shutil.copy(traj, Path(folder / 'relax.traj'))
                print(f'Successfully copied file: {traj}')
                outp = Path(old_path + '/output.json')
                shutil.copy(outp, Path(folder / 'output_tmp.json'))
                print(f'Successfully copied file: {outp}')
            except FileNotFoundError:
                print(f'Could not copy file: {traj}')
        if 'gs' in name and not Path(folder / 'gs.gpw').is_file():
            try:
                gpw = Path(old_path + '/gs.gpw')
                shutil.copy(gpw, Path(folder / 'gs.gpw'))
                print(f'Successfully copied file: {gpw}')
            except FileNotFoundError:
                print(f'Could not copy file: {gpw}')


copy_restart_files(old_tree, host)
