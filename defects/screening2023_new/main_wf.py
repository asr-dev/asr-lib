"""
Main defects workflow function
"""
import taskblaster as tb

from defects.default_params import default_optimizer_params
from defects.workflows.defect_screening_wf import DefectScreeningWf


@tb.parametrize_glob('*/host')
def workflow(host):

    # XXX Need to double-check calculators against ASR defaults
    calc_relax = {'mode': {
                      'name': 'pw',
                      'ecut': 800,
                      'dedecut': 'estimate'},
                  'xc': 'PBE',
                  'kpts': {
                      'density': 3.0,
                      'gamma': True},
                  'basis': 'dzp',
                  'mixer': {'method': 'fullspin',
                            'backend': 'pulay'},
                  'symmetry': {
                      'symmorphic': False},
                  'convergence': {
                      'forces': 1e-4,
                      'maximum iterations': 200},
                  'txt': 'relax.txt',
                  'occupations': {
                      'name': 'fermi-dirac',
                      'width': 0.02},  # Note: reduced fermi smearing again
                  'spinpol': True,
                  'maxiter': 400}
    optimizer_params = default_optimizer_params.copy()

    calc_gs = {'mode': {
                   'name': 'pw',
                   'ecut': 800,
                   'dedecut': 'estimate'},
               'xc': 'PBE',
               'kpts': {
                   'density': 3.0,
                   'gamma': True},
               'basis': 'dzp',
               'mixer': {'method': 'fullspin',
                         'backend': 'pulay'},
               'symmetry': {
                   'symmorphic': False},
               'convergence': {
                   'bands': "CBM+3.0"},
               'nbands': "200%",
               'txt': 'gs.txt',
               'occupations': {
                   'name': 'fermi-dirac',
                   'width': 0.02},
               'spinpol': True,
               'maxiter': 400}

    oqmd = '/home/niflheim/fafb/db/oqmd12.db'
    factory = 'GPAWFactory'

    # Low params calculator with only Gamma point and smaller ecut
    calc_excited = {'mode': {
                          'name': 'pw',
                          'ecut': 800,
                          'dedecut': 'estimate'},
                    'xc': 'PBE',
                    'kpts': {
                        'size': [1, 1, 1],
                        'gamma': True},
                    'basis': 'dzp',
                    'symmetry': 'off',  # XXX Why?
                    'convergence': {
                        'forces': 1e-3,
                        'maximum iterations': 400},
                    'txt': 'relax.txt',
                    'occupations': {'name': 'fixed-uniform'},
                    'spinpol': True,
                    'nbands': '101%',  # defined in task
                    'maxiter': 5000,
                    'mixer': {'backend': 'no-mixing'},
                    }
    optimizer_params_excited = optimizer_params.copy()
    optimizer_params_excited['fmax'] = 0.02
    min_homo_lumo = 0.4

    setup_defect_task = \
        'defects.screening2023_new.generate_defects.costume_defect_generator'
    metals = 'B,C,N,F,Al,Si,P,S,Cl,Sc,Ti,V,Cr,Mn,Fe,Co,Ni,Cu,Zn,Ga,Ge,As,Se,Br'
    defect_kwargs = {'general_algorithm': 15.,
                     'double': 'all',  # all kinds of double defects
                     'scaling_double': 1.5,
                     'extrinsic': metals}
    charges = [-1, 0, 1]
    all_charge_states = False

    max_pot_diff = 0.5  # Maximum potential difference in energy referencing

    return DefectScreeningWf(setup_defect_task=setup_defect_task,
                             charges=charges,
                             generate_defect_kwargs=defect_kwargs,
                             host_atoms=host['atoms'],
                             hof=host['hof'],
                             calc_params_relax=calc_relax,
                             calc_params_gs=calc_gs,
                             optimizer_params=optimizer_params,
                             oqmd=oqmd,
                             factory=factory,
                             calc_params_excited=calc_excited,
                             optimizer_params_excited=optimizer_params_excited,
                             min_homo_lumo=min_homo_lumo,
                             calculate_all_charge_states=all_charge_states,
                             max_pot_diff=max_pot_diff)
