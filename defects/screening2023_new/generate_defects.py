from defects.tasks.setup_defects import setup_defect_dict


def costume_defect_generator(host, defect_kwargs):
    defects = {}
    extrinsic = defect_kwargs['extrinsic']
    metals = extrinsic.split(',')
    # Removing host atoms from extrinsic substitutions
    for hostatom in host.get_chemical_symbols():
        if hostatom in metals:
            metals.remove(hostatom)

    # Slightly cumbersome and slow but needed since we have to generate
    # only X-X and O-X extrinsic double defects
    num_metals = len(metals)
    for i, metal in enumerate(metals):
        print('setting up for extrinsic element '
              f'{i+1}/{num_metals}: {metal}')
        defect_kwargs['extrinsic'] = f'O,{metal}'
        print("kwargs:")
        print(defect_kwargs)
        defects_tmp = setup_defect_dict(host,
                                        **defect_kwargs)
        defects.update(defects_tmp)

    return defects
