import taskblaster as tb
from ase import Atoms

from asrlib.results import StandardResult
from defects.workflows.chargestates import ChargeStatesWf, PristineWf


@tb.dynamical_workflow_generator_task
def generate_defect_wf(defect_dict: dict, charges: list,
                       calc_params_relax: dict, calc_params_gs: dict,
                       calc_params_excited: dict, optimizer_params: dict,
                       optimizer_params_excited: dict,
                       min_homo_lumo: float, factory: str,
                       gs_results_pris: StandardResult,
                       calculate_all_charge_states: bool,
                       host_atoms: Atoms,
                       oqmd: str, hof: float,
                       max_pot_diff: float) -> ChargeStatesWf:
    """
    :param defect_dict: dict with defect structures.
    :param charges: list of integers with charge states.
    :param calc_params_relax: dict.
    :param calc_params_gs: dict.
    :param calc_params_excited: dict.
    :param optimizer_params: dict.
    :param optimizer_params_excited: dict.
    :param min_homo_lumo: float. minimum homo-lumo energy.
    :param factory: asrlib.tasks.Factory.
    :param gs_results_pris: GSResults pristine gs results.
    :param calculate_all_charge_states: bool. If true, all charge states
           are calculated if False new charge state is only calculated if
           there are available states in the gap.
    :param max_pot_diff maximum potential difference in energy referencing.
    :return: Generates ChargeStatesWf.
    """
    for name, item in defect_dict.items():
        if 'pristine' not in name:
            wf = ChargeStatesWf(
                atoms=item,
                charges=charges,
                calc_params_relax=calc_params_relax,
                calc_params_gs=calc_params_gs,
                calc_params_excited=calc_params_excited,
                optimizer_params=optimizer_params,
                optimizer_params_excited=optimizer_params_excited,
                min_homo_lumo=min_homo_lumo,
                factory=factory,
                gs_results_pris=gs_results_pris,
                defecttoken=name.split('.')[2:],
                calculate_all_charge_states=calculate_all_charge_states,
                host_atoms=host_atoms, oqmd=oqmd, hof=hof,
                max_pot_diff=max_pot_diff)
            yield name, wf


@tb.workflow
class DefectScreeningWf:
    """
    Main defect workflow for generating defects and initializing
    screening workflow.

    :param host_atoms: host structure.
    :param setup_defect_task: defect generation inputs.
        setup_defect_task makes it possible to have a customized
        script to generate the defects. If no customization is needed
        to choose defects.tasks.setup_defects.setup_defect_dict
    :param generate_defect_kwargs: kwargs for defect generation. defect
        generation inputs
    :param calc_params_relax:
    :param calc_params_gs:
    :param calc_params_excited:
    :param optimizer_params:
    :param optimizer_params_excited:
    :param factory: GPAWD3Factory or GPAWFactory
    :param oqmd: paths to reference database
    :param c2db: paths to reference database
    :param min_homo_lumo: Screening criteria.
    :param charges: Charge states
    :param calculate_all_charge_states: bool if True all charge
        states are calculated, if False new charge state is only
        calculated if there is an available state in the gap.
    """
    # defect generation inputs
    host_atoms = tb.var()
    setup_defect_task = tb.var()
    generate_defect_kwargs = tb.var()

    # calculation params
    calc_params_relax = tb.var()
    calc_params_gs = tb.var()
    calc_params_excited = tb.var()
    optimizer_params = tb.var()
    optimizer_params_excited = tb.var()
    factory = tb.var()

    # reference databases
    oqmd = tb.var()

    hof = tb.var()

    # screening params
    min_homo_lumo = tb.var()
    charges = tb.var()
    calculate_all_charge_states = tb.var()
    max_pot_diff = tb.var()

    @tb.task
    def generate_defects(self):
        return tb.node(self.setup_defect_task,
                       host=self.host_atoms,
                       defect_kwargs=self.generate_defect_kwargs)

    @tb.subworkflow
    def pristine_wf(self):
        return PristineWf(atoms=self.generate_defects['pristine_sc'],
                          calc_params_relax=self.calc_params_relax,
                          optimizer_params=self.optimizer_params,
                          calc_params_gs=self.calc_params_gs,
                          factory=self.factory)

    @tb.dynamical_workflow_generator({'results': '**'})
    def defect_wf(self):
        return tb.node(
            'defects.workflows.defect_screening_wf.generate_defect_wf',
            defect_dict=self.generate_defects,
            charges=self.charges,
            calc_params_relax=self.calc_params_relax,
            optimizer_params=self.optimizer_params,
            calc_params_gs=self.calc_params_gs,
            calc_params_excited=self.calc_params_excited,
            optimizer_params_excited=self.optimizer_params_excited,
            min_homo_lumo=self.min_homo_lumo,
            factory=self.factory,
            gs_results_pris=self.pristine_wf.gs,
            calculate_all_charge_states=self.calculate_all_charge_states,
            host_atoms=self.host_atoms,
            hof=self.hof, oqmd=self.oqmd,
            max_pot_diff=self.max_pot_diff)
