import numpy as np
import taskblaster as tb

from asrlib.results import StandardResult
from defects.workflows.excited import ExcitedDefectWf
from defects.workflows.slater_janak_wf import SlaterJanakWf


@tb.workflow
class PristineWf:
    atoms = tb.var()
    calc_params_relax = tb.var()
    calc_params_gs = tb.var()
    optimizer_params = tb.var()
    factory = tb.var()

    @tb.task
    def relax(self):
        return tb.node('defects.tasks.relax.relax_fixcell',
                       atoms=self.atoms,
                       calc_params=self.calc_params_relax,
                       optimizer_params=self.optimizer_params,
                       factory=self.factory)

    @tb.task
    def gs(self):
        return tb.node('defects.tasks.gs.ground_state',
                       atoms=self.relax.atoms,
                       calc_params=self.calc_params_gs,
                       factory=self.factory)


@tb.dynamical_workflow_generator_task
def generate_chargestate_wf(atoms, calc_params_relax,
                            calc_params_gs, calc_params_excited,
                            optimizer_params, optimizer_params_excited,
                            min_homo_lumo, factory, charges, gs_results_pris,
                            defecttoken, calculate_all_charge_states,
                            host_atoms, hof, oqmd, max_pot_diff):
    """ Generates a ChargeStateWf for each charge state of the defect.
        charge_-2 gets the input atoms from charge_-1 relaxation and also
        information if there are available states in the gap etc.
    """
    # Sort so that charges are computed in order of absolute charge
    # and previous calculation is used as input to current calculation
    # if not calculate_all_charge_states new charge state is only calculated
    # if previous charge state had available KS states in gap
    charges.sort(key=np.abs)
    previous_negative_results = StandardResult(eform=None,
                                               gapstates={'homo_index': None})
    previous_positive_results = StandardResult(eform=None,
                                               gapstates={'homo_index': None})
    previous_negative_atoms = atoms
    previous_positive_atoms = atoms

    for charge in charges:
        if charge < 0:
            atoms = previous_negative_atoms
            previous_results = previous_negative_results
        else:
            atoms = previous_positive_atoms
            previous_results = previous_positive_results
        wf = ChargeStateWf(
            atoms=atoms,
            charge=charge,
            calc_params_relax=calc_params_relax,
            calc_params_gs=calc_params_gs,
            calc_params_excited=calc_params_excited,
            optimizer_params=optimizer_params,
            optimizer_params_excited=optimizer_params_excited,
            min_homo_lumo=min_homo_lumo,
            factory=factory,
            gs_results_pris=gs_results_pris,
            defecttoken=defecttoken,
            results_previous_q=previous_results,
            host_atoms=host_atoms,
            oqmd=oqmd,
            hof=hof,
            calculate_all_charge_states=calculate_all_charge_states,
            max_pot_diff=max_pot_diff)

        name = f'charge_{charge}'
        yield name, wf

        if charge == 0:
            previous_negative_results = wf.results
            previous_positive_results = wf.results
            previous_negative_atoms = wf.relax.atoms
            previous_positive_atoms = wf.relax.atoms
        elif charge < 0:
            previous_negative_results = wf.results
            previous_negative_atoms = wf.relax.atoms
        else:
            previous_positive_results = wf.results
            previous_positive_atoms = wf.relax.atoms


@tb.workflow
class ChargeStateWf:
    atoms = tb.var()
    calc_params_relax = tb.var()
    calc_params_gs = tb.var()
    optimizer_params = tb.var()
    charge = tb.var()
    calc_params_excited = tb.var()
    optimizer_params_excited = tb.var()
    min_homo_lumo = tb.var()
    factory = tb.var()
    gs_results_pris = tb.var()
    defecttoken = tb.var()
    results_previous_q = tb.var()
    host_atoms = tb.var()
    oqmd = tb.var()
    hof = tb.var()
    calculate_all_charge_states = tb.var()
    max_pot_diff = tb.var()

    @tb.task
    def calc_params(self):
        return tb.node('defects.tasks.hacks.update_calc_params',
                       charge=self.charge,
                       relax=self.calc_params_relax,
                       gs=self.calc_params_gs,
                       excited=self.calc_params_excited)

    @tb.branch('entry')
    @tb._if(true='charge', false='external')
    @tb.task
    def do_charge_state(self):
        return tb.node(
            'defects.tasks.decisions.do_charge_state',
            prev_gapstates=self.results_previous_q.gapstates,
            calculate_all_charge_states=self.calculate_all_charge_states,
            charge=self.charge)

    @tb.branch('charge')
    @tb.task
    def relax(self):
        return tb.node('defects.tasks.relax.relax_fixcell',
                       atoms=self.atoms,
                       calc_params=self.calc_params.relax,
                       optimizer_params=self.optimizer_params,
                       factory=self.factory)

    @tb.branch('charge')
    @tb.task
    def gs(self):
        return tb.node('defects.tasks.gs.ground_state',
                       atoms=self.relax.atoms,
                       calc_params=self.calc_params_gs,
                       factory=self.factory)

    @tb.branch('charge')
    @tb.jump('external')
    @tb.task
    def gapstates(self):
        return tb.node('defects.tasks.analyze_gap_states.gapstates',
                       gs_results_def=self.gs,
                       gs_results_pris=self.gs_results_pris,
                       defecttoken=self.defecttoken,
                       max_pot_diff=self.max_pot_diff)

# Eventually, there would be a branch for FNV as well.
# First one would have to then decide by collecting prev results
# whether to do go to FNV or SJ branch.
    @tb.branch('charge')
    @tb.subworkflow
    def slaterjanak(self):
        return SlaterJanakWf(
            traj=self.relax.traj,
            calc_params=self.calc_params.gs,
            charge=self.charge,
            gs_results_pris=self.gs_results_pris,
            gs_results_def=self.gs,
            defecttoken=self.defecttoken,
            factory=self.factory,
            eform_result_q=self.results_previous_q.eform,
            homo_index_q=self.results_previous_q.gapstates.homo_index,
            host_atoms=self.host_atoms,
            oqmd=self.oqmd,
            hof=self.hof)

    @tb.fixedpoint
    @tb.branch('external')
    @tb.task
    def results(self):
        return tb.node('defects.tasks.decisions.collect_results',
                       gapstates=self.Phi(charge=self.gapstates,
                                          entry=self.do_charge_state),
                       charge=self.charge,
                       gs=self.Phi(charge=self.gs,
                                   entry=self.do_charge_state),
                       relax=self.Phi(charge=self.relax,
                                      entry=self.do_charge_state),
                       eform=self.Phi(charge=self.slaterjanak.eform_results,
                                      entry=self.do_charge_state))

    @tb.branch('external')
    @tb._if(true='spin')
    @tb.task
    def do_excited_calc(self):
        return tb.node('defects.tasks.decisions.do_excited_calc',
                       magmom=self.results.gs.results.properties.magmom,
                       gapstates=self.results.gapstates)

    @tb.branch('spin')
    @tb.task
    def zfs(self):
        return tb.node('defects.tasks.zfs.zero_field_splitting',
                       calc_path=self.results.gs.calculate.path,
                       magmom=self.results.gs.results.properties.magmom,
                       factory=self.factory)

    @tb.branch('spin')
    @tb.subworkflow
    def excited_states(self):
        return ExcitedDefectWf(atoms=self.relax.atoms,
                               calc_params=self.calc_params.excited,
                               optimizer_params=self.optimizer_params_excited,
                               min_homo_lumo=self.min_homo_lumo,
                               gs_result=self.results.gs,
                               factory=self.factory)


@tb.workflow
class ChargeStatesWf:
    """
    Workflow for performing structural relaxation and gs
    calculation of defects in 0, +/- 1 charge states
    """
    atoms = tb.var()
    calc_params_relax = tb.var()
    calc_params_gs = tb.var()
    optimizer_params = tb.var()
    calc_params_excited = tb.var()
    optimizer_params_excited = tb.var()
    min_homo_lumo = tb.var()
    factory = tb.var()
    charges = tb.var()
    gs_results_pris = tb.var()
    defecttoken = tb.var()
    calculate_all_charge_states = tb.var()
    host_atoms = tb.var()
    oqmd = tb.var()
    hof = tb.var()
    max_pot_diff = tb.var()

    @tb.dynamical_workflow_generator({'results': '**'})
    def charge_states(self):
        return tb.node(
            'defects.workflows.chargestates.generate_chargestate_wf',
            atoms=self.atoms,
            calc_params_relax=self.calc_params_relax,
            calc_params_gs=self.calc_params_gs,
            calc_params_excited=self.calc_params_excited,
            optimizer_params=self.optimizer_params,
            optimizer_params_excited=self.optimizer_params_excited,
            min_homo_lumo=self.min_homo_lumo,
            factory=self.factory,
            charges=self.charges,
            gs_results_pris=self.gs_results_pris,
            defecttoken=self.defecttoken,
            calculate_all_charge_states=self.calculate_all_charge_states,
            host_atoms=self.host_atoms,
            hof=self.hof,
            oqmd=self.oqmd,
            max_pot_diff=self.max_pot_diff)

# Package all the necessary inputs for next charge states into one -
# prev relaxed atoms, above below, prev eform  etc

# def prep_nextcharge_input():
#     ....
