'''
Workflow for performing Slater-Janak calculations
            --sj+0.5---> charge_1
           |
charge_0 --
           |
            --sj-0.5---> charge_m1 --
                                     |
                                      --sj-0.5---> charge_m2

transitions from q --> q'
atoms_q, atoms_q'
finding the eform for charge q' -- Current charge

REMEMBER - We need traj_q' as input in this WF.
Otherwise, we need to pass atoms_q for the halfint_gs calculation
and the traj_q' for calculating relaxation correction.
'''

import taskblaster as tb


@tb.workflow
class SlaterJanakWf:
    """
    Workflow for performing Slater-Janak calculations
    """
    traj = tb.var()
    calc_params = tb.var()
    charge = tb.var()
    gs_results_pris = tb.var()
    gs_results_def = tb.var()
    defecttoken = tb.var()
    factory = tb.var()
    eform_result_q = tb.var()
    homo_index_q = tb.var()
    host_atoms = tb.var()
    oqmd = tb.var()
    hof = tb.var()

    @tb.branch('entry')
    @tb._if(true='charge', false='neutral')
    @tb.task
    def check_charge(self):
        return tb.node('defects.tasks.decisions.check_charge',
                       charge=self.charge)

    @tb.branch('charge')
    @tb.task
    def prepare_inputs(self):
        return tb.node('defects.tasks.slaterjanak.prepare_inputs',
                       calc_params=self.calc_params,
                       charge=self.charge,
                       traj=self.traj)

    @tb.branch('charge')
    @tb.task
    def halfint_gs(self):
        return tb.node('defects.tasks.gs.ground_state',
                       atoms=self.prepare_inputs.atoms,
                       calc_params=self.prepare_inputs.calc_params,
                       factory=self.factory)

    @tb.branch('charge')
    @tb.jump('external')
    @tb.task
    def eform_charge(self):
        return tb.node('defects.tasks.slaterjanak.calc_charge_eform',
                       defecttoken=self.defecttoken,
                       homo_index_q=self.homo_index_q,
                       halfint_gs=self.halfint_gs,
                       gs_results_pris=self.gs_results_pris,
                       traj_path=self.traj,
                       charge=self.charge,
                       eform_result_q=self.eform_result_q)

    # Neutral branch
    @tb.branch('neutral')
    @tb.jump('external')
    @tb.task
    def eform_neutral(self):
        return tb.node('defects.tasks.eform.get_neutral_eforms',
                       host_atoms=self.host_atoms,
                       oqmd=self.oqmd, hof=self.hof,
                       etot_def=self.gs_results_def.results.energy,
                       etot_pris=self.gs_results_pris.results.energy,
                       defecttoken=self.defecttoken)

    @tb.fixedpoint
    @tb.branch('external')
    @tb.task
    def eform_results(self):
        return tb.node('defects.tasks.decisions.eform_results',
                       eforms=self.Phi(charge=self.eform_charge,
                                       neutral=self.eform_neutral))
