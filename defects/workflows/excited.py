import taskblaster as tb


@tb.dynamical_workflow_generator_task
def generate_excitations(relax_gs, optimizer_params,
                         excitations, factory):
    for excitation in excitations:
        wf = RelaxExcitedWf(relax_gs=relax_gs,
                            optimizer_params=optimizer_params,
                            excitation=excitation,
                            factory=factory)
        yield excitation, wf


@tb.workflow
class RelaxExcitedWf:
    optimizer_params = tb.var()
    excitation = tb.var()  # alpha or beta
    relax_gs = tb.var()
    factory = tb.var()

    @tb.task
    def relax(self):
        return tb.node('defects.tasks.excited.relax_excited',
                       restart_file=self.relax_gs.gpw_file,
                       optimizer_params=self.optimizer_params,
                       excitation=self.excitation,
                       factory=self.factory)

    @tb.task
    def results(self):
        return tb.node('defects.tasks.excited.collect_results',
                       atoms_gs=self.relax_gs.atoms,
                       atoms_exc=self.relax.atoms,
                       energy_gs=self.relax_gs.energy,
                       energy_exc=self.relax.energy)


@tb.workflow
class ExcitedDefectWf:
    """
    Workflow for performing excited states defect calculations
    """
    atoms = tb.var()
    calc_params = tb.var()
    optimizer_params = tb.var()
    min_homo_lumo = tb.var()
    gs_result = tb.var()
    factory = tb.var()

    # First check which excitations that are available in given homo_lumo_range
    @tb.task
    def check_excitations(self):
        return tb.node('defects.tasks.excited.check_excitations',
                       min_homo_lumo=self.min_homo_lumo,
                       gs_gpw_path=self.gs_result.calculate.path)

    # Checks if list of allowed excitations not empty and then launch
    # excited state workflow

    @tb._if(true='excited')
    @tb.task
    def have_excitations(self):
        return tb.node('defects.tasks.decisions.list_not_empty',
                       input_list=self.check_excitations)

    # gs relaxation with excited state calc_params
    @tb.branch('excited')
    @tb.task
    def relax_gs(self):
        return tb.node('defects.tasks.excited.relax_ground',
                       atoms=self.atoms,
                       calc_params=self.calc_params,
                       optimizer_params=self.optimizer_params,
                       factory=self.factory,
                       magmom=self.gs_result.results.properties.magmom)

    # generate excited state wfs
    @tb.branch('excited')
    @tb.dynamical_workflow_generator({'results': '**'})
    def excitations(self):
        return tb.node('defects.workflows.excited.generate_excitations',
                       relax_gs=self.relax_gs,
                       optimizer_params=self.optimizer_params,
                       excitations=self.check_excitations,
                       factory=self.factory)
