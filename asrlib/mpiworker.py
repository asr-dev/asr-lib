import json
import os
import pickle
import sys

from click.testing import CliRunner
from gpaw.mpi import broadcast, world
from taskblaster.cli import tb as tb_cli

from asrlib.test.conftest import MPIWORKER_INTERNAL_ERROR, TBOutput


def main(log):
    stdin = sys.stdin
    try:
        while True:
            # Load command
            if world.rank == 0:
                commands = json.loads(stdin.buffer.readline())
                log('COMMANDS', commands)
            else:
                commands = None
            commands = broadcast(commands, 0, world)
            log('BROADCASTED COMMANDS', commands)
            if not commands:
                break

            cwd, args = commands
            if cwd == 'quit':
                log('Quitting mpi worker peacefully')
                raise SystemExit
            os.chdir(cwd)

            assert args[0] == 'mpirun', args[0]
            assert args[1] == '-np'
            assert args[2] == str(world.size)
            assert args[3] == 'tb'
            args = args[4:]

            runner = CliRunner()
            log('INVOKING', args)
            result = runner.invoke(tb_cli, args)
            log('OUTPUT', result.exit_code, result.output)
            world.barrier()
            if world.rank == 0:
                # stderr not separately captured
                tboutput = TBOutput(result.output, '', result.exit_code)
                hexstr = pickle.dumps(tboutput).hex()
                print(hexstr, flush=True)

    except BaseException as e:
        tboutput = TBOutput(
            '', f'MPIWorker internal exception {e}', MPIWORKER_INTERNAL_ERROR
        )
        print(pickle.dumps(tboutput).hex(), flush=True)


if __name__ == '__main__':
    # For easy debugging in the future
    with open('/dev/null', 'w') as log_file:

        def log(*args):
            print(*args, file=log_file, flush=True)

        main(log)
