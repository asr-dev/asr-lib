def forward_properties(selfname, propnames):
    """Forward attributes from an object on self to the main object namespace.

    Say that we define two dataclasses,

    >>> from dataclasses import dataclass

    >>> @dataclass
    >>> class SubObj:
    >>>     a: int

    >>> @dataclass
    >>> class SuperObj:
    >>>     subobj: SubObj
    >>>     b: int

    then the variable `a` can be accessed as `SuperObj.subobj.a`. Adding now
    the `forward_properties` decorator,

    >>> @forward_properties('subobj', ['a'])
    >>> @dataclass
    >>> class SuperObj:
    >>>     subobj: SubObj
    >>>     b: int

    we forward the variable `a` to the `SuperObj` namespace, meaning that it
    can be accessed as `SuperObj.a`.
    """

    def define_getters(cls):
        for name in propnames:

            def get_thing(self, _name=name):
                subobject = getattr(self, selfname)
                return getattr(subobject, _name)

            get_thing.__name__ = name

            setattr(cls, name, property(get_thing))
        return cls

    return define_getters
