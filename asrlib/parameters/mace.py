from dataclasses import dataclass
from pathlib import Path

from ase import Atoms
from mace.calculators import MACECalculator
from mace.modules.models import ScaleShiftMACE

from asrlib.parameters import Parameter
from asrlib.results.mace import MACEProperties
from asrlib.typing import TBCommunicator


@dataclass
class MACECalculatorFactory(Parameter):
    model_path: Path

    def __post_init__(self):
        self.properties_cls = MACEProperties

    def __call__(
        self,
        # For now, the inputs are ignored, but exist to satisfy the
        # RelaxCalculatorFactory protocol
        atoms: Atoms,
        comm: TBCommunicator,
    ):
        """Generate MACE calculator."""
        # In the future, we will take a task context object as input instead of
        # the communicator. Resultantly, we will be able to determine the
        # appropriate device input to MACE. XXX
        # For now, we always evaluate the MACE-mp model using
        device = 'cpu'
        model = self.load(self.model_path, device=device)
        return self.construct_calculator(model, device=device)

    @staticmethod
    def construct_calculator(model: ScaleShiftMACE, *, device: str):
        """Construct a MACECalculator from a model in memory."""
        # In order to stop MACECalculator.__init__ from calling torch.load, we
        # monkey-patch it to return the input model
        import torch

        _tload = torch.load
        try:
            torch.load = lambda f, map_location: model
            calc = MACECalculator(model_paths=[Path()], device=device)
        finally:
            torch.load = _tload
        return calc

    @staticmethod
    def load(path: Path, *, device: str = 'cpu'):
        """Load a MACE model from disk.

        To "safely" load a MACE model, we need to whitelist only a minimum of
        globals for the unpickler. Otherwise torch.load will excute arbitrary
        code from the model file, possibly including malware.
        Of course this isn't 100% bullet proof, since we still need to trust
        that MACE itself does not distribute malware (in its source code or
        from its dependencies).
        """
        from codecs import encode

        import e3nn.o3._tensor_product as e3o3tp
        import torch
        from e3nn.math._normalize_activation import normalize2mom
        from e3nn.nn._activation import Activation
        from e3nn.nn._fc import FullyConnectedNet, _Layer
        from e3nn.o3._irreps import Irrep, Irreps, _MulIr
        from e3nn.o3._linear import Instruction, Linear
        from e3nn.o3._spherical_harmonics import SphericalHarmonics
        from mace.modules.blocks import (
            AtomicEnergiesBlock,
            EquivariantProductBasisBlock,
            LinearNodeEmbeddingBlock,
            LinearReadoutBlock,
            NonLinearReadoutBlock,
            RadialEmbeddingBlock,
            RealAgnosticResidualInteractionBlock,
            ScaleShiftBlock,
        )
        from mace.modules.irreps_tools import reshape_irreps
        from mace.modules.radial import BesselBasis, PolynomialCutoff
        from mace.modules.symmetric_contraction import (
            Contraction,
            SymmetricContraction,
        )

        # Globals from mace
        # https://github.com/ACEsuit/mace
        torch.serialization.add_safe_globals(
            [
                ScaleShiftMACE,
                LinearNodeEmbeddingBlock,
                RadialEmbeddingBlock,
                BesselBasis,
                PolynomialCutoff,
                AtomicEnergiesBlock,
                RealAgnosticResidualInteractionBlock,
                reshape_irreps,
                EquivariantProductBasisBlock,
                SymmetricContraction,
                Contraction,
                LinearReadoutBlock,
                NonLinearReadoutBlock,
                ScaleShiftBlock,
            ]
        )
        # Globals from e3nn (Euclidean Neural Networks)
        # https://github.com/e3nn
        torch.serialization.add_safe_globals(
            [
                Linear,
                Irreps,
                _MulIr,
                Irrep,
                Instruction,
                SphericalHarmonics,
                e3o3tp._tensor_product.TensorProduct,
                e3o3tp._instruction.Instruction,
                FullyConnectedNet,
                _Layer,
                normalize2mom,
                e3o3tp._sub.FullyConnectedTensorProduct,
                Activation,
            ]
        )
        # Globals from torch
        torch.serialization.add_safe_globals(
            [
                torch.nn.modules.container.ModuleList,
                torch.nn.functional.silu,
                torch.fx.graph_module.reduce_graph_module,
                torch.fx._symbolic_trace.Tracer,
                torch.nn.modules.container.ParameterList,
            ]
        )
        # Python standard library globals
        torch.serialization.add_safe_globals(
            [
                encode,
            ]
        )
        # Built-in globals
        torch.serialization.add_safe_globals(
            [
                set,
            ]
        )
        # Actually load the model and clear globals afterwards
        model = torch.load(path, weights_only=True, map_location=device)
        torch.serialization.clear_safe_globals()
        return model
