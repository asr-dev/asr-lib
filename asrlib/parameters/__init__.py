from dataclasses import dataclass, fields


@dataclass
class Parameter:
    def __new__(cls, *args, **kwargs):
        """
        Create a new Parameter object while observing which variables will fall
        back to its Field default.
        """
        # Here we do a hack and grab a private function; mypy would complain
        import dataclasses

        getfields = dataclasses._fields_in_init_order

        # Which fields aren't specified by a positional input?
        (std_init_fields, kw_only_init_fields) = getfields(fields(cls))
        possible_default_fields = (
            std_init_fields[len(args) :] + kw_only_init_fields
        )
        # Which fields will fall back to the default?
        default_fields = {
            field.name
            for field in possible_default_fields
            if field.name not in kwargs
        }
        # Create Parameter object and store this information
        obj = super().__new__(cls)
        obj._default_fields = default_fields
        return obj

    @property
    def default_fields(self):
        return self._default_fields

    def nondefault_fields_dict(self):
        return {
            field.name: getattr(self, field.name)
            for field in fields(self)
            if field.name not in self.default_fields
        }

    def new_with(self, **kwargs):
        dct = self.nondefault_fields_dict()
        dct.update(kwargs)
        return self.new_from_dict(dct)

    @classmethod
    def new_from_dict(cls, dct: dict):
        return cls(**dct)

    def tb_encode(self):
        return self.nondefault_fields_dict()

    @classmethod
    def tb_decode(cls, dct: dict):
        return cls.new_from_dict(dct)
