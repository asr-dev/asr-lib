from copy import deepcopy
from dataclasses import dataclass
from functools import cached_property
from typing import Callable

from ase import Atoms

from asrlib.calculators.ase import DFTD3
from asrlib.parameters import Parameter
from asrlib.results.gpaw import GPAWProperties
from asrlib.typing import TBCommunicator
from asrlib.typing.gpaw import BuiltInGPAWCalculator


@dataclass
class GPAWCalculatorFactory(Parameter):
    """Main GPAW calculator interface."""

    params: dict

    def __post_init__(self):
        assert 'communicator' not in self.params
        self.properties_cls = GPAWProperties

    @property
    def gpaw_new(self):
        return self.params.get('gpaw_new', False)

    @property
    def spinpol(self):
        return self.params.get('spinpol', None)

    @cached_property
    def _gpaw_params(self):
        explicit_defaults = {
            # Set explicit defaults of the inputs that we guarantee access to
            'xc': 'LDA',
        }
        exempt_keys = [
            'gpaw_new',
        ]
        input_params = {
            key: value
            for key, value in self.params.items()
            if key not in exempt_keys
        }
        return {**explicit_defaults, **input_params}

    @property
    def xc(self):
        return self._gpaw_params['xc']

    def __call__(
        self, atoms: Atoms, comm: TBCommunicator
    ) -> BuiltInGPAWCalculator:
        GPAW: Callable
        if self.gpaw_new:
            from gpaw.new.ase_interface import GPAW as NewGPAW

            GPAW = NewGPAW
        else:
            from gpaw.calculator import GPAW as OldGPAW

            GPAW = OldGPAW

        return GPAW(communicator=comm, **self.get_gpaw_params(atoms, comm))

    def get_gpaw_params(self, atoms: Atoms, comm: TBCommunicator) -> dict:
        # Subclasses may want to tailor any number of GPAW parameters depending
        # on the atoms instance in question (and possibly also the available
        # computational ressources). To do so, they can overwrite this method.
        return deepcopy(self._gpaw_params)

    def new_with(self, **kwargs):
        # For the GPAWCalculatorFactory, we overwrite the Parameter.new_with()
        # method, such that the input **kwargs target self.params keys and not
        # the fields on the GPAWCalculatorFactory
        params = self.params.copy()
        params.update(**kwargs)
        return super().new_with(params=params)

    def d3_wrapper(self, dft: BuiltInGPAWCalculator, comm: TBCommunicator):
        return DFTD3(dft=dft, xc=self.xc, comm=comm)
