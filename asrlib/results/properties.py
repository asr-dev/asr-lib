from __future__ import annotations

from abc import abstractmethod
from dataclasses import dataclass, fields
from typing import Any, Protocol, Type, TypeVar

from asrlib.results import Result


class CalculatorWithResults(Protocol):
    @property
    @abstractmethod
    def results(self) -> dict[str, Any]:
        pass


CP = TypeVar('CP', bound='CalculatorProperties')


@dataclass
class CalculatorProperties(Result):
    energy: float

    @classmethod
    def from_calc(cls: Type[CP], calc: CalculatorWithResults) -> CP:
        if not (hasattr(calc, 'results') and calc.results):
            raise ValueError(
                "Input calculator doesn't include any results", calc
            )
        results = {
            key: calc.results[key] for key in set(calc.results) & cls.names()
        }
        return cls(**results)

    @classmethod
    def names(cls):
        """Get the set of properties implemented by the calculator."""
        return {field.name for field in fields(cls)}
