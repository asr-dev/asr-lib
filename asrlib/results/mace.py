from dataclasses import dataclass

import numpy as np

from asrlib.results.properties import CalculatorProperties


@dataclass
class MACEProperties(CalculatorProperties):
    free_energy: float
    forces: np.ndarray
    stress: np.ndarray
