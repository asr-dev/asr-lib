"""
A collection of Result dataclasses for Tasks. We keep them here to find
common patterns and structures to develop better code structure.
"""

from collections.abc import Mapping
from dataclasses import dataclass, fields
from typing import Any, Dict


class StandardResult(Mapping):
    """
    Simple results class that can be used instead of returning a dict in tasks.
    For complex tasks on the other hand, it is recommended to use custom
    dataclasses (See e.g., RelaxResults below).

    :param recursive: If True, kwargs that are dicts are transformed into
        StandardResults, else they are left as dicts.
    :param kwargs: Result kwargs
    """

    def __init__(self, recursive=True, **kwargs):
        self._kwargs = kwargs
        self._recursive = recursive

    def __getattr__(self, attr):
        # Double underscore is python internal, for example __setstate__
        # used by pickle. It will never be a key in _kwargs.
        if attr.startswith('__'):
            raise AttributeError
        try:
            return self[attr]
        except KeyError as err:
            raise AttributeError from err

    def __len__(self):
        return len(self._kwargs)

    def __iter__(self):
        return iter(self._kwargs)

    def __getitem__(self, key):
        obj = self._kwargs[key]
        if isinstance(obj, Mapping) and self._recursive:
            return StandardResult(**obj)
        return obj

    def __repr__(self):
        return f'{self.__class__.__name__}({self._kwargs})'

    def tb_encode(self):
        dct = {k: v for k, v in self.items()}
        dct['recursive'] = self._recursive
        return dct

    @classmethod
    def tb_decode(cls, dct: dict):
        return cls(**dct)


@dataclass
class Result:
    def to_dict(self) -> Dict[str, Any]:
        return {
            field.name: getattr(self, field.name) for field in fields(self)
        }

    def tb_encode(self) -> Dict[str, Any]:
        return self.to_dict()

    @classmethod
    def tb_decode(cls, dct: Dict[str, Any]):
        return cls(**dct)
