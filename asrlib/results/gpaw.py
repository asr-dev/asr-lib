from __future__ import annotations

from dataclasses import dataclass
from pathlib import Path
from typing import Tuple

import numpy as np
from ase import Atoms
from ase.io.ulm import open
from gpaw import restart
from gpaw.calculator import GPAW
from gpaw.mpi import SerialCommunicator
from gpaw.new.ase_interface import GPAW as GPAW_NEW

from asrlib.calculators.ase import DFTD3
from asrlib.results import Result
from asrlib.results.properties import CalculatorProperties
from asrlib.typing import TBCommunicator
from asrlib.typing.gpaw import BuiltInGPAWCalculator


@dataclass
class GPWFile(Result):
    path: Path

    def restart(
        self,
        *,
        gpaw_new: bool | None = None,
        comm: TBCommunicator | None = None,
        **kwargs,
    ) -> Tuple[Atoms, BuiltInGPAWCalculator]:
        """
        Restart a GPAW calculation from the file.

        :param comm: Communicator to be supplied to GPAW.
        :param gpaw_new: True, False or None. In case of None it will infer the
            correct GPAW version from file.
        :param kwargs: keyword args for gpaw calculator.
        :return: Atoms, Calculator
        """
        params = kwargs.copy()
        # Update params with communicator
        if comm is None:
            comm = SerialCommunicator()
        assert 'communicator' not in params
        params['communicator'] = comm
        # Retrieve gpaw version, if not given explicitly
        if gpaw_new is None:
            with open(self.path) as reader:
                gpaw_new = reader.version >= 4
        return restart(
            filename=self.path, Class=GPAW_NEW if gpaw_new else GPAW, **params
        )

    def d3_restart(
        self, *, comm: TBCommunicator | None = None, **kwargs
    ) -> Tuple[Atoms, DFTD3]:
        params = kwargs.copy()
        if comm is None:
            comm = SerialCommunicator()
        # Extract any d3-related parameters
        d3_params = {
            k: params.pop(k) for k in ['label', 'command'] if k in params
        }
        # Restart GPAW and create a precorrected DFTD3 object
        atoms, dft = self.restart(comm=comm, **params)
        atoms.calc = DFTD3(
            dft=dft, xc=dft.get_xc_functional(), comm=comm, **d3_params
        )
        atoms.calc.calculate_corrections()
        return atoms, atoms.calc


@dataclass
class GPAWProperties(CalculatorProperties):
    free_energy: float
    dipole: np.ndarray
    magmom: float
    magmoms: np.ndarray
    # In GPAW, forces and stress are conditional
    forces: np.ndarray | None = None
    stress: np.ndarray | None = None
