from typing import Union

from gpaw.calculator import GPAW as OldGPAW
from gpaw.new.ase_interface import ASECalculator as NewGPAW

# GPAW's own calculators
BuiltInGPAWCalculator = Union[OldGPAW, NewGPAW]
