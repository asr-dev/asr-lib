from abc import abstractmethod
from datetime import datetime
from typing import Any, Dict, Protocol, Type, TypeVar, Union

import numpy as np
import numpy.typing as npt

RealArray = npt.NDArray[np.float64]
Real1DArray = RealArray  # for now, we can only truly type hint the dtype
Real2DArray = RealArray

IntegerArray = npt.NDArray[np.int_]
Integer1DArray = IntegerArray
Integer2DArray = IntegerArray

BooleanArray = npt.NDArray[np.bool_]
Boolean1DArray = BooleanArray

# Taskblaster-specific (maybe move these to taskblaster itself? XXX)
TBCommunicator = Any  # tb does not clearly define, what a communicator is...
SS = TypeVar('SS', bound='SelfSerializable')


class SelfSerializable(Protocol):
    """Taskblaster protocol requirements for an object to be self-serializable.

    If an object implements `tb_encode` and `tb_decode` methods, taskblaster
    will use those to serialize the object via the `ExtensibleCodec`.
    """

    @abstractmethod
    def tb_encode(self) -> Dict[str, Any]:
        raise NotImplementedError

    @classmethod
    @abstractmethod
    def tb_decode(cls: Type[SS], dct: Dict[str, Any]) -> SS:
        raise NotImplementedError


# ASE-specific (maybe move these to ASE itself? XXX)
CellArray = Real2DArray  # cell_cv (3 x 3 matrix)


class ASESelfSerializable(Protocol):
    """ASE protocol requirements for an object to be self-serializable.

    If an object implements a `todict`, ASE's `ase.io.jsonio.default` json
    encoder will call that to encode the object.
    """

    @abstractmethod
    def todict(self) -> Dict:
        raise NotImplementedError


# In addition to objects with a `todict` method, the ASE encoder also supports
# encoding of simple types
ASEEncodable = Union[
    ASESelfSerializable,
    np.ndarray,
    np.bool_,
    datetime,
    complex,
]

# Inputs and outputs to taskblaster tasks have to be serializable. In asrlib we
# use the ASE encoder wrapped in taskblaster's `ExtensibleCodec`. Thus, to be a
# valid task variable, an object as to satisfy
TBTaskVariable = Union[SelfSerializable, ASEEncodable]
