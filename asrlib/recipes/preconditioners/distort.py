"""If we want to use the `break_symmetry` functionality for anything serious,
we should refactor this module as an AtomsPreconditioner. XXX"""

from functools import partial
from typing import Callable, Union

import numpy as np
from ase import Atoms
from ase.spacegroup.symmetrize import check_symmetry

from asrlib.typing import CellArray

CellDistorter = Callable[[Atoms, np.random.Generator], CellArray]
CellDistorterInput = Union[CellDistorter, float, None]


def distorter_from_inputs(
    distorter: CellDistorterInput, rstdev: float
) -> CellDistorter:
    """Regularize cell distorter input"""
    if not callable(distorter):
        # Fall back to default simple_cell_distortion(). The input now
        # specifies the scale parameter
        if distorter is None:  # Fall back to rstdev
            scale = rstdev
        else:
            scale = distorter
        distorter = partial(simple_cell_distortion, scale=scale)
    return distorter


def simple_cell_distortion(
    atoms: Atoms,
    rng: np.random.Generator,
    *,
    scale: float,
) -> CellArray:
    """Generate a simple distortion of the periodic cell vectors.

    The standard deviation of the distortion is calculated based on the
    "atomic scale" `scale` input in Å.
    """
    pbc_c = atoms.get_pbc()
    Np = sum(pbc_c)  # Number of periodic directions
    cell_std = scale * len(atoms) ** (1 / Np)
    distortion_cv = rng.normal(scale=cell_std, size=(3, 3))
    # Distort only periodic cell vectors
    distortion_cv = pbc_c[:, np.newaxis] * distortion_cv
    # Make sure distortions are orthogonal to the nonperiodic cell directions
    for c in np.where(pbc_c)[0]:
        for cell_v in atoms.get_cell()[~pbc_c]:
            ncell_v = cell_v / np.linalg.norm(cell_v)
            distortion_cv[c] -= distortion_cv[c] @ ncell_v * ncell_v
    return distortion_cv


def break_symmetry(
    atoms: Atoms,
    rstdev: float = 0.01,
    distorter: CellDistorterInput = None,
    rng: Union[int, np.random.Generator] = 42,
) -> Atoms:
    """Generate a new atoms instance with all symmetries broken.

    Distorts the cell and displaces the atoms inside it.

    :param atoms: Input crystal structure.
    :param rstdev: Standard deviation of the internal atom displacement in Å.
    :param distorter: Callable producing random cell distortions.
        Alternatively, you can provide the atomic scale of the distortion in Å
        (defaults to rstdev), which will then be passed to the build-in
        distorter.
    :param rng: Seed to a random number generator.
    :return: Symmetrized Atoms
    """
    distorter = distorter_from_inputs(distorter, rstdev)
    rng = np.random.default_rng(rng)
    # Generate atoms instance with distorted cell
    distortion_cv = distorter(atoms, rng)
    atoms = distort_cell(atoms, distortion_cv)
    # Rattle the atoms
    atoms.rattle(rstdev, rng=rng)
    # Make sure that all possible symmetries have been broken
    assert check_symmetry(atoms).number == minimal_spacegroup(atoms)
    return atoms


def distort_cell(atoms: Atoms, distortion_cv: CellArray) -> Atoms:
    """Generate a new atoms instance with a distorted cell."""
    atoms = atoms.copy()
    cell_cv = atoms.get_cell().array
    cell_cv += distortion_cv
    atoms.set_cell(cell_cv, scale_atoms=True)
    return atoms


def minimal_spacegroup(atoms: Atoms) -> int:
    """Get the minimal space group of an atomic structure.

    If there is only a single atom or two identical atoms in the unit cell, the
    structure will always have inversion symmetry. In this case the minimal
    space group is 2 (P-1), otherwise it is 1 (P1).
    """
    if (
        len(atoms) == 1
        or len(atoms) == 2
        and len(set(atoms.get_chemical_symbols())) == 1
    ):
        return 2
    return 1
