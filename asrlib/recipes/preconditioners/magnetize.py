from __future__ import annotations

from dataclasses import dataclass
from typing import Union

import numpy as np
from ase import Atoms

from asrlib.parameters import Parameter
from asrlib.typing import Real1DArray

MagneticMomentsSpecification = Union[
    float, dict[str, float], list[float], Real1DArray, None
]


@dataclass
class Magnetizer(Parameter):
    spec: MagneticMomentsSpecification

    @staticmethod
    def build(spec: MagneticMomentsSpecification = 1.0) -> Magnetizer:
        """Construct a Magnetizer which sets magmoms according to specification

        :param spec: Magnetic moment specification. Give either as a float (if
        you want to assign the same magnetic moment to all atoms), as a dict
        (if you want to assign magnetic moments by chemical specie) or as a
        list/np.ndarray of floats (if you want to specify each atomic moment
        individually).
        """
        return Magnetizer(spec=spec)

    @staticmethod
    def spec_to_magmoms(
        atoms: Atoms,
        spec: MagneticMomentsSpecification,
    ) -> Real1DArray | None:
        if spec is None:
            return None
        elif isinstance(spec, float) or isinstance(spec, dict):
            magmoms = np.asarray(get_magmoms(atoms, spec))
        else:
            magmoms = np.asarray(spec)
        assert len(magmoms) == len(atoms)
        return magmoms

    def __call__(self, atoms: Atoms) -> Atoms:
        atoms = atoms.copy()
        magmoms = self.spec_to_magmoms(atoms, self.spec)
        atoms.set_initial_magnetic_moments(magmoms)
        return atoms


def get_magmoms(atoms: Atoms, spec: float | dict[str, float]) -> list[float]:
    """Generate magmoms from a float or a chemical species dict."""
    if isinstance(spec, float):
        return [spec] * len(atoms)
    elif isinstance(spec, dict):
        species = atoms.get_chemical_symbols()
        assert len(np.unique(species)) == len(spec)
        return [spec[specie] for specie in species]
    raise ValueError(f'Expected `spec` of type float or dict, got {spec}')
