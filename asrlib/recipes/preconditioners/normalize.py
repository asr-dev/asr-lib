from dataclasses import dataclass

import numpy as np
import spglib
from ase import Atoms
from ase.build import niggli_reduce
from ase.spacegroup.symmetrize import get_symmetrized_atoms
from ase.utils import atoms_to_spglib_cell

from asrlib.parameters import Parameter
from asrlib.spglib import spglib_cell_to_atoms


@dataclass
class StructureNormalizer(Parameter):
    symprec: float

    @staticmethod
    def build(symprec: float = 1e-5):
        return StructureNormalizer(symprec=symprec)

    def __call__(self, atoms: Atoms) -> Atoms:
        atoms = reduce_to_primitive(atoms, symprec=self.symprec)
        return niggli_normalize(atoms)


def niggli_normalize(atoms: Atoms) -> Atoms:
    """
    Convert atoms to a standard representation using the niggli reduction from
    ase.build.niggli_reduce.

    :param atoms: Atoms object.
    :return: Niggli reduced Atoms.
    """
    atoms = atoms.copy()
    niggli_reduce(atoms)
    return atoms


def reduce_to_primitive(atoms: Atoms, symprec: float = 1e-5) -> Atoms:
    """
    Find the primitive lattice vectors for a given cell. Removes atom
    properties.

    :param atoms: Atoms object.
    :param symprec: Symmetry precision.
    :return: Symmetry reduced Atoms instance in the primitive cell.
    """
    pbc_c = atoms.get_pbc()
    if all(pbc_c):
        # Use spglib to produce the primitive cell.
        return spglib_cell_to_atoms(
            spglib.find_primitive(
                atoms_to_spglib_cell(atoms),
                symprec=symprec,
            ),
            pbc=True,
        )
    # Since spglib.find_primitive shuffles and rotates the cell axes, we cannot
    # use it directly when there are nonperiodic cell vectors. Instead, we
    # first symmetrize the cell using ASE (which doesn't rotate and shuffle the
    # cell), and then apply the nonstandardized (in the spglib sense) primitive
    # cell mapping that spglib provides for the symmetrized structure
    atoms, dataset = get_symmetrized_atoms(atoms, symprec=symprec)
    # Mapping from symmetrized cell (A) to primitive cell (a)
    a_A = dataset.mapping_to_primitive
    n_primitive_atoms = len(np.unique(a_A))
    if n_primitive_atoms < len(atoms):
        primitive_atoms = []
        for a in range(n_primitive_atoms):
            for A in range(len(atoms)):
                if a_A[A] == a:
                    primitive_atoms.append(atoms[A])
                    break
        assert len(primitive_atoms) == n_primitive_atoms
        return Atoms(
            primitive_atoms,
            cell=dataset.primitive_lattice,
            pbc=pbc_c,
        )
    return atoms
