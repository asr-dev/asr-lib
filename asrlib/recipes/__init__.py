from __future__ import annotations

from abc import ABC, abstractmethod
from dataclasses import dataclass
from typing import Generic, List, Protocol, TypeVar

from ase import Atoms

from asrlib.parameters import Parameter
from asrlib.recipes.typing import CalculatorFactory
from asrlib.typing import SelfSerializable, TBCommunicator, TBTaskVariable

# A standard ase recipe will have some decodable output from its calculate task
RawOutput = TBTaskVariable
RO = TypeVar('RO', bound=RawOutput, covariant=True)


class AtomsPreconditioner(SelfSerializable, Protocol):
    """Encodable object with an Atoms -> Atoms mapping."""

    @abstractmethod
    def __call__(self, atoms: Atoms) -> Atoms:
        raise NotImplementedError


@dataclass
class StandardASERecipe(Generic[RO], Parameter, ABC):
    calc_factory: CalculatorFactory
    preconditioner: AtomsPreconditioner | List[AtomsPreconditioner] | None

    @property
    def preconditioners(self) -> List[AtomsPreconditioner]:
        if isinstance(self.preconditioner, list):
            return self.preconditioner
        elif self.preconditioner is None:
            return []
        return [self.preconditioner]

    def new_with(self, append: dict | None = None, **kwargs):
        """Create a new instance of the recipe, updating a number of parameters

        Parameters given as keyword arguments are overwritten, parameters given
        as key value pairs in the `append` dictionary are appended to the
        existing value of that given attribute.

        Take a generic recipe example,

        @dataclass
        class SomeRecipe(StandardASERecipe):
            a: int  # not appendable
            b: List[int]  # appendable

        then,

        >>> recipe = SomeRecipe(..., a=0, b=[1,2])
        >>> new_recipe = recipe.new_with(a=1, b=[2,3])
        >>> new_recipe is recipe
        False
        >>> new_recipe.a
        1
        >>> new_recipe.b
        [2, 3]
        >>> new_recipe.calc_factory is recipe.calc_factory
        True

        Since the parameter a is an integer, it cannot be appended to, but
        since the b parameter is a list,

        >>> new_recipe = recipe.new_with(a=-1, append=dict(b=3))
        >>> new_recipe.a
        -1
        >>> new_recipe.b
        [1, 2, 3]
        >>> recipe.b
        [1, 2]

        In this sense, the `preconditioner` parameter is always considered
        appendable, even if `recipe.preconditioner` is `None` or a single
        preconditioner. Giving a new one as

        >>> new_recipe = recipe.new_with(append(preconditioner=my_precond))

        will set up a list of the preexisting preconditioners (which may be
        empty if `recipe.preconditioners` is `None`) and append the new
        `my_precond` to the list.
        """
        if append:
            kwargs = kwargs.copy()
            for key, value in append.items():
                assert key not in kwargs, (
                    'you cannot both append to an attribute and update it'
                )
                if key == 'preconditioner':
                    values = self.preconditioners
                else:
                    values = getattr(self, key)
                values = values.copy()
                values.append(value)
                kwargs[key] = values
        return super().new_with(**kwargs)

    def precondition(self, atoms: Atoms) -> Atoms:
        for preconditioner in self.preconditioners:
            atoms = preconditioner(atoms)
        return atoms

    def calculate(self, atoms: Atoms, comm: TBCommunicator) -> RO:
        atoms = self.precondition(atoms)
        atoms.calc = self.calc_factory(atoms, comm)
        return self._calculate(atoms=atoms, comm=comm)

    @abstractmethod
    def _calculate(self, atoms: Atoms, comm: TBCommunicator) -> RO:
        raise NotImplementedError
