from __future__ import annotations

from abc import ABC, abstractmethod
from dataclasses import dataclass
from pathlib import Path
from typing import Generic, Type, TypeVar

from ase import Atoms
from ase.io.trajectory import TrajectoryReader

from asrlib.results import Result
from asrlib.results.gpaw import GPAWProperties
from asrlib.results.mace import MACEProperties
from asrlib.results.properties import CalculatorProperties
from asrlib.utils import forward_properties

ARR = TypeVar('ARR', bound='ASERelaxResults')
CP = TypeVar('CP', bound='CalculatorProperties')


@dataclass
class ASERelaxResults(Generic[CP], ABC, Result):
    atoms: Atoms
    properties: CP

    @classmethod
    def from_trajectory(cls: Type[ARR], traj: Path, im: int = -1) -> ARR:
        atoms = cls.read_image(traj, im=im)
        properties = cls.extract_properties(atoms)
        return cls(atoms, properties)

    @staticmethod
    def get_results_cls(properties_cls: Type[CP]) -> Type[ASERelaxResults]:
        if properties_cls == GPAWProperties:
            return GPAWRelaxResults
        elif properties_cls == MACEProperties:
            return MACERelaxResults
        raise ValueError('Unrecognized properties class', properties_cls)

    @staticmethod
    @abstractmethod
    def extract_properties(atoms: Atoms) -> CP:
        """Define how to extract the relevant calculator properties."""

    @classmethod
    def read_image(cls, traj: Path, im: int = -1) -> Atoms:
        return cls.reader(traj)[im]

    @staticmethod
    def reader(traj: Path) -> TrajectoryReader:
        return TrajectoryReader(traj)

    @property
    def energy(self) -> float:
        # NB: this method is not strictly necessary when decorating the
        # results with forward_properties as below. Instead, it serves as an
        # illustration of how the property forwarding works.
        return self.properties.energy

    @property
    def epa(self) -> float:
        return self.energy / len(self.atoms)


@forward_properties('properties', GPAWProperties.names())
class GPAWRelaxResults(ASERelaxResults[GPAWProperties]):
    @staticmethod
    def extract_properties(atoms: Atoms) -> GPAWProperties:
        return GPAWProperties.from_calc(atoms.calc)


@forward_properties('properties', MACEProperties.names())
class MACERelaxResults(ASERelaxResults[MACEProperties]):
    @staticmethod
    def extract_properties(atoms: Atoms) -> MACEProperties:
        return MACEProperties.from_calc(atoms.calc)
