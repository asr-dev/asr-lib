from __future__ import annotations

from abc import abstractmethod
from dataclasses import dataclass
from pathlib import Path
from typing import TYPE_CHECKING, Type, Union

from ase import Atoms
from ase.optimize.bfgs import BFGS
from ase.optimize.cellawarebfgs import CellAwareBFGS
from ase.optimize.optimize import DEFAULT_MAX_STEPS

from asrlib.recipes import AtomsPreconditioner, StandardASERecipe
from asrlib.recipes.relax.ase.cell_filter import (
    FrechetCellFilterFactory,
    StrainMask,
    UnitCellFilterFactory,
)
from asrlib.recipes.relax.ase.results import ASERelaxResults
from asrlib.recipes.relax.typing import RelaxResults
from asrlib.results import Result
from asrlib.typing import TBCommunicator

if TYPE_CHECKING:
    from asrlib.recipes.typing.gpaw import GPAWCalculatorFactory
    from asrlib.recipes.typing.mace import MACECalculatorFactory

# It is a little too cumbersome to specify the exact requirements for a
# calculator, which is fully compatible with ase.optimize.
# Therefore, we just list approved (tested) calculators.
ASERelaxCalculatorFactory = Union[
    'GPAWCalculatorFactory',
    'MACECalculatorFactory',
]


@dataclass
class Optimization(Result):
    traj: Path
    converged: bool


@dataclass
class ASERelaxRecipe(StandardASERecipe[Optimization]):
    """Relax recipe base class for ASE relaxations.

    In order to build an ASE relax recipe, you can choose between a selection
    of builder methods:
      * ASERelaxRecipe.bfgs_relax_positions()
      * ASERelaxRecipe.bfgs_relax_cell_and_positions()
    """

    calc_factory: ASERelaxCalculatorFactory
    d3: bool
    trajectory: str
    logfile: str | None
    append_trajectory: bool
    fmax: float
    steps: int

    @staticmethod
    def bfgs_relax_positions(
        calc_factory: ASERelaxCalculatorFactory,
        preconditioner: AtomsPreconditioner | None = None,
        d3: bool = False,
        identifier: str = 'relax',
        append_trajectory: bool = False,
        fmax: float = 0.05,
        steps: int = DEFAULT_MAX_STEPS,
    ) -> BFGSRelaxRecipe:
        """Build a relax recipe, which relaxes atom positions using BFGS."""
        return BFGSRelaxRecipe(
            calc_factory=calc_factory,
            preconditioner=preconditioner,
            d3=d3,
            trajectory=f'{identifier}.traj',
            logfile=f'{identifier}.log',
            append_trajectory=append_trajectory,
            fmax=fmax,
            steps=steps,
        )

    @staticmethod
    def bfgs_relax_cell_and_positions(
        calc_factory: ASERelaxCalculatorFactory,
        preconditioner: AtomsPreconditioner | None = None,
        d3: bool = False,
        identifier: str = 'relax',
        append_trajectory: bool = False,
        fmax: float = 0.05,
        steps: int = DEFAULT_MAX_STEPS,
        cell_filter: StrainMask | None = None,
        smax: float = 0.005,
    ) -> CellAwareBFGSRelaxRecipe:
        """Build a relax recipe, which relaxes both the cell and the atom
        positions using the BFGS algorithm."""
        if cell_filter is None:
            # Use the default FrechetCellFilterFactory
            cell_filter_factory = FrechetCellFilterFactory()
        else:
            # The input cell_filter is a StrainMask, which we use in
            # combination with the FrechetCellFilter
            cell_filter_factory = FrechetCellFilterFactory(mask=cell_filter)
        return CellAwareBFGSRelaxRecipe(
            calc_factory=calc_factory,
            preconditioner=preconditioner,
            d3=d3,
            trajectory=f'{identifier}.traj',
            logfile=f'{identifier}.log',
            append_trajectory=append_trajectory,
            fmax=fmax,
            steps=steps,
            cell_filter_factory=cell_filter_factory,
            smax=smax,
        )

    @property
    def optimizer_params(self) -> dict:
        return dict(
            trajectory=self.trajectory,
            logfile=self.logfile,
            append_trajectory=self.append_trajectory,
        )

    @property
    def run_params(self) -> dict:
        return dict(fmax=self.fmax, steps=self.steps)

    @property
    def trajectory_path(self) -> Path:
        return Path(self.trajectory)

    def _calculate(self, atoms: Atoms, comm: TBCommunicator) -> Optimization:
        if self.d3:
            assert hasattr(self.calc_factory, 'd3_wrapper')
            atoms.calc = self.calc_factory.d3_wrapper(atoms.calc, comm=comm)
        converged = self.optimize(atoms=atoms, comm=comm)
        return Optimization(self.trajectory_path, converged)

    @abstractmethod
    def optimize(self, atoms: Atoms, comm: TBCommunicator) -> bool:
        raise NotImplementedError

    def results(self, raw_output: Optimization) -> RelaxResults:
        cls: Type[ASERelaxResults] = ASERelaxResults.get_results_cls(
            properties_cls=self.calc_factory.properties_cls
        )
        return cls.from_trajectory(raw_output.traj)


@dataclass
class BFGSRelaxRecipe(ASERelaxRecipe):
    def optimize(self, atoms: Atoms, comm: TBCommunicator) -> bool:
        optimizer = BFGS(atoms, comm=comm, **self.optimizer_params)
        return optimizer.run(**self.run_params)


@dataclass
class CellAwareBFGSRelaxRecipe(ASERelaxRecipe):
    cell_filter_factory: UnitCellFilterFactory
    smax: float

    def optimize(self, atoms: Atoms, comm: TBCommunicator) -> bool:
        optimizer = CellAwareBFGS(
            self.cell_filter_factory(atoms), comm=comm, **self.optimizer_params
        )
        return optimizer.run(smax=self.smax, **self.run_params)
