from __future__ import annotations

from abc import abstractmethod
from dataclasses import dataclass
from typing import Callable, Protocol, TypeAlias

import numpy as np
from ase import Atoms
from ase.filters import FrechetCellFilter, UnitCellFilter

from asrlib.parameters import Parameter
from asrlib.typing import Boolean1DArray, SelfSerializable

# ----- Typing and protocols ----- #

StrainMask: TypeAlias = Boolean1DArray  # mask in (xx, yy, zz, yz, xz, xy)
StrainMaskFactory = Callable[[Atoms], StrainMask]


class UnitCellFilterFactory(SelfSerializable, Protocol):
    @abstractmethod
    def __call__(self, atoms: Atoms) -> UnitCellFilter:
        raise NotImplementedError


# ---------- #


def pbc_smask_factory(atoms: Atoms) -> StrainMask:
    """Strain mask for relaxing only periodic cell vectors."""
    smask = np.ones((6,), dtype=bool)
    for component, periodic in zip('xyz', atoms.get_pbc()):
        if periodic:
            continue
        # Don't relax nonperiodic components
        for s, strain_component in enumerate(
            ['xx', 'yy', 'zz', 'yz', 'xz', 'xy']
        ):
            if component in strain_component:
                smask[s] = 0
    return smask


@dataclass
class FrechetCellFilterFactory(Parameter):
    mask: StrainMask | StrainMaskFactory = pbc_smask_factory
    exp_cell_factor: float = 1.0

    def __call__(self, atoms: Atoms):
        return FrechetCellFilter(
            atoms,
            mask=self.get_mask(atoms),
            exp_cell_factor=self.exp_cell_factor,
        )

    def get_mask(self, atoms: Atoms):
        if callable(self.mask):
            return self.mask(atoms)
        return self.mask
