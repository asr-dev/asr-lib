from typing import Protocol, TypeVar

from ase import Atoms

from asrlib.results.properties import CalculatorProperties
from asrlib.typing import SelfSerializable

CP = TypeVar('CP', bound=CalculatorProperties, covariant=True)


class RelaxResults(SelfSerializable, Protocol[CP]):
    """Base results class for the relax workflow."""

    @property
    def atoms(self) -> Atoms:
        raise NotImplementedError

    @property
    def properties(self) -> CP:
        raise NotImplementedError

    # Should we automatically forward the calculator properties and have a
    # derived epa property? XXX
