"""
Relax recipe protocols.
"""

from abc import abstractmethod
from typing import Protocol, TypeVar

from ase import Atoms

from asrlib.recipes.relax.ase import ASERelaxRecipe
from asrlib.recipes.relax.typing import RelaxResults
from asrlib.typing import SelfSerializable, TBCommunicator, TBTaskVariable

# User interfaces to actual implementations of the RelaxRecipe protocol:
__all__ = ['ASERelaxRecipe']

# A relaxation may have variable raw output formats
RawRelaxOutput = TBTaskVariable
RRO = TypeVar('RRO', bound=RawRelaxOutput)


class RelaxRecipe(SelfSerializable, Protocol[RRO]):
    """Recipe protocol for the relax workflow.

    Overall, the purpose of the workflow is to produce RelaxResults with the
    relaxed atoms and properties, with some RawRelaxOutput stored as an
    intermediate representation.
    """

    @abstractmethod
    def calculate(self, atoms: Atoms, comm: TBCommunicator) -> RRO:
        raise NotImplementedError

    @abstractmethod
    def results(self, raw_output: RRO) -> RelaxResults:
        raise NotImplementedError
