from mace.calculators import MACECalculator

from asrlib.recipes.typing import CalculatorFactory
from asrlib.results.mace import MACEProperties

# MACE calculator factory protocol
MACECalculatorFactory = CalculatorFactory[
    MACECalculator,
    MACEProperties,
]
