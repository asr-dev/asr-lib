from asrlib.recipes.typing import CalculatorFactory
from asrlib.results.gpaw import GPAWProperties
from asrlib.typing.gpaw import BuiltInGPAWCalculator

# GPAW calculator factory protocol
GPAWCalculatorFactory = CalculatorFactory[
    BuiltInGPAWCalculator,
    GPAWProperties,
]
