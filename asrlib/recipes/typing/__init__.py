from abc import abstractmethod
from typing import Any, Protocol, Type, TypeVar

from ase import Atoms

from asrlib.results.properties import CalculatorProperties
from asrlib.typing import SelfSerializable, TBCommunicator

# There are no strict requirements to be an ASE calculator
ASECalculator = Any
AC = TypeVar('AC', bound=ASECalculator, covariant=True)
CP = TypeVar('CP', bound='CalculatorProperties')


class CalculatorFactory(SelfSerializable, Protocol[AC, CP]):
    """Encodable object which produces ASE compliant calculators."""

    properties_cls: Type[CP]

    @abstractmethod
    def __call__(self, atoms: Atoms, comm: TBCommunicator) -> AC:
        raise NotImplementedError

    @abstractmethod
    def new_with(self, **kwargs):
        raise NotImplementedError
