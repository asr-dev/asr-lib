from dataclasses import dataclass
from typing import Optional, Protocol

import numpy as np

from asrlib.parameters import Parameter
from asrlib.recipes import AtomsPreconditioner
from asrlib.recipes.preconditioners import Magnetizer
from asrlib.results import Result


@dataclass
class MagneticStateClassification(Result):
    spinpol: bool
    magnetizer: AtomsPreconditioner
    magmom: Optional[float] = None
    magmoms: Optional[np.ndarray] = None
    name: Optional[str] = None


class MagneticProperties(Protocol):
    magmom: float
    magmoms: np.ndarray


@dataclass
class MagneticStateClassifier(Parameter):
    magmoms_tol: float

    def __post_init__(self):
        assert self.magmoms_tol > 0.0

    def is_magnetic(self, properties: MagneticProperties) -> bool:
        return not np.allclose(properties.magmoms, 0.0, atol=self.magmoms_tol)

    def classify(
        self,
        properties: MagneticProperties,
        magnetizer: Optional[AtomsPreconditioner] = None,
    ) -> MagneticStateClassification:
        """Classify magnetic state of a set of calculated magnetic moments."""
        if not self.is_magnetic(properties):
            return MagneticStateClassification(
                spinpol=False,
                magnetizer=Magnetizer(None),
                name='nm',
            )
        if magnetizer is None:
            magnetizer = Magnetizer(properties.magmoms)
        return MagneticStateClassification(
            spinpol=True,
            magnetizer=magnetizer,
            magmom=properties.magmom,
            magmoms=properties.magmoms,
            name=self.fm_or_afm(properties),
        )

    def fm_or_afm(self, properties: MagneticProperties):
        assert self.is_magnetic(properties)
        if properties.magmom < self.magmoms_tol:
            return 'afm'
        return 'fm'


class StateProperties(Protocol):
    energy: float
    magmom: float
    magmoms: np.ndarray


@dataclass
class MagneticVSNonmagneticStateClassifier(Parameter):
    _classifier: MagneticStateClassifier

    @staticmethod
    def build(magmoms_tol: float = 0.1):
        return MagneticVSNonmagneticStateClassifier(
            _classifier=MagneticStateClassifier(magmoms_tol),
        )

    def is_magnetic(self, properties: StateProperties) -> bool:
        return self._classifier.is_magnetic(properties)

    def classify(
        self,
        mag_properties: StateProperties,
        nonmag_properties: Optional[StateProperties] = None,
        magnetizer: Optional[AtomsPreconditioner] = None,
    ) -> MagneticStateClassification:
        """Classify the magnetic state of a material."""
        if self.is_magnetic(mag_properties):
            # If a magnetic state calculation has succeeded in producing an
            # actual magnetic state, we compare its energy to a corresponding
            # nonmagnetic state calculation and classify the material based on
            # the state with the lowest energy:
            assert nonmag_properties is not None
            # nonmag_properties generally comes from a spin-polarized
            # calculation but has close-to-zero magmoms.
            # Let's make sure we agree about it not being magnetic:
            assert not self.is_magnetic(nonmag_properties)
            if nonmag_properties.energy < mag_properties.energy:
                return self._classifier.classify(nonmag_properties)
        return self._classifier.classify(mag_properties, magnetizer)
