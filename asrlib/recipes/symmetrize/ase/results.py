from __future__ import annotations

from dataclasses import dataclass
from functools import cached_property
from typing import Generic, TypeVar

import numpy as np

from asrlib.recipes.symmetrize.ase.interface import SymmetrizationData
from asrlib.results import Result

SD = TypeVar('SD', bound=SymmetrizationData)


@dataclass
class SymmetryAnalysis(Generic[SD], Result):
    datasets: list[SD]
    symprec_max: float  # symprec upper bound

    def __post_init__(self):
        assert np.all(self.symprecs[:-1] - self.symprecs[1:] < 0), (
            'The symprec_p should be in ascending order'
        )
        self.symprec_min = min(self.symprecs)  # symprec lower bound

    @cached_property
    def symprecs(self):
        return np.array([dataset.symprec for dataset in self.datasets])

    @classmethod
    def from_asorted_data(cls, datasets: list[SD]):
        # Filter data in ascending order of symmetry precision. Retain only
        # values of symprec which either lead to a new space group or which can
        # be used for symmetrization, whereas former symprecs (with the same
        # space group) could not.
        filtered_datasets: list[SD] = []
        for dataset in sorted(datasets, key=lambda dataset: dataset.symprec):
            if not filtered_datasets or (
                dataset.space_group != filtered_datasets[-1].space_group
                or (
                    dataset.is_symmetrizable
                    and not filtered_datasets[-1].is_symmetrizable
                )
            ):
                filtered_datasets.append(dataset)
        max_symprec = dataset.symprec  # last dataset in sorted iterator
        return cls(filtered_datasets, max_symprec)

    def in_range(self, symprec: float):
        return self.symprec_min <= symprec <= self.symprec_max

    def look_up(self, symprec: float) -> SD:
        """Find SymmetrizationData corresponding to input symprec."""
        if not self.in_range(symprec):
            raise ValueError(
                'Got symmetry precision outside the range of the symmetry '
                f'analysis symprec∈[{self.symprec_min},{self.symprec_max}]'
            )
        # Find the nearest symmetry precision from below
        return self.datasets[sum(self.symprecs <= symprec) - 1]

    def space_group(self, symprec: float):
        """Look up the space group corresponding to a given symprec."""
        return self.look_up(symprec).space_group

    def get_space_groups(self, symprecs: np.ndarray):
        return np.array([self.space_group(symprec) for symprec in symprecs])

    def symmetrized_atoms(self, symprec: float):
        if not self.is_symmetrizable(symprec):
            raise ValueError(f'Sorry, symprec={symprec} is not symmetrizable')
        return self.look_up(symprec).atoms

    def is_symmetrizable(self, symprec: float):
        """Look up if atoms are symmetrizable according to a given symprec."""
        return self.look_up(symprec).is_symmetrizable

    def get_symmetrizability(self, symprecs: np.ndarray):
        return np.array(
            [self.is_symmetrizable(symprec) for symprec in symprecs],
            dtype=bool,
        )

    def plot(self, ax, rcParams=None):
        """Plot space group and symmetrizability depending on the symprec."""
        from matplotlib.patches import Patch

        if rcParams is None:
            from matplotlib import rcParams
        # Divide x-axis in two regions
        ax.set_xlim(0.0, 2.0)
        ax.axvline(1.0, color='k', linestyle='--', linewidth=0.8)
        ax.set_xticks(
            (0.5, 1.5),
            labels=('Spacegroup', 'Symmetrizability'),
            fontsize=rcParams['axes.labelsize'],
        )
        ax.tick_params(axis='x', length=0)
        # Use logarithmic axis for the symmetry precision
        ax.set_yscale('log')
        ax.set_ylim((self.symprec_min, self.symprec_max))
        ax.set_ylabel('Symmetry precision [Å]')
        # Plot data as colored regions
        colors = rcParams['axes.prop_cycle'].by_key()['color']
        spgrp_colors = {}
        for d, dataset in enumerate(self.datasets):
            symprec_min = dataset.symprec
            if d < len(self.datasets) - 1:
                symprec_max = self.datasets[d + 1].symprec
            else:
                symprec_max = self.symprec_max
            spgrp = dataset.space_group
            if spgrp not in spgrp_colors:
                spgrp_colors[spgrp] = colors[len(spgrp_colors)]
            ax.fill_between(
                (0.0, 1.0),  # x
                (symprec_min, symprec_min),  # lower bound
                (symprec_max, symprec_max),  # upper bound
                alpha=0.666,
                color=spgrp_colors[spgrp],
            )
            if not dataset.is_symmetrizable:
                continue
            ax.fill_between(
                (1.0, 2.0),  # x
                (symprec_min, symprec_min),  # lower bound
                (symprec_max, symprec_max),  # upper bound
                hatch='/',
                alpha=0.666,
                color='0.42',
            )
        # Set up legend with space groups
        patches = [
            Patch(color=color, alpha=0.666, label=spgrp)
            for spgrp, color in spgrp_colors.items()
        ]
        ax.legend(handles=patches, loc='lower left')


def plot_symmetries(
    symmetry_analysis: SymmetryAnalysis,
    filename: str | None = None,
    show: bool = False,
):
    import matplotlib.pyplot as plt

    fig = plt.figure()
    ax = fig.gca()
    symmetry_analysis.plot(ax)
    if filename is not None:
        plt.savefig(filename)
    if show:
        plt.show()
