from __future__ import annotations

from abc import ABC, abstractmethod
from dataclasses import dataclass
from functools import cached_property
from typing import Generic, Type, TypeVar

import numpy as np
from ase import Atoms

from asrlib.parameters import Parameter
from asrlib.recipes.symmetrize.ase.interface import (
    SymmetrizationData,
    one_shot_symmetrization,
)
from asrlib.recipes.symmetrize.ase.results import SymmetryAnalysis
from asrlib.typing import Real1DArray

LSA = TypeVar('LSA', bound='GenericLogspaceSymmetryAnalyzer')
SD = TypeVar('SD', bound=SymmetrizationData)


@dataclass
class GenericLogspaceSymmetryAnalyzer(Generic[SD], Parameter, ABC):
    min_symprec: float
    max_symprec: float
    nsymprecs: int

    @classmethod
    def build(
        cls: Type[LSA],
        min_symprec: float = 1e-5,
        max_symprec: float = 2e-1,
        nsymprecs: int = 401,
    ) -> LSA:
        """Construct a LogspaceSymmetryAnalyzer from default values."""
        return cls(
            min_symprec=min_symprec,
            max_symprec=max_symprec,
            nsymprecs=nsymprecs,
        )

    def __post_init__(self):
        assert 0 < self.min_symprec < self.max_symprec

    def __call__(self, atoms: Atoms) -> SymmetryAnalysis[SD]:
        symmetrizations = self.generate_symmetrizations(atoms)
        return SymmetryAnalysis.from_asorted_data(symmetrizations)

    @abstractmethod
    def symmetrize(self, atoms: Atoms, symprec: float) -> SD:
        raise NotImplementedError

    def generate_symmetrizations(self, atoms: Atoms) -> list[SD]:
        return [
            self.symmetrize(atoms, symprec=symprec)
            for symprec in self.symprecs
        ]

    def generate_symprecs(self) -> Real1DArray:
        return np.logspace(
            np.log10(self.min_symprec),
            np.log10(self.max_symprec),
            self.nsymprecs,
        )

    @cached_property
    def _symprecs(self) -> Real1DArray:
        return self.generate_symprecs()

    @property
    def symprecs(self) -> Real1DArray:
        """Symmetry precision parameters to investigate (in Å)."""
        return self._symprecs


class LogspaceSymmetryAnalyzer(
    GenericLogspaceSymmetryAnalyzer[SymmetrizationData],
):
    def symmetrize(self, atoms: Atoms, symprec: float) -> SymmetrizationData:
        return one_shot_symmetrization(
            atoms,
            symprec=symprec,
            final_symprec=self.min_symprec,
        )
