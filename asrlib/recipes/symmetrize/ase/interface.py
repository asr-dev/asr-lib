from __future__ import annotations

from dataclasses import dataclass
from functools import cached_property
from typing import List, Union

import numpy as np
from ase import Atoms
from ase.spacegroup.symmetrize import (
    IntermediateDatasetError,
    check_symmetry,
    get_symmetrized_atoms,
)

from asrlib.results import Result
from asrlib.spglib import spglib_as_dict


def one_shot_symmetrization(
    atoms: Atoms, *, symprec: float, final_symprec: float
) -> SymmetrizationData:
    """Identify symmetries at tolerance `symprec` and try to symmetrize.

    The symmetrized crystal is checked to satisfy the identified symmetries
    according to the refined tolerance `final_symprec`.
    """
    try:
        symatoms, dataset = get_symmetrized_atoms(
            atoms, symprec=symprec, final_symprec=final_symprec
        )
    except IntermediateDatasetError:
        symatoms = None
        dataset = check_symmetry(atoms, symprec=symprec)

    dataset = spglib_as_dict(dataset)
    return SymmetrizationData(symprec, dataset, symatoms)


@dataclass
class SymmetrizationData(Result):
    """Data concerning a specific symmetrization."""

    symprec: float
    spglib_dataset: dict
    atoms: Union[Atoms, None]  # symmetrized atoms

    def __post_init__(self):
        assert isinstance(self.spglib_dataset, dict)

    @staticmethod
    def main_keys() -> List[str]:
        return [
            'is_symmetrizable',
            'space_group',
            'international_symbol',
            'point_group',
            'has_inversion_symmetry',
        ]

    @property
    def is_symmetrizable(self):
        return self.atoms is not None

    @property
    def space_group(self) -> int:
        return self.spglib_dataset['number']

    @property
    def international_symbol(self) -> str:
        return self.spglib_dataset['international']

    @property
    def point_group(self) -> str:
        return self.spglib_dataset['pointgroup']

    @cached_property
    def has_inversion_symmetry(self) -> bool:
        inversion = -np.identity(3, dtype=int)
        for rotation in self.spglib_dataset['rotations']:
            if np.all(rotation == inversion):
                return True
        return False
