from abc import ABC, abstractmethod
from dataclasses import dataclass
from typing import Generic, Protocol, TypeVar

import taskblaster as tb
from ase import Atoms

from asrlib.actions import symmetrization
from asrlib.parameters import Parameter
from asrlib.recipes.symmetrize.ase.analyzers import LogspaceSymmetryAnalyzer
from asrlib.recipes.symmetrize.ase.interface import SymmetrizationData
from asrlib.recipes.symmetrize.ase.results import SymmetryAnalysis
from asrlib.results import Result
from asrlib.typing import SelfSerializable
from asrlib.utils import forward_properties

__all__ = ['LogspaceSymmetryAnalyzer']

SD = TypeVar('SD', bound=SymmetrizationData)


@tb.actions(symmetrization=symmetrization)
@dataclass
class GenericASESymmetrizationResults(Generic[SD], Result):
    analysis: SymmetryAnalysis[SD]
    symprec: float

    def __post_init__(self):
        assert self.analysis.in_range(self.symprec), (
            '`self.symprec` is out of range of the scanned tolerances'
        )

    @property
    def data(self) -> SD:
        """Information about the symmetrization at `self.symprec`."""
        return self.analysis.look_up(self.symprec)

    @property
    def atoms(self) -> Atoms:
        """Symmetrized atoms to precision `self.symprec`."""
        assert self.data.atoms is not None, (
            'Symmetrization failed at `self.symprec`'
        )
        return self.data.atoms


@forward_properties('data', SymmetrizationData.main_keys())
class ASESymmetrizationResults(
    GenericASESymmetrizationResults[SymmetrizationData],
):
    """Symmetrization results based on SymmetrizationData."""


class SymmetryAnalyzer(Generic[SD], SelfSerializable, Protocol):
    @abstractmethod
    def __call__(self, atoms: Atoms) -> SymmetryAnalysis[SD]:
        raise NotImplementedError


@dataclass
class GenericASESymmetrizeRecipe(Generic[SD], Parameter, ABC):
    analyzer: SymmetryAnalyzer[SD]

    @abstractmethod
    def run(self, atoms: Atoms) -> GenericASESymmetrizationResults[SD]:
        raise NotImplementedError

    def analyze(self, atoms: Atoms) -> SymmetryAnalysis[SD]:
        return self.analyzer(atoms)


@dataclass
class ASESymmetrizeRecipe(GenericASESymmetrizeRecipe[SymmetrizationData]):
    symprec: float  # target symmetry precision

    def run(self, atoms: Atoms) -> ASESymmetrizationResults:
        return ASESymmetrizationResults(self.analyze(atoms), self.symprec)
