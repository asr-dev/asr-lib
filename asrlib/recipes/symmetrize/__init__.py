"""
Symmetrize recipe protocols.
"""

from abc import abstractmethod
from typing import Protocol

from ase import Atoms

from asrlib.recipes.symmetrize.ase import ASESymmetrizeRecipe
from asrlib.typing import SelfSerializable

# Currently available implementations of the SymmetrizeRecipe protocol:
__all__ = ['ASESymmetrizeRecipe']


class SymmetrizationResults(SelfSerializable, Protocol):
    """Results protocol for the symmetrize recipe."""

    @property
    @abstractmethod
    def atoms(self) -> Atoms:
        """Symmetrized atoms."""
        raise NotImplementedError

    @property
    @abstractmethod
    def space_group(self) -> int:
        """Space group of the symmetrized atoms."""
        raise NotImplementedError


class SymmetrizeRecipe(SelfSerializable, Protocol):
    """Recipe protocol for symmetrizations.

    Should take an input atoms object, identify its space group (possibly
    according to some tolerance) and produce a symmetrized version of the
    crystal, which satisfies the symmetries of that space group "exactly".
    """

    @abstractmethod
    def run(self, atoms: Atoms) -> SymmetrizationResults:
        raise NotImplementedError
