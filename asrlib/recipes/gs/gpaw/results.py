from __future__ import annotations

from dataclasses import dataclass
from functools import cached_property, partial
from typing import Any, Type, TypeVar

import numpy as np
from ase import Atoms
from ase.dft.dos import linear_tetrahedron_integration

from asrlib.results import Result
from asrlib.results.gpaw import GPAWProperties, GPWFile
from asrlib.typing import (
    Integer1DArray,
    Integer2DArray,
    Real1DArray,
    Real2DArray,
    TBCommunicator,
)
from asrlib.typing.gpaw import BuiltInGPAWCalculator


@dataclass
class GapInfo(Result):
    kvbm_s: Integer1DArray
    kcbm_s: Integer1DArray
    kdir_ss: Integer2DArray
    # We use strings rather than tuples as eigenvalue keys, since the asrlib
    # decoder cannot handle tuples yet
    veigenvalues: dict[str, float]
    ceigenvalues: dict[str, float]
    # We cannot currently use integers k-point keys since tb_decode will
    # convert them to strings.
    ibzkpts: dict[str, Real1DArray]
    ibz2bz: dict[str, Integer1DArray]
    bzkpts: dict[str, Real1DArray]

    def __post_init__(self):
        assert len(self.kcbm_s) == self.nspins
        assert self.kdir_ss.shape == (self.nspins, self.nspins)

    @classmethod
    def from_data(cls, ev_sk, ec_sk, ibzk_kc, bzk_Kc, bz2ibz_K) -> GapInfo:
        """Construct `GapInfo` dataclass from input band data arrays.

        Parameters
        ----------
        ev_sk: np.ndarray
            Valence band eigenvalues for each spin and k-point in the IBZ.
        ec_sk: np.ndarray
            Conduction band eigenvalues for each spin and k-point in the IBZ.
        ibzk_kc: np.ndarray
            IBZ k-points in relative coordinates.
        bzk_Kc: np.ndarray
            BZ k-points in relative coordinates.
        bz2ibz_K: np.ndarray
            IBZ k-point index for each BZ k-point.
        """
        assert ev_sk.shape == ec_sk.shape
        assert ev_sk.ndim == 2
        assert ev_sk.shape[1] == len(ibzk_kc)
        assert len(bzk_Kc) == len(bz2ibz_K)
        nspins = len(ev_sk)
        # Extract the valence band maximum and conduction band minimum for each
        # spin
        kvbm_s = np.argmax(ev_sk, axis=1)
        kcbm_s = np.argmin(ec_sk, axis=1)
        # Extract direct gap minimum for each component of the spin tensor
        kdir_ss = np.array(
            [[(ec_k - ev_k).argmin() for ec_k in ec_sk] for ev_k in ev_sk]
        )
        # Extract eigenvalues for each unique (s,k)-pair
        veigenvalues = {f'{s},{k}': ev_sk[s, k] for s, k in enumerate(kvbm_s)}
        ceigenvalues = {f'{s},{k}': ec_sk[s, k] for s, k in enumerate(kcbm_s)}
        for k in np.unique(kdir_ss):
            for s in range(nspins):
                veigenvalues[f'{s},{k}'] = ev_sk[s, k]
                ceigenvalues[f'{s},{k}'] = ec_sk[s, k]
        # Extract IBZ k-point and IBZ->BZ mapping for each unique k-point
        k_k = np.unique(list(kvbm_s) + list(kcbm_s) + list(kdir_ss.flatten()))
        ibzkpts = {str(k): ibzk_kc[k] for k in k_k}
        ibz2bz = {str(k): np.where(bz2ibz_K == k)[0] for k in k_k}
        bzkpts = {str(K): bzk_Kc[K] for _, K_K in ibz2bz.items() for K in K_K}
        return cls(
            kvbm_s,
            kcbm_s,
            kdir_ss,
            veigenvalues,
            ceigenvalues,
            ibzkpts,
            ibz2bz,
            bzkpts,
        )

    @property
    def nspins(self) -> int:
        return len(self.kvbm_s)

    @cached_property
    def evbm_s(self) -> Real1DArray:
        """Eigenvalue of the VBM of each spin channel."""
        return np.array(
            [self.veigenvalues[f'{s},{k}'] for s, k in enumerate(self.kvbm_s)]
        )

    @cached_property
    def ecbm_s(self) -> Real1DArray:
        """Eigenvalue of the CBM of each spin channel."""
        return np.array(
            [self.ceigenvalues[f'{s},{k}'] for s, k in enumerate(self.kcbm_s)]
        )

    @property
    def kvbm(self) -> int:
        """IBZ k-point index at the VBM."""
        return self.kvbm_s[np.argmax(self.evbm_s)]

    @property
    def kcbm(self) -> int:
        """IBZ k-point index at the CBM."""
        return self.kcbm_s[np.argmin(self.ecbm_s)]

    @property
    def ibzvbm_c(self) -> Real1DArray:
        """IBZ k-point in relative coordinates of the VBM."""
        return self.ibzkpts[str(self.kvbm)]

    @property
    def ibzcbm_c(self) -> Real1DArray:
        """IBZ k-point in relative coordinates of the CBM."""
        return self.ibzkpts[str(self.kcbm)]

    @property
    def bzvbm_Kc(self) -> Real2DArray:
        """BZ k-points in relative coordinates of the VBM."""
        return np.array(
            [self.bzkpts[str(K)] for K in self.ibz2bz[str(self.kvbm)]]
        )

    @property
    def bzcbm_Kc(self) -> Real2DArray:
        """BZ k-points in relative coordinates of the CBM."""
        return np.array(
            [self.bzkpts[str(K)] for K in self.ibz2bz[str(self.kcbm)]]
        )

    @cached_property
    def edirgap_ss(self) -> Real2DArray:
        """Direct band gap from s0->s1 for each combination of s0 and s1."""
        return np.array(
            [
                [
                    self.ceigenvalues[f'{s1},{k}']
                    - self.veigenvalues[f'{s0},{k}']
                    for s1, k in enumerate(kdir_s)
                ]
                for s0, kdir_s in enumerate(self.kdir_ss)
            ]
        )

    @property
    def band_gap(self) -> np.float64:
        """Fundamental (minimal) band gap."""
        return np.min(self.ecbm_s) - np.max(self.evbm_s)

    @property
    def direct_band_gap(self) -> np.float64:
        """Direct band gap (k-conserving)."""
        return np.min(self.edirgap_ss)

    @property
    def optical_band_gap(self) -> np.float64:
        """Optical band gap (spin and k-conserving)."""
        return np.min(np.diagonal(self.edirgap_ss))

    def is_direct(self) -> bool:
        """Is the fundamental band gap also direct?"""
        return self.kvbm == self.kcbm

    def is_spin_conserving(self) -> bool:
        """Is the fundamental band gap spin-conserving?"""
        return np.argmax(self.evbm_s) == np.argmin(self.ecbm_s)

    def is_optical(self) -> bool:
        """Is the fundamental band gap optical?"""
        return self.is_direct() and self.is_spin_conserving()

    def direct_is_optical(self) -> bool:
        """Is the direct band gap also optical (spin-conserving)?"""
        s0, s1 = np.unravel_index(
            self.edirgap_ss.argmin(), self.edirgap_ss.shape
        )
        return s0 == s1


@dataclass
class BandInfo(Result):
    efermi: float
    focc_s: np.ndarray
    pocc_s: np.ndarray
    dos_efermi_s: np.ndarray
    _gap_info: GapInfo | None = None

    def __post_init__(self):
        assert len(self.pocc_s) == self.nspins
        if self.is_gapped():
            assert self.gap_info is not None
            assert self.half_gap_info is None
        elif self.is_metallic():
            assert self.gap_info is None
            assert self.half_gap_info is None
        elif self.is_half_metallic():
            assert self.nspins == 2
            assert self.gap_info is None
            assert self.half_gap_info is not None
            assert self.half_gap_info.nspins == 1
        else:
            raise ValueError('Unexpected classification')

    @classmethod
    def from_restart(
        cls, atoms: Atoms, calc: BuiltInGPAWCalculator
    ) -> BandInfo:
        """Construct `BandInfo` dataclass from a GPAW calculator."""
        efermi = calc.get_fermi_level()
        ibzk_kc = calc.get_ibz_k_points()
        e_skn = np.array(
            [
                [
                    calc.get_eigenvalues(kpt=k, spin=s)
                    for k in range(len(ibzk_kc))
                ]
                for s in range(calc.get_number_of_spins())
            ]
        )
        bzk_Kc = calc.get_bz_k_points()
        bz2ibz_K = calc.get_bz_to_ibz_map()
        N_c = calc.wfs.kd.N_c
        cell_cv = atoms.cell
        return cls.from_data(
            efermi,
            e_skn,
            ibzk_kc,
            bzk_Kc,
            bz2ibz_K,
            N_c,
            cell_cv,
            comm=calc.world,
        )

    @classmethod
    def from_data(
        cls, efermi, e_skn, ibzk_kc, bzk_Kc, bz2ibz_K, N_c, cell_cv, comm=None
    ) -> BandInfo:
        """Construct `BandInfo` dataclass from input band data arrays.

        Parameters
        ----------
        efermi: float
            Fermi energy.
        e_skn: np.ndarray
            Band eigenvalues.
        ibzk_kc: np.ndarray
            IBZ k-points in relative coordinates.
        bzk_Kc: np.ndarray
            BZ k-points in relative coordinates.
        bz2ibz_K: np.ndarray
            IBZ k-point index for each BZ k-point.
        N_c: np.ndarray
            Number of k-points in each relative coordinate.
        cell_cv: np.ndarray
            Cell vectors in absolute coordinates.
        """
        # Number of occupied bands for each spin and k
        nocc_sk = (e_skn < efermi).sum(2)
        # Number of partially occupied bands for each spin
        pocc_s = np.ptp(nocc_sk, axis=1)
        # Number of fully occupied bands for each spin
        focc_s = np.max(nocc_sk, axis=1) - pocc_s
        # Density of states at the fermi level
        dos_efermi_s = cls.calculate_dos_at_efermi(
            efermi, e_skn, bz2ibz_K, N_c, cell_cv, comm=comm
        )
        if np.all(pocc_s):  # metallic
            # No gap information to be extracted
            return cls(efermi, focc_s, pocc_s, dos_efermi_s)
        elif np.any(pocc_s):  # half-metallic
            # Extract valence and conduction bands for the gapped spin
            gappeds_s = np.where(pocc_s == 0)[0]
            assert len(gappeds_s) == 1
            gappeds = gappeds_s[0]
            ev_sk = np.array([e_skn[gappeds, :, focc_s[gappeds] - 1]])
            ec_sk = np.array([e_skn[gappeds, :, focc_s[gappeds]]])
        else:  # gapped
            # Extract valence and conduction bands for each spin
            ev_sk = np.array(
                [e_skn[s, :, focc - 1] for s, focc in enumerate(focc_s)]
            )
            ec_sk = np.array(
                [e_skn[s, :, focc] for s, focc in enumerate(focc_s)]
            )
        gap_info = GapInfo.from_data(ev_sk, ec_sk, ibzk_kc, bzk_Kc, bz2ibz_K)
        return cls(efermi, focc_s, pocc_s, dos_efermi_s, gap_info)

    @staticmethod
    def calculate_dos_at_efermi(
        efermi, e_skn, bz2ibz_K, N_c, cell_cv, comm=None
    ) -> np.ndarray:
        """Calculate the DOS at the Fermi level for each spin channel.

        Parameters
        ----------
        efermi: float
            Fermi energy.
        e_skn: np.ndarray
            Band eigenvalues.
        bz2ibz_K: np.ndarray
            IBZ k-point index for each BZ k-point.
        N_c: np.ndarray
            Number of k-points in each relative coordinate.
        cell_cv: np.ndarray
            Cell vectors in absolute coordinates.
        comm: TBCommunicator or None
            MPI communicator to distribute integration over.
        """
        # Fold out eigenvalues to the full BZ
        assert e_skn.ndim == 3
        nspins, _, nbands = e_skn.shape
        e_sKn = e_skn[:, bz2ibz_K]
        # Lay out on Monkhorst-Pack grid
        eig_skn = e_sKn.reshape((nspins,) + tuple(N_c) + (nbands,))
        # Run linear tetrahedron integration
        return BandInfo.ase_lti_dos_at_efermi(
            efermi, eig_skn, cell_cv, comm=comm
        )

    @staticmethod
    def ase_lti_dos_at_efermi(
        efermi, eig_skn, cell_cv, comm=None
    ) -> np.ndarray:
        """Use ASE's linear tetrahedron integration to calculat DOS at E_F.

        Parameters
        ----------
        efermi: float
            Fermi energy.
        eig_skn: np.ndarray
            Band eigenvalues on the Monkhorst-Pack grid.
        cell_cv: np.ndarray
            Cell vectors in absolute coordinates.
        comm: TBCommunicator or None
            MPI communicator to distribute integration over.
        """
        lti = (
            partial(linear_tetrahedron_integration, comm=comm)
            if comm
            else linear_tetrahedron_integration
        )
        assert eig_skn.ndim == 5
        nspins = len(eig_skn)
        dos_efermi_s = np.array(
            [lti(cell_cv, eig_kn, energies=[efermi])[0] for eig_kn in eig_skn]
        )
        assert dos_efermi_s.shape == (nspins,)
        # Account for spin-degeneracy
        assert nspins in [1, 2]
        if nspins == 1:
            dos_efermi_s *= 2
        return dos_efermi_s

    @property
    def nspins(self) -> int:
        """Number of spin channels."""
        return len(self.focc_s)

    @property
    def dos_at_efermi(self) -> float:
        """Density of states at the Fermi level."""
        return sum(self.dos_efermi_s)

    def is_gapped(self) -> bool:
        """Is the band structure gapped?"""
        return not np.any(self.pocc_s)

    def is_metallic(self) -> bool:
        """Is the band structure metallic?"""
        return bool(np.all(self.pocc_s))

    def is_half_metallic(self) -> bool:
        """Is the band structure half-metallic?"""
        return not (self.is_metallic() or self.is_gapped())

    @property
    def gap_info(self) -> GapInfo | None:
        if self.is_gapped():
            return self._gap_info
        return None

    @property
    def half_gap_info(self) -> GapInfo | None:
        if self.is_half_metallic():
            return self._gap_info
        return None

    @property
    def gap(self) -> np.float64 | None:
        """Fundamental band gap."""
        return self.gap_info.band_gap if self.gap_info else None

    @property
    def direct_gap(self) -> np.float64 | None:
        """Direct band gap."""
        return self.gap_info.direct_band_gap if self.gap_info else None

    @property
    def optical_gap(self) -> np.float64 | None:
        """Optical band gap."""
        return self.gap_info.optical_band_gap if self.gap_info else None

    @property
    def half_metallic_gap(self) -> np.float64 | None:
        """Minimal half-metallic gap."""
        return self.half_gap_info.band_gap if self.half_gap_info else None

    @property
    def half_metallic_direct_gap(self) -> np.float64 | None:
        """Direct half-metallic gap."""
        return (
            self.half_gap_info.direct_band_gap if self.half_gap_info else None
        )


GPAWGSR = TypeVar('GPAWGSR', bound='GPAWGroundStateResults')


@dataclass
class GPAWGroundStateResults(Result):
    properties: GPAWProperties

    @classmethod
    def from_gpw(
        cls: Type[GPAWGSR],
        gpw_file: GPWFile,
        d3: bool = False,
        comm: TBCommunicator | None = None,
    ) -> GPAWGSR:
        atoms, calc, properties = cls.read(gpw_file, d3=d3, comm=comm)
        return cls(properties=properties, **cls.extract_data(atoms, calc))

    @classmethod
    def read(
        cls,
        gpw_file: GPWFile,
        *,
        d3: bool,
        comm: TBCommunicator | None,
    ) -> tuple[Atoms, BuiltInGPAWCalculator, GPAWProperties]:
        """Restart GPAW calculator from gpw file and extract properties."""
        if d3:
            atoms, d3calc = gpw_file.d3_restart(comm=comm)
            return atoms, d3calc.dft, GPAWProperties.from_calc(d3calc)
        atoms, calc = gpw_file.restart(comm=comm)
        return atoms, calc, GPAWProperties.from_calc(calc)

    @staticmethod
    def extract_data(
        atoms: Atoms, calc: BuiltInGPAWCalculator
    ) -> dict[str, Any]:
        """Overwrite this method to extract more data for your subclass."""
        return {}

    @property
    def energy(self):
        return self.properties.energy


@dataclass
class GroundStateResults(GPAWGroundStateResults):
    band_info: BandInfo

    @staticmethod
    def extract_data(atoms, calc):
        return dict(band_info=BandInfo.from_restart(atoms, calc))
