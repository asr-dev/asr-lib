from __future__ import annotations

from dataclasses import dataclass
from pathlib import Path

from ase import Atoms

from asrlib.recipes import AtomsPreconditioner, StandardASERecipe
from asrlib.recipes.gs.gpaw.results import GroundStateResults
from asrlib.recipes.typing.gpaw import GPAWCalculatorFactory
from asrlib.results.gpaw import GPWFile
from asrlib.typing import TBCommunicator
from asrlib.typing.gpaw import BuiltInGPAWCalculator


@dataclass
class GPAWGroundStateRecipe(StandardASERecipe[GPWFile]):
    """Ground state recipe base class for GPAW calculations.

    Consult the static `build()` method to see how to construct different
    variants of the recipe.
    """

    calc_factory: GPAWCalculatorFactory
    filename: str
    write_mode: str
    d3: bool

    @staticmethod
    def build(
        calc_factory: GPAWCalculatorFactory,
        preconditioner: AtomsPreconditioner | None = None,
        filename: str = 'gs.gpw',
        write_mode: str = '',
        d3: bool = False,
    ) -> GPAWGroundStateRecipe:
        """Build a ground state recipe.

        Use the `preconditioner` and `d3` and inputs to tailor the recipe to
        your needs.

        preconditioner: Provide a preconditioner, if you want to manipulate the
            input atoms ahead of the ground state calculation in any way. Give
            e.g. a Magnetizer if you want to set the initial magnetic moments.
        d3: Set `d3 == True`, if you want d3-corrected GroundStateResults.
        """
        return GPAWGroundStateRecipe(
            calc_factory=calc_factory,
            preconditioner=preconditioner,
            filename=filename,
            write_mode=write_mode,
            d3=d3,
        )

    def _calculate(self, atoms: Atoms, comm: TBCommunicator) -> GPWFile:
        atoms.get_potential_energy()
        return self.write(atoms.calc)

    def write(self, calc: BuiltInGPAWCalculator) -> GPWFile:
        path = Path(self.filename)
        calc.write(path, mode=self.write_mode)
        return GPWFile(path)

    def results(self, raw_output: GPWFile, comm: TBCommunicator):
        return GroundStateResults.from_gpw(raw_output, d3=self.d3, comm=comm)
