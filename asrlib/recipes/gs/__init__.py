"""
Ground state recipe protocols.
"""

from abc import abstractmethod
from typing import Protocol, TypeVar

from ase import Atoms

from asrlib.recipes.gs.gpaw import GPAWGroundStateRecipe
from asrlib.results.properties import CalculatorProperties
from asrlib.typing import SelfSerializable, TBCommunicator, TBTaskVariable

# User interfaces to actual implementations of the GroundStateRecipe protocol:
__all__ = ['GPAWGroundStateRecipe']


class GroundStateResults(SelfSerializable, Protocol):
    """Base results class for the ground state workflow."""

    properties: CalculatorProperties

    # In the future, we might want to guarantee property forwarding using
    # CalculatorProperties.names. To do so, we would need to rely on
    # inheritance XXX


# A ground state calculation may have many different raw output formats
# depending on which electronic structure code is used.
RawGSOutput = TBTaskVariable
RGSO = TypeVar('RGSO', bound=RawGSOutput)


class GroundStateRecipe(SelfSerializable, Protocol[RGSO]):
    """Recipe protocol for the ground state workflow.

    Overall, the purpose of the workflow is to calculate GroundStateResults
    from the input Atoms, with the RawGSOutput stored as an intermediate
    representation.
    """

    @abstractmethod
    def calculate(self, atoms: Atoms, comm: TBCommunicator) -> RGSO:
        raise NotImplementedError

    @abstractmethod
    def results(
        self, raw_output: RGSO, comm: TBCommunicator
    ) -> GroundStateResults:
        raise NotImplementedError
