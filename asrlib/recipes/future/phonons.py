from __future__ import annotations

from functools import cached_property
from pathlib import Path
from typing import TYPE_CHECKING, Dict

import numpy as np
import taskblaster as tb
from ase.atoms import Atoms

from asrlib.actions import phonon_dos
from asrlib.parameters.gpaw import GPAWCalculatorFactory
from asrlib.results import StandardResult

if TYPE_CHECKING:
    from phonopy.structure.atoms import PhonopyAtoms


class LazyPhonopy:
    @cached_property
    def _phonopy(self):
        import phonopy

        return phonopy

    @property
    def Phonopy(self):
        return self._phonopy.Phonopy

    @property
    def PhonopyAtoms(self):
        from phonopy.structure.atoms import PhonopyAtoms

        return PhonopyAtoms

    @property
    def load(self):
        return self._phonopy.load


phonopy = LazyPhonopy()


def phonopy2atoms(atoms: PhonopyAtoms, pbc: np.ndarray) -> Atoms:
    """
    Convert phonopy atoms object to ASE Atoms.

    :param atoms: Crystal structure to convert.
    :param pbc: The periodic boundary conditions.
    :return: ASE Atoms object.
    """
    return Atoms(
        symbols=atoms.symbols,
        scaled_positions=atoms.scaled_positions,
        cell=atoms.cell,
        pbc=pbc,
    )


def generate_displacements(
    atoms: Atoms,
    phonopy_constructor: dict | None = None,
    disp_kwargs: dict | None = None,
) -> StandardResult:
    """
    Generate displacements for the given Atoms using Phonopy's
    generate_displacements.

    :param atoms: Crystal structure.
    :param phonopy_constructor: Key word arguments to instantiate phonopy's
        API.
    :param disp_kwargs: Key word arguments for phonopy's generate_displacements
    :return: StandardResults(images=List[Atoms],
                             phonopy_disp=Path('phonopy_disp.yaml'))
    """
    # Assert that strings are only auto otherwise unintended behaviour occurs.
    if disp_kwargs is None:
        disp_kwargs = {}
    if phonopy_constructor is None:
        phonopy_constructor = {}
    if isinstance(disp_kwargs.get('is_plusminus', False), str):
        assert disp_kwargs['is_plusminus'] == 'auto'

    unitcell = phonopy.PhonopyAtoms(
        symbols=atoms.symbols,
        cell=atoms.get_cell(),
        magnetic_moments=atoms.get_initial_magnetic_moments(),
        scaled_positions=atoms.get_scaled_positions(),
    )
    phonon = phonopy.Phonopy(unitcell, **phonopy_constructor)
    phonon.generate_displacements(**disp_kwargs)

    images = []
    for idx, image in enumerate(phonon.supercells_with_displacements):
        images.append(phonopy2atoms(atoms=image, pbc=atoms.pbc))
    phonon.save(filename='phonopy_disp.yaml')
    return StandardResult(
        images=images, phonopy_disp=Path('phonopy_disp.yaml')
    )


@tb.mpi
def phonon_image_force(
    image: Atoms,
    calc_factory: GPAWCalculatorFactory,
    mpi: tb.mpi,
    d3: bool = False,
) -> np.ndarray:
    """
    Use the provided `calc_factory`, compute the forces for a single phonon
    image.

    This task produces individual phonon forces that are then collected
    by the workflow using
    `asrlib.recipes.future.phonons.calculate_force_constants`.

    :param image: Image crystal structure.
    :param calc_factory: GPAWCalculatorFactory.
    :param mpi: Taskblaster MPI object.
    :return: np.ndarray(forces)
    """
    dft = calc_factory(atoms=image, comm=mpi.comm)
    image.calc = calc_factory.d3_wrapper(dft, comm=mpi.comm) if d3 else dft

    F_av = image.get_forces()  # atomic forces of the image
    F_av -= np.average(F_av, axis=0)  # no net force (phonopy convention)
    return F_av


@tb.actions(phonon_dos=phonon_dos)
def calculate_force_constants(
    phonopy_disp: Path,
    image_forces: Dict[str, np.ndarray],
    force_constants_kwargs: Dict,
) -> Path:
    """
    Function used by TaskBlaster as a `dynamical_workflow_generator_task`.
    The `image_forces` dictionary is automatically created when tasks are
    parameterized over using the `dynamical_workflow_generator_task`.

    :param phonopy_disp: Original `phonopy_disp.yaml` file produced by
        `asrlib.recipes.future.phonons.generate_displacements`.
    :param image_forces: The individual forces produced by workflow calls
        of a `dynamical_workflow_generator_task` on `phonon_image_force`.
    :param force_constants_kwargs: Key word arguments for
        phonon.produce_force_constants.
    :return: Path('phonopy_data.yaml')
    """
    # ensure the keys are sorted for appending the forces in correct order
    forces = [
        image_forces[key]
        for key in sorted(
            image_forces.keys(), key=lambda x: int(x.split('image_')[1])
        )
    ]
    phonon = phonopy.load(str(phonopy_disp))
    phonon.produce_force_constants(forces=forces, **force_constants_kwargs)
    phonon.symmetrize_force_constants()

    phonon.save(
        filename='phonopy_data.yaml', settings={'force_constants': True}
    )
    return Path('phonopy_data.yaml')
