"""
Actions are functions that act upon tasks.
They take record objects, provided by taskblaster's command line utility
`tb view --action <action name>`.

A record (TaskView) object gives the user access to each task's output, input,
realized_input (deserialized), and node information.
"""

from ase.visualize import view
from taskblaster import TaskView


def phonon_dos(record: TaskView):
    import phonopy

    phonon = phonopy.load(record.output)
    phonon.run_mesh([20, 20, 20])
    phonon.run_total_dos()
    phonon.plot_total_dos().show()


def interact(record: TaskView):
    """
    Start an interactive console session (REPL) in the current environment for
    the given task allowing the user to execute commands dynamically with
    access to the record and decoded input, output, and node information.
    """
    output, input = record.output, record.realized_input
    from code import interact

    # locals() initializes the session by passing in the local scope (variables
    # defined in the current context) to the session. This makes all variables
    # defined at this point, including 'output', 'input', and 'record',
    # accessible and modifiable within the interactive console.
    interact(local=locals())


def write_output_atoms_to_json(record: TaskView):
    """Write the output atoms to a json file."""
    # node.name is tree-less. Construct the correct file path to the node's
    # directory.
    filename = record.directory / 'structure.json'
    print(f'Writing structure.json file of output.atoms to {filename}')
    record.output.atoms.write(filename)


def view_input_atoms(record: TaskView):
    print(f'Viewing structure: {record.node.name}')
    if record.realized_input and hasattr(record.realized_input, 'atoms'):
        view(record.realized_input.atoms)
    else:
        print(
            'No atoms attribute found on input. Try the interact '
            'action to view the input.'
        )


def view_output_atoms(record: TaskView):
    print(f'Viewing structure: {record.node.name}')
    if record.output and hasattr(record.output, 'atoms'):
        view(record.output.atoms)
    else:
        print(
            'No atoms attribute found on the output. Try the interact '
            'action to view the output.'
        )


def symmetrization(record: TaskView):
    """Plot SymmetryAnalysis resulting from ASESymmetrizeRecipe.run()."""
    from asrlib.recipes.symmetrize.ase.results import plot_symmetries

    plot_symmetries(record.output.analysis, show=True)
