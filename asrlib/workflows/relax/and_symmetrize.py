import taskblaster as tb
from ase import Atoms

from asrlib import recipe_node
from asrlib.recipes.relax import RelaxRecipe
from asrlib.recipes.symmetrize import SymmetrizeRecipe
from asrlib.workflows.relax import RelaxRecipeWorkflow


@tb.workflow
class RelaxAndSymmetrize:
    unrelaxed: Atoms = tb.var()
    relax_recipe: RelaxRecipe = tb.var()
    symmetrize_recipe: SymmetrizeRecipe = tb.var()

    @tb.subworkflow
    def relaxation(self):
        return RelaxRecipeWorkflow(
            atoms=self.unrelaxed,
            recipe=self.relax_recipe,
        )

    @tb.task
    def symmetrize(self):
        return recipe_node(
            'run', self.symmetrize_recipe, atoms=self.relaxation.results.atoms
        )
