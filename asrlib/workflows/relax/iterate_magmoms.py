"""
Workflows for performing structure optimization while determining magmoms.
"""

from __future__ import annotations

from dataclasses import dataclass

import taskblaster as tb
from ase import Atoms

from asrlib.recipes.gs import GPAWGroundStateRecipe
from asrlib.recipes.magstate import (
    MagneticProperties,
    MagneticStateClassification,
    MagneticStateClassifier,
    MagneticVSNonmagneticStateClassifier,
)
from asrlib.recipes.preconditioners import Magnetizer
from asrlib.recipes.relax import ASERelaxRecipe, RelaxResults
from asrlib.results import Result
from asrlib.workflows.gs import GroundStateRecipeWorkflow
from asrlib.workflows.magstate import MagneticStateWorkflow
from asrlib.workflows.relax import RelaxRecipeWorkflow
from asrlib.workflows.relax.and_magstate import RelaxAndDetermineMagneticState


@dataclass
class RelaxAndMagstateResults(Result):
    atoms: Atoms
    relaxation: RelaxResults
    classification: MagneticStateClassification


@tb.workflow
class IterativelyDetermineMagneticStateAndRelax:
    """Worflow determining the magnetic state before and after relaxation.

    Starts by determining the initial magnetic state of the input atoms.
    Afterwards, the atoms are relaxed with the calculator `spinpol` set
    according to the initial state classification and the magnetic state of the
    relaxed structure is determined.
    If the magnetic state of the relaxed structure is qualitatively different
    from the initial, we repeat the relaxation step with updated `spinpol`.
    """

    atoms: Atoms = tb.var()
    relax_recipe: ASERelaxRecipe = tb.var()
    gs_recipe: GPAWGroundStateRecipe = tb.var()
    magstate_magnetizer = tb.var(default=Magnetizer.build())
    magstate_classifier = tb.var(
        default=MagneticVSNonmagneticStateClassifier.build()
    )

    @tb.branch('entry')
    @tb.jump('iterate')
    @tb.subworkflow
    def initial_state(self):
        return MagneticStateWorkflow(
            atoms=self.atoms,
            gs_recipe=self.gs_recipe,
            magnetizer=self.magstate_magnetizer,
            classifier=self.magstate_classifier,
        )

    @tb.branch('iterate', loop=True)
    @tb.subworkflow
    def relax_and_magstate(self):
        return RelaxAndDetermineMagneticState(
            atoms=self.input_atoms,
            relax_recipe=self.relax_recipe_from(
                classification=self.input_classification,
            ),
            gs_recipe=self.gs_recipe,
            magstate_magnetizer=self.magstate_magnetizer,
            magstate_classifier=self.magstate_classifier,
        )

    def relax_recipe_from(self, classification: MagneticStateClassification):
        return self.relax_recipe.new_with(
            calc_factory=self.relax_recipe.calc_factory.new_with(
                spinpol=classification.spinpol,
            ),
            append=dict(preconditioner=classification.magnetizer),
        )

    @property
    def input_atoms(self):
        """Input atoms to the current while-loop iteration."""
        return self.Phi(
            entry=self.atoms,
            iterate=self.relax_and_magstate.relaxation.results.atoms,
        )

    @property
    def input_classification(self):
        """Input magstate classification of current while-iteration."""
        return self.Phi(
            entry=self.initial_state.classification,
            iterate=self.relax_and_magstate.magnetic_state.classification,
        )

    @property
    def output_classification(self):
        """Output magstate classification of last/current while-iteration."""
        return self.relax_and_magstate.magnetic_state.classification

    @property
    def in_first_iteration(self):
        return self.Phi(entry=True, iterate=False)

    @tb.branch('iterate', loop=True)
    @tb._if(true='final', false='iterate')
    @tb.task
    def stop_iteration(self):
        return tb.node(
            self._self_consistent_classification,
            input_classification=self.input_classification,
            output_classification=self.output_classification,
            in_first_iteration=self.in_first_iteration,
        )

    @staticmethod
    def _self_consistent_classification(
        input_classification: MagneticStateClassification,
        output_classification: MagneticStateClassification,
        in_first_iteration: bool,
    ) -> bool:
        self_consistent_classification = (
            input_classification.spinpol == output_classification.spinpol
        )
        if not self_consistent_classification and not in_first_iteration:
            raise ValueError(
                'Did not manage to reach a self-consistent spinpol '
                'classification during the second iteration of '
                'relax_and_magstate. This should not happen!\n'
                'Please check that the calculator of your input relax_recipe '
                'and gs_recipe reach identical output magnetic moments.'
            )
        return self_consistent_classification

    @tb.branch('final')
    @tb.fixedpoint
    @tb.task
    def results(self):
        return tb.node(
            self._results,
            gs_recipe=self.gs_recipe,
            relax_results=self.relax_and_magstate.relaxation.results,
            classification=self.output_classification,
        )

    @staticmethod
    def _results(
        gs_recipe: GPAWGroundStateRecipe,
        relax_results: RelaxResults,
        classification: MagneticStateClassification,
    ):
        # The final magstate classification has been performed on a
        # preconditioned set of relaxed atoms. Reproduce that here and set the
        # initial magnetic moments.
        atoms = gs_recipe.precondition(relax_results.atoms)
        atoms = classification.magnetizer(atoms)
        return RelaxAndMagstateResults(
            atoms=atoms,
            relaxation=relax_results,
            classification=classification,
        )


@tb.workflow
class RelaxToGSWithOrWithoutMagmoms:
    """
    Perform a Factory-driven spin polarized calculation, followed by an
    analysis of the material's magnetic state.

    If the state is non-magnetic, the workflow branches to perform a spin
    paired relaxation. If it is magnetic, jump to the post-processing step.
    """

    atoms = tb.var()
    magmoms = tb.var()

    mag_relax_recipe = tb.var()
    nm_relax_recipe = tb.var()
    classifier = tb.var(default=MagneticStateClassifier(magmoms_tol=0.1))
    gs_recipe = tb.var()

    @tb.subworkflow
    def relax_mag(self):
        return RelaxRecipeWorkflow(
            atoms=self.atoms,
            recipe=self.mag_relax_recipe.new_with(
                append=dict(preconditioner=Magnetizer(self.magmoms)),
            ),
        )

    @tb._if({'fm': 'final', 'afm': 'final', 'nm': 'non_mag'})
    @tb.task
    def check_magnetic_state(self):
        return tb.node(
            self.get_magstate,
            properties=self.relax_mag.results.properties,
            classifier=self.classifier,
        )

    @staticmethod
    def get_magstate(
        properties: MagneticProperties, classifier: MagneticStateClassifier
    ):
        return classifier.classify(properties).name

    @tb.branch('non_mag')
    @tb.jump('final')
    @tb.subworkflow
    def relax_nm(self):
        return RelaxRecipeWorkflow(
            atoms=self.relax_mag.results.atoms,
            recipe=self.nm_relax_recipe.new_with(
                append=dict(preconditioner=Magnetizer(0.0)),
            ),
        )

    @tb.branch('final')
    @tb.fixedpoint
    @tb.task
    def finalizer(self):
        """The finalizer maps branched data onto a single reference point,
        providing one reference name to access the output data for
        dependencies."""
        return tb.node(
            'define',  # use reference obj as output obj
            obj=self.Phi(  # map branch to reference
                non_mag=self.relax_nm.results, entry=self.relax_mag.results
            ),
        )

    @tb.subworkflow
    def gs(self):
        return GroundStateRecipeWorkflow(
            atoms=self.finalizer.atoms,
            recipe=self.gs_recipe,
        )
