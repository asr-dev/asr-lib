import taskblaster as tb
from ase import Atoms

from asrlib import recipe_node
from asrlib.recipes.relax import RelaxRecipe


@tb.workflow
class RelaxRecipeWorkflow:
    atoms: Atoms = tb.var()
    recipe: RelaxRecipe = tb.var()

    @tb.task
    def calculate(self):
        return recipe_node(
            'calculate', self.recipe, pass_comm=True, atoms=self.atoms
        )

    @tb.task
    def results(self):
        return recipe_node('results', self.recipe, raw_output=self.calculate)
