import taskblaster as tb
from ase import Atoms

from asrlib.recipes.gs import GPAWGroundStateRecipe
from asrlib.recipes.preconditioners import Magnetizer
from asrlib.recipes.relax import ASERelaxRecipe
from asrlib.workflows.magstate import (
    MagneticStateWorkflow,
    MagneticVSNonmagneticStateClassifier,
)
from asrlib.workflows.relax import RelaxRecipeWorkflow


@tb.workflow
class RelaxAndDetermineMagneticState:
    """Relax a crystal structure and determine if it is magnetic."""

    atoms: Atoms = tb.var()
    relax_recipe: ASERelaxRecipe = tb.var()
    gs_recipe: GPAWGroundStateRecipe = tb.var()
    magstate_magnetizer = tb.var(default=Magnetizer.build())
    magstate_classifier = tb.var(
        default=MagneticVSNonmagneticStateClassifier.build()
    )

    @tb.subworkflow
    def relaxation(self):
        return RelaxRecipeWorkflow(atoms=self.atoms, recipe=self.relax_recipe)

    @tb.subworkflow
    def magnetic_state(self):
        return MagneticStateWorkflow(
            atoms=self.relaxation.results.atoms,
            gs_recipe=self.gs_recipe,
            magnetizer=self.magstate_magnetizer,
            classifier=self.magstate_classifier,
        )
