"""
Base magnetic state workflow
"""

import taskblaster as tb

from asrlib import recipe_node
from asrlib.recipes.magstate import MagneticVSNonmagneticStateClassifier
from asrlib.recipes.preconditioners import Magnetizer
from asrlib.workflows.gs import GroundStateRecipeWorkflow


@tb.workflow
class MagneticStateWorkflow:
    """Workflow determining whether a given material is magnetic.

    Starts by performing a ground state calculation starting from finite
    initial magnetic moments. If the magnetic moments vanish during the
    calculation, the material is classified (as nonmagnetic) based on this
    fact. If the system has a stable magnetic state, a second ground state
    calculation is performed starting from vanishing magnetic moments and the
    material is classified based on an (energy) comparison of the two states.
    """

    atoms = tb.var()  # Atoms
    gs_recipe = tb.var()  # GPAWGroundStateRecipe or similar
    magnetizer = tb.var(default=Magnetizer.build())
    classifier = tb.var(default=MagneticVSNonmagneticStateClassifier.build())

    @property
    def calc_factory(self):
        """Make sure to consistently perform spinful calculations such that the
        trial magnetic and nonmagnetic states can be compared on equal terms.
        """
        return self.gs_recipe.calc_factory.new_with(spinpol=True)

    @tb.subworkflow
    def trial_magnetic_state(self):
        """Ground state calculation starting from finite magnetic moments."""
        return GroundStateRecipeWorkflow(
            atoms=self.atoms,
            recipe=self.gs_recipe.new_with(
                calc_factory=self.calc_factory,
                append=dict(preconditioner=self.magnetizer),
            ),
        )

    @tb._if(true='nonmag', false='final')
    @tb.task
    def stayed_magnetic(self):
        """Did the `trial_magnetic_state` remain magnetic?

        If true, perform a ground state calculation with vanishing initial
        magnetic moments. If false, jump straight to the classification.
        """
        return recipe_node(
            'is_magnetic',
            self.classifier,
            properties=self.trial_magnetic_state.results.properties,
        )

    @tb.branch('nonmag')
    @tb.jump('final')
    @tb.subworkflow
    def nonmagnetic_state(self):
        """Groundstate calculation starting from vanishing magnetic moments."""
        return GroundStateRecipeWorkflow(
            atoms=self.atoms,
            recipe=self.gs_recipe.new_with(
                calc_factory=self.calc_factory,
                append=dict(preconditioner=Magnetizer(0.0)),
            ),
        )

    @tb.branch('final')
    @tb.fixedpoint
    @tb.task
    def classification(self):
        return recipe_node(
            'classify',
            self.classifier,
            mag_properties=self.trial_magnetic_state.results.properties,
            nonmag_properties=self.Phi(
                entry=None,  # the `nonmagnetic_state` was skipped
                nonmag=self.nonmagnetic_state.results.properties,
            ),
            magnetizer=self.magnetizer,
        )
