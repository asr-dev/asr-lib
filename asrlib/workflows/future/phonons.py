from typing import List

import taskblaster as tb
from ase import Atoms

from asrlib.parameters.gpaw import GPAWCalculatorFactory


@tb.workflow
class PhononWf:
    atoms = tb.var()
    calc_factory = tb.var()
    disp_kwargs = tb.var()
    phonopy_constructor = tb.var()
    force_constants_kwargs = tb.var({'calculate_full_force_constants': False})

    @tb.task
    def generate_displacements(self):
        """
        :return: StandardResult(images=List[Displaced Atoms],
                                phonopy_disp=Path(phonopy.yaml))
        """
        return tb.node(
            'asrlib.recipes.future.phonons.generate_displacements',
            atoms=self.atoms,
            phonopy_constructor=self.phonopy_constructor,
            disp_kwargs=self.disp_kwargs,
        )

    @tb.dynamical_workflow_generator({'collect_image_forces': '**'})
    def calculate_image_forces(self):
        return tb.node(
            'asrlib.workflows.future.phonons.phonon_image_forces',
            images=self.generate_displacements.images,
            calc_factory=self.calc_factory,
        )

    @tb.task
    def results(self):
        return tb.node(
            'asrlib.recipes.future.phonons.calculate_force_constants',
            phonopy_disp=self.generate_displacements.phonopy_disp,
            image_forces=self.calculate_image_forces.collect_image_forces,
            force_constants_kwargs=self.force_constants_kwargs,
        )


@tb.dynamical_workflow_generator_task
def phonon_image_forces(
    images: List[Atoms], calc_factory: GPAWCalculatorFactory
):
    for idx, image in enumerate(images):
        yield (
            f'image_{idx}',
            tb.node(
                'asrlib.recipes.future.phonons.phonon_image_force',
                calc_factory=calc_factory,
                image=image,
            ),
        )
