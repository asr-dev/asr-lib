import taskblaster as tb
from ase import Atoms

from asrlib import recipe_node
from asrlib.recipes.gs import (
    GroundStateRecipe,
    GroundStateResults,
    RawGSOutput,
)


@tb.workflow
class GroundStateRecipeWorkflow:
    atoms: Atoms = tb.var()
    recipe: GroundStateRecipe = tb.var()

    @tb.task
    def calculate(self) -> RawGSOutput:
        return recipe_node(
            'calculate', self.recipe, pass_comm=True, atoms=self.atoms
        )

    @tb.task
    def results(self) -> GroundStateResults:
        return recipe_node(
            'results', self.recipe, pass_comm=True, raw_output=self.calculate
        )
