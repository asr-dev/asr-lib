from functools import partial

import numpy as np
from ase.calculators.dftd3 import DFTD3 as _DFTD3


class DFTD3(_DFTD3):
    def calculate_corrections(self):
        """Calculate D3 corrections to any preexisting self.dft.results.

        Calls DFTD3.calculate to compute the d3 corrections, but in a "safe"
        way where we make sure not to start any actual dft calculations, but
        only use results that already exist in the self.dft calculator.
        """
        assert self.dft.results, 'No DFT results to correct'
        # We monkey-patch get_property to extract the dft results straight from
        # the results dictionary. In addition to ensuring that no dft
        # calculation is started, this allows us to forward gpaw_new results,
        # even when the calculator was restarted as gpaw_old.
        actual_get_property = self.dft.get_property
        try:
            self.dft.get_property = partial(
                _get_property, results=self.dft.results
            )
            self.calculate(
                atoms=self.dft.atoms,
                properties=self.dft.results.keys(),
                system_changes=None,
            )
        finally:
            self.dft.get_property = actual_get_property


def _get_property(name, atoms, results):
    result = results[name]
    if isinstance(result, np.ndarray):
        result = result.copy()
    return result
