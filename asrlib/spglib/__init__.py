from dataclasses import asdict, is_dataclass
from typing import Any

import numpy as np
from ase import Atoms
from ase.utils import atoms_to_spglib_cell


def spglib_cell_to_atoms(cell, pbc=True):
    cell, spos, numbers = cell
    return Atoms(scaled_positions=spos, numbers=numbers, cell=cell, pbc=pbc)


def check_layer_symmetry(atoms, symprec: float = 1e-6) -> dict:
    from spglib.spglib import get_symmetry_layerdataset

    npbc_c = np.where(atoms.get_pbc() == 0)[0]
    assert len(npbc_c) == 1
    obj = get_symmetry_layerdataset(
        atoms_to_spglib_cell(atoms),
        aperiodic_dir=npbc_c[0],
        symprec=symprec,
    )
    # We can remove this, when Niflheim modules have spglib>=2.5.0
    if isinstance(obj, dict):
        return obj
    return asdict(obj)


def spglib_as_dict(dataset: Any) -> dict:
    # spglib will return a dataclass-like object.
    # our code uses dicts in style with old spglib api.
    # This ensures that our dataset is in dict form.
    if is_dataclass(dataset):
        return asdict(dataset)  # type: ignore
    return dict(dataset)
