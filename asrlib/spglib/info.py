"""Tabled data from spglib papers."""

import json
from dataclasses import asdict, dataclass
from pathlib import Path

SPACEGROUP_INFO = Path(__file__).parent.joinpath('spacegroup.json').resolve()
LAYERGROUP_INFO = Path(__file__).parent.joinpath('layergroup.json').resolve()


@dataclass
class Layergroup:
    number: str
    international: str
    # pointgroup: str # It would be nice to add this also at some point
    crystal_system: str
    lattice_system: str
    bravais_type: str
    bravais_type_long: str

    def todict(self):
        return asdict(self)


@dataclass
class Spacegroup:
    number: str
    international: str
    pointgroup: str
    crystal_system: str
    bravais_type: str
    bravais_type_long: str

    def todict(self):
        return asdict(self)


@dataclass
class SpacegroupInfo:
    def __post_init__(self):
        with open(SPACEGROUP_INFO, 'r') as file:
            spacegroup_info = json.load(file)

        self._spacegroups_info = self._create_spacegroup_info_objects(
            data=spacegroup_info
        )

    def _create_spacegroup_info_objects(self, data):
        return [Spacegroup(**info) for info in data]

    def info_from_spacegroupnum(self, spacegroup: int) -> Spacegroup:
        if spacegroup not in range(1, 230 + 1):
            raise ValueError('Spacegroup number is only valid between 1-230.')

        filtered_spacegroup = filter(
            lambda sgi: int(sgi.number) == spacegroup, self._spacegroups_info
        )
        return next(filtered_spacegroup)


@dataclass
class LayergroupInfo:
    def __post_init__(self):
        with open(LAYERGROUP_INFO, 'r') as file:
            layergroup_info = json.load(file)

        self._layergroups_info = self._create_layergroup_info_objects(
            data=layergroup_info
        )

    def _create_layergroup_info_objects(self, data: list[dict]):
        return [Layergroup(**info) for info in data]

    def info_from_layergroupnum(self, layergroup: int) -> Layergroup:
        if layergroup not in range(1, 80 + 1):
            raise ValueError('Layergroup number is only valid between 1-80.')

        filtered_layergroup = filter(
            lambda lgi: int(lgi.number) == layergroup, self._layergroups_info
        )
        return next(filtered_layergroup)
