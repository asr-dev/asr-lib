from dataclasses import asdict, dataclass
from itertools import product

import numpy as np
import pytest
from ase.build import bulk
from ase.dft.bandgap import bandgap

from asrlib.parameters.gpaw import GPAWCalculatorFactory
from asrlib.recipes.gs import GPAWGroundStateRecipe
from asrlib.recipes.gs.gpaw.results import BandInfo, GapInfo
from asrlib.recipes.preconditioners import Magnetizer
from asrlib.results.gpaw import GPAWProperties
from crysp.postprocess.gs import CryspGapResults


def generate_test_arrays(de_skn: np.ndarray):
    assert de_skn.shape in [(1, 2, 2), (2, 2, 2)]
    ev_sk = np.zeros(de_skn.shape[:2]) + de_skn[..., 0]
    ec_sk = np.ones(de_skn.shape[:2]) + de_skn[..., 1]
    return ev_sk, ec_sk, *generate_test_k_arrays()


def generate_test_k_arrays():
    ibzk_kc = np.array([[0.0, 0.0, 0.0], [0.0, 0.0, 0.5]])
    bzk_Kc = np.array([[0.0, 0.0, 0.0], [0.0, 0.0, 0.5], [0.0, 0.0, -0.5]])
    bz2ibz_K = np.array([0, 1, 1])
    return ibzk_kc, bzk_Kc, bz2ibz_K


@dataclass
class GapInfoTestData:
    de_skn: np.ndarray
    direct: bool
    spin_conserving: bool
    optical: bool
    direct_is_optical: bool
    gap: float
    direct_gap: float
    optical_gap: float
    kvbm: int
    kcbm: int

    def check(self, gap_info: GapInfo):
        # Qualitative features
        assert gap_info.is_direct() == self.direct
        assert gap_info.is_spin_conserving() == self.spin_conserving
        assert gap_info.is_optical() == self.optical
        assert gap_info.direct_is_optical() == self.direct_is_optical
        # Actual gaps
        assert gap_info.band_gap == pytest.approx(self.gap)
        assert gap_info.direct_band_gap == pytest.approx(self.direct_gap)
        assert gap_info.optical_band_gap == pytest.approx(self.optical_gap)
        # VBM/CBM k-points
        assert gap_info.kvbm == self.kvbm
        if self.kvbm:  # X-point
            assert gap_info.ibzvbm_c == pytest.approx([0.0, 0.0, 0.5])
            assert gap_info.bzvbm_Kc.shape == (2, 3)
        else:  # Γ-point
            assert gap_info.ibzvbm_c == pytest.approx(np.zeros(3))
            assert gap_info.bzvbm_Kc.shape == (1, 3)
        assert gap_info.kcbm == self.kcbm
        if self.kcbm:  # X-point
            assert gap_info.ibzcbm_c == pytest.approx([0.0, 0.0, 0.5])
            assert gap_info.bzcbm_Kc.shape == (2, 3)
        else:  # Γ-point
            assert gap_info.ibzcbm_c == pytest.approx(np.zeros(3))
            assert gap_info.bzcbm_Kc.shape == (1, 3)


def direct_optical_test_data(de_skn, *, k):
    return GapInfoTestData(
        de_skn,
        direct=True,
        spin_conserving=True,
        optical=True,
        direct_is_optical=True,
        gap=1.0,
        direct_gap=1.0,
        optical_gap=1.0,
        kvbm=k,
        kcbm=k,
    )


def indirect_spinconserving_test_data(de_skn, *, dgap, kv):
    return GapInfoTestData(
        de_skn,
        direct=False,
        spin_conserving=True,
        optical=False,
        direct_is_optical=True,
        gap=1.0,
        direct_gap=dgap,
        optical_gap=dgap,
        kvbm=kv,
        kcbm=1 - kv,
    )


def generate_spinless_gap_info_test_data():
    all_data = []
    # Direct gap at Γ
    de_skn = np.zeros((1, 2, 2))
    de_skn[0, 1, 0] -= 1.0
    de_skn[0, 1, 1] += 1.0
    all_data.append(direct_optical_test_data(de_skn, k=0))
    # Direct gap at X
    de_skn = np.zeros((1, 2, 2))
    de_skn[0, 0, 0] -= 1.0
    de_skn[0, 0, 1] += 1.0
    all_data.append(direct_optical_test_data(de_skn, k=1))
    # Indirect gap from Γ->X
    de_skn = np.zeros((1, 2, 2))
    de_skn[0, 1, 0] -= 0.5
    de_skn[0, 0, 1] += 1.0
    all_data.append(indirect_spinconserving_test_data(de_skn, dgap=1.5, kv=0))
    # Indirect gap from X->Γ
    de_skn = np.zeros((1, 2, 2))
    de_skn[0, 0, 0] -= 0.5
    de_skn[0, 1, 1] += 1.0
    all_data.append(indirect_spinconserving_test_data(de_skn, dgap=1.5, kv=1))
    return all_data


def generate_gap_info_test_data():
    spinless_data = generate_spinless_gap_info_test_data()
    all_data = []
    for data in spinless_data:
        all_data.append(data)
        # Spin-conserving (decrease gap for s=0)
        de_skn = np.array([list(data.de_skn[0])] * 2)
        de_skn[0, :, 0] += 1e-8
        de_skn[0, :, 1] -= 1e-8
        all_data.append(GapInfoTestData(**{**asdict(data), 'de_skn': de_skn}))
        # Spin-nonconserving (decrease gap from s=0 to s=1)
        de_skn = np.array([list(data.de_skn[0])] * 2)
        de_skn[0, :, 0] += 1e-8
        de_skn[1, :, 1] -= 1e-8
        all_data.append(
            GapInfoTestData(
                **{
                    **asdict(data),
                    'de_skn': de_skn,
                    'spin_conserving': False,
                    'optical': False,
                    'direct_is_optical': False,
                }
            )
        )
    return all_data


@pytest.mark.parametrize('data', generate_gap_info_test_data())
def test_gap_info(data):
    gap_info = GapInfo.from_data(*generate_test_arrays(data.de_skn))
    data.check(gap_info)


@pytest.mark.parametrize('data', generate_spinless_gap_info_test_data())
def test_half_metallic_band_info(data):
    # Generate a half-metallic test system
    e_skn = np.zeros((2, 2, 2))
    # Gapped spin channel
    e_skn[0, :, 0] += data.de_skn[0, :, 0]
    e_skn[0, :, 1] += 1.0 + data.de_skn[0, :, 1]
    # Metallic spin channel
    e_skn[1, 0, :] -= 0.5
    e_skn[1, 1, :] += 0.5
    # Construct and test band info
    band_info = BandInfo.from_data(
        0.1,
        e_skn,
        *generate_test_k_arrays(),
        N_c=np.array([1, 1, 3]),
        cell_cv=np.eye(3),
    )
    assert not band_info.is_gapped()
    assert not band_info.is_metallic()
    assert band_info.is_half_metallic()
    assert band_info.gap is None
    assert band_info.direct_gap is None
    assert band_info.optical_gap is None
    assert band_info.half_metallic_gap == pytest.approx(data.gap)
    assert band_info.half_metallic_direct_gap == pytest.approx(data.direct_gap)
    assert band_info.gap_info is None
    data.check(band_info.half_gap_info)
    assert band_info.dos_efermi_s == pytest.approx([0.0, 4 / 3])
    assert band_info.dos_at_efermi == pytest.approx(4 / 3)


conditional_array = (np.ndarray, type(None))
gs_attrs = {
    'properties': GPAWProperties,
}
prop_attrs = {
    'energy': float,
    'free_energy': float,
    'dipole': np.ndarray,
    'magmom': float,
    'magmoms': np.ndarray,
    'forces': conditional_array,
    'stress': conditional_array,
}


def get_gs_recipe(
    new: bool = True, d3: bool = False, mag: bool = False, **kwargs
):
    ecut = 150
    txt = 'gs.txt'
    parallel = {'band': 1}
    mode = {'name': 'pw', 'ecut': ecut}
    occupations = {'name': 'fermi-dirac', 'width': 0.5}

    calc_params_gs = {
        'xc': 'PBE',
        'mode': mode,
        'parallel': parallel,
        'txt': txt,
        'nbands': '200%',
        'charge': 0,
        'convergence': {'bands': 'CBM+1.0', 'forces': 5e-2},
        'kpts': (2, 2, 2),
        'occupations': occupations,
        'gpaw_new': new,
    }
    calc_params_gs.update(**kwargs)
    preconditioner = Magnetizer(1.0) if mag else None
    return GPAWGroundStateRecipe.build(
        GPAWCalculatorFactory(calc_params_gs),
        preconditioner=preconditioner,
        d3=d3,
    )


@pytest.mark.parametrize('d3,new', list(product([False, True], [True, False])))
def test_gs(run_gs_recipe, d3, new):
    from crysp.postprocess.gs import gsresults

    gpw_file, gs_results = run_gs_recipe(
        get_gs_recipe(d3=d3, new=new), bulk('Si')
    )

    for key, value in gs_attrs.items():
        print(key, value, getattr(gs_results, key))
        assert hasattr(gs_results, key)
        assert isinstance(getattr(gs_results, key), value)
    for key, value in prop_attrs.items():
        assert hasattr(gs_results.properties, key)
        assert isinstance(getattr(gs_results.properties, key), value)
    assert gpw_file.path.is_file()  # writes gs.gpw per default

    # crysp backwards compatibility
    gapinfo = {
        'efermi': 5.569,
        'gap': 2.293,
        'gap_dir': 2.803,
        'cbm': 6.838,
        'vbm': 4.545,
        'cbm_dir': 7.348,
        'vbm_dir': 4.545,
    }
    crysp_results = gsresults(gpw_path=gpw_file.path, d3=d3)
    assert isinstance(crysp_results.gap_info, CryspGapResults)
    for key, test_value in gapinfo.items():
        assert getattr(crysp_results.gap_info, key) == pytest.approx(
            test_value, rel=1e-2
        )

    # XXX test that values don't change. Note: this calculation is poorly
    # converged. If values change, this maybe be due to the accuracy.
    gsinfo = {'energy': -10.80 if d3 else -10.51}
    things_that_vanish = {
        'magmom': 0.0,
        'magmoms': [0.0, 0.0],
        'forces': np.array(
            [
                [-3.64542492e-21, 2.91633993e-21, 3.64542492e-21],
                [-1.45816997e-21, 1.82271246e-21, 3.28088243e-21],
            ]
        ),
    }
    for key, test_value in gsinfo.items():
        assert getattr(gs_results.properties, key) == pytest.approx(
            test_value, rel=1e-2
        )
    for key, some_zeros in things_that_vanish.items():
        value = getattr(gs_results.properties, key)
        if value is not None:
            assert value == pytest.approx(some_zeros, abs=1e-3)
    assert gs_results.band_info.efermi == pytest.approx(5.569, rel=1e-2)
    assert not gs_results.band_info.is_metallic()
    assert not gs_results.band_info.is_half_metallic()
    assert gs_results.band_info.gap == pytest.approx(2.293, rel=1e-2)
    assert gs_results.band_info.direct_gap == pytest.approx(2.803, rel=1e-2)
    assert gs_results.band_info.optical_gap == pytest.approx(
        gs_results.band_info.direct_gap
    )  # Si is nonmagnetic
    assert gs_results.band_info.half_metallic_gap is None
    assert gs_results.band_info.half_metallic_direct_gap is None
    assert gs_results.band_info.dos_at_efermi == pytest.approx(0.0)
    gap_info = gs_results.band_info.gap_info
    assert not gap_info.is_direct()
    assert gap_info.is_spin_conserving()
    assert not gap_info.is_optical()
    assert gap_info.direct_is_optical()
    assert gap_info.ibzvbm_c == pytest.approx([1 / 4] * 3)  # Γ isn't included
    assert gap_info.ibzcbm_c == pytest.approx([1 / 4, 1 / 4, -1 / 4])
    assert len(gap_info.bzvbm_Kc) == 2
    assert len(gap_info.bzcbm_Kc) == 6

    # Test that we get the same energy from a restarted calculator
    _restart = gpw_file.d3_restart if d3 else gpw_file.restart
    atoms, calc = _restart()
    assert atoms.get_potential_energy() == pytest.approx(gs_results.energy)
    scf_step_taken = False
    try:  # this will fail, if no scf step is taken
        if d3:
            dft = atoms.calc.dft
        else:
            dft = atoms.calc
        dft.get_pseudo_wave_function()
        scf_step_taken = True
    except Exception:
        pass
    assert not scf_step_taken
    # Test that band gap matches the one extracted by ase
    assert abs(gs_results.band_info.gap - bandgap(calc=dft)[0]) < 1e-6
    assert (
        abs(
            gs_results.band_info.direct_gap - bandgap(calc=dft, direct=True)[0]
        )
        < 1e-6
    )


def test_magnetic_gs(run_gs_recipe):
    # Take a magnetic structure without initial magnetic moments
    atoms = bulk('Fe')
    atoms.set_initial_magnetic_moments(None)
    # Run a ground state calculation with the magnetic gs recipe
    gpw_file, results = run_gs_recipe(get_gs_recipe(mag=True), atoms)

    assert results.properties.magmom > 1e-6
    assert results.properties.energy == pytest.approx(10.122, abs=0.05)
    assert results.band_info.efermi == pytest.approx(9.343, abs=0.05)
    assert results.band_info.is_metallic()
    assert not results.band_info.is_half_metallic()
    assert results.band_info.gap is None
    assert results.band_info.half_metallic_gap is None

    # Compare BandInfo to DOS at E_F calculated by conventional means
    atoms, calc = gpw_file.restart()
    efermi = calc.get_fermi_level()
    dos = calc.dos(shift_fermi_level=False)
    dos_efermi_s = [
        dos.raw_dos([efermi], spin=s, width=0.0)[0] for s in range(2)
    ]
    assert results.band_info.dos_efermi_s == pytest.approx(dos_efermi_s)
    dos_at_efermi = dos.raw_dos([efermi], width=0.0)[0]
    assert results.band_info.dos_at_efermi == pytest.approx(dos_at_efermi)
    assert results.band_info.dos_at_efermi == pytest.approx(0.0917, rel=1e-2)
