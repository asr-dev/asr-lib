from asrlib.utils import forward_properties


@forward_properties('wrappee', ['hello1', 'hello2'])
class Wrapper:
    def __init__(self, obj):
        self.wrappee = obj

    def hello3(self):
        return 'hello three'


class Wrappee:
    def hello1(self):
        return 'hello one'

    def hello2(self):
        return 'hello two'


def test_forward_properties():
    thing = Wrapper(Wrappee())
    assert thing.hello1.__name__ == 'hello1'
    assert thing.hello1() == 'hello one'
    assert thing.hello2.__name__ == 'hello2'
    assert thing.hello2() == 'hello two'
    assert thing.hello3.__name__ == 'hello3'
    assert thing.hello3() == 'hello three'
