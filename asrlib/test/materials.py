"""
Module containing a set of test materials.

Important module variables:

  - ``all_test_materials``: List of all test materials in this module.
"""

import numpy as np
from ase import Atoms
from ase.build import bulk

# Make some 3D test materials
Si = bulk('Si')
Ag = bulk('Ag')
Ag2 = bulk('Ag').repeat((2, 1, 1))
Fe = bulk('Fe')
Fe.set_initial_magnetic_moments([1])
GaAs = Atoms(
    'GaAs',
    cell=bulk('Ga', 'fcc', a=5.68).cell,
    pbc=True,
    scaled_positions=((0, 0, 0), (0.25, 0.25, 0.25)),
)
MoS2 = Atoms(
    'MoS2',
    cell=[
        [3.1840664646845926, 0.0, 0.0],
        [-1.5920332323422963, 2.757482445754964, 0.0],
        [0.0, 0.0, 18.12711264635152],
    ],
    positions=[
        [0.0, 0.0, 9.06355632],
        [1.59203323, 0.91916082, 10.62711265],
        [1.59203323, 0.91916082, 7.5],
    ],
    pbc=[True, True, False],
)
BN = Atoms(
    symbols='BN',
    pbc=[True, True, True],
    cell=[[2.51, 0, 0], [-1.25, 2.17, 0], [0, 0, 15]],
    scaled_positions=np.array([[0.33, 0.67, 0.5], [0.67, 0.33, 0.5]]),
)
