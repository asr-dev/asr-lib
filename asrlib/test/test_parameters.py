from dataclasses import KW_ONLY, dataclass, field

import taskblaster as tb

from asrlib.parameters import Parameter


@dataclass
class BaseParameter(Parameter):
    name: str
    _: KW_ONLY
    num: int = 42


@dataclass
class SomeParameter(BaseParameter):
    label: str = 'world'
    content: list[int] = field(default_factory=lambda: [37, 73], kw_only=True)

    def __post_init__(self):
        print(self.default_fields)


def test_parameter_base_class():
    check_instance(
        SomeParameter('hello'), defaults={'label', 'num', 'content'}
    )
    check_instance(
        SomeParameter('hello', 'world'), defaults={'num', 'content'}
    )
    check_instance(
        SomeParameter(name='hello', label='world'), defaults={'num', 'content'}
    )
    check_instance(
        SomeParameter('hello', 'world', num=42, content=[37, 73]),
        defaults=set(),
    )
    check_instance(
        SomeParameter(name='hello', label='world', num=42),
        defaults={'content'},
    )
    check_instance(
        SomeParameter('hello', label='world', content=[37, 73]),
        defaults={'num'},
    )


def check_instance(param: SomeParameter, *, defaults: set):
    # Check values
    assert param.name == 'hello'
    assert param.label == 'world'
    assert param.num == 42
    assert param.content == [37, 73]
    # Check that defaults was detected correctly
    assert param.default_fields == defaults
    # Check that the parameter can be encoded/decoded without the defaults
    dct = param.tb_encode()
    for name in defaults:
        assert name not in dct
    assert SomeParameter.tb_decode(dct) == param


@dataclass
class TwoParameters(Parameter):
    a: int
    b: int = 1


@tb.workflow
class SelfTestingParameterUpdateWorkflow:
    base_params: TwoParameters = tb.var()

    @tb.task
    def check_base(self):
        return tb.node(
            check_base,
            params=self.base_params,
        )

    @tb.task
    def check_a1(self):
        return tb.node(
            check_a1,
            params=self.base_params.new_with(a=1),
        )

    @tb.task
    def check_b2(self):
        return tb.node(
            check_b2,
            params=self.base_params.new_with(b=2),
        )

    @tb.task
    def check_a2b2(self):
        return tb.node(
            check_a2b2,
            params=self.base_params.new_with(a=2, b=self.check_b2.b),
        )


def check_base(params: TwoParameters):
    assert params.a == 0
    assert params.b == 1


def check_a1(params: TwoParameters):
    assert params.a == 1
    assert params.b == 1


def check_b2(params: TwoParameters) -> TwoParameters:
    assert params.a == 0
    assert params.b == 2
    return params


def check_a2b2(params: TwoParameters):
    assert params.a == 2
    assert params.b == 2


def test_parameter_updates(run_workflow, check_tb_tasks):
    wf = SelfTestingParameterUpdateWorkflow(
        base_params=TwoParameters(a=0),
    )
    run_workflow(wf)
    check_tb_tasks(ntasks=4)
