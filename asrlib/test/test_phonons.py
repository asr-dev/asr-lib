import numpy as np
import pytest

from asrlib.parameters.gpaw import GPAWCalculatorFactory
from asrlib.recipes.future.phonons import (
    calculate_force_constants,
    generate_displacements,
    phonon_image_force,
)

test_images = {False: 4, 'auto': 8}


@pytest.fixture
def phonopy():
    return pytest.importorskip('phonopy')


@pytest.mark.parametrize(
    ['d3', 'is_plusminus'], [[False, False], [True, 'auto']]
)
def test_generate_displacements_calc_phonons(
    runtask, newrepo, d3, phonopy, asrlib_test_materials, is_plusminus
):
    # Call generate_displacements and get_phonon_forces
    disp_record = runtask(
        generate_displacements,
        atoms=asrlib_test_materials['BN'],
        phonopy_constructor={
            'supercell_matrix': [[1, 0, 0], [0, 1, 0], [0, 0, 1]]
        },
        disp_kwargs={'distance': 0.01, 'is_plusminus': is_plusminus},
    )

    # phonopy calculation parameters
    params = {
        'mode': {'name': 'pw', 'ecut': 150},
        'txt': 'phonons.txt',
        'xc': 'PBE',
        'kpts': {'size': [1, 1, 1], 'gamma': True},
    }

    calc_factory = GPAWCalculatorFactory(params=params)
    forces = {}  # construct forces dictionary for testing dynamical task
    for idx, image in enumerate(disp_record.output.images):
        force = phonon_image_force(
            image=image, calc_factory=calc_factory, d3=d3
        )
        assert isinstance(force, np.ndarray)
        forces[f'image_{idx}'] = force

        # skip computing all forces in d3 - it's slow and redundant
        if d3:
            break

    # validate that we generate the number of structures we think we do
    assert test_images[is_plusminus] == len(disp_record.output.images)

    # mock a return of a dynamical workflow generator task and check task
    if not d3:
        record = runtask(
            calculate_force_constants,
            phonopy_disp=disp_record.output.phonopy_disp,
            image_forces=forces,
            force_constants_kwargs={'calculate_full_force_constants': False},
        )
        assert record.output.is_file()
