from __future__ import annotations

from dataclasses import dataclass

from ase import Atoms
from ase.spacegroup.symmetrize import check_symmetry

from asrlib.parameters import Parameter
from asrlib.recipes.preconditioners.distort import break_symmetry
from asrlib.spglib import check_layer_symmetry


@dataclass
class AtomsTestData(Parameter):
    atoms: Atoms
    spgrp: int
    min_spgrp: int
    lagrp: int | None = None

    def __post_init__(self):
        symcheck = check_symmetry(self.atoms)
        assert symcheck.number == self.spgrp
        if self.lagrp:
            assert check_layer_symmetry(self.atoms)['number'] == self.lagrp

    def unpack(self):
        return self.atoms, self.spgrp, self.min_spgrp

    def to_dict(self):
        return self.nondefault_fields_dict()

    def break_symmetry(self) -> BrokenAtomsTestData:
        return BrokenAtomsTestData(
            atoms=break_symmetry(self.atoms),
            spgrp=self.spgrp,
            min_spgrp=self.min_spgrp,
            lagrp=self.lagrp,
        )

    @property
    def international_symbol(self):
        return {
            187: 'P-6m2',
            225: 'Fm-3m',
            227: 'Fd-3m',
            229: 'Im-3m',
        }[self.spgrp]

    @property
    def point_group(self):
        return {
            187: '-6m2',
            225: 'm-3m',
            227: 'm-3m',
            229: 'm-3m',
        }[self.spgrp]

    @property
    def has_inversion_symmetry(self):
        return {
            187: False,
            225: True,
            227: True,
            229: True,
        }[self.spgrp]

    @property
    def international_layer_group_symbol(self):
        return (
            {
                78: 'p-6m2',
            }[self.lagrp]
            if self.lagrp
            else None
        )

    @property
    def layer_bravais_type(self):
        return (
            {
                78: 'Hexagonal (hp)',
            }[self.lagrp]
            if self.lagrp
            else None
        )


class BrokenAtomsTestData(AtomsTestData):
    def __post_init__(self):
        assert check_symmetry(self.atoms).number == self.min_spgrp
        if self.lagrp:
            assert check_layer_symmetry(self.atoms)['number'] == self.min_spgrp
