import pytest

from asrlib.test.conftest import RunTaskNameConflict


def add(a, b):
    with open('somefile.txt', 'w') as fd:
        fd.write(f'hello {a} + {b}')
    return a + b


def test_run_task(runtask):
    # Run task once
    record = runtask(add, a=2, b=3)
    assert record.output == 5
    assert (record.directory / 'somefile.txt').read_text() == 'hello 2 + 3'
    # Run task twice
    with pytest.raises(RunTaskNameConflict):
        # To run the same target twice, we need to take control over task names
        record = runtask(add, a=3, b=4)
    record = runtask(add, taskname='my_second_add_task', a=3, b=4)
    assert record.output == 7
    assert (record.directory / 'somefile.txt').read_text() == 'hello 3 + 4'
