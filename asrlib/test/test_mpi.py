import taskblaster as tb


def test_mpi(runtask, resolve_mpirun_params):
    """Test that TB can see GPAW communicator and pass it to tasks."""
    runtask(mpi_task, subworker_count=resolve_mpirun_params[1])


@tb.mpi
def mpi_task(mpi, subworker_count):
    from gpaw.mpi import world

    world_size = world.size // subworker_count
    comm = mpi.comm
    ranksum_expected = (world_size - 1) * world_size // 2
    # Sanity check that the GPAW communicator works:
    ranksum_obtained = comm.sum_scalar(comm.rank)
    assert ranksum_obtained == ranksum_expected
