from dataclasses import dataclass
from functools import cached_property

import numpy as np
import pytest
import taskblaster as tb
from ase.build import bulk
from ase.spacegroup.symmetrize import check_symmetry

from asrlib import recipe_node
from asrlib.recipes.preconditioners.distort import break_symmetry
from asrlib.recipes.symmetrize import ASESymmetrizeRecipe
from asrlib.recipes.symmetrize.ase import (
    LogspaceSymmetryAnalyzer,
)
from asrlib.recipes.symmetrize.ase.results import (
    SymmetryAnalysis,
    plot_symmetries,
)
from asrlib.test.data import AtomsTestData, BrokenAtomsTestData


def generate_symmetric_structures():
    """Test set of symmetric strucutures and space groups."""
    return [
        # (atoms, space group, minimal space group)
        AtomsTestData(bulk('Fe'), 229, 2),  # single atom in the unit cell
        AtomsTestData(bulk('Si'), 227, 2),  # two identical atoms in the cell
        AtomsTestData(bulk('Fe').repeat((3, 1, 1)), 229, 1),
        AtomsTestData(bulk('NaCl', 'rocksalt', a=5.64), 225, 1),
    ]


@pytest.mark.parametrize('structure_data', generate_symmetric_structures())
def test_break_symmetry(in_tmp_dir, structure_data):
    atoms, spgrp, min_spgrp = structure_data.unpack()
    dataset = check_symmetry(atoms)
    assert dataset.number == spgrp
    new_atoms = break_symmetry(atoms)
    assert new_atoms is not atoms
    new_dataset = check_symmetry(new_atoms)
    assert new_dataset.number == min_spgrp


@dataclass
class RandomizedSymmetryAnalyzer(LogspaceSymmetryAnalyzer):
    seed: int = 42

    def generate_symprecs(self):
        symprecs = super().generate_symprecs()
        self.rng.shuffle(symprecs)
        return symprecs

    @cached_property
    def rng(self):
        return np.random.default_rng(self.seed)


@pytest.mark.parametrize('structure_data', generate_symmetric_structures())
def test_parametric_symmetrization(in_tmp_dir, structure_data):
    atoms, spgrp, min_spgrp = structure_data.unpack()
    broken_atoms = break_symmetry(atoms)
    analyzer = LogspaceSymmetryAnalyzer.build(max_symprec=0.1, nsymprecs=21)
    result = analyzer(broken_atoms)
    # Check that we can (json) encode/decode the symmetry analysis object
    encoded_result = result.tb_encode()
    symmetry_analysis = SymmetryAnalysis.tb_decode(encoded_result)
    # Check that symmetrization ranges between the minimal space group and the
    # original space group
    assert symmetry_analysis.space_group(symprec=0.001) == min_spgrp
    assert (
        symmetry_analysis.space_group(symprec=analyzer.symprecs[-1]) == spgrp
    )
    # Check that we can interpolate the symmetry precision, resulting in space
    # groups of ascending order
    symprecs = np.logspace(-3, -1, 41)
    space_groups = symmetry_analysis.get_space_groups(symprecs)
    assert all(space_groups[:-1] - space_groups[1:] <= 0.0)
    # Check internal space group consistency of the symmetrized atoms
    symmetrizable = symmetry_analysis.get_symmetrizability(symprecs)
    for symprec in symprecs[symmetrizable]:
        symatoms = symmetry_analysis.symmetrized_atoms(symprec)
        assert check_symmetry(
            symatoms
        ).number == symmetry_analysis.space_group(symprec)
    # Check that the resulting symmetry analysis is the same, when symprecs are
    # given in a random order
    new_analyzer = RandomizedSymmetryAnalyzer.build(
        max_symprec=0.1, nsymprecs=21
    )
    assert not all(new_analyzer.symprecs[:-1] - new_analyzer.symprecs[1:] <= 0)
    new_symmetry_analysis = new_analyzer(broken_atoms)
    assert (
        abs(symmetry_analysis.symprec_max - new_symmetry_analysis.symprec_max)
        < 1e-10
    )
    assert len(symmetry_analysis.datasets) == len(
        new_symmetry_analysis.datasets
    )
    for dataset, new_dataset in zip(
        symmetry_analysis.datasets, new_symmetry_analysis.datasets
    ):
        assert dataset.space_group == new_dataset.space_group
        if dataset.is_symmetrizable:
            assert new_dataset.is_symmetrizable
            # Compare atoms
            atoms = dataset.atoms
            natoms = new_dataset.atoms
            assert all(
                [
                    a == b
                    for (a, b) in zip(
                        atoms.get_chemical_symbols(),
                        natoms.get_chemical_symbols(),
                    )
                ]
            )
            assert np.allclose(atoms.get_cell().array, natoms.get_cell().array)
            assert np.allclose(atoms.get_positions(), natoms.get_positions())
        else:
            assert not new_dataset.is_symmetrizable
    # Check that the symmetry analysis can be plotted
    filename = atoms.get_chemical_formula() + f'_{spgrp}_broken_symmetry.png'
    plot_symmetries(symmetry_analysis, filename=filename)


class Dataset(BrokenAtomsTestData):
    def check(self, symresults):
        # We do not declare the symeresults type, because it uses
        # forward_properties() which cannot be statically type checked.
        assert symresults.is_symmetrizable
        assert check_symmetry(symresults.atoms).number == self.spgrp
        assert symresults.space_group == self.spgrp
        assert symresults.international_symbol == self.international_symbol
        assert symresults.point_group == self.point_group
        assert symresults.has_inversion_symmetry is self.has_inversion_symmetry
        return 'great success!'


@tb.workflow
class SelfTestingSymmetrizeWorkflow:
    data: Dataset = tb.var()
    recipe: ASESymmetrizeRecipe = tb.var()

    @tb.task
    def symmetrize(self):
        return recipe_node('run', self.recipe, atoms=self.data.atoms)

    @tb.task
    def test(self):
        return recipe_node('check', self.data, symresults=self.symmetrize)


@pytest.mark.parametrize('structure_data', generate_symmetric_structures())
def test_symmetrize_workflow(structure_data, run_workflow, check_tb_tasks):
    # Set up test workflow with symmetry broken atoms
    wf = SelfTestingSymmetrizeWorkflow(
        data=Dataset(**structure_data.break_symmetry().to_dict()),
        recipe=ASESymmetrizeRecipe(
            analyzer=LogspaceSymmetryAnalyzer.build(nsymprecs=50),
            symprec=0.1,
        ),
    )
    # Run and check that all tasks runs succesfully (including wf.test)
    run_workflow(wf)
    check_tb_tasks(ntasks=2)
