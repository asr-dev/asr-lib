import numpy as np
import pytest
from ase.build import bulk

from asrlib.recipes.preconditioners.magnetize import Magnetizer


def generate_test_set():
    """Tuples of (spec, magmoms) with input and expected output."""
    return [
        # test default value
        (None, [1.0] * 2),
        # test nondefault float
        (5.0, [5.0] * 2),
        # test species dict
        ({'Si': 2.0, 'C': 5.0}, [2.0, 5.0]),
        # test as list
        ([1.0, 0.5], [1.0, 0.5]),
        # test as array
        (np.array([0.0, 3.0]), [0.0, 3.0]),
    ]


@pytest.mark.parametrize('spec,magmoms', generate_test_set())
def test_magnetize(spec, magmoms):
    atoms = bulk('SiC', crystalstructure='zincblende', a=4.0)
    magnetizer = Magnetizer.build() if spec is None else Magnetizer(spec)
    atoms = magnetizer(atoms)
    assert list(atoms.get_initial_magnetic_moments()) == magmoms
