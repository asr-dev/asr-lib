from __future__ import annotations

from dataclasses import dataclass

import numpy as np
import pytest

from asrlib.parameters import Parameter
from asrlib.recipes.magstate import (
    MagneticStateClassifier,
    MagneticVSNonmagneticStateClassifier,
)


@dataclass
class MagProps:
    magmom: float
    magmoms: np.ndarray


def generate_magprops_test_set():
    """Generate different combinations of magnetic properties and expected
    classification according to input tolerance."""
    return [
        # (magprops, tol, (spinpol, name))
        (MagProps(0.0, np.array([0.0])), 0.1, (False, 'nm')),
        (MagProps(0.4, np.array([0.2, 0.2])), 0.3, (False, 'nm')),
        (MagProps(0.4, np.array([0.2, 0.2])), 0.1, (True, 'fm')),
        (MagProps(0.2, np.array([0.4, -0.4])), 0.3, (True, 'afm')),
        (MagProps(0.2, np.array([0.5, -0.3])), 0.1, (True, 'fm')),
    ]


@pytest.mark.parametrize('magprops,tol,ref', generate_magprops_test_set())
def test_magnetic_state_classifier(magprops, tol, ref):
    classifier = MagneticStateClassifier(tol)
    classification = classifier.classify(magprops)
    check_classification(magprops, classification, ref)


def check_classification(props, classification, ref):
    spinpol, name = ref
    assert classification.spinpol is spinpol
    if spinpol:
        assert classification.magmom is props.magmom
        assert classification.magmoms is props.magmoms
    else:
        assert classification.magmom is None
        assert classification.magmoms is None
    assert classification.name == name


@dataclass
class StateProps(Parameter):
    energy: float
    magmom: float | None
    magmoms: np.ndarray | None

    @staticmethod
    def from_magprops(*, energy: float, magprops: MagProps):
        return StateProps(
            energy=energy, magmom=magprops.magmom, magmoms=magprops.magmoms
        )


@pytest.mark.parametrize('magprops,tol,ref', generate_magprops_test_set())
def test_magnetic_vs_nonmagnetic_state_classifier(magprops, tol, ref):
    classifier = MagneticVSNonmagneticStateClassifier.build(tol)
    stateprops = StateProps.from_magprops(energy=0.0, magprops=magprops)
    if ref[0]:  # spinpol is True
        # Make a nonmag reference with higher and lower energy respectively
        for energy, ref in zip([1.0, -1.0], [ref, (False, 'nm')]):
            nonmag_props = StateProps(
                energy=energy,
                magmom=0.0,
                magmoms=np.array([0.0] * len(magprops.magmoms)),
            )
            classification = classifier.classify(stateprops, nonmag_props)
            check_classification(stateprops, classification, ref)
    else:
        classification = classifier.classify(stateprops)
        check_classification(stateprops, classification, ref)
