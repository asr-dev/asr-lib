from dataclasses import dataclass, field
from pathlib import Path

import numpy as np
import pytest
from ase.build import bulk
from ase.optimize.optimize import DEFAULT_MAX_STEPS

from asrlib.actions import write_output_atoms_to_json
from asrlib.parameters.gpaw import GPAWCalculatorFactory
from asrlib.recipes.preconditioners import Magnetizer
from asrlib.recipes.relax import ASERelaxRecipe
from asrlib.recipes.relax.ase import CellAwareBFGSRelaxRecipe
from asrlib.recipes.relax.ase.cell_filter import (
    FrechetCellFilterFactory,
    UnitCellFilterFactory,
    pbc_smask_factory,
)
from asrlib.recipes.relax.ase.results import GPAWRelaxResults
from crysp.tasks.factory import CryspCalculatorFactory


@dataclass
class RelaxTestRecipe(CellAwareBFGSRelaxRecipe):
    """Simulate a project level recipe, which defines its own defaults."""

    preconditioner: None = None
    d3: bool = False
    trajectory: str = 'relax.traj'
    logfile: str = 'relax.log'
    append_trajectory: bool = False
    fmax: float = 0.05
    steps: int = DEFAULT_MAX_STEPS
    cell_filter_factory: UnitCellFilterFactory = field(
        default_factory=FrechetCellFilterFactory
    )
    smax: float = 0.005


def run_relaxation(atoms, recipe, run_recipe_task, taskname):
    record = run_recipe_task(
        'calculate', recipe, pass_comm=True, taskname=taskname, atoms=atoms
    )
    opt, path = record.output, record.directory
    assert opt.traj.name == f'{taskname}.traj'
    assert (path / opt.traj.name).exists()

    return record.output


def test_gpaw_relax(newrepo, exec_dftd3, run_recipe_task):
    atoms = bulk('Si')
    atoms.rattle(0.02)
    calc_params = {
        'mode': {'name': 'pw', 'ecut': 200},
        'xc': 'PBE',
        'kpts': {'density': 1, 'symmetry': False},
        'txt': 'relax.txt',
    }

    # Run vanilla relaxation
    taskname = 'vanilla'
    recipe = ASERelaxRecipe.bfgs_relax_cell_and_positions(
        calc_factory=CryspCalculatorFactory(calc_params), identifier=taskname
    )
    opt = run_relaxation(atoms, recipe, run_recipe_task, taskname)
    result_pbe = run_recipe_task('results', recipe, raw_output=opt).output

    # check the calculation is converged
    assert 0.05 > np.linalg.norm(result_pbe.forces, axis=1).max()
    assert 0.005 > result_pbe.stress.max()
    assert result_pbe.energy == pytest.approx(-10.813067, 1e-3)

    # Run d3 relaxation
    taskname = 'd3'
    recipe = ASERelaxRecipe.bfgs_relax_cell_and_positions(
        calc_factory=recipe.calc_factory, d3=True, identifier=taskname
    )
    opt_d3 = run_relaxation(
        result_pbe.atoms, recipe, run_recipe_task, taskname
    )
    result_d3 = run_recipe_task(
        'results', recipe, raw_output=opt_d3, taskname='d3.results'
    ).output
    result_d30 = GPAWRelaxResults.from_trajectory(traj=opt_d3.traj, im=0)

    # check that the d3 is added to energy (making energy more negative)
    assert -0.29105 == pytest.approx(
        result_d30.energy - result_pbe.energy, 1e-2
    )
    # test that the forces and stress are updated with the correction
    assert result_d30.forces != pytest.approx(result_pbe.forces)
    assert result_d30.stress.max() > result_pbe.stress.max()

    # check the calculation converges
    assert 0.05 > np.linalg.norm(result_d3.forces, axis=1).max()
    assert 0.005 > result_d3.stress.max()


def test_magnetic_relax(run_recipe_task):
    # Take a magnetic system without initial magnetic moments
    atoms = bulk('Fe')
    atoms.set_initial_magnetic_moments(None)
    # Build relax recipe which preconditions the atoms with magnetic moments
    recipe = ASERelaxRecipe.bfgs_relax_cell_and_positions(
        calc_factory=GPAWCalculatorFactory(
            dict(
                mode={'name': 'pw', 'ecut': 250},
                kpts={'density': 1.5},
                txt='relax.txt',
            )
        ),
        preconditioner=Magnetizer.build(),
        smax=0.1,
    )
    # Run relaxation
    record = run_recipe_task('calculate', recipe, pass_comm=True, atoms=atoms)
    results = run_recipe_task(
        'results', recipe, raw_output=record.output
    ).output
    # Check that we stabilized a ferromagnetic state
    assert results.properties.magmom == pytest.approx(1.75, abs=0.1)


def test_mace_relax(run_recipe_task, mace_model_in_tree):
    from asrlib.parameters.mace import MACECalculatorFactory

    atoms = bulk('Si')
    # Run relaxation
    recipe = RelaxTestRecipe(
        calc_factory=MACECalculatorFactory(mace_model_in_tree),
        fmax=0.01,
        smax=0.001,
    )
    record = run_recipe_task('calculate', recipe, pass_comm=True, atoms=atoms)
    traj = record.output.traj
    assert traj.name == 'relax.traj' and traj.exists()
    # Produce results
    record = run_recipe_task('results', recipe, raw_output=record.output)
    assert record.output.epa == pytest.approx(-10.7417909 / 2, 1e-3)

    # test an action
    write_output_atoms_to_json(record)
    assert Path(record.directory / 'structure.json').exists()


def generate_pbc_smask_reference():
    return [  # (pbc_c, smask)
        ([1, 1, 1], [1, 1, 1, 1, 1, 1]),
        ([1, 1, 0], [1, 1, 0, 0, 0, 1]),
        ([1, 0, 1], [1, 0, 1, 0, 1, 0]),
        ([0, 1, 1], [0, 1, 1, 1, 0, 0]),
        ([1, 0, 0], [1, 0, 0, 0, 0, 0]),
        ([0, 1, 0], [0, 1, 0, 0, 0, 0]),
        ([0, 0, 1], [0, 0, 1, 0, 0, 0]),
        ([0, 0, 0], [0, 0, 0, 0, 0, 0]),
    ]


@pytest.mark.parametrize('pbc_c, ref_smask', generate_pbc_smask_reference())
def test_pbc_smask_factory(pbc_c, ref_smask):
    atoms = bulk('Si')
    atoms.set_pbc(pbc_c)
    smask = pbc_smask_factory(atoms)
    assert np.all(smask == ref_smask)
