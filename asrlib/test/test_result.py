from asrlib.results import StandardResult


def test_results():
    testdict = {'zero': 0, 'one': 1, 'two': 2}
    test_result = StandardResult(**testdict)
    assert test_result.zero == 0
    assert test_result.one == 1
    assert test_result.two == 2

    encoded = test_result.tb_encode()
    assert encoded['zero'] == 0
    decoded = StandardResult.tb_decode(encoded)
    assert decoded == test_result

    # test nested dict
    nested_dict = {'A': {'B': 0}}
    test_result = StandardResult(**nested_dict)
    assert test_result.A.B == 0
    encoded = test_result.tb_encode()
    assert '_recursive' not in encoded.keys()
    assert encoded['A']['B'] == 0
    decoded = StandardResult.tb_decode(encoded)
    assert decoded == test_result
    test_result2 = StandardResult(**nested_dict, recursive=False)
    assert test_result2.A['B'] == 0
    assert isinstance(test_result2.A, dict)
