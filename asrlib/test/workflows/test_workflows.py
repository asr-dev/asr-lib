"""
Here we test workflows in the 2 ways in which you can give them to taskblaster,
namely, called via a tb workflow script.py with script.py having
def workflow(material) which runs on a parameterization
"""

from pathlib import Path

import pytest
import taskblaster as tb

from asrlib import recipe_node
from asrlib.parameters.gpaw import GPAWCalculatorFactory
from asrlib.recipes.gs import GPAWGroundStateRecipe
from asrlib.recipes.relax import ASERelaxRecipe
from crysp.tasks.factory import CryspCalculatorFactory

calc_params = {
    'mode': {'name': 'pw', 'ecut': 150},
    'xc': 'PBE',
    'kpts': {'density': 1, 'symmetry': False},
    'txt': 'gpaw.txt',
    'parallel': {'band': 1},
    'occupations': {'name': 'fermi-dirac', 'width': 0.05},
}
calc_factory = CryspCalculatorFactory(calc_params)
relax_recipe = ASERelaxRecipe.bfgs_relax_positions(
    calc_factory=calc_factory, fmax=0.5, steps=40
)
gs_recipe = GPAWGroundStateRecipe.build(calc_factory=calc_factory)


def mats_from_db(db: Path, **kwargs):
    structures = {}
    from ase.db import connect

    with connect(db) as con:
        for idx, row in enumerate(con.select(**kwargs)):
            atoms = row.toatoms()
            structures[f'{atoms.symbols}_{idx}'] = atoms

    return structures


@pytest.fixture
def totree(newrepo, asrlib_database_path):
    selections = ['Si2', 'Ag', 'Fe']

    def materials_filter(row):
        return row.formula in selections

    rn = newrepo.runner()
    with newrepo:
        tb.totree(
            mats_from_db(asrlib_database_path, filter=materials_filter),
            name='material',
        )(rn)


@tb.workflow
class DummyIntegrationWorkflow:
    atoms = tb.var()
    relax_recipe = tb.var()

    @tb.task
    def test_relax(self):
        return recipe_node(
            'calculate', self.relax_recipe, pass_comm=True, atoms=self.atoms
        )


# test declaration of a workflow class
def test_wf_integration(
    asrlib_test_materials,  # run on a material
    run_workflow,
    check_tb_tasks,
):
    atoms = asrlib_test_materials['Si']
    workflow = DummyIntegrationWorkflow(
        atoms=atoms,
        relax_recipe=relax_recipe,
    )

    # 1 task
    run_workflow(workflow)
    check_tb_tasks(ntasks=1)


# test the declaration of a workflow script (where workflow() is called)
def test_wf_relax(
    totree,  # run on a tree of different materials
    tbrun_command,  # run also in parallel
    run_workflow,
    check_tb_tasks,
):
    from asrlib.workflows.relax.iterate_magmoms import (
        RelaxToGSWithOrWithoutMagmoms,
    )

    @tb.parametrize_glob('*/material')
    def workflow(material):
        return RelaxToGSWithOrWithoutMagmoms(
            atoms=material,
            mag_relax_recipe=relax_recipe,
            nm_relax_recipe=relax_recipe,
            gs_recipe=gs_recipe,
            magmoms=1.0,
        )

    run_workflow(workflow)
    check_tb_tasks(ntasks={'done': 12, 'new': 9})

    run_workflow(workflow)
    check_tb_tasks(ntasks=25)


def test_phonons(
    asrlib_test_materials,  # run on a materials
    run_workflow,
    check_tb_tasks,
):
    from asrlib.workflows.future.phonons import PhononWf

    material = asrlib_test_materials['Si']
    param = calc_params.copy()
    param.update({'kpts': {'size': [1, 1, 1], 'gamma': True}})
    phonopy_constructor = {
        'supercell_matrix': [[1, 0, 0], [0, 1, 0], [0, 0, 1]]
    }
    disp_kwargs = {'distance': 0.01, 'is_plusminus': False}
    workflow = PhononWf(
        atoms=material,
        calc_factory=GPAWCalculatorFactory(params=param),
        phonopy_constructor=phonopy_constructor,
        disp_kwargs=disp_kwargs,
    )

    run_workflow(workflow)
    check_tb_tasks(ntasks=5)
