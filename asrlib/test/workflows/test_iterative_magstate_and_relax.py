from __future__ import annotations

from dataclasses import dataclass

import numpy as np
import pytest
import taskblaster as tb
from ase import Atoms
from ase.build import bulk

from asrlib import recipe_node
from asrlib.parameters import Parameter
from asrlib.parameters.gpaw import GPAWCalculatorFactory
from asrlib.recipes import StandardASERecipe
from asrlib.recipes.gs import GPAWGroundStateRecipe
from asrlib.recipes.preconditioners import Magnetizer, StructureNormalizer
from asrlib.recipes.preconditioners.distort import break_symmetry
from asrlib.recipes.relax import ASERelaxRecipe
from asrlib.test.test_magstate import StateProps
from asrlib.typing import TBCommunicator
from asrlib.workflows.relax.iterate_magmoms import (
    IterativelyDetermineMagneticStateAndRelax,
)


@dataclass
class Checker(Parameter):
    expected_initial_magmoms: np.ndarray
    expected_initial_spinpol: bool
    expected_final_magmoms: np.ndarray
    expected_final_spinpol: bool
    expected_epa: float

    def run(
        self,
        initial_atoms: Atoms,
        initial_relax_recipe: ASERelaxRecipe,
        initial_spinpol: bool,
        final_magmoms: np.ndarray,
        final_spinpol: bool,
        relaxed_epa: float,
    ) -> str:
        # Please note that the check of matching magmoms implicitly also checks
        # whether the number of atoms is as expected
        initial_magmoms = initial_relax_recipe.precondition(
            initial_atoms,
        ).get_initial_magnetic_moments()
        assert np.allclose(
            initial_magmoms - self.expected_initial_magmoms, 0.0
        )
        assert initial_spinpol == self.expected_initial_spinpol
        assert np.allclose(final_magmoms - self.expected_final_magmoms, 0.0)
        assert final_spinpol == self.expected_final_spinpol
        assert relaxed_epa == pytest.approx(self.expected_epa, abs=1e-2)
        return 'all good!'


@tb.workflow
class SelfTestingMagstateAndRelaxWf(IterativelyDetermineMagneticStateAndRelax):
    checker: Checker = tb.var()

    @tb.branch('final')
    @tb.task
    def test(self):
        return recipe_node(
            'run',
            self.checker,
            initial_atoms=self.atoms,
            initial_relax_recipe=self.relax_recipe_from(
                classification=self.initial_state.classification
            ),
            initial_spinpol=self.initial_state.classification.spinpol,
            final_spinpol=self.results.classification.spinpol,
            final_magmoms=self.results.atoms.get_initial_magnetic_moments(),
            relaxed_epa=self.results.relaxation.epa,
        )


def h2_molecular_crystal():
    # Data from OQMD relaxation
    return Atoms(
        symbols='H2',
        cell=[
            [0.0, 0.0, 2.791],
            [1.517, 1.517, -1.395],
            [1.517, -1.517, 1.395],
        ],
        positions=[[0.1341, 0.0, 0.0], [0.8659, 0.0, 0.0]],
        pbc=True,
    )


@pytest.mark.parametrize('d3', [False, True])
def test_gpaw_iterative_magstate_and_relax(d3, run_workflow, check_tb_tasks):
    atoms = h2_molecular_crystal()

    calc_factory = GPAWCalculatorFactory(
        dict(
            xc='PBE',
            mode={'name': 'pw', 'ecut': 200},
            kpts=(2, 2, 2),
            txt='gpaw.out',
        ),
    )

    # We repeat the atoms and rely on the structure normalizer to reduce them
    # back to the primitive cell
    atoms = atoms.repeat((1, 1, 2))
    preconditioner = StructureNormalizer.build()

    wf = SelfTestingMagstateAndRelaxWf(
        atoms=atoms,
        relax_recipe=ASERelaxRecipe.bfgs_relax_cell_and_positions(
            calc_factory, preconditioner=preconditioner, steps=3, d3=d3
        ),
        gs_recipe=GPAWGroundStateRecipe.build(
            calc_factory, preconditioner=preconditioner, d3=d3
        ),
        magstate_magnetizer=Magnetizer(0.5),
        checker=Checker(
            # Check that both the initial and final magstates are spin neutral
            # and that the structure normalization has reduced the number of
            # atoms in the unit cell to 2 (otherwise comparison of magmoms
            # arrays will fail due to mismatching lengths)
            expected_initial_spinpol=False,
            expected_initial_magmoms=np.array([0.0] * 2),
            expected_final_spinpol=False,
            expected_final_magmoms=np.array([0.0] * 2),
            expected_epa=-3.301 - 0.067 * d3,
        ),
    )

    # Because H2 doesn't support finite magnetic moments, we never have to
    # perform extra nonmagnetic reference calculations and only need a single
    # loop of relax_and_magstate -> i.e. the number of actual workflow tasks
    # (12) is known beforehand. The test task itself will not be added before
    # the workflow stops iterating and jumps to the final branch.

    # Run initial_state until and including initial_state.stayed_magnetic
    run_workflow(wf)
    check_tb_tasks(ntasks={'done': 3, 'new': 9})
    # Run initial_state.classification and first iteration of
    # relax_and_magstate until and including
    # relax_and_magstate.magnetic_state.stayed_magnetic
    run_workflow(wf)
    check_tb_tasks(ntasks={'done': 9, 'new': 3})
    # Run relax_and_magstate.magnetic_state.classification and stop_iteration
    run_workflow(wf)
    check_tb_tasks(ntasks={'done': 11, 'new': 1})
    # Run results (fixedpoint) and test
    run_workflow(wf)
    check_tb_tasks(ntasks=13)


@dataclass
class EnergyCorrectionCalculator(Parameter):
    """Calculates an energy correction based on the magnetic moment."""

    e_no_spin: float
    # Use the following parameters, if you want the correction to distinguish
    # between low and high total magnetic moments.
    low_spin_tol: float | None = None  # defines, what is low and high spin
    e_low_spin: float | None = None
    e_high_spin: float | None = None

    def __call__(self, magmom: float) -> float:
        if abs(magmom) < 1e-10:
            return self.e_no_spin
        if self.low_spin_tol is None:
            # If we only need to distinguish between no spin or not, use
            # e_no_spin to control the energy difference
            return 0.0
        assert isinstance(self.e_low_spin, float) and isinstance(
            self.e_high_spin, float
        )
        if magmom < self.low_spin_tol:
            return self.e_low_spin
        return self.e_high_spin


@dataclass
class PseudoGSResults(Parameter):
    properties: StateProps


@dataclass
class PseudoGSRecipe(StandardASERecipe[StateProps]):
    """Pseudo ground state recipe, calculating only the ground state energy.

    By simply passing on the initial magnetic moments directly to the results,
    we can use the MACECalculator for pseudo ground state calculations, while
    gaining full control of the output magnetic moments.

    To make magnetic and nonmagnetic states distinguishable in terms of their
    energy, we add a custom correction to the ground state energy calculated
    based on the total initial magnetic moment.
    """

    correction_calc: EnergyCorrectionCalculator

    def _calculate(self, atoms: Atoms, comm: TBCommunicator) -> StateProps:
        energy = atoms.get_potential_energy()
        magmoms = atoms.get_initial_magnetic_moments()
        magmom = np.sum(magmoms)
        energy += self.correction_calc(magmom)
        return StateProps(energy, magmom, magmoms)

    def results(
        self, raw_output: StateProps, comm: TBCommunicator
    ) -> PseudoGSResults:
        return PseudoGSResults(properties=raw_output)


@pytest.mark.parametrize('looping', [False, True])
def test_mace_iterative_magstate_and_relax(
    looping, run_workflow, check_tb_tasks, mace_model_in_tree
):
    from asrlib.test.workflows.utils import DummyMACECalculatorFactory

    # We repeat the Fe crystal structure and break its symmetry, such that the
    # structure normalization initially cannot recover the monoatomic unit cell
    atoms = bulk('Fe')
    atoms = atoms.repeat((1, 1, 3))
    atoms = break_symmetry(atoms, rstdev=0.05)
    preconditioner = StructureNormalizer.build(symprec=0.1)
    assert len(preconditioner(atoms)) == 3

    calc_factory = DummyMACECalculatorFactory(mace_model_in_tree)
    correction_calc, excecute_wf = looping_parametrization(
        looping, run_workflow, check_tb_tasks
    )

    # We set up the workflow with preconditioners, such that the cell will be
    # reduced to the monoatomic cell, but only *after* the relaxation
    wf = SelfTestingMagstateAndRelaxWf(
        atoms=atoms,
        relax_recipe=ASERelaxRecipe.bfgs_relax_cell_and_positions(
            calc_factory,
            preconditioner=preconditioner,
            fmax=0.001,
            smax=0.0001,
        ),
        gs_recipe=PseudoGSRecipe(
            calc_factory,
            preconditioner=preconditioner,
            correction_calc=correction_calc,
        ),
        magstate_magnetizer=Magnetizer(2.0),
        checker=Checker(
            # Check that there initially are three Fe atoms in the unit
            # cell, but that they have been reduced to a single atom at the
            # end, thanks to the symmetry normalization. When looping is True,
            # we run a second iteration of relax_and_magstate by letting the
            # initial spinpol classification be inconsistent with the one of
            # the relaxed structure
            expected_initial_spinpol=not looping,
            expected_initial_magmoms=np.array([2.0 - 2.0 * looping] * 3),
            expected_final_spinpol=True,
            expected_final_magmoms=np.array([2.0]),
            expected_epa=-8.401,
        ),
    )
    excecute_wf(wf)


def looping_parametrization(looping: bool, run_workflow, check_tb_tasks):
    if looping:
        return looping_correction_and_warden(run_workflow, check_tb_tasks)
    return nonlooping_correction_and_warden(run_workflow, check_tb_tasks)


def nonlooping_correction_and_warden(run_workflow, check_tb_tasks):
    """Run a single iteration of the workflow, resulting in a magnetic state.

    To make both the initial and post-relax classifications magnetic, we define
    an energy correction, which punishes the nonmagnetic state but ignores the
    size of the total magnetic moment and hence the number of Fe atoms.
    """
    # Punish the nonmagnetic state, leaving magnetic state energies unaltered
    correction_calc = EnergyCorrectionCalculator(e_no_spin=1.0)

    # Since we here manage to stabilize finite magnetic moments always (output
    # magmoms are identical to the input magmoms), the workflow cannot predict
    # the number of tasks beforehand, but adds instead 2 nonmagnetic_state
    # tasks at the end of calls to the MagneticStateWorkflow.

    def excecute_wf(wf):
        # Run initial_state until and including initial_state.stayed_magnetic
        run_workflow(wf)
        check_tb_tasks(ntasks={'done': 3, 'new': 9})
        # Run initial_state's nonmagnetic_state (+2) and classification along
        # with the first iteration of relax_and_magstate until and including
        # relax_and_magstate.magnetic_state.stayed_magnetic
        run_workflow(wf)
        check_tb_tasks(ntasks={'done': 11, 'new': 3})
        # Run relax_and_magstate.magnetic_state's nonmagnetic_state (+2) and
        # classification along with stop_iteration on the main workflow
        run_workflow(wf)
        check_tb_tasks(ntasks={'done': 15, 'new': 1})
        # Run results (fixedpoint) and test
        run_workflow(wf)
        check_tb_tasks(ntasks=17)

    return correction_calc, excecute_wf


def looping_correction_and_warden(run_workflow, check_tb_tasks):
    """Run two iterations of the iterative relax workflow.

    We force a second round-trip of the relax_and_magstate loop by punishing
    having a magnetic moment above 3. (i.e. punishing the spin-polarized state
    in the triple atomic unit cell), but rewarding a total magnetic moment
    below 3 (i.e. rewarding spin-polarization of the monoatomic unit cell).
    This way, the initial state (before relaxation) will be characterized as
    nonmagnetic, whereas the state will be classified as magnetic *after*
    relaxation thanks to the recovered symmetries of the crystal.
    """
    correction_calc = EnergyCorrectionCalculator(
        e_no_spin=0.0,  # energy correction of the nonmagnetic state
        low_spin_tol=3.0,  # boundary between low and high spin
        e_low_spin=-1.0,  # reward low spin
        e_high_spin=1.0,  # punish high spin
    )

    # Again, the workflow cannot predict the number of tasks beforehand, but
    # adds instead 2 nonmagnetic_state tasks at the end of calls to the
    # MagneticStateWorkflow, as well as 6 relax_and_magstate tasks once
    # entering the second loop.

    def excecute_wf(wf):
        # Run initial_state until and including initial_state.stayed_magnetic
        run_workflow(wf)
        check_tb_tasks(ntasks={'done': 3, 'new': 9})
        # Run initial_state's nonmagnetic_state (+2) and classification along
        # with the first iteration of relax_and_magstate until and including
        # relax_and_magstate.magnetic_state.stayed_magnetic
        run_workflow(wf)
        check_tb_tasks(ntasks={'done': 11, 'new': 3})
        # Run relax_and_magstate.magnetic_state's nonmagnetic_state (+2) and
        # classification along with stop_iteration on the main workflow
        run_workflow(wf)
        check_tb_tasks(ntasks={'done': 15, 'new': 1})
        # Enter the second iteration of relax_and_magstate (+7), perform
        # relaxation along with magnetic_state's trial_magnetic_state and
        # stayed_magnetic
        run_workflow(wf)
        check_tb_tasks(ntasks={'done': 20, 'new': 3})
        # Run relax_and_magstate.magnetic_state's nonmagnetic_state (+2) and
        # classification along with stop_iteration on the main workflow, thus
        # concluding the second iteration of the 'iterate' branch
        run_workflow(wf)
        check_tb_tasks(ntasks={'done': 24, 'new': 1})
        # Run results (fixedpoint) and test
        run_workflow(wf)
        check_tb_tasks(ntasks=26)

    return correction_calc, excecute_wf
