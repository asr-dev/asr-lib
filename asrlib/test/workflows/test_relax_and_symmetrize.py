import pytest
import taskblaster as tb
from ase import Atoms
from ase.calculators.calculator import kptdensity2monkhorstpack
from ase.spacegroup.symmetrize import check_symmetry

from asrlib.parameters.gpaw import GPAWCalculatorFactory
from asrlib.recipes.preconditioners.distort import break_symmetry
from asrlib.recipes.relax import ASERelaxRecipe
from asrlib.recipes.symmetrize import (
    ASESymmetrizeRecipe,
    SymmetrizationResults,
)
from asrlib.recipes.symmetrize.ase import LogspaceSymmetryAnalyzer
from asrlib.test.test_symmetry import generate_symmetric_structures
from asrlib.workflows.relax.and_symmetrize import RelaxAndSymmetrize


@tb.workflow
class SelfTestingRelaxAndSymmetrize(RelaxAndSymmetrize):
    spgrp: int = tb.var()
    min_spgrp: int = tb.var()

    @tb.task
    def test(self):
        return tb.node(
            check_symmetries,
            unrelaxed=self.unrelaxed,
            symresults=self.symmetrize,
            spgrp=self.spgrp,
            min_spgrp=self.min_spgrp,
        )


def check_symmetries(
    unrelaxed: Atoms,
    symresults: SymmetrizationResults,
    spgrp: int,
    min_spgrp: int,
):
    """Check relax and symmetrization procedure."""
    # Check that the unrelaxed atoms were symmetry broken, even with a loose
    # symmetry tolerance of symprec == 0.1
    assert check_symmetry(unrelaxed).number == min_spgrp
    assert check_symmetry(unrelaxed, symprec=0.1).number < spgrp
    # Check that the full symmetry was recovered by the RelaxAndSymmetrize
    # workflow
    assert symresults.space_group == spgrp
    assert check_symmetry(symresults.atoms).number == spgrp


def generate_structures_and_mark():
    return [
        pytest.param(structure_data)
        if len(structure_data.atoms) == 1
        else pytest.param(structure_data, marks=pytest.mark.nightly)
        for structure_data in generate_symmetric_structures()
    ]


@pytest.mark.parametrize('structure_data', generate_structures_and_mark())
def test_relax_and_symmetrize(structure_data, run_workflow, check_tb_tasks):
    atoms, spgrp, min_spgrp = structure_data.unpack()
    # Set up k-point grid
    density = 1.0
    fmax = 0.2
    if len(atoms) == 3:  # 3 x Fe
        fmax = 0.5
    elif spgrp == 225:  # NaCl
        fmax = 0.025  # otherwise no steps are taken
        density = 1.5
    elif spgrp == 227:  # Si
        density = 2.0
    # We make sure that the k-point grid is fixed to the original symmetry in
    # order to make the test run fast
    size = kptdensity2monkhorstpack(atoms, kptdensity=density, even=False)
    wf = SelfTestingRelaxAndSymmetrize(
        unrelaxed=break_symmetry(atoms, rstdev=0.05),
        relax_recipe=ASERelaxRecipe.bfgs_relax_cell_and_positions(
            calc_factory=GPAWCalculatorFactory(
                dict(
                    mode={'name': 'pw', 'ecut': 300},
                    kpts={'size': size},
                    txt='relax.txt',
                )
            ),
            fmax=fmax,
            smax=0.1,
        ),
        symmetrize_recipe=ASESymmetrizeRecipe(
            analyzer=LogspaceSymmetryAnalyzer.build(nsymprecs=25),
            symprec=0.1,
        ),
        spgrp=spgrp,
        min_spgrp=min_spgrp,
    )
    run_workflow(wf)
    check_tb_tasks(ntasks=4)


@pytest.mark.parametrize('structure_data', generate_symmetric_structures())
def test_mace_relax_and_symmetrize(
    structure_data, run_workflow, mace_model_in_tree, check_tb_tasks
):
    from asrlib.parameters.mace import MACECalculatorFactory

    atoms, spgrp, min_spgrp = structure_data.unpack()
    wf = SelfTestingRelaxAndSymmetrize(
        unrelaxed=break_symmetry(atoms, rstdev=0.05),
        relax_recipe=ASERelaxRecipe.bfgs_relax_cell_and_positions(
            calc_factory=MACECalculatorFactory(mace_model_in_tree),
            fmax=0.01,
            smax=0.001,
        ),
        symmetrize_recipe=ASESymmetrizeRecipe(
            analyzer=LogspaceSymmetryAnalyzer.build(nsymprecs=25),
            symprec=0.1,
        ),
        spgrp=spgrp,
        min_spgrp=min_spgrp,
    )
    run_workflow(wf)
    check_tb_tasks(ntasks=4)
