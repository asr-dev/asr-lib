from dataclasses import dataclass

from asrlib.parameters.mace import MACECalculatorFactory


@dataclass
class DummyMACECalculatorFactory(MACECalculatorFactory):
    spinpol: bool = True  # allow, but ignore, updates to spinpol
