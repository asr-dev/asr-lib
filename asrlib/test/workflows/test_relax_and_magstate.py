import pytest
import taskblaster as tb
from ase.build import bulk

from asrlib.recipes.relax import ASERelaxRecipe
from asrlib.test.workflows.test_magstate import (
    LocalGSRecipe,
    check_classification,
)
from asrlib.workflows.relax.and_magstate import RelaxAndDetermineMagneticState


@tb.workflow
class SelfTestingRelaxAndMagstate(RelaxAndDetermineMagneticState):
    expected_volume: float = tb.var()
    expected_magmoms: float = tb.var()

    @tb.task
    def test(self):
        return tb.node(
            check_cell_volume_and_classification,
            atoms=self.relaxation.results.atoms,
            classification=self.magnetic_state.classification,
            expected_volume=self.expected_volume,
            expected_magmoms=self.expected_magmoms,
        )


def check_cell_volume_and_classification(
    atoms, classification, expected_volume: float, expected_magmoms: float
):
    assert atoms.get_volume() == pytest.approx(expected_volume, rel=0.002)
    check_classification(classification, expected_magmoms)


def generate_test_data():
    return [
        # crystal, expected_volume, expected_magmoms, ntasks
        ('Si', 40.787, 0.0, 7),
        ('Fe', 11.713, 2.02, 9),
    ]


@pytest.mark.parametrize('data', generate_test_data())
def test_relax_and_magstate(
    data, mace_model_in_tree, run_workflow, check_tb_tasks
):
    from asrlib.parameters.mace import MACECalculatorFactory

    mat, volume, magmoms, ntasks = data

    wf = SelfTestingRelaxAndMagstate(
        atoms=bulk(mat),
        relax_recipe=ASERelaxRecipe.bfgs_relax_cell_and_positions(
            calc_factory=MACECalculatorFactory(mace_model_in_tree),
            fmax=0.01,
            smax=0.001,
        ),
        gs_recipe=LocalGSRecipe(),
        expected_volume=volume,
        expected_magmoms=magmoms,
    )

    run_workflow(wf)
    check_tb_tasks(ntasks={'done': 5, 'new': 2})
    run_workflow(wf)
    check_tb_tasks(ntasks=ntasks)
