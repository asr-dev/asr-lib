from __future__ import annotations

from dataclasses import dataclass, field

import numpy as np
import pytest
import taskblaster as tb
from ase.build import bulk

from asrlib.parameters.gpaw import GPAWCalculatorFactory
from asrlib.recipes import AtomsPreconditioner
from asrlib.recipes.gs import GPAWGroundStateRecipe
from asrlib.recipes.preconditioners import StructureNormalizer
from asrlib.workflows.magstate import MagneticStateWorkflow


@tb.workflow
class SelfTestingMagneticStateWorkflow(MagneticStateWorkflow):
    expected_magmoms: np.ndarray = tb.var()

    @tb.task
    def test(self):
        return tb.node(
            check_classification,
            classification=self.classification,
            expected_magmoms=self.expected_magmoms,
        )


def check_classification(classification, expected_magmoms: np.ndarray | float):
    if np.allclose(expected_magmoms, 0.0):
        assert not classification.spinpol
        assert classification.magmoms is None
    else:
        assert classification.spinpol
        assert classification.magmoms == pytest.approx(
            expected_magmoms, abs=0.02
        )


def generate_test_data():
    return [
        # crystal, expected_magmoms, ntasks
        ('Si', np.array([0.0] * 2), 5),
        ('Fe', np.array([2.03]), 7),
    ]


@dataclass
class LocalGSRecipe(GPAWGroundStateRecipe):
    """Set defaults to test that the workflow works with a recipe subclass."""

    calc_factory: GPAWCalculatorFactory = field(
        default_factory=lambda: GPAWCalculatorFactory(
            dict(
                mode={'name': 'pw', 'ecut': 250},
                kpts={'density': 1.5},
                txt='gs.txt',
            )
        )
    )
    preconditioner: AtomsPreconditioner = field(
        default_factory=StructureNormalizer.build
    )
    filename: str = 'gs.gpw'
    write_mode: str = ''
    d3: bool = False


@pytest.mark.parametrize('data', generate_test_data())
def test_magstate_wf(data, run_workflow, check_tb_tasks):
    mat, magmoms, ntasks = data
    atoms = bulk(mat)
    # Run workflow on repeated atoms to check that structure normalization
    # works as intended
    rep_atoms = atoms.repeat((1, 1, 2))

    wf = SelfTestingMagneticStateWorkflow(
        atoms=rep_atoms,
        gs_recipe=LocalGSRecipe(),
        expected_magmoms=magmoms,
    )

    run_workflow(wf)
    check_tb_tasks(ntasks={'done': 3, 'new': 2})
    run_workflow(wf)
    check_tb_tasks(ntasks=ntasks)
