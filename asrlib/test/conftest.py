import atexit
import codecs
import json
import os
import pickle
import shutil
import subprocess
import sys
from collections import Counter
from contextlib import contextmanager
from dataclasses import dataclass
from pathlib import Path
from typing import Dict

import pytest
from ase import Atoms
from ase.db import connect
from click.testing import CliRunner
from gpaw.mpi import broadcast, world
from taskblaster.cli import tb as tb_cli
from taskblaster.repository import Repository

from asrlib import (
    AsrLibRepository,
    method_on_recipe_task,
    mpi_method_on_recipe_task,
)

MPIWORKER_INTERNAL_ERROR = 1337


decode_hex = codecs.getdecoder('hex_codec')


def parse_mpirun_params(param_str):
    mpirun_params = tuple(map(int, param_str.split(',')))
    if len(mpirun_params) == 1:
        return (*mpirun_params, 1)

    return mpirun_params


@pytest.fixture
def resolve_mpirun_params(pytestconfig):
    param_str = pytestconfig.getoption('--mpi-args')
    return parse_mpirun_params(param_str)


def pytest_addoption(parser):
    # one needs to set addoption in multiple places to be able to run
    # single/multiple packages, but when running multiple, we create a
    # conflict. Now we check if we set mpi_args, and don't set it again
    # if it is already set.
    for option in parser._anonymous.options:
        if option.dest == 'mpi_args':
            return

    parser.addoption(
        '--mpi-args',
        default='1',
        help=(
            'Run parallel tests. Format of input is'
            '--mpi-args=WORLD_SIZE,SUBWORKER_COUNT.'
        ),
    )


def pytest_report_header(config, start_path):
    mpi_procs, subworker_count = parse_mpirun_params(config.option.mpi_args)
    assert mpi_procs % subworker_count == 0
    if mpi_procs <= 1:
        yield 'Running SERIAL tests.'
    else:
        yield 'Running PARALLEL tests.'
        yield (
            f'world: {mpi_procs} subworker_count: {subworker_count}'
            f' subworker_size: {mpi_procs // subworker_count}'
        )


class TBOutput:
    def __init__(self, output, error, exit_code):
        self.output = output
        self.error = error
        self.exit_code = exit_code


@pytest.fixture(params=[1, 2, 4])
def parallel(request, resolve_mpirun_params):
    mpi_procs, subworker_count = resolve_mpirun_params
    if mpi_procs != request.param:
        pytest.skip(f'Needs pytest --mpi-args={request.param}.')
    return request.param


class MPIWorkerPipe:
    def __init__(self, cores):
        self.cores = cores
        self.handle = subprocess.Popen(
            [
                'mpirun',
                '-np',
                str(cores),
                sys.executable,
                '-m',
                'asrlib.mpiworker',
            ],
            stdout=subprocess.PIPE,
            stdin=subprocess.PIPE,
            env=os.environ,
        )

        def cleanup():
            print('Requesting mpi worker to quit')
            self.send(('quit', ''))
            try:
                self.handle.communicate(timeout=5)
            except subprocess.TimeoutExpired:
                print('Killing mpi worker')
                self.handle.kill()
            else:
                print('mpi worker quit peacefully')

        atexit.register(cleanup)

    def send(self, obj):
        data = json.dumps(obj)
        self.handle.stdin.write(data.encode('utf-8'))
        self.handle.stdin.write('\n'.encode('utf-8'))
        self.handle.stdin.flush()

    def run(self, commands):
        self.send((os.getcwd(), commands))
        data = self.handle.stdout.readline()
        data = data.decode('utf-8').rstrip()
        try:
            data = decode_hex(data)[0]
        except Exception as e:
            pytest.exit(f'{e} occurred when trying to decode {data}')

        result = pickle.loads(data)

        # Internal error by mpiworker
        if result.exit_code == MPIWORKER_INTERNAL_ERROR:
            pytest.exit(result.error)

        return result


demon = None


@pytest.fixture
def tbrun_command(parallel, resolve_mpirun_params):
    _, subworker_count = resolve_mpirun_params

    global demon
    if parallel > 1:
        if demon is None:
            demon = MPIWorkerPipe(parallel)
        else:
            assert demon.cores == parallel

    def run_parallel(args):
        if args[0] == 'run':
            args.append(f'--subworker-count={subworker_count}')
        full_args = ['mpirun', '-np', str(parallel), 'tb', *args]
        return demon.run(full_args)

    return run_parallel if parallel > 1 else run_tb_cli


@pytest.fixture
def tb_submit_run_ls(tbrun_command):
    def _tb_submit_run_ls(verbose=True):
        result_submit = run_tb_cli(args=['submit', '--color=never', 'tree/'])
        result_run = tbrun_command(args=['run', '--color=never', 'tree/'])
        result_ls = run_tb_cli(
            args=['ls', '--color=never', '-c', 's', 'tree/']
        )

        if verbose:
            print(result_submit.output)
            print(result_run.output)
            print(result_ls.output)

        return result_submit, result_run, result_ls

    return _tb_submit_run_ls


@pytest.fixture
def check_tb_tasks(tb_submit_run_ls):
    def _check_tb_tasks(ntasks: int):
        all_results = tb_submit_run_ls()  # click results for submit, run, ls
        for idx, result in enumerate(all_results):
            if result.exit_code != 0:
                raise RuntimeError(result.error)
            # check that the tasks that were run are all successful
            if idx == 2:
                states = get_task_states(result=result.output)
                if isinstance(ntasks, int):
                    assert {'done': ntasks} == Counter(states)
                else:
                    assert ntasks == Counter(states)

    return _check_tb_tasks


def get_task_states(result: str):
    task_states = ('d', 'F', 'q', 'n', 'r', 'p', 'C', 'f')
    lines = [line.strip() for line in result.split('\n') if line.strip()]
    print('lines', lines)
    states = [line for line in lines if line.startswith(task_states)]
    print('task_states states', states)
    return states


def make_database(
    temp_dir: str, db_name: str, test_materials: Dict[str, Atoms]
):
    db_path = os.path.join(temp_dir, db_name)
    db = connect(db_path)
    for idx, mat in enumerate(test_materials):
        # just add some random data for oqmd row objects
        data = {'file.json': {'key': 'value'}, 'uid': 'some-123-uid'}
        kvp = {'oqmd_entry_id': idx}
        db.write(test_materials[mat], key_value_pairs=kvp, data=data)
    return db_path


@pytest.fixture(scope='session')
def asrlib_test_materials():
    from asrlib.test.materials import BN, Ag, Ag2, Fe, GaAs, MoS2, Si

    return dict(Si=Si, Ag=Ag, Fe=Fe, Ag2=Ag2, GaAs=GaAs, MoS2=MoS2, BN=BN)


@pytest.fixture(scope='session')
def asrlib_database_path(tmp_path_factory, asrlib_test_materials):
    database_name = 'database.db'
    temp_dir = tmp_path_factory.mktemp('database')
    db_path = temp_dir / database_name
    make_database(
        temp_dir=temp_dir,
        db_name=database_name,
        test_materials=asrlib_test_materials,
    )
    return str(db_path)


@pytest.fixture
def run_workflow(newrepo):
    def run(workflow):
        rn = newrepo.runner()
        with newrepo:
            if callable(workflow):
                workflow(rn)
            else:
                rn.run_workflow(workflow)
                # unset runner
                workflow._rn = None

    return run


@pytest.fixture
def actually_run_workflow(run_workflow, tb_submit_run_ls):
    def excecute_commands(workflow):
        """Excecute tb commands to run a workflow.

        Equivalent to:
        $ tb workflow <workflow>
        $ tb submit
        $ tb run
        $ tb ls

        Returns the listing output.
        """
        run_workflow(workflow)
        return tb_submit_run_ls()[2]

    return excecute_commands


@pytest.fixture
def newrepo(in_tmp_dir):
    # Unlocked repository
    if world.rank == 0:
        _repo = Repository.create(in_tmp_dir, modulename='asrlib')
        assert isinstance(_repo, AsrLibRepository)
    world.barrier()

    return Repository.find()


@pytest.fixture
def repo(testdir):
    from asrlib import AsrLibRepository

    with AsrLibRepository.create(testdir, modulename='asrlib') as repo:
        yield repo


@pytest.fixture
def testdir(tmp_path):
    """Create and go to temporary directory."""
    cwd = Path.cwd()
    try:
        os.chdir(tmp_path)
        yield tmp_path
    finally:
        os.chdir(cwd)
        print('tmp_path:', tmp_path)


@contextmanager
def execute_in_tmp_path(request, tmp_path_factory):
    if world.rank == 0:
        # Obtain basename as
        # * request.function.__name__  for function fixture
        # * request.module.__name__    for module fixture
        basename = getattr(request, request.scope).__name__
        path = tmp_path_factory.mktemp(basename)
    else:
        path = None
    path = broadcast(path)
    cwd = os.getcwd()
    os.chdir(path)
    try:
        yield path
    finally:
        os.chdir(cwd)


@pytest.fixture(scope='function')
def in_tmp_dir(request, tmp_path_factory):
    """Run test function in a temporary directory."""
    with execute_in_tmp_path(request, tmp_path_factory) as path:
        yield path


@pytest.fixture(scope='module')
def module_tmp_path(request, tmp_path_factory):
    """Run test module in a temporary directory."""
    with execute_in_tmp_path(request, tmp_path_factory) as path:
        yield path


@pytest.fixture(scope='session')
def exec_dftd3():
    dftd3_binary = shutil.which('dftd3')
    if dftd3_binary is None:
        pytest.skip('No "dftd3" binary in PATH')
    return dftd3_binary


def run_tb_cli(args: list):
    runner = CliRunner()
    result = runner.invoke(tb_cli, args, catch_exceptions=False)
    return result


def cli_submit_run_ls(verbose=True):
    result_submit = run_tb_cli(args=['submit', '--color=never', 'tree/'])
    result_run = run_tb_cli(args=['run', '--color=never', 'tree/'])
    result_ls = run_tb_cli(args=['ls', '--color=never', '-c', 's', 'tree/'])

    if verbose:
        print(result_submit.output)
        print(result_run.output)
        print(result_ls.output)

    return result_submit, result_run, result_ls


class RunTaskNameConflict(Exception):
    """Naming conflict between test tasks."""


@dataclass
class Record:
    """Subset of attributes from previous taskblaster "record" object
    that we need in the asrlib tests."""

    output: object
    directory: Path

    def __post_init__(self):
        self.directory = self.directory.resolve()


@pytest.fixture
def runtask(newrepo, tbrun_command):
    from taskblaster import Node
    from taskblaster.namedtask import Task

    print(newrepo.root)

    def runtask(target, *, taskname: str | None = None, **kwargs):
        if not isinstance(target, str):
            # The target input is the task function itself
            target = f'{target.__module__}.{target.__name__}'
        if not taskname:
            taskname = f'my_{target}_task'

        task = Task(taskname, Node(target, kwargs), branch='entry', source='')

        with newrepo:
            rn = newrepo.runner()
            action_str, _ = rn._add_task(task)

        if action_str == 'conflict':
            raise RunTaskNameConflict(
                'Taskblaster task name conflict(s) detected.\n'
                'Likely because runtask() was called multiple times using the '
                'same target in a single test.\n'
                'To do so, please provide unique `taskname` inputs:\n'
                ">>> record0 = runtask(target, taskname='task0', **kwargs0)\n"
                ">>> record1 = runtask(target, taskname='task1', **kwargs1)"
            )

        treepath = f'tree/{taskname}'
        tbrun_command(['run', treepath])

        with newrepo:
            directory = newrepo.root / treepath
            entry = newrepo.cache.entry(taskname)
            try:
                output = entry.output()
            except FileNotFoundError:
                # The task must have crashed so we expect there to be an error
                errfiles = sorted(directory.glob(entry.stacktracepattern))
                stacktrace = errfiles[0].read_text()
                raise TaskFailedUnexpectedly(stacktrace)
            return Record(output, directory)

    return runtask


class TaskFailedUnexpectedly(Exception):
    pass


@pytest.fixture
def run_recipe_task(runtask):
    def run_recipe_task(
        target: str,
        recipe: object,
        /,
        taskname: str | None = None,
        pass_comm: bool = False,
        **kwargs,
    ):
        if taskname is None:
            rtype = type(recipe)
            recipename = f'{rtype.__module__}.{rtype.__qualname__}'
            taskname = f'my_{recipename}.{target}_task'
        task = (
            mpi_method_on_recipe_task if pass_comm else method_on_recipe_task
        )
        return runtask(
            task, taskname=taskname, method=target, recipe=recipe, **kwargs
        )

    return run_recipe_task


@pytest.fixture
def run_gs_recipe(run_recipe_task):
    def run_gs_recipe(recipe, atoms):
        gpw_file = run_recipe_task(
            'calculate', recipe, pass_comm=True, atoms=atoms
        ).output
        results = run_recipe_task(
            'results', recipe, pass_comm=True, raw_output=gpw_file
        ).output
        return gpw_file, results

    return run_gs_recipe


@pytest.fixture
def mace_model_in_tree(newrepo, small_mace_model):
    # skip test if mace not installed
    import importlib.util

    mace_path = importlib.util.find_spec('mace')
    if mace_path is None:
        pytest.skip('Mace not installed')

    # Create link to model data within the tmp test directory
    local_path = newrepo.root / 'tree/mace-mp.model'
    local_path.symlink_to(small_mace_model)
    return local_path


@pytest.fixture(scope='session')
def small_mace_model(mace_test_files):
    return mace_test_files / '2023-12-10-mace-128-L0_energy_epoch-249.model'


@pytest.fixture(scope='session')
def mace_test_files():
    import asrlibtestfiles

    return asrlibtestfiles.root() / 'asrlibtestfiles/mace'


def pytest_collection_modifyitems(items, config):
    mpi_procs, subworker_count = parse_mpirun_params(config.option.mpi_args)
    if mpi_procs == 1:
        return

    selected_items = []
    deselected_items = []

    for item in items:
        fixturenames = set(getattr(item, 'fixturenames', ()))
        mpi_fixtures = {
            'tb_run_command',
            'tb_submit_run_ls',
            'runtask',
            'check_tb_tasks',
        }
        if mpi_fixtures & fixturenames:
            selected_items.append(item)
        else:
            deselected_items.append(item)
    config.hook.pytest_deselected(items=deselected_items)
    items[:] = selected_items
