import pytest
import taskblaster as tb


def valid_entry_parameters():
    return [1, -1]


@tb.workflow
class BranchingTestWorkflow:
    a: int = tb.var()  # 1 or -1

    @tb.task
    def b(self):
        return tb.node(double, x=self.a)

    @tb._if(true='negation', false='final')
    @tb.task
    def is_negative(self):
        return tb.node(is_negative, x=self.b)

    @tb.branch('negation')
    @tb.jump('final')
    @tb.task
    def negate(self):
        return tb.node(negate, x=self.b)

    @tb.branch('final')
    @tb.fixedpoint
    @tb.task
    def c(self):
        return tb.node(
            double,
            x=self.Phi(
                entry=self.b,
                negation=self.negate,
            ),
        )

    @tb.task
    def test(self):
        return tb.node(check_results, x=self.c)


def double(x: int):
    return 2 * x


def is_negative(x: int):
    return x < 0


def negate(x: int):
    return -x


def check_results(x: int):
    assert x == 4


@pytest.mark.parametrize('a', valid_entry_parameters())
def test_run_workflow(a, run_workflow, check_tb_tasks):
    wf = BranchingTestWorkflow(a=a)
    run_workflow(wf)
    check_tb_tasks({'done': 2, 'new': 2})
    run_workflow(wf)
    if a >= 0:
        check_tb_tasks(ntasks=4)
    else:
        check_tb_tasks(ntasks=5)
