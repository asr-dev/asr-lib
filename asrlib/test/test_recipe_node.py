from dataclasses import dataclass
from itertools import product

import numpy as np
import pytest
import taskblaster as tb

from asrlib import recipe_node
from asrlib.parameters import Parameter
from asrlib.typing import TBCommunicator


@dataclass
class DummyRecipe(Parameter):
    a: int

    def add(self, b):
        return self.a + b

    def add_via_broadcast(self, b, comm: TBCommunicator):
        out = np.empty(1, dtype=int)
        if comm.rank == 0:
            out[:] = self.a + b
        comm.broadcast(out, 0)
        return out[0]


@pytest.mark.parametrize('a,b', product([0, 1], [1, 2]))
def test_as_task(run_recipe_task, a, b):
    recipe = DummyRecipe(a)
    record = run_recipe_task('add', recipe, b=b)
    assert record.output == a + b
    record = run_recipe_task('add_via_broadcast', recipe, pass_comm=True, b=b)
    assert record.output == a + b


def create_recipe(a: int):
    return DummyRecipe(a)


@tb.workflow
class AddSomeIntegersTestWorkflow:
    recipe: DummyRecipe = tb.var()
    b: int = tb.var()
    c: int = tb.var()

    @tb.task
    def a_plus_b(self):
        # Test that we can run recipe defined as input
        return recipe_node('add', self.recipe, b=self.b)

    @tb.task
    def add_another(self):
        return tb.node(create_recipe, a=self.a_plus_b)

    @tb.task
    def a_plus_b_plus_c(self):
        # Test that we can run recipe defined on-the-fly
        return recipe_node('add', self.add_another, b=self.c)

    @tb.task
    def two_a_plus_two_b_plus_c(self):
        # Test that we can run recipe defined on-the-fly in parallel
        return recipe_node(
            'add_via_broadcast',
            self.add_another,
            pass_comm=True,
            b=self.a_plus_b_plus_c,
        )

    @tb.task
    def three_a_plus_two_b_plus_c(self):
        # Test that we can run recipe defined as input in parallel
        return recipe_node(
            'add_via_broadcast',
            self.recipe,
            pass_comm=True,
            b=self.two_a_plus_two_b_plus_c,
        )

    @tb.task
    def test(self):
        return tb.node(
            self._test,
            a=self.recipe.a,
            b=self.b,
            c=self.c,
            result=self.three_a_plus_two_b_plus_c,
        )

    @staticmethod
    def _test(a: int, b: int, c: int, result: int):
        assert 3 * a + 2 * b + c == result
        return 'great success!'


@pytest.mark.parametrize('a,b,c', product([0, 1], [1, 2], [2, 3]))
def test_as_workflow(run_workflow, check_tb_tasks, a, b, c):
    wf = AddSomeIntegersTestWorkflow(recipe=DummyRecipe(a=a), b=b, c=c)
    run_workflow(wf)
    check_tb_tasks(ntasks=6)
