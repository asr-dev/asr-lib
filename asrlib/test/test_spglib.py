import pytest

from asrlib.spglib.info import (
    Layergroup,
    LayergroupInfo,
    Spacegroup,
    SpacegroupInfo,
)


def test_spacegroupinfo(in_tmp_dir):
    attrs = [
        'number',
        'international',
        'pointgroup',
        'crystal_system',
        'bravais_type',
        'bravais_type_long',
    ]

    sgi = SpacegroupInfo()
    assert len(sgi._spacegroups_info) == 230

    with pytest.raises(ValueError) as e_info:
        sgi.info_from_spacegroupnum(spacegroup=0)
        print(e_info)

    for i in range(1, 231):
        sg = sgi.info_from_spacegroupnum(spacegroup=i)
        assert isinstance(sg, Spacegroup)
        assert all(hasattr(sg, i) for i in attrs)


def test_layergroupinfo(in_tmp_dir):
    attrs = [
        'number',
        'international',
        'crystal_system',
        'lattice_system',
        'bravais_type',
        'bravais_type_long',
    ]  # 'pointgroup',

    lgi = LayergroupInfo()
    assert len(lgi._layergroups_info) == 80

    with pytest.raises(ValueError) as e_info:
        lgi.info_from_layergroupnum(layergroup=0)
        print(e_info)

    for i in range(1, 81):
        lg = lgi.info_from_layergroupnum(layergroup=i)
        assert isinstance(lg, Layergroup)
        assert all(hasattr(lg, i) for i in attrs)
