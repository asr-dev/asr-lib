from __future__ import annotations

from dataclasses import dataclass
from typing import Optional, Tuple

import pytest
from ase import Atoms
from ase.build import bulk
from ase.calculators.emt import EMT
from ase.spacegroup.symmetrize import check_symmetry

from asrlib.parameters import Parameter
from asrlib.recipes import AtomsPreconditioner, StandardASERecipe
from asrlib.recipes.preconditioners import Magnetizer, StructureNormalizer
from asrlib.recipes.preconditioners.distort import break_symmetry
from asrlib.recipes.preconditioners.normalize import (
    niggli_normalize,
    reduce_to_primitive,
)
from asrlib.results.properties import CalculatorProperties
from asrlib.typing import TBCommunicator


def emt_energy(atoms):
    atoms = atoms.copy()
    atoms.calc = EMT()
    return atoms.get_potential_energy()


def emt_energy_equal(atoms1, atoms2, abs=1e-10):
    e1 = emt_energy(atoms1)
    e2 = emt_energy(atoms2)
    return e1 == pytest.approx(e2, abs=abs)


def get_test_system(repeat: Optional[Tuple] = None):
    # Set up target system
    orig_atoms = bulk('Si')
    orig_atoms.symbols[:] = 'Au'
    orig_atoms.rattle()

    # Edit cell
    atoms = orig_atoms.repeat(repeat) if repeat else orig_atoms.copy()
    atoms.cell[2] += atoms.cell[0]  # new linear combination of cell vectors
    assert atoms.cell.angles() != pytest.approx(60)

    def check(normalized_atoms):
        assert normalized_atoms.cell.angles() == pytest.approx(60)
        assert normalized_atoms.cell.cellpar() == pytest.approx(
            orig_atoms.cell.cellpar()
        )
        # It is difficult to fully verify that two structures are equivalent.
        # But if they have the same EMT energy, then they are "probably" equal.
        assert emt_energy_equal(orig_atoms, normalized_atoms)

    return atoms, check


def test_niggli():
    atoms, check = get_test_system()  # niggli cannot reduce repeated crystals
    check(niggli_normalize(atoms))


def test_reduce_to_primitive():
    atoms, check = get_test_system(repeat=(1, 2, 2))
    check(reduce_to_primitive(atoms))


def test_reduce_to_primitive_symmetry():
    atoms = break_symmetry(bulk('Fe').repeat((3, 1, 1)))
    broken_sym = check_symmetry(atoms, symprec=1e-5).number
    assert broken_sym < 229  # Fe has spacegroup 229
    strict_normalizer = StructureNormalizer.build()
    normalized_atoms = strict_normalizer(atoms)
    assert check_symmetry(normalized_atoms).number == broken_sym
    assert len(normalized_atoms) == 3
    loose_normalizer = StructureNormalizer(symprec=0.1)
    symmetrized_atoms = loose_normalizer(atoms)
    assert check_symmetry(symmetrized_atoms).number == 229
    assert len(symmetrized_atoms) == 1


@dataclass
class PseudoFactory(Parameter):
    def __post_init__(self):
        self.properties_cls = CalculatorProperties

    def __call__(self, atoms: Atoms, comm: TBCommunicator):
        return None

    def new_with(self, **kwargs):
        return PseudoFactory()


class PreconditionerRecipe(StandardASERecipe[Atoms]):
    @staticmethod
    def build(preconditioner: AtomsPreconditioner) -> PreconditionerRecipe:
        return PreconditionerRecipe(
            calc_factory=PseudoFactory(), preconditioner=preconditioner
        )

    def _calculate(self, atoms: Atoms, comm: TBCommunicator) -> Atoms:
        # We don't do any actual calculation, but just return the atoms
        return atoms


def test_recipe_preconditioning(run_recipe_task):
    # Test that we can run normalization through a preconditioner
    atoms, check = get_test_system(repeat=(1, 2, 2))
    recipe = PreconditionerRecipe.build(
        preconditioner=StructureNormalizer.build()
    )
    record = run_recipe_task('calculate', recipe, pass_comm=True, atoms=atoms)
    check(record.output)


@pytest.mark.parametrize('append', [True, False])
def test_recipe_multiple_preconditioners(append, run_recipe_task):
    atoms, check = get_test_system(repeat=(1, 2, 2))
    if append:
        # Test that we can append preconditioners
        recipe = PreconditionerRecipe.build(
            preconditioner=StructureNormalizer.build()
        )
        recipe = recipe.new_with(
            append=dict(preconditioner=Magnetizer({'Au': 1.2}))
        )
    else:
        # Test that we can give a list of preconditioners
        recipe = PreconditionerRecipe.build(
            preconditioner=[
                StructureNormalizer.build(),
                Magnetizer({'Au': 1.2}),
            ]
        )
    record = run_recipe_task('calculate', recipe, pass_comm=True, atoms=atoms)
    check(record.output)
    assert record.output.get_initial_magnetic_moments() == pytest.approx(
        [1.2] * 2
    )
