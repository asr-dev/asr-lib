from taskblaster import mpi, node
from taskblaster.repository import Repository
from taskblaster.storage import JSONCodec


def recipe_node(
    target: str, recipe: object, /, pass_comm: bool = False, **kwargs
):
    task = mpi_method_on_recipe_task if pass_comm else method_on_recipe_task
    return node(task, method=target, recipe=recipe, **kwargs)


def call_mpi_method(target: str, recipe: object, /, **kwargs):
    return mpi_method_on_recipe_task(method=target, recipe=recipe, **kwargs)


def method_on_recipe_task(*, method: str, recipe: object, **kwargs):
    _method = getattr(recipe, method)
    return _method(**kwargs)


@mpi
def mpi_method_on_recipe_task(
    *, method: str, recipe: object, mpi: mpi, **kwargs
):
    return method_on_recipe_task(
        method=method, recipe=recipe, comm=mpi.comm, **kwargs
    )


class ASECodec(JSONCodec):
    def encode(self, obj):
        from ase.io.jsonio import default

        return default(obj)

    def decode(self, dct):
        from ase.io.jsonio import object_hook

        return object_hook(dct)


class AsrLibRepository(Repository):
    def worker_start_hook(self):
        from os import getenv

        from gpaw import gpu

        if getenv('GPAW_GPU'):
            print('XXXX calling GPU Setup')
            gpu.setup()

    def worker_finish_hook(self):
        pass

    def mpi_world(self):
        try:
            from gpaw.mpi import world
        except ModuleNotFoundError:
            return super().mpi_world()

        return TBCommunicator(world)


# Magical "initializer" for taskblaster repository using existing
# asr encoder.  Taskblaster imports this and uses it to initialize
# any repository.
def tb_init_repo(root):
    return AsrLibRepository(root, usercodec=ASECodec())


class TBCommunicator:
    """GPAW/taskblaster communicator compatibility layer."""

    def __init__(self, comm):
        self._comm = comm  # GPAW communicator
        self.rank = comm.rank
        self.size = comm.size

    def broadcast_object(self, obj):
        from ase.parallel import broadcast

        return broadcast(obj, root=0, comm=self._comm)

    def split(self, newsize):
        import numpy as np

        assert self.size % newsize == 0

        mygroup = self.rank // newsize

        startrank = mygroup * newsize
        endrank = startrank + newsize
        ranks = np.arange(startrank, endrank)
        _comm = self._comm.new_communicator(ranks)
        assert _comm is not None
        return TBCommunicator(_comm)

    def usercomm(self):
        # This is the communicator object passed to the tasks as mpi.comm.
        # Since the tasks speak GPAW language, this will be a GPAW
        # communicator if we run in parallel.
        return self._comm
