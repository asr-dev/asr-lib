import asr
from ase.db import connect


def add_materials_from_db(filename: str) -> dict:
    """
    Helper function for a high-throughput workflow that loads an ase.db,
    reads the structures, and adds them to the asr workflow.

    :param filename: the file path + filename of the ase database.

    :return: a dictionary containing chemical formula: ase atoms object for
    each structure.
    """
    cforms = []
    db_structures = {}
    # connect local db file and
    with connect(filename) as con:
        for row in con.select():
            atoms = row.toatoms().copy()  # extra copy to remove calculators
            cform = atoms.get_chemical_formula()
            cforms.append(cform)
            db_structures[cform] = atoms

    return db_structures


filename = 'oqmd_benchmarks.db'
workflow = asr.totree(add_materials_from_db(filename), name='material')
