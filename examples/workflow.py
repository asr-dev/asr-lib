import asr


@asr.workflow
class MyWorkflow:
    atoms = asr.var()
    calculator = asr.var()

    @asr.task
    def reduce_to_primitive(self):
        """
        Task used to obtain reduce a cell to a primitive lattice.

        :return: asr.node
        """
        return asr.node('crysp.tasks.relax.reduce_to_primitive',
                        atoms=self.atoms)

    @asr.task
    def niggli_normalize(self):
        """
        Task used to obtain a standardized structure format.

        :return: path object pointing to the niggli normalized file
        """
        return asr.node('crysp.tasks.relax.niggli_normalize',
                        atoms=self.reduce_to_primitive)

    @asr.task
    def get_kpoints(self):
        """
        Task used to determine the k-point grid to be used in the relaxation

        :return: k-point grid for the particular structure
        """
        return asr.node('crysp.tasks.general.get_mindist_kpoints',
                        calculator=self.calculator,
                        atoms=self.niggli_normalize)

    @asr.task
    def relax3d(self):
        """
        Task to perform a single structure relaxation given smax and fmax (
        convergence.forces) using BFGS optimizer.

        :return: ASRResults
        """
        return asr.node('crysp.tasks.relax.relax3d',
                        atoms=self.niggli_normalize,
                        calculator=self.get_kpoints)


@asr.parametrize_glob('*/material')
def workflow(material):
    calculator = {'kpts': {'density': 1.0}, 'xc': 'PBE', 'txt': 'relax.txt',
                  'mode': {'name': 'pw', 'ecut': 200}, 'convergence': {
            'forces': 5e-1}}
    return MyWorkflow(atoms=material, calculator=calculator)
