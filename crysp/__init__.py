import sys

from asrlib import tb_init_repo


def drop_argument(argname):
    def deco(cls):
        old_init = cls.__init__

        def my_init(self, **kwargs):
            kwargs.pop(argname, None)
            old_init(self, **kwargs)

        cls.__init__ = my_init
        return cls

    return deco


class OQMDRow:
    def __init__(self, atoms, oqmd_entry_id):
        self.atoms = atoms
        self.oqmd_entry_id = oqmd_entry_id

    @classmethod
    def from_row(cls, dbrow):
        return cls(dbrow.toatoms().copy(), dbrow.oqmd_entry_id)

    def get_tree_id(self):
        return f'{self.atoms.symbols}/{self.oqmd_entry_id}'

    def tb_encode(self):
        return {'atoms': self.atoms, 'oqmd_entry_id': self.oqmd_entry_id}

    @classmethod
    def tb_decode(cls, dct):
        return cls(**dct)

    def __getitem__(self, item):
        return getattr(self, item)


def import_old_stub(old_module_name, new_module_path, new_module_name):
    # print('old name', old_module_name)
    # print('new path', new_module_path)
    # print('new mod name', new_module_name)
    if old_module_name not in sys.modules:
        # Dynamically import the new module
        new_module = __import__(new_module_path, fromlist=[new_module_name])

        # Create a new module with the old module name
        old_module = type(old_module_name, (object,), {})

        # Copy all functions from the new module to the old module
        for func_name in dir(new_module):
            if callable(getattr(new_module, func_name)):
                # print('old module type', type(old_module))
                # print('func_name', func_name)
                # print('new mod', new_module, func_name)
                try:
                    setattr(
                        old_module, func_name, getattr(new_module, func_name)
                    )
                except TypeError as e:
                    print(e)
                    pass
        # Set the reference to the old module location
        sys.modules[old_module_name] = old_module


# Example usage to leave stubs
import_old_stub('crysp.oqmd', 'crysp', 'OQMDRow')
import_old_stub('crysp.tbintegrations.oqmd', 'crysp', 'OQMDRow')


__all__ = ['tb_init_repo']
