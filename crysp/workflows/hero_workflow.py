# pragma: no cover
"""
This is the old CrystalBank workflow. It is here just until we can port the
old data to the new format.
"""

import taskblaster as tb


@tb.workflow
class HeroRelaxWorkflow:
    material = tb.var()
    magmoms = tb.var(default=1.0)

    calculator_relax = tb.var()
    calculator_groundstate = tb.var()

    # NEW INTERFACE PARAMS
    relax_recipe = tb.var()
    gs_recipe = tb.var()

    @tb.subworkflow
    def structure_normalize(self):
        from crysp.workflows.structure_normalize import StructureNormalize

        return StructureNormalize(atoms=self.material)

    @tb.subworkflow
    def gpaw_relax(self):
        from crysp.projectfiles.relax import Relax3DWorkflow

        return Relax3DWorkflow(
            atoms=self.structure_normalize.niggli_normalize,
            calculator=self.calculator_relax,
        )

    @tb.subworkflow
    def gs(self):
        from crysp.projectfiles.relax import GsWorkflow

        return GsWorkflow(
            atoms=self.gpaw_relax.relax3d_magnetic['atoms'],
            calculator=self.calculator_groundstate,
        )

    @tb.subworkflow
    def pbe_d3(self):
        from crysp.workflows.relax_with_gs import RelaxAndGsWf

        return RelaxAndGsWf(
            atoms=self.gpaw_relax.relax3d_magnetic['atoms'],
            magmoms=self.gs.gsresults.properties.magmoms,
            relax_recipe=self.relax_recipe,
            gs_recipe=self.gs_recipe,
        )
