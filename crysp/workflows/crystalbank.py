import taskblaster as tb


@tb.workflow
class CrystalBankWf:
    """
    Newest version of the hero workflow. Use this for starting new trees
    """

    material = tb.var()
    magmoms = tb.var(default=1.0)

    relax_recipe = tb.var()
    gs_recipe = tb.var()

    @tb.subworkflow
    def relax2gs(self):
        from asrlib.recipes.preconditioners import StructureNormalizer
        from asrlib.workflows.relax.iterate_magmoms import (
            RelaxToGSWithOrWithoutMagmoms,
        )

        return RelaxToGSWithOrWithoutMagmoms(
            atoms=self.material,
            magmoms=self.magmoms,
            mag_relax_recipe=self.relax_recipe.new_with(
                # Normalize the structure ahead of the first relaxation
                preconditioner=StructureNormalizer.build()
            ),
            nm_relax_recipe=self.relax_recipe,
            gs_recipe=self.gs_recipe,
        )

    @tb.subworkflow
    def pbed3(self):
        from crysp.workflows.relax_with_gs import RelaxAndGsWf

        return RelaxAndGsWf(
            atoms=self.relax2gs.finalizer.atoms,
            magmoms=self.relax2gs.gs.results.properties.magmoms,
            relax_recipe=self.relax_recipe.new_with(d3=True),
            gs_recipe=self.gs_recipe.new_with(d3=True),
        )
