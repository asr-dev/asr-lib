import taskblaster as tb


@tb.workflow
class RelaxAndGsWf:
    atoms = tb.var()
    magmoms = tb.var()
    relax_recipe = tb.var()
    gs_recipe = tb.var()

    @tb.subworkflow
    def relax(self):
        from asrlib.recipes.preconditioners import Magnetizer
        from asrlib.workflows.relax import RelaxRecipeWorkflow

        return RelaxRecipeWorkflow(
            atoms=self.atoms,
            recipe=self.relax_recipe.new_with(
                preconditioner=Magnetizer(self.magmoms),
            ),
        )

    @tb.subworkflow
    def gs(self):
        from asrlib.workflows.gs import GroundStateRecipeWorkflow

        return GroundStateRecipeWorkflow(
            atoms=self.relax.results.atoms,
            recipe=self.gs_recipe,
        )
