# pragma: no cover
import taskblaster as tb
from ase.db import connect

from crysp import OQMDRow


def add_materials_from_db(
    filename: str, nmax: int = -1, *, selection: str
) -> dict:
    """
    Helper function for a high-throughput workflow that loads an ase.db, reads
    the structures, and adds them to the asr workflow.

    :param filename: the file path + filename of the ase database.
    :return: a dictionary containing chemical formula: ase atoms object for
    each structure.
    """
    db_structures = {}
    # connect local db file and
    counter = 0
    with connect(filename) as con:
        for row in con.select(selection=selection):
            oqmdrow = OQMDRow.from_row(row)
            db_structures[oqmdrow.get_tree_id()] = oqmdrow
            counter += 1
            if counter == nmax:
                break

    return db_structures


def define_workflow(
    nmax=20,
    selection='natoms=4,oqmd_stability<0.1',
    filename='oqmd_v1_5_excl_elements+duplicates.db',
):
    return tb.totree(
        add_materials_from_db(filename, nmax=nmax, selection=selection),
        name='material',
    )
