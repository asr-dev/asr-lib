import taskblaster as tb


@tb.workflow
class StructureNormalize:
    atoms = tb.var()

    @tb.task
    def reduce_to_primitive(self):
        return tb.node(
            'asrlib.recipes.preconditioners.normalize.reduce_to_primitive',
            atoms=self.atoms,
        )

    @tb.task
    def niggli_normalize(self):
        return tb.node(
            'asrlib.recipes.preconditioners.normalize.niggli_normalize',
            atoms=self.reduce_to_primitive,
        )
