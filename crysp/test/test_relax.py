import pytest
from ase.build import bulk
from ase.io import read

from crysp.projectfiles.relax import magnetize, relax3d


def test_relax3d(newrepo):
    # tests mag to non-mag relax routine works
    atoms = bulk('Si')

    atoms = magnetize(atoms, magmoms=1.0)
    calc_params = {
        'mode': {'name': 'pw', 'ecut': 150},
        'xc': 'PBE',
        'kpts': {'density': 1, 'symmetry': False},
        'txt': 'relax.txt',
        'fmax': 5e-1**2,
        'smax': 5e-2,
        'append_trajectory': False,
    }

    relax = relax3d(atoms=atoms, calculator=calc_params)

    # read results from relaxation run
    traj = read(relax['traj_path'])
    pe = traj.get_potential_energy()

    # where did this number come from. This value changed for some reason it
    # was -10.1598173.
    assert pe == pytest.approx(-10.5918962, 1e-3)
    # test that the hardcoded file is written
    assert relax['traj_path'].name == 'relax-nm.traj'
