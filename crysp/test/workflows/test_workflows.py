import numpy as np
import pytest
import taskblaster as tb

from asrlib.recipes.gs import GPAWGroundStateRecipe
from asrlib.recipes.relax import ASERelaxRecipe
from crysp.tasks.factory import CryspCalculatorFactory

gpaw_params = {
    'mode': {'name': 'pw', 'ecut': 200},
    'xc': 'PBE',
    'kpts': {'density': 1, 'symmetry': False},
    'txt': 'gpaw.txt',
    'parallel': {'band': 1},
    'occupations': {'name': 'fermi-dirac', 'width': 0.05},
}
opt_params = {
    'smask': np.array([0, 0, 0, 0, 0, 0]),
    'fmax': 5e-1,
    'smax': 5e-2,
    'steps': 40,
    'append_trajectory': True,
}


# test totree workflow declaration
@pytest.fixture
def totree(newrepo, asrlib_database_path):
    from crysp.workflows.totree import add_materials_from_db

    subset_mats = add_materials_from_db(
        asrlib_database_path, selection='Ag,natoms=1'
    )

    rn = newrepo.runner()
    with newrepo:
        tb.totree(subset_mats, name='material')(rn)


def test_wf_structnorm(
    totree,  # run on a tree of different materials
    run_workflow,
    check_tb_tasks,
):
    @tb.parametrize_glob('*/material')
    def workflow(material):
        from crysp.workflows.structure_normalize import StructureNormalize

        return StructureNormalize(atoms=material.atoms)

    run_workflow(workflow)
    check_tb_tasks(ntasks=3)


def test_wf_hero(run_workflow, totree, check_tb_tasks):
    @tb.parametrize_glob('*/material')
    def workflow(material):
        from crysp.workflows.hero_workflow import HeroRelaxWorkflow

        calculator_relax = {**gpaw_params, **opt_params, 'gmax': 40}

        factory = CryspCalculatorFactory(gpaw_params)
        return HeroRelaxWorkflow(
            material=material.atoms,
            calculator_relax=calculator_relax,
            calculator_groundstate=gpaw_params,
            relax_recipe=ASERelaxRecipe.bfgs_relax_positions(factory),
            gs_recipe=GPAWGroundStateRecipe.build(factory, d3=True),
        )

    run_workflow(workflow)
    check_tb_tasks(ntasks=11)


def test_crystalbank(run_workflow, totree, check_tb_tasks):
    @tb.parametrize_glob('*/material')
    def workflow(material):
        from crysp.workflows.crystalbank import CrystalBankWf

        relax_recipe = ASERelaxRecipe.bfgs_relax_positions(
            CryspCalculatorFactory(gpaw_params)
        )
        gs_recipe = GPAWGroundStateRecipe.build(
            CryspCalculatorFactory({**gpaw_params, 'txt': 'gs.txt'})
        )

        return CrystalBankWf(
            material=material.atoms,
            relax_recipe=relax_recipe,
            gs_recipe=gs_recipe,
        )

    run_workflow(workflow)
    check_tb_tasks(ntasks={'done': 4, 'new': 7})

    run_workflow(workflow)
    check_tb_tasks(ntasks=13)
