import pytest

from asrlib.spglib.info import Layergroup, Spacegroup
from crysp.postprocess.symmetry import SymmetryAnalyzer


@pytest.mark.parametrize('material_index', ['Si', 'MoS2'])
def test_sym_analyzer(in_tmp_dir, material_index, asrlib_test_materials):
    atoms = asrlib_test_materials[material_index]  # Si and MoS2
    sym_precs = dict(symprec=0.01, angle_tolerance=0.01, hall_number=0)
    formula_keys = dict(mode='metal', empirical=False)

    # initialize symm analyzer
    sa = SymmetryAnalyzer(atoms=atoms, **sym_precs)

    # generate spglib data for given precs
    if atoms.pbc.sum() < 3:
        layergroup_info = sa.get_layer_group(sym_precs['symprec'])
        assert isinstance(layergroup_info, dict)

    # test properties
    assert isinstance(sa.dataset, dict)
    assert isinstance(sa.formula(**formula_keys), str)
    assert isinstance(sa.reduced_formula(**formula_keys), str)
    assert isinstance(sa.anonymous_reduced_formula, str)
    assert isinstance(sa.crystal_type, str)
    assert isinstance(sa.cell_area_xy, float) and isinstance(sa.volume, float)

    assert isinstance(sa.spacegroup_info, Spacegroup)
    if atoms.pbc.sum() == 2:
        assert isinstance(sa.layergroup_info, Layergroup)
