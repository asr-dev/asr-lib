import pytest

# Using fixtures from another repo. Need to import them in order of
# their dependencies. flake is not smart enough to know this, hence noqa: F401.
from asrlib.test.conftest import (  # noqa: F401
    asrlib_database_path,
    asrlib_test_materials,  # noqa: F401
    check_tb_tasks,  # noqa: F401
    in_tmp_dir,  # noqa: F401
    module_tmp_path,  # noqa: F401
    newrepo,  # noqa: F401
    parallel,  # noqa: F401
    parse_mpirun_params,  # noqa: F401
    pytest_addoption,  # noqa: F401
    pytest_report_header,  # noqa: F401
    resolve_mpirun_params,  # noqa: F401
    run_workflow,  # noqa: F401
    runtask,  # noqa: F401
    tb_submit_run_ls,  # noqa: F401
    tbrun_command,
    testdir,
)


@pytest.fixture
def repo_crysp(testdir):
    from taskblaster.repository import Repository

    with Repository.create(testdir, modulename='crysp') as repo:
        yield repo


__all__ = ['testdir']
