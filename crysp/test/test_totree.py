import taskblaster as tb

from crysp.workflows.totree import add_materials_from_db


def test_database_totree(testdir, repo_crysp, asrlib_database_path):
    tree = repo_crysp.root / 'tree'
    assert tree.is_dir()

    selection = ''
    tb.totree(
        add_materials_from_db(asrlib_database_path, selection=selection),
        name='material',
    )
    assert len(list(tree.glob('*'))) == 0  # No files after dry run
