import numpy as np
import pytest
from ase.build import bulk
from gpaw.mpi import world

from crysp.tasks.factory import CryspCalculatorFactory
from crysp.tasks.kpoints import (
    KPoints,
    mindistance2monkhorstpack,
    mp_from_density,
)


def test_mindistance2monkhorstpack(in_tmp_dir):
    atoms = bulk('Kr')
    # test mindistance2monkhorstpack outside KPoints class, so we
    # have more control to make the test faster using symmetry=True
    assert np.all(
        [6, 6, 6]
        == mindistance2monkhorstpack(
            atoms, min_distance=20, symmetry=True, comm=world, maxperdim=12
        )[0]
    )


@pytest.mark.parametrize(
    'kpts',
    [
        {'density': 1, 'symmetry': False},
        pytest.param(
            {'density': 1, 'symmetry': True},
            marks=pytest.mark.skipif(
                world.size == 1,
                reason='symmetry=True is too '
                'slow if serial. Please '
                'run in parallel.',
            ),
        ),
    ],
)
def test_kpts_from_density(kpts):
    atoms = bulk('Kr')
    # provide a density if you want to set the kpt grid by density
    kpts_meth, ibz = mp_from_density(atoms=atoms, **kpts, comm=world)

    assert isinstance(kpts_meth, np.ndarray)
    if not kpts['symmetry']:
        print(kpts_meth)
        assert np.array_equal(kpts_meth, np.array([2, 2, 2]))
        assert ibz == 8
    if kpts['symmetry']:
        assert np.array_equal(kpts_meth, np.array([2, 2, 2]))
        assert ibz == 2


@pytest.mark.parametrize(
    'kpts',
    [
        [2, 2, 2],
        np.array([2, 2, 2]),
        {'density': 1, 'gamma': True},
    ],
)
def test_kpts_from_mpgrid(kpts):
    atoms = bulk('Kr')
    # provided a list, we determine the number of sub-divisions
    # provided a np.array, we assume the kpt grid has been explicitly defined

    # KPoints is called in Factory, so we check here that the same kpts
    # settings give the same results in the factory as in KPoints.
    kpts_object = KPoints(kpts=kpts)
    kpts_meth = kpts_object.get_kpts_from_mpgrid(atoms)
    factory = CryspCalculatorFactory(params={'kpts': kpts})
    factory_kpts = factory.get_gpaw_params(atoms=atoms, comm=world)['kpts']

    # make sure the output is consistent with something gpaw will accept as
    # kpts & the factory produces identical k-points
    assert isinstance(factory_kpts, np.ndarray)
    if isinstance(kpts, list):
        # user asks us to find the kpt grid. For this system, we just check
        # that the size of the np array doesn't change.
        test = np.array(
            [
                [-0.25, -0.25, -0.25],
                [-0.25, -0.25, 0.25],
                [-0.25, 0.25, -0.25],
                [-0.25, 0.25, 0.25],
                [0.25, -0.25, -0.25],
                [0.25, -0.25, 0.25],
                [0.25, 0.25, -0.25],
                [0.25, 0.25, 0.25],
            ]
        )
        assert np.array_equal(test, kpts_meth)
        assert len(kpts_meth) == len(factory_kpts) == 8
        assert np.array_equal(kpts_meth, factory_kpts)
    elif isinstance(kpts, np.ndarray):
        # user set the k-point grid, so we must return what the user says
        assert np.array_equal(kpts_meth, kpts)
        assert np.array_equal(factory_kpts, kpts)
    elif 'density' and 'gamma' in kpts:
        test = np.array(
            [
                [0, 0, 0],
                [0, 0, 0.5],
                [0, 0.5, 0],
                [0, 0.5, 0.5],
                [0.5, 0, 0],
                [0.5, 0, 0.5],
                [0.5, 0.5, 0],
                [0.5, 0.5, 0.5],
            ]
        )
        # provided gamma/density, return a gamma center grid
        assert np.array_equal(factory_kpts, test)
