"""Structural information."""

import re
import string
from collections.abc import Iterable
from functools import reduce
from math import gcd

import numpy as np
from ase import Atoms
from spglib import get_symmetry_dataset
from spglib.spglib import get_symmetry_layerdataset

from asrlib.spglib import spglib_as_dict
from asrlib.spglib.info import (
    Layergroup,
    LayergroupInfo,
    Spacegroup,
    SpacegroupInfo,
)


class SymmetryAnalyzer:
    def __init__(
        self,
        atoms: Atoms,
        symprec: float,
        angle_tolerance: float,
        hall_number: int = 0,
    ):
        """
        :param atoms: ASE atoms object to analyze symmetry for.
        :param symprec: Spglib symmetry precision.
        :param angle_tolerance: Spglib angle tolerance.
        :param hall_number: Method to down-map the Hall symbols to a
            space-group-type since this is a many-to-one mapping. See
            (Seto's web site)](https://yseto.net/?page_id=29%3E)) for possible
            choices and settings. hall_number=0 is the default.
        """
        self.atoms = atoms
        self.spglib_cell = self._to_spglib()
        self._dataset = self.get_space_group(
            symprec=symprec,
            angle_tolerance=angle_tolerance,
            hall_number=hall_number,
        )

        if self.atoms.pbc.sum() == 2:
            # lg_dct ref key, spglib dataset key
            layergroup_info = zip(
                ['international', 'number'], ['layergroup', 'lgnum']
            )
            lg_dct = self.get_layer_group(symprec=symprec)

            self._dataset.update(
                {key: lg_dct[lg_key] for lg_key, key in layergroup_info}
            )

        self._add_inversion_data_to_dataset()

    def get_space_group(
        self, symprec: float, angle_tolerance: float, hall_number: int
    ) -> dict:
        return spglib_as_dict(
            get_symmetry_dataset(
                cell=self.spglib_cell,
                symprec=symprec,
                hall_number=hall_number,
                angle_tolerance=angle_tolerance,
            )
        )

    def get_layer_group(self, symprec: float) -> dict:
        assert self.atoms.pbc.sum() == 2
        aperiodic_dir = np.where(~self.atoms.pbc)[0][0]
        # Prepare for spglib v3 API change to always have the aperiodic_dir==2
        # See: https://github.com/spglib/spglib/issues/314.
        if aperiodic_dir != 2:
            perm = np.array([0, 1, 2])
            # Swap axes such that aperiodic is always 2
            perm[2], perm[aperiodic_dir] = perm[aperiodic_dir], perm[2]

            atoms = self.atoms.copy()
            atoms.set_pbc(atoms.get_pbc()[perm])

            # Atoms are stored in cartesian coordinates, therefore, we're free
            # to permute the cell vectors, and the system remains invariant
            atoms.set_cell(atoms.get_cell()[perm], scale_atoms=False)
            aperiodic_dir = 2

        assert aperiodic_dir == 2
        return dict(
            spglib_as_dict(
                get_symmetry_layerdataset(
                    cell=(
                        self.atoms.get_cell(),
                        self.atoms.get_scaled_positions(),
                        self.atoms.get_atomic_numbers(),
                    ),
                    symprec=symprec,
                    aperiodic_dir=aperiodic_dir,
                )
            )
        )

    def formula(self, mode: str = 'metal', empirical: bool = False):
        return self.atoms.get_chemical_formula(mode=mode, empirical=empirical)

    def reduced_formula(
        self,
        mode: str = 'metal',
        empirical: bool = False,
        anonymous: bool = False,
    ):
        formula = self.atoms.get_chemical_formula(
            mode=mode, empirical=empirical
        )
        return self._reduce_formula_string(
            formula=formula, anonymous=anonymous
        )

    @property
    def anonymous_reduced_formula(self):
        formula = self.formula(mode='metal', empirical=False)
        return self._reduce_formula_string(formula=formula, anonymous=True)

    @property
    def spacegroup_info(self) -> Spacegroup:
        return SpacegroupInfo().info_from_spacegroupnum(
            spacegroup=self._dataset['number']
        )

    @property
    def layergroup_info(self) -> Layergroup:
        if self.atoms.pbc.sum() != 2:
            raise ValueError(
                'This is not a 2D system. Please indicate layered direction '
                'with non-periodic boundary in the layered direction.'
            )
        return LayergroupInfo().info_from_layergroupnum(
            layergroup=self._dataset['lgnum']
        )

    @property
    def crystal_type(self):
        wyckoffs = ''.join(sorted(set(self.wyckoffs)))
        crystal_type = (
            f'{self.atoms.symbols.formula.stoichiometry()[0]}-'
            f'{self.number}-{wyckoffs}'
        )
        return crystal_type

    @property
    def volume(self):
        return np.dot(
            self.atoms.cell[2],
            np.cross(self.atoms.cell[0], self.atoms.cell[1]),
        )

    @property
    def cell_area_xy(self):
        return np.linalg.norm(np.cross(self.atoms.cell[0], self.atoms.cell[1]))

    @property
    def dataset(self):
        """Dictionary of key value pairs returned by spglib."""
        return self._dataset

    def __getitem__(self, item):
        return self._dataset[item]

    def __getattr__(self, item):
        if hasattr(type(self), item):
            return getattr(type(self), item)
        elif item in self._dataset:
            return self._dataset[item]
        else:
            raise AttributeError(
                f"'{type(self).__name__}' object has no attribute '{item}'"
            )

    def __repr__(self):
        return f'{self.__class__.__name__}(atoms={self.atoms})'

    # XXX This just seems like a less comprehensive version of what the
    # Formula class is capable of doing. In what way is this meaningfully
    # different from Formula.stoichiometry()[0]. the results look identical
    def _reduce_formula_string(self, formula, anonymous=True):
        """
        Get reduced formula from formula according to the
        ase.atoms.get_chemical_formula().

        :param mode: Chose from all, reduce, hill, and metal.
        :param empirical: Divide the symbol counts by their greatest common
            divisor to yield an empirical formula. Only for mode `metal` and
            `hill`.
        :param anonymous: If True, return the anonymous stoichiometry, for
            example, "AB2" rather than "MoS2."
        :return: The reduced formula
        """
        split = re.findall('[A-Z][^A-Z]*', formula)
        matches = [re.match('([^0-9]*)([0-9]+)', x) for x in split]
        numbers = [int(x.group(2)) if x else 1 for x in matches]
        symbols = [
            matches[i].group(1) if matches[i] else split[i]
            for i in range(len(matches))
        ]
        divisor = reduce(gcd, numbers)
        result = ''
        numbers = [x // divisor for x in numbers]
        numbers = [str(x) if x != 1 else '' for x in numbers]
        if anonymous:
            numbers = sorted(numbers)
            symbols = string.ascii_uppercase
        for symbol, number in zip(symbols, numbers):
            result += symbol + number
        return result

    def _to_spglib(self):
        """
        Return a structure object that spglib understands from an ASE atoms
        object.

        :param wrap:
        :return:
        """
        lattice = self.atoms.get_cell().array
        positions = self.atoms.get_scaled_positions()
        numbers = self.atoms.get_atomic_numbers()

        return lattice, positions, numbers

    def _add_inversion_data_to_dataset(self):
        """
        Determine if atoms have inversion symmetry.

        :return: Whether rotations contain inversion symmetry.
        """
        rotations = self.rotations
        if isinstance(rotations, Iterable):
            inversion = -np.identity(3, dtype=int)
            self._dataset['has_inversion_symmetry'] = np.any(
                [np.all(rotation == inversion) for rotation in rotations]
            )
        else:
            self._dataset['has_inversion_symmetry'] = False
