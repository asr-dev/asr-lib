"""
Postprocessing steps for the ground state workflow
"""

from __future__ import annotations

from dataclasses import dataclass
from pathlib import Path

import taskblaster as tb

from asrlib.recipes.gs.gpaw.results import GPAWGroundStateResults
from asrlib.results import Result
from asrlib.results.gpaw import GPWFile
from crysp.actions.export import todb
from crysp.postprocess.utils import gap_info_dict


@dataclass
class CryspGapResults(Result):
    efermi: float
    vbm: float
    cbm: float
    gap: float
    vbm_dir: float
    cbm_dir: float
    gap_dir: float

    @classmethod
    def from_calc(cls, calc) -> CryspGapResults:
        return cls(**gap_info_dict(calc))


@dataclass
class CryspGroundStateResults(GPAWGroundStateResults):
    gap_info: CryspGapResults

    @staticmethod
    def extract_data(atoms, calc):
        return dict(gap_info=CryspGapResults.from_calc(calc))


@tb.actions(todb=todb)
def gsresults(gpw_path: Path, d3: bool = False) -> CryspGroundStateResults:
    return CryspGroundStateResults.from_gpw(GPWFile(gpw_path), d3=d3)
