from ase.dft.bandgap import bandgap


def gap_info_dict(calc) -> dict:
    """Extract band gap information from an ASE calculator."""
    keys = ['vbm', 'cbm', 'gap']
    gap_info = {'efermi': calc.get_fermi_level()}
    for direct in [True, False]:
        gap_keys = [key + '_dir' for key in keys] if direct else keys

        gap, skn1, skn2 = bandgap(calc, direct=direct, output=None)
        vbm, cbm = None, None
        if skn1[1] is not None:
            vbm = calc.get_eigenvalues(spin=skn1[0], kpt=skn1[1])[skn1[2]]
            cbm = calc.get_eigenvalues(spin=skn2[0], kpt=skn2[1])[skn2[2]]
        gap_info.update(dict(zip(gap_keys, [vbm, cbm, gap])))

    return gap_info
