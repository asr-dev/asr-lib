from ase.visualize import view


def viewer(record):
    """
    This is the old version of the viewer.
    it trys to read from both dicts and result objects, but it is really
    messy and should be removed once the disk data is updated.
    """
    key, output, input = 'atoms', record.output, record.input

    # view output if there
    print('Getting atoms from output')
    if output and key in output:
        view(atoms=output[key])
    if output and hasattr(output, key):
        view(atoms=output.atoms)

    # try to view input
    print('Getting atoms from input')
    if hasattr(input, key):
        view(atoms=input.atoms)
    if key in input:
        view(atoms=input[key])
