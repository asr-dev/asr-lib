"""
Actions you can take on workflows
"""

import matplotlib.pyplot as plt
from ase.io.jsonio import read_json
from ase.spectrum.band_structure import BandStructure


def read_band_structure(filename):
    bs = read_json(filename)
    if not isinstance(bs, BandStructure):
        raise ValueError(f'Expected band structure, but file contains: {bs}')
    return bs


def plot_bs(filename, erange=(-3, 3), output=None):
    """
    Plot the bandstructure from a json file.

    :param filename: Filepath to the bandstructure.json file.
    :param erange: The emin and emax for plotting the bs around efermi.
    :param output: Filename to save the bandstructure. Default=None.
    """
    bs = read_band_structure(filename)
    emin, emax = (float(e) for e in erange)

    fig = plt.figure()
    ax = fig.gca()
    bs.plot(
        ax=ax,
        filename='bandstructure.png',
        emin=emin + bs.reference,
        emax=emax + bs.reference,
    )

    if output is None:
        plt.show()
    else:
        plt.savefig(output)
        plt.show()
