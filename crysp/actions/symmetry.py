from crysp.postprocess.symmetry import SymmetryAnalyzer


def crystalinfo(record):
    output, input = record.output, record.input
    try:
        atoms = output.atoms
    except AttributeError:
        atoms = output['atoms']
    except KeyError:
        atoms = input['atoms']

    symprec, angle_tolerance, hall_number, wrap = 0.1, 0.01, 0, True
    sa = SymmetryAnalyzer(
        atoms=atoms,
        symprec=symprec,
        wrap=wrap,
        angle_tolerance=angle_tolerance,
        hall_number=hall_number,
    )

    print(sa.spacegroup_info)
    if atoms.pbc.sum() == 2:
        print(sa.layergroup_info)

    sg = sa.spacegroup_info
    print(sg.todict())

    return sa
