from pathlib import Path

import numpy as np


def convert_to_conventional(structure):
    from pymatgen.core import Structure
    from pymatgen.symmetry.analyzer import SpacegroupAnalyzer

    struct = Structure.from_ase_atoms(structure)
    sga = SpacegroupAnalyzer(struct)
    conventional_structure = sga.get_conventional_standard_structure()
    atoms = conventional_structure.to_ase_atoms()
    return atoms


def todb(record):
    from crysp.postprocess.symmetry import SymmetryAnalyzer

    output, node = record.output, record.node
    write_structure, data = False, {}

    print('Collecting database information from completed task:', node.name)
    task_name = Path(node.name).name

    # XXX make the structure the conventional unit cell with the x aligned with
    # cartisian x
    if task_name == 'relax3d_magnetic':
        write_structure = True
        if output['traj_path'].name == 'relax.traj':
            data = {'magstate': 'Mag'}
        else:
            data = {'magstate': 'NM'}

        atoms = output['atoms']
        sa = SymmetryAnalyzer(
            atoms=atoms,
            symprec=0.1,
            angle_tolerance=0.01,
            hall_number=0,
        )
        sg = sa.spacegroup_info

        data.update(
            {
                'crystal_type': sa.crystal_type,
                'formula': sa.reduced_formula(),
                'stoichiometry': sa.anonymous_reduced_formula,
                'number': sg.number,
                'cell_volume': sa.volume,
                'crystal_system': sg.crystal_system,
                'num_atoms': len(sa.atoms),
                'pointgroup': sg.pointgroup,
                'has_inversion_symmetry': str(sa.has_inversion_symmetry),
                'oqmd': Path(node.name).parts[1],
                'atoms': atoms,
                'international': sg.international,
                'bravais_type': f'{sg.bravais_type_long} ({sg.bravais_type})',
            }
        )

    if task_name == 'postprocess':
        data = {
            'fmax': np.linalg.norm(output['forces'], axis=1).max(),
            'smax': abs(output['stresses']).max(),
        }

    if task_name == 'gsresults':
        data = {
            'energy': output.properties.energy,
            'gap': output.gap_info.gap,
            'gap_dir': output.gap_info.gap_dir,
        }

    return data, write_structure
