import collections.abc
from typing import Tuple, Union

import numpy as np
from ase.atoms import Atoms
from ase.utils import jsonable


class KPTGridNotFound(ValueError):
    pass


# XXX i propose we refactor this to the same module as gpaw
@jsonable('kpoints')
class KPoints:
    supported_keys = ['symmetry', 'gamma', 'density', 'size']

    def __init__(
        self,
        kpts: Union[
            np.ndarray, collections.abc.Sequence, collections.abc.Mapping
        ],
    ):
        # NOTE: kpts2kpts does many things, so we only want to allow use of
        # inputs we explicitly test and know are reliable.
        if isinstance(kpts, collections.abc.Mapping):
            assert all(item in self.supported_keys for item in kpts), (
                'Unsupported values used in kpts'
            )
        elif isinstance(kpts, np.ndarray):
            assert (3,) == kpts.shape, 'Invalid kpts array.'
        elif isinstance(kpts, collections.abc.Sequence):
            assert len(kpts) == 3, 'kpts must contain a length of 3'
        self.kpts = kpts

    def get_kpts(self, atoms: Atoms, comm) -> np.ndarray:
        """Generate k-point grid based for a specific crystal.

        If the user supplies 'density' and 'symmetry' inputs in a kpts dict, we
        generate a custom kpoint grid based on a minimum distance requirement.
        """
        if isinstance(self.kpts, collections.abc.Mapping):
            if 'density' in self.kpts and 'symmetry' in self.kpts:
                assert 'gamma' not in self.kpts
                assert 'size' not in self.kpts
                return mp_from_density(atoms, **self.kpts, comm=comm)[0]
            else:
                assert 'symmetry' not in self.kpts, (
                    'use only the symmetry keyword in combination w. density'
                )
        return self.get_kpts_from_mpgrid(atoms)

    def get_kpts_from_mpgrid(self, atoms: Atoms) -> np.ndarray:
        """
        Use ase's kpts2kpts to return a monkhorstpack kpoints grid for GPAW.

        :param atoms: Atoms object to set the kpt grid.
        :param kpts: List or np.array or dict.
            If it is a list: [nx, ny, nz] where n
            represents the number of subdivisions along the respective axis
            determined with internal methods. If np.array, these are assumed to
            be the monkhorstpack grid used by GPAW.
            If a dict: {'density': float, 'gamma': True} or
            {'size': [int, int, int], 'gamma': True}
        :return: np.array([k-points])
        """
        from ase.calculators.calculator import kpts2kpts

        return kpts2kpts(self.kpts, atoms).kpts

    def todict(self):
        return vars(self)


def mp_from_density(
    atoms: Atoms, density: float, *, symmetry: bool, comm
) -> Tuple[np.ndarray, int]:
    """
    Get a monkhorstpack grid with the lowest number of k-points in the
    reducible/irreducible Brillouin zone that still satisfies a given
    minimum distance condition in the real space (nx, ny, nz)-supercell.

    :param atoms: Atoms object used to the k-points
    :param density: Density of k-points (kpts/Å^-1)
    :param symmetry: Consider symmetry (irreducible) or not (reducible)
        when determining the k-point grid.
    :param comm: Communicator used to parallelize the symmetry reduction.
    :return: np.array([nx, ny, nz]), number of points in the brillouin
        zone
    """
    # For orthogonal cells: min_distance = 2 * np.pi * density
    min_distance = 2 * np.pi * density
    return mindistance2monkhorstpack(
        atoms=atoms,
        min_distance=min_distance,
        maxperdim='auto',
        symmetry=symmetry,
        comm=comm,
    )


def mindistance2monkhorstpack(
    atoms, *, min_distance, maxperdim=16, even=True, symmetry=False, comm
) -> Tuple[np.ndarray, int]:
    """
    If symmetry==False (default), find a Monkhorst-Pack grid
    (nx, ny, nz) with lowest number of k-points in the *reducible*
    Brillouin zone, which still satisfying a given minimum distance
    (`min_distance`) condition in real space (nx, ny, nz)-supercell.
    Returns kpt_c.

    If symmetry==True (requires gpaw), returns the lowest number
    of k-points in the *irreducible* Brillouin zone, with same
    minimum distance condition.
    Returns a tuple (kpt_c, nibz).

    Compared to ase.calculators.calculator kptdensity2monkhorstpack
    routine, this metric is based on a physical quantity (real space
    distance), and it doesn't depend on non-physical quantities, such as
    the cell vectors, since basis vectors can be always transformed
    with integer determinant one matrices. In other words, it is
    invariant to particular choice of cell representations.

    On orthogonal cells, min_distance = 2 * np.pi * kptdensity.
    """
    if symmetry:
        from gpaw.kpt_descriptor import KPointDescriptor
        from gpaw.symmetry import Symmetry

        id_a = atoms.get_chemical_symbols()
        symmetry = Symmetry(id_a, atoms.cell, atoms.pbc)
        symmetry.analyze(atoms.get_scaled_positions())

        def get_nibz(nkpts_c):
            # Note: Neglects magnetic moments for now
            kd = KPointDescriptor(nkpts_c)
            kd.set_symmetry(atoms, symmetry)
            return len(kd.ibzk_kc)

        key = get_nibz
    else:

        def get_nk(nkpts_c):
            return np.prod(nkpts_c)

        key = get_nk  # lambda nkpts_c: np.prod(nkpts_c)

    _maxperdim = maxperdim if maxperdim != 'auto' else 16
    while 1:
        try:
            kpt_c = _mindistance2monkhorstpack(
                atoms.cell,
                atoms.pbc,
                min_distance,
                _maxperdim,
                even,
                key,
                comm=comm,
            )
        except KPTGridNotFound:
            if maxperdim != 'auto':
                raise
            print(
                f'kpt grid not found with maxperdim={_maxperdim}, doubling it.'
            )
            _maxperdim *= 2
            continue
        break

    return kpt_c, key(kpt_c)


def _mindistance2monkhorstpack(
    cell,
    pbc_c,
    min_distance,
    maxperdim,
    even,
    key=lambda nkpts_c: np.prod(nkpts_c),
    *,
    comm,
):
    from ase import Atoms
    from ase.neighborlist import NeighborList

    step = 2 if even else 1
    nl = NeighborList(
        [min_distance / 2], skin=0.0, self_interaction=False, bothways=False
    )

    def err():
        raise KPTGridNotFound(
            'Could not find a proper k-point grid for the '
            'system. Try running with a larger maxperdim.'
        )

    def check(nkpts_c):
        nl.update(Atoms('H', cell=cell @ np.diag(nkpts_c), pbc=pbc_c))
        return len(nl.get_neighbors(0)[1]) == 0

    rank, size = (0, 1)
    # rank, size = (comm.rank, comm.size)
    ranges = [
        range(step, maxperdim + 1, step) if pbc else range(1, 2)
        for pbc in pbc_c
    ]
    kpts_nc = np.column_stack([*map(np.ravel, np.meshgrid(*ranges))])[
        rank::size
    ]
    kpts_nx = np.array(
        [[*nkpts_c, key(nkpts_c)] for nkpts_c in kpts_nc if check(nkpts_c)]
    )
    if len(kpts_nx):
        minid = np.argmin(kpts_nx[:, 3])
        minkpt_x = kpts_nx[minid]
    else:
        err()
        # To enable parallelization, this is required
        minkpt_x = np.array([0, 0, 0, 100_000_000], dtype=int)
    # if comm is None:
    if 1:
        # XXX DISABLED PARALLEL CODE DUE TO APPARENT BUG.  --askhl
        len(minkpt_x) or err()
        value_c = minkpt_x[:3]
        return value_c

    minkpt_rx = np.zeros((4 * comm.size,), dtype=int)
    comm.all_gather(minkpt_x, minkpt_rx)
    minkpt_rx = minkpt_rx.reshape((-1, 4))
    value_c = minkpt_rx[np.argmin(minkpt_rx[:, 3]), :3]
    value_c[0] > 0 or err()
    print('VALUE_C', value_c.shape)
    assert len(value_c) == 3, 'BROKEN, or so I think.'
    return value_c
