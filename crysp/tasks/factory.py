from ase import Atoms

from asrlib.parameters.gpaw import GPAWCalculatorFactory
from asrlib.typing import TBCommunicator
from crysp.tasks.kpoints import KPoints


class CryspCalculatorFactory(GPAWCalculatorFactory):
    def get_gpaw_params(self, atoms: Atoms, comm: TBCommunicator) -> dict:
        # Overwrite the standard gpaw_params with a special k-point sampling of
        # our choosing.
        params = super().get_gpaw_params(atoms, comm)
        if 'kpts' in params:
            params['kpts'] = KPoints(params['kpts']).get_kpts(
                atoms.copy(), comm=comm
            )
        return params
