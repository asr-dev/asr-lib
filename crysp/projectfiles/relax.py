# pragma: no cover
from pathlib import Path

import numpy as np
import taskblaster as tb
from ase import Atoms
from ase.calculators.calculator import PropertyNotImplementedError
from ase.io import Trajectory

from asrlib.recipes.gs import GPAWGroundStateRecipe
from asrlib.recipes.relax import ASERelaxRecipe
from crysp.actions.export import todb
from crysp.actions.symmetry import crystalinfo
from crysp.actions.utils import viewer
from crysp.tasks.factory import CryspCalculatorFactory


def magnetize(atoms: Atoms, *, magmoms: float) -> Atoms:
    atoms.set_initial_magnetic_moments([magmoms] * len(atoms))
    return atoms


@tb.mpi
@tb.actions(todb=todb, view=viewer, crystalinfo=crystalinfo)
def relax3d(atoms: Atoms, calculator: dict, mpi: tb.mpi) -> dict:
    """
    Perform a 3D structure relaxation using GPAW where spins are handled by
    GPAW.

    :param atoms: Atoms object to relax.
    :param calculator: Dictionary containing the GPAW input parameters.
        Dictionary containing optimizer key word arguments: fmax, smask,
        smax, gmax, identifier (for traj & log file), append_trajectory.
    :param mpi: Communicator or Taskblaster MPI object.
    :return: {'atoms': final_atoms, 'traj_path': Path}
    """
    # these are the default values that could be in the calculator but must
    # be set to preserve previous behavior.
    default_opts = {
        'fmax': 0.05,
        'smax': 0.005,
        'append_trajectory': True,
        'smask': np.array([1, 1, 1, 1, 1, 1]),
    }
    # remove optimizer keys from gpaw calculator
    opt_params = {
        key: calculator.pop(key) for key in default_opts if key in calculator
    }
    # if steps/gmax in calculator remove into opt_params. Set default value
    opt_params['steps'] = calculator.pop('gmax', 100)
    if 'steps' in calculator:
        opt_params['steps'] = calculator.pop('steps')

    atoms, is_restart = restart_from_traj(
        atoms=atoms, identifier='relax', mpi=mpi
    )

    # restart MAG, or start new relaxation
    recipe = construct_recipe(
        opt_params,
        identifier='relax',
        calc_factory=CryspCalculatorFactory(calculator.copy()),
    )
    if is_restart in [None, 'relax.traj']:
        opt = recipe.calculate(atoms=atoms, comm=mpi.comm)
        mpi.comm.barrier()

    # restart NM, or start NM relaxation
    if is_restart == 'relax-nm.traj':
        opt = nm_relax(
            atoms=atoms,
            calc_params=calculator.copy(),
            opt_params=opt_params,
            mpi=mpi,
        )
    # continuing from a Mag calc, or need to check if NM
    elif check_magstate(Trajectory(opt.traj, comm=mpi.comm)[-1].calc) == 'nm':
        opt = nm_relax(
            atoms=atoms,
            calc_params=calculator.copy(),
            opt_params=opt_params,
            mpi=mpi,
        )  # keep the returned output the same until we can migrate outputs
    mpi.comm.barrier()
    result = recipe.results(opt)
    return {'atoms': result.atoms.copy(), 'traj_path': opt.traj}


def nm_relax(atoms: Atoms, calc_params: dict, opt_params: dict, mpi):
    # set 0 on atoms to run nm calc
    atoms = magnetize(atoms, magmoms=0.0)
    calc_params.update({'txt': 'relax-nm.txt'})
    recipe = construct_recipe(
        opt_params,
        'relax-nm',
        calc_factory=CryspCalculatorFactory(calc_params),
    )
    return recipe.calculate(atoms=atoms, comm=mpi.comm)


def construct_recipe(opt_params, identifier, *, calc_factory):
    # this is so we can switch between the 2 optimizer factories in the old
    # code
    params = opt_params.copy()

    if 'smask' in params and not params['smask'].any():
        params.pop('smask')
        params.pop('smax')
        return ASERelaxRecipe.bfgs_relax_positions(
            calc_factory=calc_factory, identifier=identifier, **params
        )
    else:
        if 'smask' in params:
            params['cell_filter'] = params.pop('smask')
        return ASERelaxRecipe.bfgs_relax_cell_and_positions(
            calc_factory=calc_factory, **params, identifier=identifier
        )


def restart_from_traj(atoms: Atoms, identifier: str, mpi):
    """
    Helper function to find restart files for relax task. It checks if a mag
    calculation is done, or a NM calculation, then restarts the correct point
    of the relax task. This should be removed once we handle restarts in a
    better fashion.

    :param atoms: Original atoms object to use if no traj file is found.
    :param identifier: Identifier from optimizer_params.
    :param comm: Communicator for parallel printing
    :return: atoms, traj name
    """
    trajs = [f'{identifier}-nm.traj', f'{identifier}.traj']

    for idx, t in enumerate(trajs):
        # check if the file exists and is not empty
        if Path(t).is_file() and Path(t).stat().st_size > 0:
            mpi.parprint(
                f'\tAttempting to restart from trajectory file {trajs[idx]}'
            )
            try:
                # be extra paranoid about reading on proper rank
                traj = Trajectory(
                    filename=t, master=(mpi.comm.rank == 0), comm=mpi.comm
                )[-1]
                return traj, t
            except Exception as e:
                mpi.parprint(
                    f'\tReading trajectory file failed with exception:\n{e}'
                )
            finally:
                mpi.comm.barrier()  # wait for all ranks to finish
    mpi.parprint('\tNo trajectory file containing data found.')

    return atoms, None


def check_magstate(
    calc, atomic_mom_threshold: float = 0.1, total_mom_threshold: float = 0.1
) -> str:
    """
    compatibility code to extend backwards compatibility of old workflow data.
    """
    from crysp.projectfiles.utils import classify_magnetism

    # get magmoms
    try:
        magmoms = calc.results.get('magmoms')  # new interface
    except PropertyNotImplementedError:
        magmoms = calc.get_magnetic_moments()  # old interface
    except PropertyNotImplementedError:
        return 'unknown'

    # get magmom
    try:
        magmom = calc.results.get('magmom')  # new interface
    except Exception as e:
        print(f'Using calc.get_magnetic_moment() due to error {e}')
        magmom = calc.get_magnetic_moment()  # old interface
    return classify_magnetism(
        magmoms=magmoms,
        magmom=magmom,
        atomic_mom_threshold=atomic_mom_threshold,
        total_mom_threshold=total_mom_threshold,
    )


@tb.actions(todb=todb)
def relaxresults(traj_path: Path):
    from asrlib.recipes.relax.ase.results import ASERelaxResults

    return ASERelaxResults.from_trajectory(traj_path)


@tb.mpi
@tb.actions(todb=todb)
def gs_calculate(atoms, calculator: dict, mpi: tb.mpi):
    recipe = GPAWGroundStateRecipe.build(CryspCalculatorFactory(calculator))
    return recipe.calculate(atoms=atoms, comm=mpi.comm).path


@tb.workflow
class Relax3DWorkflow:
    """
    Performs a 3D structure optimization for on the initial structure
    assuming an initial magnetic moment of 1.0 for all atoms.
    """

    atoms = tb.var()
    calculator = tb.var()

    @tb.task
    def magnetize(self):
        return tb.node(magnetize, atoms=self.atoms, magmoms=1.0)

    @tb.task
    def relax3d_magnetic(self):
        return tb.node(
            'crysp.projectfiles.relax.relax3d',
            atoms=self.magnetize,
            calculator=self.calculator,
        )


@tb.workflow
class GsWorkflow:
    atoms = tb.var()
    calculator = tb.var()

    @tb.task
    def groundstate(self):
        return tb.node(
            'crysp.projectfiles.relax.gs_calculate',
            atoms=self.atoms,
            calculator=self.calculator,
        )

    @tb.task
    def gsresults(self):
        return tb.node(
            'crysp.postprocess.gs.gsresults',
            gpw_path=self.groundstate,
            d3=False,
        )  # hardcoded false since this code is only
        # used for pbe calculations. The new wfs are used for d3.
