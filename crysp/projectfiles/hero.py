# pragma: no cover
import taskblaster as tb

from asrlib.recipes.gs import GPAWGroundStateRecipe
from asrlib.recipes.relax import ASERelaxRecipe
from crysp.tasks.factory import CryspCalculatorFactory
from crysp.workflows.hero_workflow import HeroRelaxWorkflow


def define_workflow():
    @tb.parametrize_glob('*/material', globmethod='new')
    def workflow(material):
        # from gpaw import Davidson
        # old workflow format
        gpaw_params = {
            # 'eigensolver': Davidson(3),
            # 'mixer': {'method': 'fullspin', 'backend': 'pulay'},
            'convergence': {'density': 1e-06},
            'xc': 'PBE',
            'kpts': {'density': 4, 'symmetry': True},
            'mode': {'ecut': 800, 'name': 'pw'},
            'occupations': {'name': 'fermi-dirac', 'width': 0.05},
            'parallel': {'band': 1, 'domain': 1},
        }
        calc_params_relax = {**gpaw_params, 'txt': 'relax.txt'}
        calc_params_gs = {
            **gpaw_params,
            'txt': 'gs.txt',
            'nbands': '200%',
            'convergence': {'bands': 'CBM+3.0'},
            'kpts': {'density': 8, 'symmetry': True},
        }

        # new workflow inputs: w/ d3
        params = {**gpaw_params, 'd3': True, 'txt': 'relax.txt'}
        params_gs = {**calc_params_gs, 'd3': True, 'txt': 'gs.txt'}

        return HeroRelaxWorkflow(
            material=material.atoms,
            calculator_relax=calc_params_relax,
            calculator_groundstate=calc_params_gs,
            relax_recipe=ASERelaxRecipe.bfgs_relax_cell_and_positions(
                calc_factory=CryspCalculatorFactory(params),
                append_trajectory=True,
            ),
            gs_recipe=GPAWGroundStateRecipe.build(
                CryspCalculatorFactory(params_gs),
                d3=True,
            ),
        )

    return workflow


workflow = define_workflow()
