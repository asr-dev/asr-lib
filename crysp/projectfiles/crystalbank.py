import taskblaster as tb

from asrlib.recipes.gs import GPAWGroundStateRecipe
from asrlib.recipes.relax import ASERelaxRecipe
from crysp.tasks.factory import CryspCalculatorFactory
from crysp.workflows.crystalbank import CrystalBankWf


def define_workflow():
    @tb.parametrize_glob('*/material', globmethod='new')
    def workflow(material):
        # from gpaw import Davidson
        gpaw_params = {
            # 'eigensolver': Davidson(3),
            # 'mixer': {'method': 'fullspin', 'backend': 'pulay'},
            'convergence': {'density': 1e-06},
            'xc': 'PBE',
            'gpaw_new': True,
            'kpts': {'density': 4, 'symmetry': True},
            'mode': {'ecut': 800, 'name': 'pw'},
            'occupations': {'name': 'fermi-dirac', 'width': 0.05},
            'parallel': {'band': 1, 'domain': 1, 'gpu': True},
        }
        gpaw_gs = {
            'nbands': '200%',
            'convergence': {'bands': 'CBM+3.0'},
            'kpts': {'density': 8, 'symmetry': True},
        }

        relax_recipe = ASERelaxRecipe.bfgs_relax_cell_and_positions(
            CryspCalculatorFactory({**gpaw_params, 'txt': 'relax.txt'}),
            append_trajectory=True,
        )
        gs_recipe = GPAWGroundStateRecipe.build(
            CryspCalculatorFactory({**gpaw_params, **gpaw_gs, 'txt': 'gs.txt'})
        )

        return CrystalBankWf(
            material=material.atoms,
            relax_recipe=relax_recipe,
            gs_recipe=gs_recipe,
        )

    return workflow


workflow = define_workflow()
