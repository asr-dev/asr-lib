"""Various utility functions that may be of use to multiple tasks."""


def classify_magnetism(
    magmoms: list,
    magmom: list,
    atomic_mom_threshold: float,
    total_mom_threshold: float,
) -> str:
    # find any atoms with magmom above the threshold
    max_atom_mom = abs(magmoms).max()
    if max_atom_mom < atomic_mom_threshold:
        return 'nm'

    # test if there is afm ordering if magmoms above the threshold
    total_mom = abs(magmom)
    if total_mom < total_mom_threshold and max_atom_mom > atomic_mom_threshold:
        return 'afm'
    return 'fm'
