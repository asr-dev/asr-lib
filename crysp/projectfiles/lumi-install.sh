#!/bin/bash
set -e

export EBU_USER_PREFIX=/projappl/project_465000939/EasyBuild
module purge
module load LUMI/22.12 partition/G
module load cpeGNU/22.12
module load craype-accel-amd-gfx90a
module load rocm/5.2.3
module load cray-python/3.9.13.1
module load cray-fftw/3.3.10.1
module load CuPy/12.2.0-cpeGNU-22.12
module load libxc/6.2.2-cpeGNU-22.12

echo "Modules loaded succesfully" 

cd /projappl/project_465000939
mkdir -p $USER
cd $USER
python -m venv gpaw-venv
cd gpaw-venv
git clone git@gitlab.com:ase/ase.git
git clone git@gitlab.com:gpaw/gpaw.git
git clone git@gitlab.com:taskblaster/taskblaster.git
git clone git@gitlab.com:asr-dev/asr-lib.git

echo "GPAW, TaskBlaster, ASR-LIB, and ASE cloned"

cp bin/activate tmp_activate

echo "export EBU_USER_PREFIX=/projappl/project_465000939/EasyBuild \
&& module purge \
&& module load LUMI/22.12 partition/G \
&& module load cpeGNU/22.12 \
&& module load craype-accel-amd-gfx90a \
&& module load rocm/5.2.3 \
&& module load cray-python/3.9.13.1 \
&& module load cray-fftw/3.3.10.1 \
&& module load CuPy/12.2.0-cpeGNU-22.12 \
&& module load libxc/6.2.2-cpeGNU-22.12
export MPICH_GPU_SUPPORT_ENABLED=1
export GPAW_SETUP_PATH=/projappl/project_465000939/gpaw-setups-0.9.20000
export GPAW_NEW=1
export GPAW_GPU=1
" > bin/activate

cat tmp_activate >> bin/activate
rm tmp_activate

source bin/activate

cd taskblaster
pip install -e .
cd ..
echo "taskblaster installed"

cd asr-lib 
git checkout dynamic_main 
pip install -e . 
cd ..
echo "asr-lib installed"

pip install myqueue
echo "myqueue installed"

cd ase 
pip install -e . 
cd ..
echo "ASE installed"

cd gpaw 
cp doc/platforms/Cray/siteconfig-lumi-gpu.py siteconfig.py 
echo "
# Overwriting ELPA configuration (set to true above)
elpa=False
libraries.remove('elpa')
" >> siteconfig.py
pip install -v -e . 
cd ..
echo "GPAW installed"

