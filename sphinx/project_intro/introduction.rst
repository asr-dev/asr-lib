============
Introduction
============

The `asr-lib` package utilized: `ASE`_, `TaskBlaster`_, and `GPAW`_ to
perform high-throughput ab-initio calculations. The `asr-lib` package is
tailored to addressing scientific questions regarding the stability,
reactivity, optical, and catalytic properties of crystals.

We utilize the `TaskBlaster`_ package as our high-throughput framework for
constructing workflows and enabling automatic tracking, maintaining data
province, managing task/workflow dependencies, among others. `TaskBlaster`
interfaces with `MyQueue`_ and extends the functionality of queue submission
and job management jobs.


.. _ASE: https://gitlab.com/ase/ase
.. _GPAW: https://gitlab.com/gpaw/gpaw
.. _TaskBlaster: https://gitlab.com/taskblaster/taskblaster
.. _MyQueue: https://myqueue.readthedocs.io/en/latest/

Overview of ASR-LIB Package
=============================

To run or build your own workflows within `asr-lib` or `TaskBlaster` package
one would find the Recipes, Workflows, and Tasks directories most useful. These
3 directories contain pre-build computational workflows/sub-workflows/tasks
(Recipes), a collection of well-defined and related workflow tasks (Workflows),
and individual components of workflows (Tasks). A more detailed explanation
is provided below:

1. tasks

    A "task" is the lowest-level instruction within `TaskBlaster`. It should
    be a simple, well-defined function: e.g. make a supercell, optimize this
    structure, perform a bandstructure. The key idea is a task is a short
    and specific piece of code that performs a singular function. The *tasks*
    directory is a library of these simple, re-usable bits of code that are
    commonly used to construct workflows. These tasks should accept some
    well-defined input and return some output.

    The key to writing a good task is to have well-defined, short, and
    digestible code. It should not contain important decisions (if X do
    that) about the flow of data throughout the workflow. These kinds of
    decisions are best to represent as a `dynamic task`.

2. workflows

    If a task is the smallest part of a workflow, then a workflow is
    composed of a collection of related `tasks`. The "workflows" directory
    contains a set of pre-defined *tasks* that have been built up into
    useful workflows that computes a materials' property.

    Workflows outline all the high level, important scientific decisions made
    in a workflow. They should be easy to read and understand. They are not
    concerned with the nitty-gritty details of the calculations as that
    is what a `task` is for.

3. recipes

    The highest level of abstraction in this framework is a "recipe", composed
    of a set of parameters, workflows, and sub-workflows. A recipe is more of a
    user defined master workflow. Recipes generally tend to be very
    user-specific, therefore this directory is provided merely to show users
    how to construct more complex workflows.

    A recipe here means the same as the old `ASR` just in expressed in a
    slightly different syntax: recipes provided a set of instructions
    that when carried out computes some materials' property. These recipes
    typically contain workflows or sub-workflows that are a collection of
    well-defined computational *tasks* and calculation parameters.

    The major difference between the old and new workflow format is in how the
    connection between each task is expressed (syntax). In this new format,
    important scientific parameters/decisions are expressed in a high level
    syntax via workflows, sub-workflows, and tasks.

Additionally, there is the `post-processing` & `actions` directories containing
routines for analyzing results and decorating tasks with specific actions once
the task has been run.

1. actions

    To equip workflow tasks with special functionality we use decorators.
    This feature is currently under activate development.

2. postprocess

    The *postprocess* directory contains leaf node tasks that analyze the
    results of the pre-defined workflows. One can use this as a basis to
    construct your own post-processing routines to create a database.


How to cite ASR-LIB
=====================

If you use `asr-lib` in your research, please consider citing the following
work:

    <citations to come>

License
=======

The `asr-lib` package is released under the GNU General Public Version 3
`License <https://fsf.org/>`_. Copyright (C) 2007 Free Software Foundation,
Inc. The terms of the license can be found in the main directory of this
software package under the LICENSE file.

About the Team
==============

Kristian Sommer Thygesen (P.I.) in the Computational Atomic-scale Materials
Design (CAMD) started `asr-lib` in 2022, and is project lead.

**Developers**

1. Ask Hjorth Larsen

2. Mark Kamper Svendsen

3. Tara Maria Boland

4. Mikael Kuisma

**Previous Developers**

1. Julian Heske

Copyright Policy
================

`asr-lib` uses a shared copyright model. Each contributor maintains
copyright over their contributions to `asr-lib`. But, it is important
to note that these contributions are typically only changes to the 
repositories. Thus, the `asr-lib` source code, in its entirety is not
the copyright of any single person or institution. Instead, it is the 
collective copyright of the entire `asr-lib` Development Team.

With this in mind, the following banner should be used in any source 
code file to indicate the copyright and license terms::

  # Copyright (c) CAMD Development Team.
  # Distributed under the terms of the GNU License.

