==================
Installing ASR-LIB
==================

There are two options for installing `asr-lib`: 1) anaconda environment or
2) via the `install_asrlib.sh` script.

Install with bash script
========================
1. Download `install_asrlib.sh` from GitLab `here <https://gitlab.com/asr-dev/asr-lib
.git>`_ provided within the `asr-lib` top-level directory.

2. `bash install_asrlib.sh venv-asrlib`


Install via Anaconda
====================
1. Install `asr-lib` in a clean environment using `python=3.8`.

  * ``conda create --name asrlib python=3.8``

2. Activate your virtual environment:

  * ``conda activate asrlib``

3. Clone the repo from GitLab `here <https://gitlab.com/asr-dev/asr-lib
.git>`_ or using the line below to a location of your choosing.

  * ``git clone https://gitlab.com/asr-dev/asr-lib.git``

4. From within the ASR-LIB directory run:

  * ``python setup.py develop`` or ``python setup.py install``

5. [OPTIONAL] If you use jupyter notebooks follow these instructions to
ensure the ipykernel can be found.

  * Activate your environment. For anaconda: ``conda activate asrlib``
  * Check ipykernel is installed ``pip install ipykernel``
  * ``python -m ipykernel install --user --name asrlib``

Setting up dependencies
========================
All dependencies are currently handled by the setup script.


