References
==========

Some of `asr-lib`’s functionality is based on scientific advances/principles
developed by various scientists. If you use `asr-lib` in your research, you
may wish to consider citing the following works:

    Gjerding, M., Skovhus, T., Rasmussen, A., Bertoldo, F., Larsen, A. H.,
    Mortensen, J. J., and Thygesen, K. S. (2021). `Atomic Simulation Recipes: A
    Python framework and library for automated workflows. <https://www.doi
    .org/10.1016/j.commatsci.2021.110731>`_ Comput. Mater. Sci., 199, 110731.

    Larsen, A. H., Mortensen, J. J., Blomqvist, J., Castelli, I. E.,
    Christensen, R., Dułak, M., et. al. (2017). `The atomic simulation
    environment—a Python library for working with atoms. <https://www.doi
    .org/10.1088/1361-648X/aa680e>`_ J. Phys.: Condens. Matter, 29 (27),
    273002.

    Mortensen, J. J., Hansen, L. B., and Jacobsen K. W. (2005). `Real-space
    grid implementation of the projector augmented wave method. <https://www
    .doi.org/10.1103/PhysRevB.71.035109>`_ Phys. Rev. B, 71, 035109.

    Enkovaara, J., Rostgaard, C., Mortensen, J. J., et al. (2010). `Electronic
    structure calculations with GPAW: a real-space implementation of the
    projector augmented-wave method. <https://www.doi.org/10
    .1088/0953-8984/22/25/253202>`_ J. Phys.: Condens. Matter, 22, 253202.