================
Development Team
================

The `asr-lib` package has a small development team.

Lead Maintainers
================

| **Ask Hjorth Larsen**   |askhl|  |0000-0001-5267-6852|
| Postdoctoral Researcher, Department of Physics
| Danmarks Tekniske Universitet, Denmark

.. |askhl| image:: ../images/gitlab.png
   :target: https://gitlab.com/askhl
   :width: 26
   :height: 26
   :alt: GitLab profile for askhl

.. |0000-0001-5267-6852| image:: ../images/orcid.jpg
   :target: https://orcid.org/0000-0001-5267-6852
   :width: 26
   :height: 26
   :alt: ORCID profile for 0000-0001-5267-6852


| **Tara M. Boland**   |tboland1|   |0000-0002-2587-5677|
| Postdoctoral Researcher, Department of Physics
| Danmarks Tekniske Universitet, Denmark

.. |tboland1| image:: ../images/gitlab.png
   :target: https://gitlab.com/tboland1
   :width: 26
   :height: 26
   :alt: GitLab profile for tboland1

.. |0000-0002-2587-5677| image:: ../images/orcid.jpg
   :target: https://orcid.org/0000-0002-2587-5677
   :width: 26
   :height: 26
   :alt: ORCID profile for 0000-0002-2587-5677


| **Fredrik Mosse Nilsson**   |fredrik.mosse.nilsson|  |0000-0002-0163-3024|
| Postdoctoral Researcher, Department of Physics
| Danmarks Tekniske Universitet, Denmark

.. |fredrik.mosse.nilsson| image:: ../images/gitlab.png
   :target: https://gitlab.com/fredrik.mosse.nilsson
   :width: 26
   :height: 26
   :alt: GitLab profile for fredrik.mosse.nilsson

.. |0000-0002-0163-3024| image:: ../images/orcid.jpg
   :target: https://orcid.org/0000-0002-0163-3024
   :width: 26
   :height: 26
   :alt: ORCID profile for 0000-0002-0163-3024


List of Previous Developers (A-Z)
=================================
| **Mark Kamper Svendsen**   |Mark_Kamper|  |0000-0001-9718-849X|
| Postdoctoral Researcher, Department of Physics
| Danmarks Tekniske Universitet, Denmark

.. |Mark_Kamper| image:: ../images/gitlab.png
   :target: https://gitlab.com/Mark_Kamper
   :width: 26
   :height: 26
   :alt: GitLab profile for Mark_Kamper

.. |0000-0001-9718-849X| image:: ../images/orcid.jpg
   :target: https://orcid.org/0000-0001-9718-849X
   :width: 26
   :height: 26
   :alt: ORCID profile for 0000-0001-9718-849X


| **Julian Heske**   |jjheske|  |0000-0001-6503-9967|

.. |jjheske| image:: ../images/gitlab.png
   :target: https://gitlab.com/jjheske
   :width: 26
   :height: 26
   :alt: GitLab profile for jjheske

.. |0000-0001-6503-9967| image:: ../images/orcid.jpg
   :target: https://orcid.org/0000-0001-6503-9967
   :width: 26
   :height: 26
   :alt: ORCID profile for 0000-0001-6503-9967