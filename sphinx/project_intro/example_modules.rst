Overview
========

To get started using `asr-lib`, various tutorials and examples have been
created using Jupyter Notebooks. These notebooks demonstrate the basic
functionality of `asr-lib` to enable users to quickly learn how to use the
various modules within this package. All these tutorials are located at
*asr-lib/examples* and should work out of the box .

Currently examples need to be written to show the user how to:

- write post-processing

- deal with:

    - failed job
    - restart a job
    - delete a job

TaskBlaster
===========

# Improve User Interface

To improve the experience of TaskBlaster's command line interface, you can add the following to your ~/.bashrc.

1. shopt -s globstar  # this enables one to use glob to expand pattern matching through directories and subdirectories via '**'
2. The output from `tb completion`


# Submit tasks into the TaskBlaster Queue
TaskBlaster has its own queue which workers will query to find available jobs. If you submit a job through the 
`tb worker submit` command, once the queue system (SLURM, PBS, ect.) picks up the job, the TaskBlaster worker will 
check for jobs which have fulfilled dependences, and are marked in the state 'queue' and therefore are available
to be picked up by the worker.

Submit tasks into the TaskBlaster queue:
- `tb submit tree/<path_to_tasks>` wild card supported

Update a tasks input:
- update the workflow file
- `tb workflow workflow.py`
- If the job state is not new, unrun the task, apply workflow, then the
parameters will be updated.


Removing tables from the registry
=================================
While developing taskblaster, columns might be added or removed to tables in registry database. If this happens, you will encounter an error like:

``sqlite3.OperationalError: no such column <some_name>``

However, it is only the table ```registry``` in the registry.db that is essential for performing the calculations. The rest of the tables can safely be removed without causing issues.
Thus, if the runlog table (which contains information about the workers) in the registry.db has been changed in taskblaster and causing issues, you can safely delete the table from your database, and it will be regenerated as an empty table the next time you issue command ```tb workflow```. You delete the runlogs table from the registry database with the following steps:
#. ``cd .taskblaster``
#. ``sqlite3 registry.db``
#. ``sqlite> drop table runlog;``

Unlocking the registry
======================
The database is exclusively locked during some processes. If these processes are killed at the wrong point, an exclusive lock will remain on the database. Sometimes it is sufficient to look up the process and kill it but other times the processes is killed without unlocking the database. This section outlines how to remove the lock on the database.

In the directory where the `tree` is located, a `.taskblaster` directory also exists. Note: If ``tb stat`` works after any of these steps, you can safely delete the locked database.

#. ``cd .taskblaster``
#. Make a copy for safty. 

   * ``cp registry.db registry.db_locked``

#. Check for processes locking your database.

   * ``fuser registry.db`` 

#. Kill the process to free the database, if there is process hanging.

   * ``kill -9 <process_id>``

#. Next try to move the database and free the lock.

   * ``mv registry.db registry_temp.db``
   * ``mv registry_temp.db registry.db``

#. If this does not work, try to vacuum the database.

   * ``sqlite3 registry.db "VACUUM;"``

#. If the database is still not unlocked make a backup database.

   * ``sqlite3 registry.db ".backup registry.db.bak"``
   * ``cp registry.db.bak registry.db``

#. Try to use sqlite to recover the database.

   * ``sqlite3 registry.db ".recover" | sqlite3 registry-recovered.db``
   * check that it is no longer locked: ``sqlite3_analyzer registry-recovered.db``
   * ``cp registry-recovered.db registry.db``

.. toctree::
   :maxdepth: 1

   examples/ExampleWorkflow
