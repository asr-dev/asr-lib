===============
Developer Docs
===============
To keep our work consistent across multiple on-going projects we have a set
of coding conventions which developers must follow to provide users with a
consistent experience using our software.


Naming Conventions
==================
Here we outline general guidelines for naming variables, functions, and
classes within the repository. These are conventions discussed by the dev
team. If you wish to propose a change, we can discuss the change as a group.

A general guiding principle of naming things is the name should answer the
question "what". What it does, what value it holds. The code explains the
"how". How it does the "what". Comments explain "why". Why something is the
way it is, why it was needed. This is needed when the purpose or reason
behind a design decision was not obvious.

#. A max length of 24 characters is used for naming.
#. If possible, avoid abbreviations in user facing functions. For clarity, we
   require the abbreviation to be defined in a comment or doc string
   (user-facing code) when used.

   * Allowed abbreviations:

     * gs: groundstate
     * bs: bandstructure
     * wf: workflow

#. function naming

   * lower_case
   * can contain underscores

#. class naming

   * CamelCase
   * no underscores


General Coding Conventions
==========================
#. We require inputs and have few default values. If you want to suggest a
   default value, do so in the doc string.
#. Code needs A DOC STRING. So add them to classes/functions/variables so we
   know what the intended use is and what values to provide.
#. When possible, explicitly state variable and return types with ``typing``.
#. Use ``flake8`` to keep code formatting consistent (PEP8).
#. Never hard code anything.
#. Generally, avoid nesting functions and classes calls more than 3 levels.

   * Example: ``plot(make_plot(get_plot)))`` where none of them actually
     produce a plot, is not allowed. This makes the code hard to follow and
     understand.

#. Do NOT copy-paste code. This indicates you either need a function or a class.
#. TODO, Warnings, or important notations are indicated in code comments using
   "XXX", for consistency.


Workflow Conventions
====================
#. All workflow classes are named with the suffix Wf, indicating they are a
   workflow.

   * Example: BsWf -> bandstructure workflow. 

#. All input structures are called ``atoms`` for workflows. Exceptions are made
   for logical cases such as ``host_atoms``.
#. All calculator input parameters are called: ``calc_params_<specification>``.
   This avoids any confusing between calculator parameters and a calculator
   object.

   * Example: ``calc_params_relax``.

#. For workflows, it is okay if we nest when needed. But try to avoid sending
   the user on a wild goose chase to identify the tasks which compose a workflow.


Directory names  
================
#. ``workflows``

     * These are classes which provide a high level view of all important
       decisions made with regards to a set of tasks.
     * They don't have any calculation parameters defined.

#. ``tasks``

   * These are functions that perform a singular instruction.
   * Tasks can have task inspection functions which live near the relevant
     tasks.

      * Use ``tb view --help`` for more information

   * These actions are meant to provide utility to a task, it doesn't produce
     output.

#. ``postprocess``

   * Used to process the data from a calculation. 
   * In general, postprocessing is a task you would want to equip (decorate)
     with task inspection actions.
   * This is not strict but I think that makes the most logical place to see
     results

  
Plotting has 3 purposes  
========================
#. daily work 

   * this code goes in ``asrlib`` next to the task  

#. webpages 

   * specific implementation for pages can be tied to project?? 

#. publications 

   * code lives in project repositories  


GitLab
=======
* Create an issue for new code to track it’s progress 
* Branch from master, merge to master quickly
* Report known bugs via issues
* Report concerns via issues
* Make minimum merge requests and get it merged into master
* We get feedback early. This means we get a reviewer even at an early stage,
  make a Draft.

