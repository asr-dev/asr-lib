from dataclasses import dataclass

import pytest
import taskblaster as tb
from ase.spacegroup.symmetrize import check_symmetry

from asrlib import recipe_node
from asrlib.results.gpaw import GPAWProperties
from asrlib.workflows.relax import RelaxRecipeWorkflow
from c2db.recipes.relax import (
    C2DBRelaxAndSymmetrizeRecipe,
    C2DBRelaxCalculatorFactory,
)
from c2db.recipes.symmetrize import (
    C2DBSymmetrizationResults,
    C2DBSymmetrizeAndNormalizeRecipe,
    C2DBSymmetryAnalyzer,
)
from c2db.test.test_symmetrize import Dataset as _Dataset
from c2db.test.utils import get_test_factory


@dataclass
class Dataset(_Dataset):
    epa: float = 0.0

    def check(self, results):
        assert isinstance(results.properties, GPAWProperties)
        assert isinstance(results.symresults, C2DBSymmetrizationResults)
        # Check symmetrization
        super().check(results.symresults)
        # Check output
        assert check_symmetry(results.atoms).number == self.spgrp
        assert results.epa == pytest.approx(self.epa, abs=0.02)


@tb.workflow
class SelfTestingRelaxAndSymmetrizeWorkflow:
    data = tb.var()
    recipe = tb.var()

    @tb.subworkflow
    def relaxation(self):
        return RelaxRecipeWorkflow(
            atoms=self.data.atoms,
            recipe=self.recipe,
        )

    @tb.task
    def test(self):
        return recipe_node('check', self.data, results=self.relaxation.results)


def test_mos2_relaxation(mos2, run_workflow, check_tb_tasks):
    # Check that the structure cannot be symmetrized initially within the
    # target precision
    data = Dataset(**mos2.break_symmetry().to_dict(), epa=-7.21)
    symprec = 0.02
    assert check_symmetry(data.atoms, symprec=symprec).number < mos2.spgrp
    # Set up workflow to relax, symmetrize and check the final structure
    wf = SelfTestingRelaxAndSymmetrizeWorkflow(
        data=data,
        recipe=C2DBRelaxAndSymmetrizeRecipe(
            calc_factory=get_test_factory(
                C2DBRelaxCalculatorFactory, spinpol=False
            ),
            symmetrize_recipe=C2DBSymmetrizeAndNormalizeRecipe(
                analyzer=C2DBSymmetryAnalyzer.build(nsymprecs=25),
                symprec=symprec,
            ),
            # Make sure to finish up in a few steps
            fmax=0.08,
            smax=0.05,
        ),
    )
    run_workflow(wf)
    check_tb_tasks(ntasks=3)
