from pathlib import Path

import pytest
from ase import Atoms
from gpaw import PW
from gpaw.new.ase_interface import GPAW

from asrlib.results.gpaw import GPWFile
from c2db.recipes.band_structure import C2DBDFTBandStructureRecipe


def test_bs(run_recipe_task):
    atoms = Atoms('H', cell=[1.1, 1.1, 5], pbc=(1, 1, 0))
    atoms.center()
    atoms.calc = GPAW(mode=PW(300), nbands=2, kpts=(2, 2, 1))
    atoms.get_potential_energy()
    atoms.calc.write('tree/gs.gpw')
    band_path = atoms.cell.bandpath('GX', pbc=atoms.pbc)

    recipe = C2DBDFTBandStructureRecipe(upper_energy_cutoff=0.5, npoints=10)
    record = run_recipe_task(
        'calculate',
        recipe,
        pass_comm=True,
        gpw_file=GPWFile(Path('tree/gs.gpw').resolve()),
        band_path=band_path,
    )
    gpw_file, band_path = record.output
    assert gpw_file.path.is_file()

    record = run_recipe_task(
        'results', recipe, gpw_file=gpw_file, band_path=band_path
    )
    assert record.output.fermi_level == pytest.approx(-1.682, abs=0.01)
