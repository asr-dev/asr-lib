import pytest

from asrlib.test.test_symmetry import Dataset as _Dataset
from asrlib.test.test_symmetry import SelfTestingSymmetrizeWorkflow
from c2db.recipes.preconditioners.normalize import check_2d_cell_normalization
from c2db.recipes.symmetrize import (
    C2DBSymmetrizeAndNormalizeRecipe,
    C2DBSymmetryAnalyzer,
)


class Dataset(_Dataset):
    def __post_init__(self):
        # Check that symmetry is broken initially
        super().__post_init__()
        # Check that cell isn't normalized initially
        with pytest.raises(AssertionError):
            check_2d_cell_normalization(self.atoms)

    def check(self, symresults):
        # Check symmetrization
        super().check(symresults)
        assert symresults.layer_group == self.lagrp
        assert (
            symresults.international_layer_group_symbol
            == self.international_layer_group_symbol
        )
        assert symresults.layer_bravais_type == self.layer_bravais_type
        # Check normalization
        check_2d_cell_normalization(symresults.atoms)


def test_mos2_symmetrization(mos2, run_workflow, check_tb_tasks):
    wf = SelfTestingSymmetrizeWorkflow(
        data=Dataset(**mos2.break_symmetry().to_dict()),
        recipe=C2DBSymmetrizeAndNormalizeRecipe(
            analyzer=C2DBSymmetryAnalyzer.build(nsymprecs=25),
        ),
    )
    run_workflow(wf)
    check_tb_tasks(ntasks=2)
