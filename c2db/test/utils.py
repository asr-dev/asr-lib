from c2db.parameters import C2DBKPointSampling


def _default_test_params():
    # Go-to test parameters, defining test defaults through `get_test_factory`
    return dict(
        ecut=200,
        kpts=C2DBKPointSampling(density=1.0),
        convergence={},
    )


def get_test_factory(cls, **kwargs):
    return cls(**{**_default_test_params(), **kwargs})
