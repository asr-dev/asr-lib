import pytest
from ase.spacegroup.symmetrize import check_symmetry

from asrlib.recipes.preconditioners.distort import break_symmetry
from c2db.recipes.preconditioners import C2DBStructureNormalizer
from c2db.recipes.preconditioners.normalize import (
    check_2d_cell_normalization,
    normalize_2d_cell,
)


def test_mos2_cell_normalization(mos2):
    # Check that the original MoS2 atoms aren't normalized
    with pytest.raises(AssertionError):
        check_2d_cell_normalization(mos2.atoms)
    # Normalize, check cell and space group
    atoms = normalize_2d_cell(mos2.atoms)
    check_2d_cell_normalization(atoms)
    assert check_symmetry(atoms).number == mos2.spgrp


def test_mos2_prenormalization(mos2):
    # Set up repeated atoms with broken symmetries
    atoms = break_symmetry(mos2.atoms.repeat((3, 1, 1)), rstdev=0.005)
    assert check_symmetry(atoms).number == mos2.min_spgrp
    with pytest.raises(AssertionError):
        check_2d_cell_normalization(atoms)
    # Normalize and check symmetries and cell
    normalizer = C2DBStructureNormalizer()
    atoms = normalizer(atoms)
    assert check_symmetry(atoms).number == mos2.spgrp
    check_2d_cell_normalization(atoms)
    assert len(atoms) == len(mos2.atoms)
