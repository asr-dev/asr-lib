from __future__ import annotations

from pathlib import Path

from taskblaster import State
from taskblaster.records import get_record_from_repo
from taskblaster.repository import Repository


def get_record_from_tree(taskname: str, root: Path | None = None):
    root = root if root else Path()
    directory = root / 'tree' / taskname
    with Repository.find(directory=directory) as repo:
        for node in repo.tree([directory.resolve()]).nodes():
            assert node.name == taskname, (
                f'Got {node.name}, expected {taskname}'
            )
            if node.state is not State.done:
                raise ValueError(
                    f'Task {taskname} in {root} has not been completed'
                )
            return get_record_from_repo(node.name, repo)
