from collections.abc import Mapping
from dataclasses import dataclass, fields, is_dataclass
from functools import cached_property, partial
from pathlib import Path

import pytest
from taskblaster.records import get_record_from_repo
from taskblaster.repository import Repository
from taskblaster.state import State

from asrlib.recipes.preconditioners import Magnetizer
from c2db.parameters import C2DBKPointSampling
from c2db.recipes.gs import (
    C2DBGroundStateCalculatorFactory,
    C2DBGroundStateRecipe,
)
from c2db.recipes.preconditioners import (
    C2DBMagnetizerCollection,
    C2DBStructureNormalizer,
)
from c2db.recipes.relax import (
    C2DBRelaxAndSymmetrizeRecipe,
    C2DBRelaxCalculatorFactory,
)
from c2db.test.utils import get_test_factory
from c2db.test.workflows import get_record_from_tree
from c2db.workflows.core import (
    C2DBCoreWorkflow,
    C2DBRelaxWorkflowConfiguration,
)


@dataclass
class C2DBRelaxAndSymmetrizeTestRecipe(C2DBRelaxAndSymmetrizeRecipe):
    steps: int = 1  # overwrite the number of relaxation steps


class C2DBRelaxWorkflowTestConfiguration(C2DBRelaxWorkflowConfiguration):
    @cached_property
    def relax_recipe(self):
        return C2DBRelaxAndSymmetrizeTestRecipe(
            calc_factory=self.calc_factory,
            d3=self.d3,
        )


def test_mos2_core_workflow(mos2, actually_run_workflow):
    wf = C2DBCoreWorkflow(
        monolayer=mos2.atoms,
        # Use a small vacuum for testing
        normalizer=C2DBStructureNormalizer(vacuum=5.0),
        relax_wf_config=C2DBRelaxWorkflowTestConfiguration(
            # Use cheap test factory
            calc_factory=get_test_factory(
                C2DBRelaxCalculatorFactory,
                ecut=100,
                kpts=C2DBKPointSampling(density=0.5),
            ),
            # Mock the magnetic state assessment by starting in a
            # nonmagnetic configuration
            magnetizer=C2DBMagnetizerCollection(
                {'low_spin': Magnetizer(0.0)}, 'low_spin'
            ),
        ),
        gs_recipe=C2DBGroundStateRecipe(
            calc_factory=get_test_factory(C2DBGroundStateCalculatorFactory),
        ),
    )

    # ----- Run the workflow ----- #

    # Run the `unrelaxed` task (normalizing the structure) as well as the
    # `trial_magnetic_state` tasks (`calculate` and `results`) and
    # `stayed_magnetic` of `iterative_relax.initial_state`.
    actually_run_workflow(wf)
    record = get_record_from_tree(
        'iterative_relax/initial_state/stayed_magnetic'
    )
    assert record.output is False  # gpaw does not stabilize magmoms in MoS2
    # Run `iterative_relax.initial_state.classification`,
    # `iterative_relax.relax_and_magstate.relaxation` (with `calculate` and
    # `results`) as well as the `trial_magnetic_state` (with `calculate` and
    # `results`) and `stayed_magnetic` tasks of
    # `iterative_relax.relax_and_magstate.magnetic_state`
    actually_run_workflow(wf)
    record = get_record_from_tree(
        'iterative_relax/relax_and_magstate-001/magnetic_state/stayed_magnetic'
    )
    assert record.output is False
    # Run `iterative_relax.relax_and_magstate.magnetic_state.classification`,
    # and `iterative_relax.stop_iteration`
    actually_run_workflow(wf)
    record = get_record_from_tree('iterative_relax/stop_iteration-001')
    assert record.output is True  # we stop after the first iteration
    # Run `iterative_relax.results` and `ground_state` (consisting of
    # `calculate` and `results`)
    actually_run_workflow(wf)
    record = get_record_from_tree('ground_state/results')
    assert record.output.cell_area == pytest.approx(5.8704, abs=1e-3)

    # ----- Test the output ----- #

    # Set up whitelist of allowed input/output types in addition to the c2db
    # specific inputs and outputs (the latter we are in full control off, hence
    # we need not to worry about those). The whitelist has a hierachy:
    # 1) First of all, we whitelist widely used objects from well established
    #    external libraries, that is, we trust that we can decode these
    #    indefinately:
    whitelist = [
        # __module__, __name__
        ('ase.atoms', 'Atoms'),
        ('numpy', 'ndarray'),
        ('pathlib', 'PosixPath'),
    ]
    # 2) Secondly, we whitelist all asrlib `Result` and `Parameter`
    #    dataclasses, which (a) aren't nested, (b) have only a few data fields,
    #    each of a simple type. Thanks to the simplicity of these objects, we
    #    can more or less *guarantee* that they continue to be deserializable.
    whitelist += [
        ('asrlib.recipes.preconditioners.magnetize', 'Magnetizer'),
        ('asrlib.recipes.relax.ase', 'Optimization'),
        ('asrlib.results.gpaw', 'GPWFile'),
        ('asrlib.recipes.magstate', 'MagneticStateClassification'),
    ]
    # 3) Thirdly, we whitelist asrlib `Result` and `Parameter` objects which
    #    are nested, but only have a very few data fields, all of which should
    #    belong to a more serene rung of the hierachy. Due to the nesting, we
    #    might want to refactor these objects at some points, but since the
    #    actual fields are expected to remain deserializable, one can always
    #    account for the former version of these objects in their `tb_decode`
    #    implementation.
    whitelist += [
        ('asrlib.recipes.relax.ase.cell_filter', 'FrechetCellFilterFactory'),
        ('asrlib.recipes.symmetrize.ase.results', 'SymmetryAnalysis'),
        ('asrlib.workflows.relax.iterate_magmoms', 'RelaxAndMagstateResults'),
    ]
    # 4) Fourthly, we whitelist asrlib `Result` and `Parameter` objects which
    #    aren't nested but have many data fields on them. Hopefully, there is a
    #    physically sound reason for the many fields. If not, it is very likely
    #    that someone will need to remove or add fields in the future, which
    #    again would require implementation of a backwards compatible
    #    `tb_decode` function.
    whitelist += [
        ('asrlib.results.gpaw', 'GPAWProperties'),
        ('asrlib.recipes.gs.gpaw.results', 'GapInfo'),
    ]
    # 5) Lastly, we allow ourselves to whitelist a *very few* number of
    #    `Result` and `Parameter` objects, which are both nested and have more
    #    than a couple of data fields. The objects in this category are in dire
    #    risk of change, so we keep the list short and pray...
    whitelist += [
        ('asrlib.recipes.gs.gpaw.results', 'BandInfo'),
    ]
    _in_whitelist = partial(in_whitelist, whitelist=whitelist)

    root = Path()
    counter = 0
    with Repository.find() as repo:
        for node in repo.tree([root.resolve()]).nodes():
            assert node.state is State.done
            record = get_record_from_repo(node.name, repo)
            for obj in [record.inputs, record.output]:
                check_obj_against_whitelist(obj, _in_whitelist)
            # Check output values of a selection of results
            if node.name == 'iterative_relax/results':
                assert (
                    record.output.relaxation.symresults.space_group
                    == mos2.spgrp
                )
            elif node.name == 'ground_state/results':
                out = record.output
                assert out.energy == pytest.approx(-16.2935, abs=1e-2)
                assert out.band_info.gap == pytest.approx(1.0762, abs=1e-3)
            counter += 1
    assert counter == 15


def in_whitelist(obj: object, *, whitelist):
    module = type(obj).__module__
    name = type(obj).__name__
    for _module, _name in whitelist:
        if module == _module and name == _name:
            return True
    return False


def check_obj_against_whitelist(obj, _in_whitelist):
    if not (
        type(obj).__name__[:4] == 'C2DB'
        or type(obj).__module__ == 'builtins'
        or _in_whitelist(obj)
    ):
        print(type(obj).__name__, type(obj).__module__)
        raise AssertionError(f'{type(obj)} not in whitelist')
    # If the object is a dataclass, some form of Mapping or a list, check all
    # its items as well (recursively)
    if is_dataclass(obj):
        obj = {field.name: getattr(obj, field.name) for field in fields(obj)}
    if isinstance(obj, Mapping):
        obj = list(obj.values())
    if isinstance(obj, list):
        for value in obj:
            check_obj_against_whitelist(value, _in_whitelist)
