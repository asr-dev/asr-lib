import pytest
from ase.build import mx2

from asrlib.test.conftest import (
    actually_run_workflow,
    check_tb_tasks,
    in_tmp_dir,
    newrepo,
    parallel,
    pytest_addoption,
    resolve_mpirun_params,
    run_gs_recipe,
    run_recipe_task,
    run_workflow,
    runtask,
    tb_submit_run_ls,
    tbrun_command,
)
from asrlib.test.data import AtomsTestData

__all__ = [
    'actually_run_workflow',
    'check_tb_tasks',
    'in_tmp_dir',
    'newrepo',
    'parallel',
    'pytest_addoption',
    'resolve_mpirun_params',
    'runtask',
    'run_gs_recipe',
    'run_recipe_task',
    'run_workflow',
    'tbrun_command',
    'tb_submit_run_ls',
]


@pytest.fixture
def mos2():
    # Set up MoS2 and rotate the sulfur atoms out of the actual unit cell
    atoms = mx2('MoS2', vacuum=3.14159)
    atoms.rotate(120, 'z')
    return AtomsTestData(atoms=atoms, spgrp=187, min_spgrp=1, lagrp=78)


@pytest.fixture
def mosse():
    # Set up MoSSe based on the parameters from C2DB
    atoms = mx2('MoS2', a=3.251, thickness=3.232, vacuum=3.14159)
    atoms.set_chemical_symbols(['Mo', 'S', 'Se'])
    return AtomsTestData(atoms=atoms, spgrp=156, min_spgrp=1, lagrp=69)


@pytest.fixture
def mno2():
    # Set up MnO2 based on the parameters from C2DB
    atoms = mx2('MnO2', kind='1T', a=2.894, thickness=1.92, vacuum=3.14159)
    return AtomsTestData(atoms=atoms, spgrp=164, min_spgrp=1, lagrp=72)
