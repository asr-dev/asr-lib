import numpy as np
import pytest
import taskblaster as tb

from asrlib.recipes.preconditioners import Magnetizer
from asrlib.test.workflows.test_magstate import check_classification
from asrlib.workflows.magstate import MagneticStateWorkflow
from c2db.recipes.gs import (
    C2DBGroundStateCalculatorFactory,
    C2DBGroundStateRecipe,
    C2DBMagneticStateRecipe,
    C2DBMagneticVSNonmagneticStateClassifier,
)
from c2db.recipes.preconditioners import (
    C2DBHundsMagnetizer,
    C2DBMagnetizerCollection,
)
from c2db.test.utils import get_test_factory

mno2_calc_factory = get_test_factory(
    C2DBGroundStateCalculatorFactory,
    fermi_smearing=0.1,
)


def test_mno2_gs(mno2, run_gs_recipe):
    _, results = run_gs_recipe(
        C2DBGroundStateRecipe(
            mno2_calc_factory,
            preconditioner=Magnetizer.build(),
        ),
        mno2.atoms,
    )
    # Test output
    assert results.energy / len(mno2.atoms) == pytest.approx(6.959, abs=0.02)
    assert results.properties.magmoms == pytest.approx(
        np.array([2.847, 0.019, 0.019]), abs=0.02
    )
    assert results.out_of_plane_dipole == pytest.approx(0.0)
    assert results.vacuum_level() == pytest.approx(6.588, rel=0.01)
    assert results.vacuum_level_difference == pytest.approx(0.0)
    assert (
        results.vacuum_level_above() - results.vacuum_level_below()
    ) == pytest.approx(0.0, abs=0.05)
    bands = results.band_info
    assert bands.efermi == pytest.approx(0.222, rel=1e-2)
    assert not bands.is_metallic()
    assert not bands.is_half_metallic()
    assert bands.gap == pytest.approx(1.003, rel=1e-2)
    assert bands.direct_gap == pytest.approx(1.003, rel=1e-2)
    assert bands.optical_gap == pytest.approx(1.944, rel=1e-2)
    assert bands.half_metallic_gap is None
    assert bands.half_metallic_direct_gap is None
    assert bands.dos_at_efermi == pytest.approx(0.0)
    gap = bands.gap_info
    assert gap.is_direct()  # gap isn't really direct if you add more k-points
    assert not gap.is_spin_conserving()  # gap is from spin-up to spin-down
    assert not gap.is_optical()
    assert gap.ibzvbm_c == pytest.approx([0.0] * 3)
    assert gap.ibzcbm_c == pytest.approx([0.0] * 3)
    assert len(gap.bzvbm_Kc) == len(gap.bzcbm_Kc) == 1  # only one Γ-point
    assert gap.bzvbm_Kc[0] == pytest.approx([0.0] * 3)
    assert gap.bzcbm_Kc[0] == pytest.approx([0.0] * 3)


def test_mosse_gs(mosse, run_gs_recipe):
    _, results = run_gs_recipe(
        C2DBGroundStateRecipe(
            get_test_factory(C2DBGroundStateCalculatorFactory, spinpol=False)
        ),
        mosse.atoms,
    )
    # Test output
    assert results.energy / len(mosse.atoms) == pytest.approx(-6.862, abs=0.02)
    assert results.properties.magmom == pytest.approx(0.0)
    assert results.out_of_plane_dipole == pytest.approx(-0.0321, rel=0.01)
    assert results.vacuum_level() == pytest.approx(8.594, rel=0.01)
    assert results.vacuum_level_difference == pytest.approx(-0.635, rel=0.01)
    assert (
        results.vacuum_level_above() - results.vacuum_level_below()
    ) == pytest.approx(-results.vacuum_level_difference, rel=0.1)
    bands = results.band_info
    assert bands.is_gapped()
    assert bands.gap == pytest.approx(2.125, rel=0.01)
    assert not bands.gap_info.is_direct()


@tb.workflow
class SelfTestingMagstateWithSymlinkWorkflow(MagneticStateWorkflow):
    expected_magmoms = tb.var()

    @tb.task
    def test(self):
        return tb.node(
            check_classification_and_trial_magnetic_state,
            classification=self.classification,
            expected_magmoms=self.expected_magmoms,
            magstate_calculate=self.trial_magnetic_state.calculate,
            magstate_results=self.trial_magnetic_state.results,
        )


def check_classification_and_trial_magnetic_state(
    classification,
    expected_magmoms,
    magstate_calculate,
    magstate_results,
):
    check_classification(classification, expected_magmoms)
    assert magstate_calculate.path.is_symlink()
    gpw_filename = magstate_calculate.path.readlink().name
    assert magstate_results.properties.magnetizer_key == gpw_filename[:-4]
    if magstate_results.properties.magnetizer_key == 'low_spin':
        assert isinstance(classification.magnetizer, Magnetizer)
        assert classification.magnetizer.spec == 1.0
    elif magstate_results.properties.magnetizer_key == 'high_spin':
        assert isinstance(classification.magnetizer, C2DBHundsMagnetizer)
    else:
        raise ValueError(f'Invalid magnetizer {classification.magnetizer}')


def test_mno2_magstate(mno2, run_workflow, check_tb_tasks):
    wf = SelfTestingMagstateWithSymlinkWorkflow(
        atoms=mno2.atoms,
        gs_recipe=C2DBMagneticStateRecipe(mno2_calc_factory),
        magnetizer=C2DBMagnetizerCollection(),
        classifier=C2DBMagneticVSNonmagneticStateClassifier(),
        expected_magmoms=np.array([2.847, 0.019, 0.019]),
    )
    run_workflow(wf)
    # Run `trial_magnetic_state`'s `calculate` and `results` tasks along with
    # the `stayed_magnetic` task
    check_tb_tasks(ntasks={'done': 3, 'new': 2})
    # Since the system manages to stabilize finite magnetic moments, a
    # `nonmagnetic_state` branch is added to the workflow including a
    # `calculate` and a `results` task.
    # Run the `nonmagnetic_state` along with `classification` and `test`.
    run_workflow(wf)
    check_tb_tasks(ntasks=7)
