import numpy as np
import pytest

from c2db.recipes.preconditioners import (
    C2DBHundsMagnetizer,
    C2DBMagnetizerCollection,
)


def parametrize(mos2, mno2):
    return [
        # atoms, hunds_magmoms
        (mos2.atoms, np.array([6.0, 2.0, 2.0])),
        (mno2.atoms, np.array([5.0, 2.0, 2.0])),
    ]


class MagnetizerChecker:
    def __init__(self, run_recipe_task):
        self.run_recipe_task = run_recipe_task
        self.task_counter = 0

    def get_taskname(self):
        name = f'magnetize_task_{self.task_counter}'
        self.task_counter += 1
        return name

    def __call__(self, magnetizer, atoms, expected_magmoms):
        atoms = atoms.copy()
        atoms.set_initial_magnetic_moments(None)
        atoms = self.run_recipe_task(
            '__call__',
            magnetizer,
            atoms=atoms,
            taskname=self.get_taskname(),
        ).output
        magmoms = atoms.get_initial_magnetic_moments()
        assert magmoms == pytest.approx(expected_magmoms)


def test_hunds_rule(mos2, mno2, run_recipe_task):
    checker = MagnetizerChecker(run_recipe_task)
    magnetizer = C2DBHundsMagnetizer()
    for i, (atoms, hunds_magmoms) in enumerate(parametrize(mos2, mno2)):
        checker(magnetizer, atoms, hunds_magmoms)


def test_collection(mos2, mno2, run_recipe_task):
    checker = MagnetizerChecker(run_recipe_task)
    collection = C2DBMagnetizerCollection()
    for i, (atoms, hunds_magmoms) in enumerate(parametrize(mos2, mno2)):
        # Check default magnetizer
        checker(collection, atoms, hunds_magmoms)
        # Check magnetizers individually
        assert set(collection.keys()) == {'low_spin', 'high_spin'}
        checker(collection['low_spin'], atoms, [1.0] * len(atoms))
        checker(collection['high_spin'], atoms, hunds_magmoms)
