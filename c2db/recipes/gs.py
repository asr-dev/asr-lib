"""C2DB ground state recipes.

In the C2DB project, ground state recipes serve two purposes:
1) To classify the spin-polarization of the monolayers (magnetic or not).
2) To perform the "final" ground state calculation of the relaxed structure,
   from which a variety of results are extracted.

Since the ground state calculations of (1) determine what relaxation etc. will
come after, there are many strong dependencies on its results. For this reason,
we *really* want to avoid changes to the results object in order to avoid
rerunning a whole bunch of if-statements early in the workflow. On the other
hand, we want to be able to rerun the results extraction of step (2), such that
more information can be extracted at a later time.

This motivates splitting the ground state recipe in two:
1) The `C2DBMagneticStateRecipe`, which supports exploration of the multiple
   initial guesses for the magnetic moments (high/low spin) and produces a lean
   `C2DBMagneticStateResults` object, which data fields are supposed to remain
   static (more or less indefinately).
2) The `C2DBGroundStateRecipe`, which works like a normal asrlib ground state
   recipe, producing an advanced `C2DBGroundStateResults` object, to which new
   data fields can be added at a later time.
"""

from __future__ import annotations

from dataclasses import dataclass, field
from functools import cached_property
from pathlib import Path

import numpy as np
import taskblaster as tb
from ase import Atoms
from ase.units import Bohr, Hartree

from asrlib.recipes import AtomsPreconditioner, StandardASERecipe
from asrlib.recipes.gs import GPAWGroundStateRecipe
from asrlib.recipes.gs.gpaw.results import BandInfo, GPAWGroundStateResults
from asrlib.recipes.magstate import (
    MagneticStateClassification,
    MagneticStateClassifier,
    MagneticVSNonmagneticStateClassifier,
)
from asrlib.recipes.preconditioners import Magnetizer
from asrlib.results import Result
from asrlib.results.gpaw import GPAWProperties, GPWFile
from asrlib.typing import TBCommunicator
from asrlib.typing.gpaw import BuiltInGPAWCalculator
from c2db.parameters import C2DBCalculatorFactory, C2DBKPointSampling
from c2db.recipes.preconditioners import C2DBMagnetizerCollection
from c2db.recipes.preconditioners.normalize import check_2d_cell_directions
from c2db.recipes.relax import C2DBRelaxCalculatorFactory


@dataclass
class C2DBGroundStateCalculatorFactory(C2DBCalculatorFactory):
    kpts: C2DBKPointSampling = field(
        default_factory=lambda: C2DBKPointSampling(density=12.0),
    )


@tb.actions(plot_electrostatic_potential='_plot_electrostatic_potential')
@dataclass
class C2DBGroundStateResults(GPAWGroundStateResults):
    """Ground state results extracted for the C2DB specifically."""

    cell_area: float
    cell_height: float
    v_z: np.ndarray
    band_info: BandInfo

    @staticmethod
    def extract_data(atoms, calc):
        # Make sure the cell is normalized as expected
        check_2d_cell_directions(atoms)
        return dict(
            # Extract cell parameters
            cell_area=abs(np.linalg.det(atoms.cell[:2, :2])),
            cell_height=atoms.cell[2, 2],
            # Extract the layer-averaged potential (in eV)
            v_z=calc.get_electrostatic_potential().mean(0).mean(0),
            # Extract band information (including gap etc.)
            band_info=BandInfo.from_restart(atoms, calc),
        )

    @property
    def out_of_plane_dipole(self) -> float:
        """Dipole moment (in units of e⋅Å) in the z-direction."""
        return self.properties.dipole[2]

    @property
    def vacuum_level_difference(self) -> float:
        """Potential difference above/below the layer at the cell boundary.

        If someone knows where this formula comes from, please insert some
        documentation XXX
        """
        # Vacuum level difference in atomic units
        dipz = self.out_of_plane_dipole / Bohr
        area = self.cell_area / Bohr**2
        devac = 4 * np.pi * dipz / area
        # Vacuum level difference in eV
        return devac * Hartree

    def vacuum_level(self, offset: int = 8) -> float:
        """Vacuum level, evaluated at a slight offset from the cell boundary.

        Calculates the vacuum level as the average electrostatic potential
        above and below the monolayer. Since the potential can be slightly
        curved at the actual cell boundary, we extract the vacuum level at a
        slight offset.

        Parameters
        ----------
        offset: int
            Number of grid points from the cell boundary to extract the vacuum
            level.
        """
        vabove = self.vacuum_level_above(offset=offset)
        vbelow = self.vacuum_level_below(offset=offset)
        return (vabove + vbelow) / 2.0

    def vacuum_level_above(self, offset: int = 8) -> float:
        """Vacuum level above the monolayer."""
        return self.v_z[-offset]

    def vacuum_level_below(self, offset: int = 8) -> float:
        """Vacuum level below the monolayer."""
        return self.v_z[offset]

    @cached_property
    def z_z(self) -> np.ndarray:
        return np.linspace(
            0.0, self.cell_height, len(self.v_z), endpoint=False
        )

    def _plot_electrostatic_potential(self, task_view, offset: int = 8):
        import matplotlib.pyplot as plt

        ax = plt.subplot()
        ax.plot(self.z_z, self.v_z, label='Potential')
        evac = self.vacuum_level(offset=offset)
        ax.axhline(evac, linestyle='--', label='Vacuum level')
        devac = self.vacuum_level_difference
        if abs(devac) > 1e-4:
            ax.axhline(evac - devac / 2, c='0.5')
            ax.axhline(evac + devac / 2, c='0.5')
            ax.fill_between(
                self.z_z,
                evac - devac / 2,
                evac + devac / 2,
                color='C1',
                alpha=0.5,
                label='Vacuum level difference',
            )
        ax.scatter(
            [self.z_z[offset], self.z_z[-(offset + 1)]],
            [self.v_z[offset], self.v_z[-(offset + 1)]],
            c='C0',
            ec='k',
        )
        ax.set_xlabel(r'$z$ [Å]')
        ax.set_xlim((min(self.z_z), max(self.z_z)))
        ax.set_ylabel(r'$V(z)$ [eV]')
        ax.legend()
        plt.show()


@dataclass
class C2DBGroundStateRecipe(GPAWGroundStateRecipe):
    """Primary ground state recipe."""

    calc_factory: C2DBCalculatorFactory = field(
        default_factory=C2DBGroundStateCalculatorFactory
    )
    preconditioner: Magnetizer | None = None
    filename: str = 'gs.gpw'
    write_mode: str = ''
    d3: bool = True

    def results(self, raw_output: GPWFile, comm: TBCommunicator):
        return C2DBGroundStateResults.from_gpw(
            raw_output, d3=self.d3, comm=comm
        )


@dataclass
class C2DBMagneticStateProperties(GPAWProperties):
    magnetizer_key: str | None = None


@dataclass
class C2DBMagneticStateResults(Result):
    """We use the leanest possible results object for magnetic state
    calculations since the data fields on this object are supposed to be
    static, cf. the description above."""

    properties: C2DBMagneticStateProperties


@dataclass
class C2DBMagneticStateRecipe(StandardASERecipe[GPWFile]):
    """Ground state recipe for magnetic state calculations."""

    calc_factory: C2DBCalculatorFactory = field(
        default_factory=C2DBRelaxCalculatorFactory
    )
    preconditioner: AtomsPreconditioner | None = None
    d3: bool = True

    @property
    def magnetizer_collection(self):
        if isinstance(self.preconditioner, C2DBMagnetizerCollection):
            return self.preconditioner
        elif (
            isinstance(self.preconditioner, list)
            and len(self.preconditioner) == 1
            and isinstance(self.preconditioner[0], C2DBMagnetizerCollection)
        ):
            return self.preconditioner[0]

    def calculate(self, atoms: Atoms, comm: TBCommunicator) -> GPWFile:
        """Perform GPAW ground state calculation.

        Ideally, we should not need to overwrite the
        `StandardASERecipe.calculate` method, but for now, it is the only way
        to inject additional ground state calculations (with different initial
        magnetic moments) in the `GroundStateRecipeWorkflow`. In the future,
        taskblaster should support subworkflow polymorphism by allowing
        subworkflows to be generated from calls to input object methods, see
        https://gitlab.com/asr-dev/asr-lib/-/merge_requests/341#note_2298176914
        Until then, we cannot really avoid the `GroundStateRecipeWorkflow`,
        since it is explicitly generated by asrlib's `MagneticStateWorkflow`,
        which itself is explicitly generated by
        `IterativelyDetermineMagneticStateAndRelax`.

        To perform ground state calculations with both a low-spin and a
        high-spin starting configuration for the initial magnetic moments, we
        therefore define a special `C2DBMagnetizerCollection` preconditioner,
        which we intercept here and use to inject the desired behaviour.
        """
        if self.magnetizer_collection is None:
            # Preconditioner isn't a C2DBMagnetizerCollection, so we perform a
            # normal ground state calculation
            return super().calculate(atoms, comm)
        # Perform multiple calculations with different initial magnetic moments
        # and return the one which converges to the lowest energy
        energies: dict[str, float] = {}
        gpw_files = {}
        exceptions = {}
        for key, magnetizer in self.magnetizer_collection.items():
            tmp = magnetizer(atoms)
            calc_factory = self.calc_factory.new_with(txt=f'{key}.txt')
            tmp.calc = calc_factory(tmp, comm)
            try:
                energies[key] = tmp.get_potential_energy()
                gpw_files[key] = self.write(tmp.calc, f'{key}.gpw')
            except Exception as exc:
                exceptions[key] = exc
        if len(energies) == 0:
            raise Exception('Got these exceptions:', exceptions)
        path = Path('gs.gpw')
        if comm.rank == 0:
            # Pick out the lowest energy and set up symlink
            min_key = min(energies, key=lambda key: energies.get(key, np.inf))
            path.symlink_to(gpw_files[min_key].path)
        comm.barrier()
        return GPWFile(path)

    def _calculate(self, atoms: Atoms, comm: TBCommunicator) -> GPWFile:
        atoms.get_potential_energy()
        return self.write(atoms.calc, 'gs.gpw')

    @staticmethod
    def write(calc: BuiltInGPAWCalculator, filename: str) -> GPWFile:
        path = Path(filename)
        calc.write(path)
        return GPWFile(path)

    def results(self, raw_output: GPWFile, comm: TBCommunicator):
        if self.magnetizer_collection:
            # We have performed different initial guesses for the magnetic
            # moments and identify the magnetizer key which results in the
            # lowest energy based on the symlink (cf. the calculate() method)
            assert raw_output.path.is_symlink()
            magnetizer_key = raw_output.path.readlink().name[:-4]
            assert magnetizer_key in self.magnetizer_collection
        else:
            # The magnetizer is unique
            magnetizer_key = None
        _, _, gpw_properties = GPAWGroundStateResults.read(
            raw_output, d3=self.d3, comm=comm
        )
        return C2DBMagneticStateResults(
            C2DBMagneticStateProperties(
                **gpw_properties.to_dict(), magnetizer_key=magnetizer_key
            )
        )


@dataclass
class C2DBMagneticStateClassifier(MagneticStateClassifier):
    magmoms_tol: float = 0.1

    def classify(
        self, properties, magnetizer=None
    ) -> MagneticStateClassification:
        if isinstance(magnetizer, C2DBMagnetizerCollection):
            assert isinstance(properties, C2DBMagneticStateProperties)
            assert isinstance(properties.magnetizer_key, str)
            magnetizer = magnetizer[properties.magnetizer_key]
        return super().classify(properties, magnetizer)


@dataclass
class C2DBMagneticVSNonmagneticStateClassifier(
    MagneticVSNonmagneticStateClassifier
):
    _classifier: C2DBMagneticStateClassifier = field(
        default_factory=C2DBMagneticStateClassifier
    )
