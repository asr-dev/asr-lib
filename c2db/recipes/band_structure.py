"""Band-structure calculation with GPAW."""

from __future__ import annotations

from dataclasses import dataclass
from pathlib import Path

import numpy as np
import taskblaster as tb
from ase.dft.kpoints import BandPath
from ase.spectrum.band_structure import BandStructure

from asrlib.parameters import Parameter
from asrlib.results import Result
from asrlib.results.gpaw import GPWFile
from asrlib.typing import TBCommunicator
from asrlib.typing.gpaw import NewGPAW


@tb.actions(plot='_plot_bs')
@dataclass
class C2DBBandStructureResults(Result):
    eig_skn: np.ndarray
    fermi_level: float
    band_path: BandPath

    def _plot_bs(self, task_view) -> None:
        """Plot band-structure."""
        bs = BandStructure(
            self.band_path, self.eig_skn, reference=self.fermi_level
        )
        bs.plot(show=True, emin=self.eig_skn.min(), emax=self.eig_skn.max())


@dataclass
class C2DBDFTBandStructureRecipe(Parameter):
    """Do a band-structure calculation.

    Bands up to CBM + upper_energy_cutoff will be converged.
    """

    upper_energy_cutoff: float = 3.0
    npoints: int = 100

    def calculate(
        self, gpw_file: GPWFile, band_path: BandPath, comm: TBCommunicator
    ) -> tuple[GPWFile, BandPath]:
        """Do fixed-density calculation along path in BZ."""
        atoms, calc = gpw_file.restart(comm=comm, gpaw_new=True)
        assert isinstance(calc, NewGPAW)
        band_path = band_path.interpolate(npoints=self.npoints)
        cell_diff_cv = band_path.cell - atoms.cell
        assert abs(cell_diff_cv[atoms.pbc]).max() < 1e-14
        bs_calc = calc.fixed_density(
            kpts=band_path,
            convergence={'bands': f'CBM+{self.upper_energy_cutoff}'},
            symmetry='off',
            txt='gpaw.txt',
        )
        path = Path('bs.gpw')
        bs_calc.write(path, precision='single')
        return GPWFile(path), band_path

    def results(
        self, gpw_file: GPWFile, band_path: BandPath
    ) -> C2DBBandStructureResults:
        atoms, calc = gpw_file.restart()
        assert isinstance(calc, NewGPAW)
        eig_skn = calc.eigenvalues()
        fermi_level = calc.get_fermi_level()
        return C2DBBandStructureResults(eig_skn, fermi_level, band_path)
