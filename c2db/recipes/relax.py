from __future__ import annotations

from dataclasses import dataclass, field

import taskblaster as tb
from ase import Atoms
from ase.optimize.optimize import DEFAULT_MAX_STEPS

from asrlib.recipes import AtomsPreconditioner
from asrlib.recipes.relax.ase import CellAwareBFGSRelaxRecipe, Optimization
from asrlib.recipes.relax.ase.cell_filter import (
    FrechetCellFilterFactory,
    UnitCellFilterFactory,
)
from asrlib.results import Result
from asrlib.results.gpaw import GPAWProperties
from c2db.parameters import C2DBCalculatorFactory, C2DBKPointSampling
from c2db.recipes.symmetrize import (
    C2DBSymmetrizationResults,
    C2DBSymmetrizeAndNormalizeRecipe,
)


@dataclass
class C2DBRelaxCalculatorFactory(C2DBCalculatorFactory):
    kpts: C2DBKPointSampling = field(
        default_factory=lambda: C2DBKPointSampling(density=6.0)
    )


@dataclass
class C2DBRelaxRecipe(CellAwareBFGSRelaxRecipe):
    calc_factory: C2DBCalculatorFactory = field(
        default_factory=C2DBRelaxCalculatorFactory
    )
    preconditioner: AtomsPreconditioner | None = None
    d3: bool = True
    trajectory: str = 'relax.traj'
    logfile: str = 'relax.log'
    append_trajectory: bool = True
    fmax: float = 0.01
    steps: int = DEFAULT_MAX_STEPS
    cell_filter_factory: UnitCellFilterFactory = field(
        default_factory=FrechetCellFilterFactory
    )
    smax: float = 0.005


@tb.actions(plot_symmetries='_plot_symmetries')
@dataclass
class C2DBRelaxAndSymmetrizeResults(Result):
    unsym_atoms: Atoms
    properties: GPAWProperties
    symresults: C2DBSymmetrizationResults

    @property
    def atoms(self) -> Atoms:
        assert self.symresults.atoms is not None, 'Symmetrization failed...'
        return self.symresults.atoms

    @property
    def energy(self) -> float:
        return self.properties.energy

    @property
    def epa(self) -> float:
        return self.energy / len(self.atoms)

    def _plot_symmetries(self, task_view):
        from asrlib.recipes.symmetrize.ase.results import plot_symmetries

        plot_symmetries(self.symresults.analysis, show=True)


@dataclass
class C2DBRelaxAndSymmetrizeRecipe(C2DBRelaxRecipe):
    symmetrize_recipe: C2DBSymmetrizeAndNormalizeRecipe = field(
        default_factory=C2DBSymmetrizeAndNormalizeRecipe
    )

    def results(
        self, raw_output: Optimization
    ) -> C2DBRelaxAndSymmetrizeResults:
        """Extract relax results and symmetrize the relaxed structure.

        NB: Ideally, the symmetrization step could be its own task, but as long
        as taskblaster does not support subworkflow polymorphism, we have to do
        it along with the extraction of relax results. XXX
        """
        relax_results = super().results(raw_output)
        return C2DBRelaxAndSymmetrizeResults(
            unsym_atoms=relax_results.atoms,
            properties=relax_results.properties,
            symresults=self.symmetrize_recipe.run(relax_results.atoms.copy()),
        )
