from __future__ import annotations

from collections.abc import Mapping
from dataclasses import dataclass, field
from functools import cached_property
from typing import List

from ase import Atoms
from ase.spacegroup.symmetrize import check_symmetry

from asrlib.recipes.symmetrize.ase import (
    GenericASESymmetrizationResults,
    GenericASESymmetrizeRecipe,
)
from asrlib.recipes.symmetrize.ase.analyzers import (
    GenericLogspaceSymmetryAnalyzer,
)
from asrlib.recipes.symmetrize.ase.interface import (
    SymmetrizationData,
    one_shot_symmetrization,
)
from asrlib.spglib import check_layer_symmetry, spglib_as_dict
from asrlib.spglib.info import Layergroup, LayergroupInfo
from asrlib.utils import forward_properties
from c2db.recipes.preconditioners.normalize import normalize_2d_cell


def one_shot_symmetrization_and_normalization(
    atoms: Atoms,
    *,
    symprec: float,
    final_symprec: float,
) -> C2DBSymmetrizationData:
    original_spglib_dataset = check_symmetry(atoms, symprec=symprec)
    original_layer_dataset = check_layer_symmetry(atoms, symprec=symprec)
    # Symmetrize atoms according to space group
    symdata: SymmetrizationData = one_shot_symmetrization(
        atoms, symprec=symprec, final_symprec=final_symprec
    )
    if symdata.atoms is None:  # Symmetrization failed
        return C2DBSymmetrizationData.from_dataset(
            symdata, original_layer_dataset
        )
    # Normalize cell
    final_atoms = normalize_2d_cell(symdata.atoms)
    # Check that space group and layer groups match the original target
    final_spglib_dataset = check_symmetry(final_atoms, symprec=final_symprec)
    final_layer_dataset = check_layer_symmetry(
        final_atoms, symprec=final_symprec
    )
    if (
        final_spglib_dataset.number != original_spglib_dataset.number
        or final_layer_dataset['number'] != original_layer_dataset['number']
    ):
        # Symmetrization + normalization failed
        return C2DBSymmetrizationData(
            symprec=symprec,
            spglib_dataset=spglib_as_dict(original_spglib_dataset),
            atoms=None,
            layer_dataset=original_layer_dataset,
        )
    return C2DBSymmetrizationData(
        symprec=symprec,
        spglib_dataset=spglib_as_dict(final_spglib_dataset),
        atoms=final_atoms,
        layer_dataset=final_layer_dataset,
    )


@dataclass
class C2DBSymmetrizationData(SymmetrizationData):
    layer_dataset: Mapping

    @classmethod
    def from_dataset(cls, dataset: SymmetrizationData, layer_dataset: Mapping):
        return cls(**dataset.to_dict(), layer_dataset=layer_dataset)

    @staticmethod
    def main_keys() -> List[str]:
        keys = SymmetrizationData.main_keys()
        keys += [
            'layer_group',
            'international_layer_group_symbol',
            'layer_bravais_type',
        ]
        return keys

    @property
    def layer_group(self) -> int:
        return self.layer_dataset['number']

    @property
    def international_layer_group_symbol(self) -> str:
        return self.layer_dataset['international']

    @cached_property
    def _table_data(self) -> Layergroup:
        table = LayergroupInfo()
        return table.info_from_layergroupnum(self.layer_group)

    @property
    def layer_bravais_type(self) -> str:
        data = self._table_data
        return f'{data.bravais_type_long} ({data.bravais_type})'


@dataclass
class C2DBSymmetryAnalyzer(
    GenericLogspaceSymmetryAnalyzer[C2DBSymmetrizationData],
):
    """Analyze symmetries accounting also for the 2D normalization."""

    def symmetrize(
        self, atoms: Atoms, symprec: float
    ) -> C2DBSymmetrizationData:
        return one_shot_symmetrization_and_normalization(
            atoms,
            symprec=symprec,
            final_symprec=self.min_symprec,
        )


@forward_properties('data', C2DBSymmetrizationData.main_keys())
class C2DBSymmetrizationResults(
    GenericASESymmetrizationResults[C2DBSymmetrizationData],
):
    """Symmetrization results based on the C2DBSymmetrizationData."""


@dataclass
class C2DBSymmetrizeAndNormalizeRecipe(
    GenericASESymmetrizeRecipe[C2DBSymmetrizationData],
):
    analyzer: C2DBSymmetryAnalyzer = field(
        default_factory=C2DBSymmetryAnalyzer.build
    )
    symprec: float = 0.1  # target symmetry precision

    def run(self, atoms: Atoms) -> C2DBSymmetrizationResults:
        return C2DBSymmetrizationResults(self.analyze(atoms), self.symprec)
