from __future__ import annotations

from collections.abc import Mapping
from dataclasses import dataclass, field

import numpy as np
from ase import Atoms

from asrlib.parameters import Parameter
from asrlib.recipes.preconditioners import Magnetizer


class C2DBHundsMagnetizer(Parameter):
    """Magnetize according to Hund's rule.

    In principle, this would be a useful asrlib feature, but the implementation
    here is a very hacky one, where we are relying on the GPAW setups to
    provide the Hund's rule moments. In asrlib, a Hund's rule magnetizer should
    be independent from GPAW.
    """

    @staticmethod
    def get_hunds_rule_moments(atoms: Atoms) -> np.ndarray:
        from gpaw.calculator import GPAW
        from gpaw.mpi import SerialCommunicator

        tmp = atoms.copy()
        comm = SerialCommunicator()
        calc = GPAW(xc='PBE', mode='pw', txt=None, communicator=comm)
        calc.initialize(tmp)
        return np.array(
            [setup.get_hunds_rule_moment() for setup in calc.setups]
        )

    def __call__(self, atoms: Atoms) -> Atoms:
        atoms = atoms.copy()
        atoms.set_initial_magnetic_moments(self.get_hunds_rule_moments(atoms))
        return atoms


@dataclass
class C2DBMagnetizerCollection(Mapping, Parameter):
    """Special preconditioner, which alters the gs recipe behaviour.

    This class exists mainly such that the `C2DBGroundStateRecipe` can detect
    whether it is supposed to try out multiple initial magnetic moment
    configurations. It satisfies the `AtomsPreconditioner` protocol (has a
    `__call__` method mapping `Atoms` -> `Atoms`) such that it can be passed
    around and detected as a preconditioner.

    Once taskblaster supports subworkflow polymorphism, the
    `C2DBMagnetizerCollection` can be made redundant, see e.g. discussion in
    the `C2DBGroundStateRecipe` documentation. For now, the `default_entry`,
    `__post_init__` and `__call__` methods exist only to make it an
    `AtomsPreconditioner`, while the rest of the (`Mapping`) methods exist to
    make it behave like its underlying `magnetizers` dictionary.
    """

    magnetizers: dict[str, Magnetizer | C2DBHundsMagnetizer] = field(
        default_factory=lambda: dict(
            low_spin=Magnetizer(1.0),
            high_spin=C2DBHundsMagnetizer(),
        ),
    )
    default_entry: str = 'high_spin'

    def __post_init__(self):
        assert self.default_entry in self.magnetizers

    def __call__(self, atoms: Atoms) -> Atoms:
        """Apply the default magnetizer."""
        return self.magnetizers[self.default_entry](atoms)

    def __getitem__(self, key: str):
        return self.magnetizers[key]

    def __iter__(self):
        return iter(self.magnetizers)

    def __len__(self):
        return len(self.magnetizers)
