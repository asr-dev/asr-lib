from dataclasses import dataclass

import numpy as np
from ase import Atoms

from asrlib.recipes.preconditioners.normalize import StructureNormalizer


@dataclass
class C2DBStructureNormalizer(StructureNormalizer):
    symprec: float = 0.1
    vacuum: float = 15.0

    def __call__(self, atoms: Atoms) -> Atoms:
        # Reduce to primitive cell and niggli normalize
        atoms = super().__call__(atoms)
        return normalize_2d_cell(atoms, vacuum=self.vacuum)


def check_2d_cell_directions(atoms: Atoms):
    """Check pbc and in-plane vs. out-of-plane cell directions."""
    assert list(atoms.get_pbc()) == [True, True, False]
    cell_cv = atoms.get_cell()
    assert np.linalg.norm(cell_cv[2, :2]) < 1e-10, (
        'Please align the out-of-plane cell vector to the z-axis'
    )
    for cell_v in cell_cv[:2]:
        assert abs(cell_v @ cell_cv[2]) < 1e-10, (
            'Please align the in-plane cell vectors to the xy-plane'
        )


def get_2d_material_width(atoms: Atoms):
    atoms = atoms.copy()
    atoms.center()
    check_2d_cell_directions(atoms)
    pos_av = atoms.get_positions()
    z_a = pos_av[:, 2]
    return max(z_a) - min(z_a)


def check_2d_cell_normalization(atoms: Atoms, vacuum: float = 15.0):
    # Check that at least one atom sits at (x, y) = (0, 0)
    pos_av = atoms.get_positions()
    min_dist = np.min(np.linalg.norm(pos_av[:, :2], axis=1))
    assert min_dist < 1e-10
    # Check vacuum
    width = get_2d_material_width(atoms)  # checks cell directions
    cell_cv = atoms.get_cell()
    assert np.allclose(cell_cv[2], [0.0, 0.0, width + vacuum], atol=1e-10)
    # Check that layer is centered along the z-direction
    assert abs(max(pos_av[:, 2]) - (vacuum / 2.0 + width)) < 1e-10
    assert abs(min(pos_av[:, 2]) - vacuum / 2.0) < 1e-10
    # Check that all atoms remain inside the unit cell
    assert np.allclose(pos_av, atoms.get_positions(wrap=True))


def normalize_2d_cell(atoms: Atoms, vacuum: float = 15.0) -> Atoms:
    """Normalize 2D material cell."""
    atoms = atoms.copy()
    # Adjust vacuum
    width = get_2d_material_width(atoms)  # checks cell directions
    cell_cv = atoms.get_cell()
    cell_cv[2, 2] = width + vacuum
    atoms.set_cell(cell_cv)
    # Translate the structure to have an atom at (x, y) = (0, 0)
    atoms.center()
    pos_av = atoms.get_positions()
    a_to_translate = np.argmin(np.linalg.norm(pos_av[:, :2], axis=1))
    atoms.translate(list(-pos_av[a_to_translate, :2]) + [0.0])
    # Wrap any atoms outside the unit cell back inside
    atoms.wrap()
    return atoms
