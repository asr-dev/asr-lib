from __future__ import annotations

from dataclasses import dataclass, field
from functools import cached_property

import taskblaster as tb
from ase import Atoms

from asrlib import recipe_node
from asrlib.parameters import Parameter
from asrlib.workflows.gs import GroundStateRecipeWorkflow
from asrlib.workflows.relax.iterate_magmoms import (
    IterativelyDetermineMagneticStateAndRelax,
)
from c2db.parameters import C2DBCalculatorFactory
from c2db.recipes.gs import (
    C2DBGroundStateRecipe,
    C2DBMagneticStateRecipe,
    C2DBMagneticVSNonmagneticStateClassifier,
)
from c2db.recipes.preconditioners import (
    C2DBMagnetizerCollection,
    C2DBStructureNormalizer,
)
from c2db.recipes.relax import (
    C2DBRelaxAndSymmetrizeRecipe,
    C2DBRelaxCalculatorFactory,
)


@tb.workflow
class C2DBCoreWorkflow:
    """Core C2DB workflow."""

    monolayer: Atoms = tb.var()
    normalizer: C2DBStructureNormalizer = tb.var()
    relax_wf_config: C2DBRelaxWorkflowConfiguration = tb.var()
    gs_recipe: C2DBGroundStateRecipe = tb.var()

    @tb.task
    def unrelaxed(self):
        """Normalize the input monolayer."""
        return recipe_node('__call__', self.normalizer, atoms=self.monolayer)

    @tb.subworkflow
    def iterative_relax(self):
        """Relax the monolayer and classify it magnetically."""
        return IterativelyDetermineMagneticStateAndRelax(
            atoms=self.unrelaxed,
            relax_recipe=self.relax_wf_config.relax_recipe,
            gs_recipe=self.relax_wf_config.gs_recipe,
            magstate_magnetizer=self.relax_wf_config.magnetizer,
            magstate_classifier=self.relax_wf_config.classifier,
        )

    @tb.subworkflow
    def ground_state(self):
        """Detailed ground state calculation."""
        return GroundStateRecipeWorkflow(
            atoms=self.iterative_relax.results.atoms,
            recipe=self.actual_gs_recipe,
        )

    @property
    def actual_gs_recipe(self):
        return self.gs_recipe.new_with(
            calc_factory=self.gs_recipe.calc_factory.new_with(
                # Only do a spin-polarized ground state calculation, if the
                # monolayer has been classified as magnetic
                spinpol=self.iterative_relax.results.classification.spinpol,
            ),
        )


@dataclass
class C2DBRelaxWorkflowConfiguration(Parameter):
    calc_factory: C2DBCalculatorFactory = field(
        default_factory=C2DBRelaxCalculatorFactory
    )
    magnetizer: C2DBMagnetizerCollection = field(
        default_factory=C2DBMagnetizerCollection
    )
    d3: bool = True

    @cached_property
    def relax_recipe(self):
        return C2DBRelaxAndSymmetrizeRecipe(
            calc_factory=self.calc_factory,
            d3=self.d3,
        )

    @cached_property
    def gs_recipe(self):
        return C2DBMagneticStateRecipe(
            calc_factory=self.calc_factory,
            d3=self.d3,
        )

    @cached_property
    def classifier(self):
        # hardcoded for now, can be made an input if necessary
        return C2DBMagneticVSNonmagneticStateClassifier()


@tb.parametrize_glob('*/monolayer')
def workflow(monolayer):
    return C2DBCoreWorkflow(
        monolayer=monolayer,
        normalizer=C2DBStructureNormalizer(),
        relax_wf_config=C2DBRelaxWorkflowConfiguration(),
        gs_recipe=C2DBGroundStateRecipe(),
    )
