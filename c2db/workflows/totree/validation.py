"""Set up c2db validation test tree of monolayers.

Assumes that the tb dir has a symbolic link to the old c2db database, e.g.

old_c2db.db -> /home/niflheim2/cmr/CAMD-Web/C2DB/c2db.db
"""

from pathlib import Path

import taskblaster as tb

from c2db.workflows.totree import MaterialSubset

workflow = tb.totree(
    MaterialSubset(
        Path('old_c2db.db'),
        [  # Validation test set
            '1MoS2-1',  # semiconductor
            '2C-1',  # semimetal
            '1SSeW-1',  # janus
            '2CrI3-1',  # magnetic semiconductor
            '1GeTe2Fe3-1',  # magnetic metal
            '1FeBr2-1',  # halfmetal
            '1AgAuBr4-1',  # low symmetry semiconductor
        ],
    ),
    name='monolayer',
)
