"""Set up c2db pressure test tree of monolayers.

Assumes that a `pressure_test_set.json` file has been generated and that the tb
dir has a symbolic link to the old c2db database, e.g.

old_c2db.db -> /home/niflheim2/cmr/CAMD-Web/C2DB/c2db.db

For `pressure_test_set.json`, you can create a symbolic link to the one
provided with asr-lib, or you can generate your own samples using:

$ python -m c2db.workflows.totree.pressure <PATH TO DATABASE> <N> <M>

The dataset distributed with asr-lib was generated the 20th of February 2025,
using:

$ python -m c2db.workflows.totree.pressure c2db.db 100 100

where c2db.db was located (at the time) in /home/niflheim2/cmr/CAMD-Web/C2DB
"""

from pathlib import Path

import numpy as np
import taskblaster as tb
from ase.db import connect
from ase.io.jsonio import read_json, write_json

from c2db.workflows.totree import MaterialSubset


def sample_uids(asrdb: Path, *, magnetic: int, nonmagnetic: int):
    with connect(asrdb) as db:
        return _sample_uids(db, magnetic=magnetic, nonmagnetic=nonmagnetic)


def _sample_uids(db, *, magnetic: int, nonmagnetic: int):
    print('Extracting and sorting all magnetic uids')
    magnetic_uids = _extract_uids(db, is_magnetic=True)
    print('Extracting and sorting all nonmagnetic uids')
    nonmagnetic_uids = _extract_uids(db, is_magnetic=False)
    assert 0 < magnetic <= len(magnetic_uids)
    assert 0 < nonmagnetic <= len(nonmagnetic_uids)
    rng = np.random.default_rng(42)
    magnetic_sample = rng.choice(magnetic_uids, magnetic, replace=False)
    rng = np.random.default_rng(42)
    nonmagnetic_sample = rng.choice(
        nonmagnetic_uids, nonmagnetic, replace=False
    )
    return magnetic_sample, nonmagnetic_sample


def _extract_uids(db, *, is_magnetic: bool):
    return [
        row.uid
        for row in db.select(
            is_magnetic=is_magnetic,
            sort='uid',
            include_data=False,
        )
    ]


def get_uids(datafile):
    if datafile.is_file():
        uids = read_json(datafile)
        return uids['magnetic'] + uids['nonmagnetic']
    return []


datafile = Path('pressure_test_set.json')
dbpath = Path('old_c2db.db')
workflow = tb.totree(
    MaterialSubset(dbpath, get_uids(datafile)), name='monolayer'
)


if __name__ == '__main__':
    from argparse import ArgumentParser

    parser = ArgumentParser(
        prog='Database sampler',
        description='Draw random samples from an old c2db database',
    )
    parser.add_argument(
        'asrdb', type=Path, help='Path to old asr c2db database to sample'
    )
    parser.add_argument(
        'magnetic', type=int, help='Number of magnetic monolayers'
    )
    parser.add_argument(
        'nonmagnetic', type=int, help='Number of nonmagnetic monolayers'
    )
    args = parser.parse_args()
    magnetic_uids, nonmagnetic_uids = sample_uids(**vars(args))
    print(f'\nSampled magnetic uids ({args.magnetic}):')
    for uid in magnetic_uids:
        print(f'\t{uid}')
    print(f'\nSampled nonmagnetic uids ({args.nonmagnetic}):')
    for uid in nonmagnetic_uids:
        print(f'\t{uid}')
    if datafile.is_file():
        uids = read_json(datafile)
    else:
        uids = {'magnetic': [], 'nonmagnetic': []}
    all_magnetic_uids = set(uids['magnetic']).union(magnetic_uids)
    all_nonmagnetic_uids = set(uids['nonmagnetic']).union(nonmagnetic_uids)
    new_magnetic = len(all_magnetic_uids) - len(uids['magnetic'])
    new_nonmagnetic = len(all_nonmagnetic_uids) - len(uids['nonmagnetic'])
    if new_magnetic + new_nonmagnetic == 0:
        print('No new monolayers to add')
    else:
        print(
            f'\nAdding {new_magnetic} magnetic monolayers and '
            f'{new_nonmagnetic} nonmagnetic monolayers to {datafile.name}'
        )
        write_json(
            datafile,
            {
                'magnetic': list(all_magnetic_uids),
                'nonmagnetic': list(all_nonmagnetic_uids),
            },
        )
