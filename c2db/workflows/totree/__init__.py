from collections.abc import Mapping
from dataclasses import dataclass
from functools import cached_property
from pathlib import Path
from typing import List

from ase import Atoms
from ase.db import connect


@dataclass
class MaterialSubset(Mapping):
    asrdb: Path
    uids: list[str]

    @cached_property
    def materials(self):
        return extract_materials(self.asrdb, self.uids)

    def __getitem__(self, key):
        return self.materials[key]

    def __iter__(self):
        return iter(self.materials)

    def __len__(self):
        return len(self.uids)


def extract_materials(asrdb: Path, uids: List[str]) -> dict[str, Atoms]:
    """Extract materials based on their uids."""
    materials: dict[str, Atoms] = {}
    with connect(asrdb) as db:
        for uid in uids:
            if uid[0].isdigit():
                # Input is a new style uid (db is assumed to be new)
                materials[uid] = db.get(uid=uid).toatoms()
            else:
                try:  # try new db format
                    materials[uid] = db.get(olduid=uid).toatoms()
                except KeyError:  # fall back to old db format
                    materials[uid] = db.get(uid=uid).toatoms()
    return materials
