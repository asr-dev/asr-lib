from dataclasses import dataclass, field
from typing import Dict

from ase import Atoms

from asrlib.calculators.ase import DFTD3
from asrlib.parameters import Parameter
from asrlib.parameters.gpaw import GPAWCalculatorFactory
from asrlib.results.gpaw import GPAWProperties
from asrlib.typing import TBCommunicator
from asrlib.typing.gpaw import BuiltInGPAWCalculator


@dataclass
class C2DBKPointSampling(Parameter):
    density: float
    gamma: bool = True
    fix_kpts: bool = True

    def __call__(self, atoms: Atoms) -> dict:
        # When using GPAW for relaxations and specifying the `density`, GPAW
        # will automatically *update* the k-point grid as the relaxtion
        # progresses. Unless the starting guess for the structure is rubbish,
        # this is usually not the best strategy in order to find the potential
        # energy minimum in as few steps as possible.
        if self.fix_kpts:
            # Generate the k-point grid at the construction time of the
            # calculator, thus fixing it during relaxation.
            return self.kpts_from_density(atoms)
        return {'density': self.density, 'gamma': self.gamma}

    def kpts_from_density(self, atoms: Atoms) -> dict:
        from ase.calculators.calculator import kptdensity2monkhorstpack

        return {
            'size': kptdensity2monkhorstpack(atoms, kptdensity=self.density),
            'gamma': self.gamma,
        }


@dataclass
class C2DBCalculatorFactory(Parameter):
    kpts: C2DBKPointSampling  # sampling is recipe specific, so no default here
    xc: str = 'PBE'
    spinpol: bool = True
    ecut: float = 800
    fermi_smearing: float = 0.05
    convergence: Dict[str, float] = field(
        default_factory=lambda: {'forces': 1e-4}
    )
    txt: str = 'gpaw.txt'

    def __post_init__(self):
        self.properties_cls = GPAWProperties

    def __call__(
        self, atoms: Atoms, comm: TBCommunicator
    ) -> BuiltInGPAWCalculator:
        # Parameters entered here are hardcoded for now, but can be made
        # editable in the future via attributes on the C2DBCalculatorFactory
        # itself
        calc_factory = GPAWCalculatorFactory(
            dict(
                xc=self.xc,
                mode={'name': 'pw', 'ecut': self.ecut},
                kpts=self.kpts(atoms),
                spinpol=self.spinpol,
                convergence=self.convergence,
                occupations={
                    'name': 'fermi-dirac',
                    'width': self.fermi_smearing,
                },
                poissonsolver={'dipolelayer': 'xy'},
                setups={'Cr': '14'},
                txt=self.txt,
            )
        )
        return calc_factory(atoms, comm)

    def d3_wrapper(self, dft: BuiltInGPAWCalculator, comm: TBCommunicator):
        return DFTD3(dft=dft, xc=self.xc, comm=comm)
