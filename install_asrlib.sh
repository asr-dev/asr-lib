# MAKE SURE NO OTHER ENV IS ACTIVE
deactivate
module load matplotlib/3.3.3-foss-2020b
set -e  # stop if there are errors

# SET THE NAME FOR THE VENV
NAME=$1
USAGE="Usage: $0 foldername [intel]"

if [[ $# -ne 2 && $# -ne 1 ]]; then
    echo "Wrong number of arguments, expected 1 or 2, got $#"
    echo $USAGE
    exit 1
fi


# GET VIRTUAL ENV INSTALL SCRIPT FOR GPAW 
wget https://wiki.fysik.dtu.dk/gpaw/_downloads/b715b229cd69b12e2bc05ac64707ce11/gpaw_venv.py
sed -i "s/'thul', //g" gpaw_venv.py
chmod +x gpaw_venv.py
./gpaw_venv.py $NAME
cd $NAME

# ACTIVATE GPAW INSTALL VENV
source bin/activate

# Download & INSTALL PACKAGES
pip install myqueue
pip install pytest
git clone git@gitlab.com:asr-dev/asr-lib.git
git clone git@gitlab.com:taskblaster/taskblaster.git
git clone git@gitlab.com:asr-dev/asr-lib-testfiles.git

# INSTALL TASKBLASTER
echo "#### INSTALLING TASKBLASTER ####"
cd taskblaster
pip install -e .
cd ..

# INSTALLING ASRLIB 
echo "#### INSTALLING ASRLIB ####"
cd asr-lib
pip install -e .
cd ..

# ASRLIB_TESTFILES
echo "#### INSTALLING ASRLIB_TESTFILES ####"
cd asr-lib-testfiles
pip install -e .
cd ..

# duplicate the activate script and add in gpaw.new
awk -v s="export GPAW_NEW=1" 'NR==52 {print s} 1' bin/activate > bin/activate.gpaw_new
